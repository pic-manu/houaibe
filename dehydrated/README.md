# dehydrated

Le logiciel dehydrated permet de gérer les certificats `letsencrypt` afin d'utiliser le mode https. dehydrated a été intégré à houaibe, le script d'installation permet de démarrer rapidement, en lien avec le conteneur pic-proxy.

## Installation
Installer par la commande:
```
./installdehydrated.sh
```
Ensuite **redémarrez le proxy**:
```
/usr/local/houaibe/containers/pic-proxy/run.sh
```

## Certificat wildcard

Il est utile de disposer d'un certificat wildcard pour votre nom de domaine, cela permet notamment à toutes les adresses "techniques" (`monasso-st1.exemple.org`) d'utiliser SSL. C'est imple si vous utilisez Gandi comme registre pour votre nom de domaine. Sinon, vous devrez modifier les scripts se trouvant dans le répertoire wildcard: dans ce cas merci de partager vos modifications !

Vous devrez terminer l'installation de la manière suivante:

```
cd /usr/local/houaibe/dehydrated/wildcard
cp config.py.example config.py
vi config.py
```

La génération et l'installation du certificat peut se faire par la commande suivante:

```
cd /usr/local/houaibe/dehydrated && \
./dehydrated -c -d "*.exemple.org" --alias "exemple.org" -t dns-01 -k /usr/local/houaibe/dehydrated/hooks-dns.bash
```

## Création d'un nouveau certificat:

1. Création de l'adresse de production:
   L'association monasso souhaite disposer de l'adresse de production monasso.fr 
   Il faut bien sûr que le DNS soit correctement configuré, ensuite vous pouvez créer l'adresse deproduction par:

   ```
   picgs prod monasso site 1
   ```

2. Génération du nouveau certificat:
   Simplement par la commande (attention il faut les droits sudo:

   ```
   sudo picgs https
   ```

## Renouvellement automatique des certificats:

Il convient d'ajouter les lignes suivantes dans le `crontab`:

```
02 01 * * * cd /usr/local/houaibe/dehydrated && ./dehydrated --cron --hook /usr/local/houaibe/dehydrated/hooks.bash >>/var/log/dehydrated/dehydrated.log 2>&1 
02 02 * * * cd /usr/local/houaibe/dehydrated && ./dehydrated -c -d "*.picttn.le-pic.org" --alias "picttn.le-pic.org" -t dns-01 -k /usr/local/houaibe/dehydrated/hooks-dns.bash >>/var/log/dehydrated/dehydrated.log 2>&1
```

## Vérification des certificats:

Pour vérifier les certificats utilisés et s'assurer que tout fonctionne bien, on peut utiliser la commande:

```
sudo picgs certs
```


