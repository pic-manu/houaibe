#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Ce script est pris sur https://github.com/sblaisot/certbot-dns-01-authenticators
# et modifie par manu pour:
#    - que ca fonctionne avec le compte lepic, organisation PIE13-GANDI
#    - que ca fonctionne avec la cle APIKEY de Gandiv5
#
# Necessite certbot v 0.22.1 minimum
#
# NOTE - Fonctionne aussi bien en python2 qu'en python3
#

import pprint
import requests
import os
import json
from config import *
from functions import *


pp = pprint.PrettyPrinter(indent=4)

certbot_domain = os.environ.get("CERTBOT_DOMAIN")
try:
    certbot_domain
except NameError:
    print("CERTBOT_DOMAIN environment variable is missing, exiting")
    exit(1)

certbot_validation = os.environ.get("CERTBOT_VALIDATION")
try:
    certbot_validation
except NameError:
    print("CERTBOT_VALIDATION environment variable is missing, exiting")
    exit(1)

if livedns_sharing_id == None:
    sharing_param = ""
else:
    sharing_param = "?sharing_id=" + livedns_sharing_id

headers = {
    'X-Api-Key': livedns_apikey,
}


d = findManagedDomain(certbot_domain,livedns_api,sharing_param,livedns_apikey)

if d == None:
    print("The requested domain " + certbot_domain + " was not found in this gandi account")
    exit(1)

domain_properties   = d[0]
subdomain           = d[1]
domain_records_href = domain_properties["domain_records_href"]

rrset_name = "_acme-challenge"
if subdomain != "":
    rrset_name += '.' + subdomain

response = requests.get(domain_records_href + "/" + rrset_name + sharing_param, headers=headers)

if (response.ok):
    domains = response.json()
    if len(domains) != 0:
        print("Existing " + rrset_name + " record found, exiting - Hope it's ok")
        exit(0)
else:
    print("Failed to look for existing " + rrset_name + " record")
    response.raise_for_status()
    exit(1)

newrecord = {
  "rrset_name": rrset_name,
  "rrset_type": "TXT",
  "rrset_ttl": 300,
  "rrset_values": [certbot_validation]
}

response = requests.post(domain_records_href + sharing_param, headers=headers, json=newrecord)
if (response.ok):
    print("all good, entry created")
    #pp.pprint(response.content)
else:
    print("something went wrong")
    pp.pprint(response.content)
    response.raise_for_status()
    exit(1)
