#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pprint
import requests
import os
import json


def findManagedDomain(domain,livedns_api,sharing_param,livedns_apikey):
    """Starting from domain, return a domain managed by Gandi, using our credentials
       If domain = subdomain1.subdomain2.example.com, it could be:
           - subdomain1.subdomain2.example.com
           - subdomain2.example.com
           - example.com
       If found, return a tuple:
          0 -> a dict, with the domain properties as sent by the Gandi api
          1 -> The subdomain, if any: "", "subdomain1" or "subdomain1.subdomain2"
       If not found, return None"""

    headers = {
        'X-Api-Key': livedns_apikey,
    }
    list_domain     = domain.split('.')
    list_subdomains = []
    ask_gandi = True
    while (ask_gandi):
        req    = livedns_api + "domains/" + '.'.join(list_domain) + sharing_param
        #print(req)
        response = requests.get(req, headers=headers)

        # domain managed by gandi: exit from the loop !
        if (response.ok):
            ask_gandi = False
            return [response.json(),'.'.join(list_subdomains)]

        else:
            # If domain has two components (example.com), it is not managed by our Gandi account
            if (len(list_domain) <= 2):
                return None

            # May be we are working on a subdomain (subdomain1.subdomain2.example.com), and the parent domain is managed by our Gandi account (subdomain2.example.com) ?
            else:
                list_subdomains.append(list_domain.pop(0))


