#! /bin/bash

#
# Script d'installation de dehydrated u répertoire associé au conteneur 
#
# Usage: cd ....
#        ./installcont.sh
#

if [[ ! -f ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf ]]
then
   echo "houaibe n'est pas encore configuré"
   exit 1;
fi

# Lire les paramètres généraux
. ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf

# S'assurer que le module python3-requests est bien installé:
apt update && apt install -y python3-requests

# Repertoire de dehydrated
DIR=$DEHYDRATEDDIR

# Rien à faire si le répertoire existe déjà
[[ -d $DIR ]] && echo "Le repertoire $DIR existe - Je ne fais rien" && exit 0

# accepter les termes de letsencrypt
read -p "Acceptez-vous les termes de letsencrypt (voir https://letsencrypt.org/documents/LE-SA-v1.2-November-15-2017.pdf) (O/N) ?" TERMES
[[ $TERMES == "O" ]] || exit 1

echo "installation de dehydrated dans $DIR"
tar xf dehydrated-0.6.5.tar.gz
mv dehydrated-0.6.5 $DIR

# Copie des hooks et scripts pour le certificat *
cp -r hooks.bash hooks-dns.bash wildcard-scripts $DIR

# Création de .acme-challenges et domains.txt
mkdir ${DEHYDRATEDDIR}/.acme-challenges && chown www-data ${DEHYDRATEDDIR}/.acme-challenges
echo "<html><head></head><body>coucou</body></html>" > ${DEHYDRATEDDIR}/.acme-challenges/index.html
touch ${DEHYDRATEDDIR}/domains.txt ${DEHYDRATEDDIR}/domains.txt.bkp
chgrp docker ${DEHYDRATEDDIR}/domains.txt ${DEHYDRATEDDIR}/domains.txt.bkp
chmod 660 ${DEHYDRATEDDIR}/domains.txt ${DEHYDRATEDDIR}/domains.txt.bkp

# Configuration de dehydrated - Décommentez la ligne CA= pour utiliser l'infrastructure de test  de letsencrupt !
cat >${DEHYDRATEDDIR}/config <<EOF
CONTACT_EMAIL=${ADMINMAIL}
WELLKNOWN="\${BASEDIR}/.acme-challenges"
#CA="https://acme-staging-v02.api.letsencrypt.org/directory"
EOF

# Si on est la on a accepte les termes de letsencrypt
( cd ${DEHYDRATEDDIR} && ./dehydrated --register --accept-terms )

echo "dehydrated est maintenant installé. Merci de REDEMARRER le PROXY !"

