# Mise à jour de houaibe

*Il n'y a pas de procédure de mise à jour automatique. Vous devez mettre à jour à la main, les instructions sont ci-dessous.*

## Mise à jour en 4.1.5:
- Réinstaller `picgs`
- Exécuter ./installtmpl.sh dans les répertoires nginx-fpm-83 et nginx-fpm84

## Mise à jour en 4.1.0-1,2,3,4:
- Réinstaller `picgs`

## Mise à jour en 4.0.0:
- Réinstaller `picgs`
- Exécuter ./installtmpl.sh dans les répertoires nginx-fpm81, nginx-fpm82, nginx-fpm-83 et nginx-fpm84

## Mise à jour en 3.23.5:
- Réinstaller `picgs`
- Reconstruire paheko

## Mise à jour en 3.23.4:
- Réinstaller `picgs`
- Réinstaller (par ./installtmpl) les conteneurs ngix-fpmxx

## Mise à jour en 3.23.3:
- Réinstaller `picgs`
- Réconstruire les conteneurs ngix-fpmxx

## Mise à jour en 3.23.2:
- Réinstaller `picgs`
- Modifier les 2 dernières lignes (export) dans le fichier houaibe.conf: les recopier depuis houaibe.conf.dist

## Mise à jour en 3.23.1:
- (depuis 3.23.0): Réinstaller `picgs`

## Mise à jour en 3.23.0:

- Modifier les permissions de `/usr/local/houaibe/containers`:
  ```
  sudo chmod u=rwx,g=rwxs /usr/local/houaibe/containers
  ```

- Modifier le fichier `/usr/local/houaibe/etc/houaibe.conf`:
  Ajouter une ligne `DKRSRVIMG`:

  `DKRSRVIMG="nginx-fpm81 nginx-fpm82"`

  et remplacer les deux dernières lignes par:

  ```
  export BASENAME DOMAINNAME PROXYNAME DEHYDRATEDDIR CONTAINERSDIR CMSADMIN ADMINMAIL LDAPPASSWDFILE
  export ADMIN ADMINMAIL PICGSDIR LDAPURI HSTMYSQL CLIMYSQL DKRSRVWEB DKRSRVIMG ABUSKEYFILE MUTUNOM MUTUVER BACKUP_SSH_MIN_UP
  ```

- Installer et construire (build ou pull) les images nginx-fpmxy:
  ```
  cd houaibe/images/nginx-fpm82
  ./build.py
  sudo ./installtmpl.sh
  ```

- Réinstaller `picgs`:

  ```
  cd houaibe/picg
  sudo ./installpicgs```

## Mise à jour en 3.22.8:

- Réinstaller `picgs`
- Reconstruire (build ou pull) et redémarrer `pic-maint` et `pic-ssh`

## Mise à jour en 3.22.7:

- Réinstaller `picgs`
- Reconstruire (build ou pull) et redémarrer `pic-paheko`

## Mise à jour en 3.22.6:

- Réinstaller `picgs` (optionnel)
- Reconstruire (build ou pull) et redémarrer les conteneurs `pic-ssh` et `pic-proxy`

## Mise à jour en 3.22.1, 3.22.2, 3.22.3, 3.22.4 ou 3.22.5:

- Réinstaller `picgs`

## Mise à jour en 3.22.0:

- Réinstaller `picgs`
- Réinstaller `pic-proxy` si vous souhaitez utiliser le cache
- Modifier la ligne "Permissions des utilisateurs" de la `crontab`, car le fonctionnement a changé.

## Mise à jour en 3.21.3:

- Réinstaller `picgs`
- Reconstruire et redémarrer `pic-maint`
- Reconstruire et redémarrer `pic-paheko`

## Mise à jour en 3.21.2:

- Réinstaller `picgs`

## Mise à jour en 3.21.0:

- Réinstaller `picgs`
- Reconstruire (build ou pull) les conteneurs `pic-maint`, `pic-ssh`
  ```
  cd images/pic-maint
  sudo cp run.sh.dist /usr/local/houaibe/conteneurs/pic-maint/run.sh
  ```

## Mise à jour en 3.20.6:

- Réinstaller `picgs`
- Reconstruire (build ou pull) les conteneurs `pic-itk81`, `pic-itk82`, `pic-itk82s`
  ```
  cd images/pic-maint
  sudo cp run.sh.dist /usr/local/houaibe/conteneurs/pic-maint/run.sh
  ```

## Mise à jour en 3.20.5:

- Réinstaller `picgs`
- Mettre à jour le script d'appel de `pic-maint`:
  ```
  cd images/pic-maint
  sudo cp run.sh.dist /usr/local/houaibe/conteneurs/pic-maint/run.sh
  ```

## Mise à jour en 3.20.2, 3.20.3, 3.20.4:

- Réinstaller `picgs`
- Reconstruire ou re-puller et redémarrer les images itkxx, ssh, maint

## Mise à jour en 3.20.1:

- Réinstaller `picgs`
- Pour les images itk82 et itk82s, recopier le fichier `run.sh.dist` dans `run.sh`

## Mise à jour en 3.20.0:

- Mettre à jour picgs
- Reconstruire les conteneurs ou:
  ```
  cd images/scripts
  ./pullall.sh
  ```
- Mettre à jour les scripts `run.sh`:

  ```
  sudo ./upgd-320.sh
  for d in ssh proxy $DKRBUILDALL
  do 
     ( 
       cd images/$d && ./upgd-320.sh
     ) 
  done
  ```
## Mise à jour en 3.19.5:

- Réinstaller `picgs` (pour avoir le numéro de version correct)
- Sur un serveur de sauvegarde, conteneur `pic-bbackup-srv`:  

    ```
    cd images/pic-bbackup-srv
    ./build.sh
    sudo cp run.sh.dist /usr/local/houaibe/conteneurs/pic-bbackup-srv/run.sh
    ```
- Sur un client de sauvegarde, conteneur `pic-backup`:
    ```
    cd images/pic-bbackup
    sudo cp run.sh.dist /usr/local/houaibe/conteneurs/pic-bbackup/run.sh
    ```

## Mise à jour en 3.19.4:

- Réinstaller `picgs` (pour avoir le numéro de version correct)
- Conteneur `pic-brestit`:  

    ```
    cd images/pic-brestit
    ./build.sh
    ```
## Mise à jour en 3.19.3:

- Réinstaller `picgs` (pour avoir le numéro de version correct)
- Conteneur `pic-brestit`:  

    ```
    cd images/pic-brestit
    sudo cp run.sh.dist /usr/local/houaibe/containers/pic-brestit/run.sh
    ```
## Mise à jour en 3.19.2:

- Réinstaller `picgs`

## Mise à jour en 3.19.0, 3.19.1:

- Reconstruire et réinstaller les conteneurs `pic-bbackup-srv`, `pic-bbackup` et `pic-brestit`
- Réinstaller `picgs`

## Mise à jour en 3.18.5, 3.16.6:

- Reconstruire les conteneurs `deb4pic`, `deb4pica` et tous les `pic-itkxy`
- Reconstruire les conteneurs `pic-maint` et `pic-ssh`

## Mise à jour en 3.18.4:

- Réinstaller `picgs`

## Mise à jour en 3.18.3:

- Reconstruisez le conteneur `bbackup-srv`

## Mise à jour en 3.18.2:

- Si vous voulez utiliser `borg-compact.bash` (*recommandé*), reconstruisez le conteneur `bbackup-srv` et insérez la commande dans un crontab
- Reconstruisez le conteneur `bbackup` (nouvelle version du script de sauvegarde)

## Mise à jour en 3.18.1:

- Réinstaller `picgs`

## Mise à jour en 3.18:

- Réinstaller `picgs`
- Mettre à jour les tous les conteneurs (y compris `bbackup`, `bbackup-srv` et `brestit`)
- Tous les scripts de sauvegarde ont été mis à jour, et borg est passé de la version 1.1.17 à la version 1.2.4. Vous devrez donc refaire quelques installations:
  - *Sauvegardes sur une machine distante*: `bbackup-srv` ayant été mis à jour, les fichiers `known_hosts` de **tous** vos clients (conteneurs `pic-bbackup`, répertoire `.ssh`) devront être mis à jour: 
    - Supprimez le fichier `known_hosts`
    - Régénérez-le en suivant la procédure d'installation décrite dans le fichier `https://framagit.org/pic-manu/houaibe/-/tree/master/images/bbackup#cas-de-d%C3%A9p%C3%B4ts-sur-une-machine-distante`
  - Vous devrez convertir vos dépôts pour utilisation avec borg 1.2.4, comme indiqué ici: https://borgbackup.readthedocs.io/en/stable/changes.html#change-log Pour cela vous aurez besoin de connaître le nom de vos repositories: le script `borg_repos.bash`, introduit en 3.17.4, peut être utilisé. Voir le `README` du répertoire `bbackup`
  - Mettez à jour le fichier `run.sh` de vos conteneurs `pic-bbackup` (à partir de `bbackup/run.sh.dist`)   sur chaque serveur client. 

## Mise à jour en 3.17.4:

- Reconstruire l'image `bbackup`

## Mise à jour en 3.17.1, 3.17.2, 3.17.3:

- Réinstaller `picgs`

## Mise à jour en 3.17.0:

- Réinstaller `picgs`
- Reconstruire le conteneur `pic-ssh` afin de bénéficier de l'installation de `httrack`

## Mise à jour en 3.16.0:

- Réinstaller `picgs`
- Installer éventuellement les nouveaux conteneurs reposant sur `php 8.2`, ou le conteneur qui ne contient pas du tout de php. Ces conteneurs exposent les fichiers `mpm_prefork.conf` (réglages d'Apache)  ainsi que `php.ini` (sauf `itk00`)

## Mise à jour en 3.15.0:

- Réinstaller `picgs`

- Si vous souhaitez activer ipv6 (dans les logs):

  - Reconstruisez les images deb4pica, pic-itkxxx et pic-phpmyadmin:
    cd .../ima

    ```
    cd .../images/deb4pica
    ./build.sh
    ```

  - Installez `ipv6nat` comme expliqué dans le dossier ipv6 

## Mise à jour en 3.14.5:

- Réinstaller `picgs`

## Mise à jour en 3.14.4:

- Dans chaque répertoire de conteneur pic-itkxxx, régénérer run.sh:
  `cp run.sh.dist run.sh`
- Reconstruire ou tirer depuis dockerhub (cf. pullAll.sh) tous les conteneurs pic-itkxx
- Réinstaller picgs (optionnel si on vient de la 3.14.3)

## Passage de 3.13.x à 3.14.x:

- Si nécessaire, installer sur le host beautifulsoup4: `pip install beautifulsoup4`
- Réinstaller `picgs`

## Passage de 3.12.x à 3.13.x:

- Réinstaller `picgs` 

## Passage de 3.11.x à 3.12.x:

- Réinstaller `picgs`
- Reconstruire `deb4pic` et toutes les autres images
- Redémarrer les conteneurs

## Passage de 3.11.2 à 3.11.3:

- Le fichier `HEALTHPASSWDFILE` a changé de format, il faut le modifier
- Reconstruire les images `deb4pica` et toutes les images `itkxx` 
- Redémarrer les conteneurs `pic-itkxx`

## Passage de 3.11.0 à 3.11.2:

- Refaire l'installation de `picgs`

## Passage de 3.10.x à 3.11.x:

- Mettre à jour le fichier `/usr/local/etc/houaibe.conf`:

  - Paramètre `DKRBUILDALL`: il doit contenir **seulement** les images `itkxx`
  - Ajouter les paramètres `HEALTHPASSWDFILE` et `HEALTH_MAIL`, et configurer la base de données comme indiqué dans `crontab/README.md`

- Reconstruire *tous les conteneurs* par `upgradeall.sh` ou les télécharger par `pullall.sh`:

  ```
  cd images/scripts
  ./upgradeall.sh
  ```

- Mettre à jour les scripts `run.sh`:

  ```
  cd images
  . /usr/local/houaibe/etc/houaibe.conf
  for d in maint ssh phpmyadmin proxy $DKRBUILDALL
  do 
     ( 
       cd $d && sudo cp run.sh.dist run.sh
     ) 
  done
  ```

- Redémarrer tous les conteneurs:

  ```
  . /usr/local/houaibe/etc/houaibe.conf
  for d in ssh phpmyadmin proxy $DKRSRVWEB; do toctoc restart $d; done
  ```

- Refaire l'installation de `picgs`:

  ```
  cd picgs
  sudo ./installpicgs
  ```

## Passage de 3.10.3 vers 3.10.4:

- Refaire l'installation de `picgs`

## Passage de 3.10.2 vers 3.10.3:

- Reconstruire ou "repuller" les images: `debian11` et `maint`
- Refaire l'installation de `picgs`

## Passage de 3.10.1 vers 3.10.2:

- Refaire l'installation de `picgs`

## Passage de 3.9.x vers 3.10.x:

- Refaire l'installation de `picgs`

- Vérifiez votre `houaibe.conf` par rapport à `houaibe.conf.dist`, il y a de nouveaux paramètres !

- Ajoutez une ligne à votre `crontab` pour vérifier régulièrement la santé des conteneurs (voire le répertoire `crontab`)

- Si vous souhaitez utiliser les conteneurs de dockerhub, vous devez:
  - Ajouter le paramètre `DKRCPTE` à votre `etc/houaibe.conf` (voire `etc/houaibe.conf.dist`)
  - Changer tous les houaibe.conf pour les conteneurs que vous souhaitez télécharger depuis dockerhub, par exemple:
  ```
  cd images/itk73
  sudo cp houaibe.con /usr/local/containers/pic-itk73
  ```

- Redémarrer tous les conteneurs.

## Passage de 3.8.x à 3.9.x:

- Refaire l'installation de `picgs`
- Appliquer les mises à jour ci-dessous si ce n'est déjà fait
- *Si vous souhaitez utiliser les sauvegardes utilisant borgbackup:*
  - Ajoutez les paramètres nécessaires au fichier `houaibe.conf`
  - Installez les conteneurs en suivant les readme

### Version 3.9.2:

- Reconstruire les images `itk7x` et redémarrer les conteneurs correspondants
- Reconstruire le conteneur `ssh` et redémarrer `pic-ssh`

## Passage de 3.8.0 à 3.8.3:

- Refaire l'intallation de `picgs`
- Remplacer le fichier `pic-proxy/vhost/default` par la nouvelle version proposée (`images/proxy/vhost/default`) et redémarrer `pic-proxy`

## Passage de 3.7.x à 3.8.x:

- Sauvegarder le répertoire `/usr/local/houaibe/conteneurs/pic-ssh`
  
- Reconstruire et redémarrer le conteneur `pic-ssh`:
  
   ```
   cd .../images/ssh
   ./build.sh
   ./run.sh
   ```
   
- Construire et installer le conteneur `pic-itk74`:
  
   ```
   cd .../images/itk74
   ./build.sh
   ./run.sh 
   ```
   
- Modifier `/usr/local/houaibe/etc/houaibe.conf`:
  - Ajouter `pic-itk74` à la déclaration de variable `DKRSRVWEB`

- Mettre à jour `picgs` (s'installe à côté de la version précédente, un retour en arrière est toujours possible)
   ```
   cd .../picgs
   sudo ./installpicgs
   ```

## Passage de 3.6.x à 3.7.x:

- Modifier `/usr/local/houaibe/etc/houaibe.conf`:
  - Le paramètre `DOMAINNAME` est à présent une variable d'environnement: 
    modifier les dernières lignes à partir de houaibe.dist
  
- Mettre à jour `picgs` (s'installe à côté de la version 3.6, un retour en arrière est toujours possible)

   ```
   cd .../picgs
   sudo ./installpicgs
   ```

- Conteneur `pic-maint`:
  ```
  cd .../images/paint
  ./build.sh
  sudo cp run.sh.dist /usr/local/houaibe/containers/pic-main
  ./run.sh
  ```
  
- Conteneur `pic-itk73`:
   ```
   cd .../images/itk73
   ./build.sh
   sudo cp run.sh.dist /usr/local/houaibe/containers/pic-itk73
   ./run.sh 
   ```
   
- Conteneur `pic-itk74s`:
   ```
   cd .../images/itk74s
   ./build.sh
   sudo cp run.sh.dist /usr/local/houaibe/containers/pic-itk74s
   ./run.sh 
   ```

