# Un nat pour Ipv6

Cette architecture à base de conteneurs docker et de proxy inverse présente un grave inconvénient: lorsqu'une requête arrive sur le proxy, si elle provient d'une adresse Ipv6 on n'a pas l'adresse de source dans les logs, retrouve l'adresse Ipv4 de la gateway. Cela n'est pas du tout informatif.

Il est cependant possible de remédier à ce problème de la manière suivante:

- Activer ipv6 dans Docker, avec un préfixe ULA (adresses non routables), par exemple: `fd00:dead:beef::/48`
- Donner au réseau picnet une adresse ULA non routable également, par exemple: `fd00:dead:beee::/48`
- Installer et démarrer le conteneur `ipv6nat`, voir https://github.com/robbertkl/docker-ipv6nat

## En pratique:

Le répertoire `$WORK` correspond au répertoire de travail, celui qui a été créé par le `git clone` initial (par exemple `~/houaibe`).

1/ Ajouter ou modifier le paramètre `IPV6` dans `/usr/local/houaibe/etc/houaibe.conf`:

```
IPV6=1
```

2/ Modifier le run.sh de pic-proxy:

```
cd $WORK/images/proxy
cp run.sh.dist /usr/local/houaibe/conteneurs/pic-proxy/run.sh
```

3/ Ajouter dans `/etc/docker/daemon.json`(ou créer ce fichier s'il n'existe pas encore):

```
{
   "ipv6": true,
   "fixed-cidr-v6": "fd00:dead:beef::/48"
}
```
4/ Redémarrer docker:

```
systemctl stop docker
systemctl start docker
```

5/ Installer et démarrer le conteneur `pic-ipv6nat`:

```
cd $WORK/images/ipv6nat
sudo ./installcont.sh
toctoc restart pic-ipv6nat
```

6/ Rechercher, arrêter et supprimer tous les conteneurs qui utilisent le réseau `picnet`

```
docker network inspect picnet
docker rm -f pic-xxx
```

7/ Recréer le réseau picnet et redémarrer les conteneurs supprimés précédemment:

```
docker network rm picnet
docker network create --ipv6 --subnet fd00:dead:beee::/48 picnet
toctoc restart pic-itkxxx
```

8/ tester: 

- Refaire les tests avec `nmap` comme indiqué au chapitre sécurité
- Vérifier que les sites web sont accessibles à partir d'une adresse IPv6
- Vérifier que l'adresse est correctement logguée

