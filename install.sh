#! /bin/bash

#
# Script d'installation des répertoires pour les conteneurs de houaibe
#
# Usage: sudo ./install.sh
#

#set -v

[[ "$(id -u)" -ne "0" ]] && echo "VOUS DEVEZ ETRE ROOT !" && exit 1

if [[ ! -f ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf ]]
then
   mkdir -p ${HOUAIBEDIR-/usr/local/houaibe}/etc
   cp etc/houaibe.conf.dist ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf 
   echo "Veuillez configurer ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf selon vos besoins, puis appelez $0 à nouveau"
   exit 1;
fi

# Lire les paramètres
. ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf

# Installer createEnv.sh au bon endroit
mkdir -p $CONTAINERSDIR || exit
cp images/scripts/createEnv.sh $CONTAINERSDIR || exit
chmod -R 550 $CONTAINERSDIR/
chmod ug+w $CONTAINERSDIR/
chmod g+s $CONTAINERSDIR/
chgrp -R docker $CONTAINERSDIR/

# Créer les répertoires pour les conteneurs
(
   cd ..
   CONT=$(ls)
   
   for d in $CONT
   do
       if [[ -d $d ]]
       then
	       (
	       cd $d
	       [[ -x ./installcont.sh ]] && ./installcont.sh
	       )
       fi
   done
)

