#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

# Vérifie si certains plugins sont installés et actifs dans un ou plusieurs site wordpress
#
# USAGE
#      picscript scripts/chk-wpplugins.py [monsite] [site] [1]

import sys
import traceback
import json

from params import *
from picgestion import PicGestion
from erreur import PicErreur
from ouinon import ouiNon
from picconfig import getConf
from main import *
from sysutils import SysUtils
from affiche import afficheRouge, afficheVert

from homedir import *
        
class HomeDirDefault(HomeDirInstallation):

    def __isInstalled(self, wpplugins):
        ''' Prérequis: self.isWordpress() renvoie True
            wpplugins: Un array de Plugin
            Renvoie un dict donnant l'état des plugins:
              0 si le plugin n'est pas installé
              1 si le plugin est installé mais pas activé
              2 si le plugin est installé et activé
        '''
        pg = self.picgestion
        www = pg.www
        user = pg.user
        conteneur = getConf('DKRMAINT')

        cmd = f"cd {www} && wp plugin list --format=json"
        out = self._system(cmd, conteneur=conteneur, interactif=False, user=user)
        if len(out) > 1:
            import pprint
            print(afficheRouge(f"ATTENTION - La commande:\n{cmd}\nretourne un truc pas clair (erreur ou warning php)\n"))
            print(afficheRouge(pprint.pformat(out)))
            print(afficheRouge("\nCorrigez et recommencez\n"))
            raise Exception

        aplgs = json.loads(out[0])
        #import pprint
        #pprint.pprint(aplgs)

        # Reformattage de aplgs array -> dictionnaire
        dplgs = {}
        for x in aplgs:
            dplgs[x['name']] = x
            
        # Préparation du Array de sortie
        rvl = {}
        for p in wpplugins:
            if p in dplgs:
                if dplgs[p]['status'] == 'active':
                    rvl[p] = 2
                else:
                    rvl[p] = 1
            else:
                rvl[p] = 0
                
        return rvl
            
    def execute(self):
        '''Ecrit l'état des plugins stops-core-theme-and-plugin-updates, ssh-sftp-updater-support et cache-control'''

        pg = self.picgestion
        www = pg.www
        user = pg.user
        conteneur = getConf('DKRMAINT')
        wpplugins = [ 'stops-core-theme-and-plugin-updates', 'ssh-sftp-updater-support', 'cache-control' ]

        try:
            if self.getWordpress() !=  "":
                dsts = self.__isInstalled(wpplugins)
                for p in wpplugins:
                    if dsts[p] == 2:
                        print(afficheVert (f"{user}: Le plugin {p} est installé et activé"))
                    if dsts[p] == 1:
                        print(afficheJaune (f"{user}: Le plugin {p} est installé, mais pas activé"))
                    if dsts[p] == 0:
                        print(afficheRouge (f"{user}: Le plugin {p} n'est pas installé"))

        except Exception as e:
            print ("----------")
            raise e
            
# PROGRAMME PRINCIPAL
erreur = False
try:
    prms = Params('chk-wpplugins.py', sys.argv, default=True, helpMsg = "Affiche l'état des plugins Wordpress indispensables pour un hébergement au Pic")
    algo = LoopAlgo()
    algo(prms, [ 'Compte', HomeDirDefault ])

except KeyboardInterrupt:
    print("\n\n===> Action interrompue par un CTRL-C")

except PicErreur as x:
    erreur = True
    print(afficheRouge(x))

except Exception as x:
    erreur = True
    print(x)
    if 'PICGS_DEBUG' in os.environ:
        traceback.print_exception(x)
        
if erreur:
    sys.exit(1)
