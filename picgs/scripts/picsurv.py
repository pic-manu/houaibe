#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

import time
from erreur import PicErreur
from datetime import datetime
from picconfig import getConf
from plugin import SysUtils

#
# Pour chaque conteneur , on verifie que apache tourne a l'interieur
# Si oui, tant mieux
# Si non, on ecrit un truc dans e fichier /root/SURV/surv.jj-m-aaaa et on redemarre apache
#

# ------------------------------------------------------------------------
# Quelques fonctions
# ------------------------------------------------------------------------
def isApacheUp(conteneur):
    """Envoie une exception si apache n'est pas demarre, retourne normalement sinon"""
    sys._system('ps --no-headers -C apache2 -o command',conteneur)


def restartApache(conteneur):
    """Redemarre Apache"""
    # Supprime le fichier de pid qui peut empecher apache de demarrer
    try:
        sys._system('rm -f /var/run/apache2/apache2.pid',conteneur)
        sys._system('apachectl start',conteneur)
    except PicErreur as e:
        print(e.message)

 
sys  = SysUtils()
now  = datetime.now()
date = str(now.day) + '-' + str(now.month) + '-' + str(now.year)
log  = '/root/SURV/surv.' + date + '.txt'
for conteneur in getConf('DKRSRVWEB'):
    ok = False
    for i in range(10):
        try:
            isApacheUp(conteneur)
            
            # Apache OK
            ok = True
            
            # Apache OK, mais on a du le redemarrer
            if i>0:
                heure= date + ' ' + str(now.hour) + ':' + str(now.minute)
                fh_log = open(log,'a')
                fh_log.write(heure + " " + conteneur + " apache redemarre \n")
                fh_log.close()
            break
                
        # Pas d'Apache a l'horizon
        except PicErreur as e:
            heure= date + ' ' + str(now.hour) + ':' + str(now.minute)
            fh_log = open(log,'a')
            fh_log.write(heure + " " + conteneur + " " + str(i+1) + "eme tentative de redemarrage d'apache\n")
            fh_log.close()
            restartApache(conteneur)
            time.sleep(5)

    # On a essaye 10 fois en vain, on laisse tomber
    if not ok:
        heure= date + ' ' + str(now.hour) + ':' + str(now.minute)
        fh_log = open(log,'a')
        fh_log.write(heure + " " + conteneur + " ATTENTION - apache n'a PAS REDEMARRE\n")
        fh_log.close()

    
