#! /usr/bin/python3

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

import sys
import argparse
import datetime
from ua_parser import user_agent_parser

#
# USAGE: python3 anal_nginx_login.py < access.log
#        Analyse le fichier access.log produit par nginx et calcule la proportion de requêtes envoyées sur 
#        les écrans de login de wordpress, spip ou autres
#        Sépare en requêtes OK (status=200) et KO (status!=200)
#

VERSION="1.0.0"

def params():
    '''Read the cli parameters'''

    parser = argparse.ArgumentParser(description="anal_nginx_login.py "+ VERSION + " - Recherche les attaques brute force sur les logins des CMS")
    parser.add_argument('--cms', action="store", type=str, default = "", dest="cms", help="Affiche les stats concernant un cms")
    parser.add_argument('--aussiko', action="store_true", help="Avec --cms: affiche aussi les requêtes en erreur")
    parser.add_argument('--ip', action="store_true", help="Avec --cms: affiche l'ip")
    parser.add_argument('--version', action='version', version='%(prog)s '+ VERSION)
    parser.add_argument('--agent',action="store_true", help="Avec --cms: affiche les stats suivant les agents")
    parser.add_argument('--date', action="store", type=int,default=0,dest="date",help="Affiche la date en clair")
    
    options = parser.parse_args()
    return options



#
# Cet objet analyse le fichier d'entrée (log nginx)
#
# Il génère une structure de données composée de dictionnaires (4 niveaux)
# Les clés du dictionnaire sont les champs du fichier d'entrée
# Les champs utiles sont:
#
#   Nom du site (0)
#   Adresse ip (1)
#   Status (9)
#   Agent (>=12)
#
# Un tuple permet de déclarer l'ordre des clés (cf. setFields)
#
# La valeur est le nombre de lignes concernées
#
# Un FILTRE permet de n'analyser que CERTAINES LIGNES
# (cf. setFilter)
#
# On compte le nombre de lignes total, en ignorant les lignes foireuses
# On fait l'hypothèse que toutes les requêtes sont faites à la même date, donc on 
# utilise la première ligne pour connaître la date
#

#
# Permet un copier coller vers LibreOffice ou OnlyOffice
#
REF_DATE = datetime.datetime(1899,12,30)

#
# Les champs 
#
REF_FIELDS = (0,1,8,12)

class Anallog(object):
    
    def __init__(self):
        self.__filter = None
        self.__fields = REF_FIELDS
        self.__date   = None
        self.__data   = None
        self.__cnt    = 0
        
    def formDate(self,date):
        '''Convertit la date depuis le format nginx vers un format reconnaissable par un tableur (un entier)'''
    
        date = date.split(':')[0]
        date = date.replace('[','')
        date = date.replace('Jan','01')
        date = date.replace('Feb','02')
        date = date.replace('Mar','03')
        date = date.replace('Apr','04')
        date = date.replace('May','05')
        date = date.replace('Jun','06')
        date = date.replace('Jul','07')
        date = date.replace('Aug','08')
        date = date.replace('Sep','09')
        date = date.replace('Oct','10')
        date = date.replace('Nov','11')
        date = date.replace('Dec','12')
        (j,m,a) = date.split('/')
        d       = datetime.datetime(int(a),int(m),int(j))
        duree   = d - REF_DATE
        date    = duree.days  
        
        self.__date = date
        
        return date

    def int2date(self, idate):
        '''Convertit et imprime la date depuis un int vers une date lisible'''
        
        duree= datetime.timedelta(int(idate))
        date = REF + duree
        print (date)

    def setFields(self,fields):
        '''Initialise le tuple t avec les numéros de champs'''
        
        if not isinstance(fields,tuple):
            raise PicErreur ("ERREUR INTERNE - fields n'est pas un tupple")
        
        if len(t) != len(REF_FIELDS):
            raise PicErreur ("ERREUR INTERNE - fields n'est pas un tuple de longueur 4")
            
        for i in fields:
            if not isinstance(i,int):
                raise PicErreur ("ERREUR INTERNE - fields n'est pas un tuple d'entiers")
                
        if sorted(t) != REF_FIELDS:
            raise PicErreur ("ERREUR INTERNE - fields ne contient pas les bons numéros")
            
        self.__fields = fields
    
    def setFilter(self,filter):
        '''filter est un callable, c-à-d une fonction à qui on passe un tableau
            et qui renvoie True/False'''
        
        if not callable(filter):
            raise PicErreur("ERREUR INTERNE - filter n'est pas appelable")
            
        self.__filter = filter
            
            
    def getFamily(self,ua_string):
        '''On extrait du paramètre la 'family' du user agent'''
        
        parsed_string = user_agent_parser.ParseUserAgent(ua_string)
        return parsed_string['family']

    def analLigne(self,line):
        '''Analyse une ligne de fichier log et renseigne le dictionnaire data'''
        
        l = line.split(' ', 12)
    
        # Détection de ligne foireuse
        if len(l) < 13:
            raise PicErreur ("LIGNE FOIREUSE")
            
        # Si on ne peut convertir le status en int, la ligne est foireuse
        try:
            status = int(l[9])

        except:
            raise PicErreur ("LIGNE FOIREUSE")
    
        # La ligne n'est pas foireuse: on la compte
        self.__cnt += 1
        
        # La première date du fichier est considérée comme la date de tout le fichier
        # OK si on logrotate un peu après 0h00
        if self.__date == None:
            date = l[4]
            date = formDate(date)
            self.__date = date
       
        # L'url visitée
        url = l[7]
        
        # On filtre
        if self.__filter != None:
            if not self.__filter(url):
                return
        
        # On remplace l[12] par la "family" de l'agent
        l[12] = self.__getFamily(l[12])
        
        # On remplit les "data"
        self.__fillData(l)
        

    def __fillData(self,line):
        '''On ajoute un élément de dictionnaire à self.__data'''
        
        data = self.__data
        level_low = 3
        for level in range(0,level_low-1):
            key = line[level]
            if not key in data:
                data[key] = {}
                data = data[key]

        level = level_low
        
        key = line[level]
        if not key in data:
            data[key] = 0
        data[key] += 1
        






def anal():
    '''Lit l'entrée standard, analyse les logs et sort une ligne de stat générales
       Renvoie un dictionnaire 
    '''

    ttl            = 0
    status         = 0
    wp_login_ok    = 0
    spip_login_ok  = 0
    autre_login_ok = 0
    
    wp_login_ko    = 0
    spip_login_ko  = 0
    autre_login_ko = 0
    
    csv  = {}
    
    # Lecture du fichier d'entrée et construction du dictionnaire csv
    for line in sys.stdin:
        l = line.split(' ')
    
        # Moins de 8 champs -> ligne foireuse
        if len(l) >= 8:
    
            # Si on ne peut convertir le status en int, la ligne est foireuse
            try:
                status = int(l[9])
            except:
                continue
    
            # La première date du fichier est considérée comme la date de tout le fichier
            # OK si on logrotate un peu après 0h00
            if not 'date' in csv:
                date = l[4]
                date = formDate(date)
                csv['date'] = date
           
            # L'url visitée
            url    = l[7]
    
            # c'est du wordpress
            if url.find('wp-login')>=0:
                if status == 200:
                    wp_login_ok += 1
                else:
                    wp_login_ko += 1
    
            # c'est du spip
            elif url.find('page=login')>=0:
                if status == 200:
                    spip_login_ok += 1
                else:
                    spip_login_ko += 1
    
            # c'est autre chose
            elif url.find('login')>=0:
                if status == 200:
                    autre_login_ok += 1
                else:
                    autre_login_ko += 1
    
            ttl += 1
    
    csv['ttl']            = ttl
    csv['wp_login_ok']    = wp_login_ok
    csv['wp_login_ko']    = wp_login_ko
    csv['spip_login_ok']  = spip_login_ok
    csv['spip_login_ko']  = spip_login_ko
    csv['autre_login_ok'] = autre_login_ok
    csv['autre_login_ko'] = autre_login_ko

    # On envoie en Tsv (séparé par des \t, uniquement des entiers
    # L'interopérabilité entre tableurs est meilleure ainsi
        
    if csv['ttl'] > 0:
        for f in ['date','ttl','wp_login_ok','wp_login_ko','spip_login_ok','spip_login_ko','autre_login_ok','autre_login_ko']:
            print ("{0}\t".format(csv[f]), end="")
    print ()
    

def cms_signature(cms):
    '''Retourne la signature à chercher dans l'url pour identifier le cms recherché'''
    
    if cms==None:
        return 'login'
        
    if cms=="wordpress":
        return 'wp-login.php'
        
    elif cms=='spip':
        return 'spip.php?page=login'
        
    elif cms=='autre':
        return 'login'
        
    else:
        raise (Exception ("ERREUR - cms " + cms + " incoonu "))


def selbit(ip,bit_ip=None):
    '''Sélectionne 8, 16, 24, 32 bits et renvoie l'adresse tronquée'''
    
    raise(Exception("ERREUR - CODE PAS ENCORE ECRIT"))
    
def anal_cms(cms,aussiko=False,bit_ip=None, agent=False, ip=False):
    '''
       params:  cms     = Le nom du cms étudié
                aussiko = Si False, on ne s'occupe pas des lignes avec status != 200
                          Si True on compte les lignes 200 d'un côté, les lignes != 200 de l'autre
                agent   = Si False on ne s'occupe pas de l'agent
                          Si True  on différentie les agents
                bit_ip = Le nombre de bits considéré pour l'ip (8,16,24,32 pour ipv4, soit 32, 64, 96, 128 pour ipv6)
                         Si None on ne tient pas compte de l'ip - PAS IMPLEMENTE
                         
       Lit l'entrée standard, analyse les logs et sort une ligne de stat
       Renvoie un dictionnaire 
    '''

    if bit_ip != None:
        if not bit_ip in [ "8", "16", "24", "32" ]:
            raise Exception("ERREUR - bit_ip=" + bit_ip)
            
    date   = ''
    sign   = cms_signature(cms)
    ttl    = 0
    sites  = {}
    
    # Lecture du fichier d'entrée et construction du dictionnaire csv
    for line in sys.stdin:
        l = line.split(' ',12)

        # Moins de 12 champs -> ligne foireuse
        if len(l) > 12:
    
            # On ne peut convertir le status en int -> ligne foireuse
            try:
                status = int(l[9])

            except:
                continue

            # On ne compte que les lignes non foireuses
            ttl += 1
    
            # La première date du fichier est considérée comme la date de tout le fichier
            # C'est pratique si on logrotate un peu après 0h00
            if date == '':
                date = l[4]
                date = formDate(date)

            # On ignore les status != 200 
            if aussiko==False and status != 200:
                continue

            # L'ip et l'url visitée
            url = l[7]
            site= l[0]
            ip  = l[1]

            ua_string     = l[12]
            parsed_string = user_agent_parser.ParseUserAgent(ua_string)
            uagent        = parsed_string['family']

            # C'est le cms recherché
            if url.find(sign)>=0:

                # Calcul de la clé
                cle = ""
                if ip:
                    cle += ip + " "
                cle += site + " "
                if agent:
                    cle += uagent + " "
                if aussiko:
                    if status==200:
                        cle += "ok"
                    else:
                        cle += "ko"

                #ip    = sel_bits(ip)
                #sitip = site + ' ' + ip
                        
                if not cle in sites:
                    sites[cle] = 0
                sites[cle] += 1
    
    # Forme le tableau final
    csv = []
    for site in sites:
        c          = {}
        c['date']  = date
        c['ttl']   = ttl
        c['site']  = site
        c['req']   = sites[site]
        
        csv.append(c)

    # Imprime le tableau final
    # On envoie en Tsv (séparé par des \t, uniquement des entiers
    # L'interopérabilité entre tableurs est meilleure ainsi
    if ttl > 0:
        csv.sort(key = lambda x: x['site'])
        for c in csv:
            # On peut définir un seuil pour ne pas afficher les ip qui n'envoient qu'une ou deux requêtes
            # Ici on affiche tout
            if c['req'] >= 0:
                for f in ['date','ttl','site','req']:
                    print ("{0}\t".format(c[f]), end="")
                print ()
    
# PROGRAMME PRINCIPAL
options = params()

if options.date > 0:
    int2date(options.date)

if options.cms != '':
    anal_cms(options.cms, options.aussiko, None, options.agent, options.ip)
else:   
    anal()
    
