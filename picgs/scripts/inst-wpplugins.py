#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

# Installation du plugin easy-update-manager dans un ou plusieurs sites wordpress si pas déjà fait
#
# USAGE
#      picscript scripts/inst-wpplugins.py [monsite] [site] [1]

import sys
import re
import time
import socket
import subprocess
import traceback

import json

from params import *
from picgestion import PicGestion
from erreur import PicErreur
from ouinon import ouiNon
from picconfig import getConf
from plugin import *
from main import *

from sysutils import SysUtils
from affiche import afficheRouge

from homedir import *

        
class HomeDirDefault(HomeDirInstallation):

    def __isInstalled(self, wpplugin):
        '''renvoie True si le plugin wordpress est déja installé'''

        pg = self.picgestion
        www = pg.www
        user = pg.user
        conteneur = getConf('DKRMAINT')
        
        cmd = f"cd {www} && wp plugin list --name={wpplugin} --format=json"
        out = self._system(cmd, conteneur,False,user)
        if len(out) > 1:
            import pprint
            print(afficheRouge(f"ATTENTION - La commande:\n{cmd}\nretourne un truc pas clair (erreur ou warning php)\n"))
            print(afficheRouge(pprint.pformat(out)))
            print(afficheRouge("\nCorrigez et recommencez\n"))
            raise Exception

        jsout = json.loads(out[0])
        if len(jsout) > 0:
            return True
        else:
            return False
        
    def execute(self):

        pg = self.picgestion
        www = pg.www
        user = pg.user
        conteneur = getConf('DKRMAINT')
        wpplugin = 'stops-core-theme-and-plugin-updates'

        if self.__isInstalled(wpplugin):
            print(afficheRouge (f"{user}: Le plugin {wpplugin} est déjà installé !"))
            return
        
        print ("==== Vérification du compte picadmin")
        if not self.chkpicadmin:
            print (afficheRouge(f"Le user {user} n'a pas de compte picadmin, pas d'installation"))
            return
            
        print ("==== Installation du plugin wordpress easy-update-manager")
        self._systemWithWarning(f"cd {www} && wp plugin install {wpplugin} --activate", conteneur, False, user)

        print ("==== Configuration du plugin wordpress easy-update-manager")
        self._configEasyUpdateManager()


        print("OK")
        
# PROGRAMME PRINCIPAL
erreur = False
try:
    prms = Params('inst-plugins.py', sys.argv, default=True,helpMsg="inst-wpplugins [nom] site [version]\n      Installe quelques plugins indispensables sur les sites wordpress")
    fonction = prms.fonction
    if fonction != 'site':
        raise PicErreur("Ce script ne s'applique qu'aux hébergements de type site")
    algo = LoopAlgo()
    algo(prms, [ 'Compte', HomeDirDefault ])

except KeyboardInterrupt:
    print("\n\n===> Action interrompue par un CTRL-C")

except PicErreur as x:
    erreur = True
    print(afficheRouge(x))

except Exception as x:
    erreur = True
    print(x)
    if 'PICGS_DEBUG' in os.environ:
        traceback.print_exception(x)
        
if erreur:
    sys.exit(1)

