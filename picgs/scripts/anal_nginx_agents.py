#! /usr/bin/python3

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

import sys
from ua_parser import user_agent_parser

# 10 pour le "top10", 5 pour le "top5", etc.
N = 5

def formDate(date):
    '''Convertit la date depuis le format nginx vers un format reconnaissable par un tableur'''

    date = date.split(':')[0]
    date = date.replace('[','')
    date = date.replace('Jan','01')
    date = date.replace('Feb','02')
    date = date.replace('Mar','03')
    date = date.replace('Apr','04')
    date = date.replace('May','05')
    date = date.replace('Jun','06')
    date = date.replace('Jul','07')
    date = date.replace('Aug','08')
    date = date.replace('Sep','09')
    date = date.replace('Oct','10')
    date = date.replace('Nov','11')
    date = date.replace('Dec','12')
    return date

ttl=0
date=''
agents    = {}

for line in sys.stdin:
    l = line.split(' ',12)
    if len(l) >= 13:
        if date == '':
            date = l[4]
            date = formDate(date)
      
        ua_string = l[12]
        parsed_string = user_agent_parser.ParseUserAgent(ua_string)
        agent = parsed_string['family']
        if not agent in agents:
            agents[agent] = 0

        agents[agent] += 1
        ttl += 1

if ttl > 0:
    
    # k_ag contient les clés de agents, triées par valeur de value croissante
    k_ag = sorted(agents,key=agents.get)

    k_topN = {}
    # Si plus de N agents, on crée un agent "autres"
    nb_autres = 0
    nb_agents = len(k_ag)
    long = False
    if nb_agents > N:
        long = True
        for i in range(nb_agents-N-1):
            nb_autres += agents[k_ag[i]]

    # Du plus important au moins important
    k_ag.reverse()

    print ("{0}\t{1}\t".format(date,ttl), end='')
    if nb_agents > N:
        nb_agents = N

    for k in range(nb_agents):
        print ("{0}\t{1}\t".format(k_ag[k],agents[k_ag[k]]), end='')
    if long:
        print ("{0}\t{1}".format("autres",nb_autres/ttl))
    else:
        print ()


