# picgs
**P**rojet **I**nternet et **C**itoyenneté, **G**estion **S**ystème

## Installation:

### Prérequis:

Quelques dépendances doivent être installées sur le serveur:

```
apt-get update && apt-get install -y python3-ldap3 python3-dateutil apg
pip install beautifulsoup4
```

### Installation:

```
sudo ./installpicgs
```

## Mise à jour:

```
sudo ./installpicgs
```

Le code de `picgs` version `x.y.z` se trouve dans `/usr/local/houaibe/picgs/x.y` Si un correctif est publié (incrémentation du nombre `z`) les fichiers de `picgs` seront écrasés par la mise à jour. Cela ne porte pas à conséquence puisqu'il s'agit de corrections mineures. Mais si les numéros `x` ou `y` sont incrémentés, les fichiers ne sont pas écrasés par prudence.

## Fonctionnement:

Les personnes qui utilisent `picgs` administrent *le système d'hébergement*, mais pas obligatoirement le serveur lui-même (au sens unix). Les utilisateurs correspondants doivent appartenir au groupe `docker`, mais il n'est pas nécessaire qu'ils aient les droits `sudo`. Donc dans la suite la commande `picgs` sera appelée sans passer par `sudo`, et  cela suppose que l'utilisateur fasse partie du groupe `docker`.

### Nom court, nom long

Chaque adhérent associatif a un "nom long" (nom de l’association), et un "nom court" : le nom court est un "nom de code" qui sera utilisé  dans les noms de répertoire. 

### Les différents types d’espace web

Plusieurs types d’espace web ont été identifiés, il faut spécifier à  `picgs` sur quel type d’espace on est en train de créer (second paramètre, appelé "fonction") :

-  **site** (**st**) : Un site web avec ou sans CMS
-  **galette** (**gt**) : Une installation de galette
-  **dolibarr** (**db**) : Une installation de Dolibarr
-  **autre** (**au**) : Une application spécifique à cette association

**NOTE** - Vous pouvez créer d'autres types d'espace web à votre convenance. Pour cela vous devez modifier les paramètres `'FONCTIONS'` et `'CODEFCT'` du fichier `lib/picconfig.py` 

### Le numéro de version

A chaque fois qu’on crée un site web, on calcule un numéro de  version, cela permet d’avoir plusieurs espaces web pour une même  association et une même fonction. Ce numéro est géré automatiquement  dans certains cas (ajout), doit être demandé explicitement dans d’autres cas (suppression, modification, etc).

### Le home directory

Le home directory correspondant au nouvel utilisateur est calculé  automatiquement à partir du nom court.  Quelques exemples :

-  asso1-st2
-  asso2-gt1
-  asso1-db1

###  L’association des poètes

Utilisée ici en tant que cas d’utilisation. Ses caractéristiques sont les suivantes :

-  nom long : Les Poètes du 19ème siecle
-  nom court : poetes
-  fonction : st, gt, db (elle bénéficie d’un site web, d’une installation galette pour gérer ses adhérents et d’une installation Dolibarr pour la comptabilité)
-  Contact technique : Une adresse mail (obligatoire) et un numéro de téléphone (facultatif)
-  Conteneur : par exemple `pic-itk80s` pour un site sous spip 

Par ailleurs on suppose dans la suite que votre nom de domaine est `exemple.org`

### Ajouter, modifier, supprimer un espace web

#### Ajouter :

```
picgs ajout poetes site
```

On doit indiquer le type d'espace web sur la ligne de commande. Le programme demande les informations évoquées ci-dessus, ainsi que **le nom du conteneur de destination**. Celui-ci dépendra du CMS prévu dans le site (version et configuration de php).

**NOTE** - En effet, les conteneurs s'appellent `pic-itk73` (php 7.3) ou `pic-itk74s` (php 7.4 configuré pour spip).

Si on a ajouté un espace de type "galette", picgs propose de faire  l’installation de galette. De même si on a ajouté un espace de type  "dolibarr", picgs propose de faire l’installation de dolibarr. Attention si on répond N, on ne pourra plus par la suite installer galette ni  dolibarr dans cet espace web en utilisant picgs. Bien sûr on pourra toujours le faire à la main (mais ce n'est pas une très bonne idée).

#### Modifier:

```
picgs modif poetes site 1
```

On doit indiquer le type d'espace web **et** le numéro de version sur la ligne de commande. On peut utiliser cette commande pour modifier certains paramètres  (nom long etc.), également pour modifier le mot de passe d'accès à sftp ou à la base de données. 

Pour ne pas modifier le mot de passe, il suffit de  répondre **’.’** à la question sur le mot de passe.

**ATTENTION !** Cette commande permet aussi de modifier  le mot de passe d’accès à la base de données ! Si vous faites cela alors qu’un CMS est fonctionnel, il faut penser à modifier à la main les  paramètres de connexion ! (fichier `config/connect.php` sous spip par exemple). Sinon le site tombera en panne. De manière  générale tant que le site fonctionne il n’y a pas de raison de modifier  ce mot de passe.

#### Changement de conteneur :

```
picgs demenage poetes site v2
```

Déménager un site permet de modifier la version de php utilisée.

#### Adresse de production:

Jusque la, le site créé a comme adresse `poetes-st2.exemple.org` Cette adresse, très utile sur un plan technique, n'est pas très... poétique. L'association hébergée a probablement un nom de domaine à elle, disons que c'est `poetes.org` Vous voudrez donc que l'url `www.poetes.org` pointe sur votre site. Pour cela deux conditions doivent être réunies:

1. Le DNS de `poetes.org` doit être configuré correctement

2. Vous devez avoir joué la commande:

   ```
   picgs prod poetes site 2
   ```

   Lorsque picgs vous posera la question, vous répondrez `www.poetes.org`

**NOTE** - Il est possible de jouer la commande plusieurs fois, cela permet d'avoir plusieurs adresses de production (`poetes.org` et `www.poetes.org`, par exemple)

#### Passage d'une adresse de prod en https :

```
sudo picgs https www.poetes.org
```

#### Supprimer :

```
picgs suppr poetes site 2
```

#### Demander des informations:

##### Sur un site :

```
picgs info poetes site 2
```

##### Sur plusieurs sites:

```
picgs info poetes
```

*ou*

```
picgs collecte --nom=poetes
```

##### Sur tous les sites :
```
picgs collecte
```
*ou*

```
picgs collecte --long
```
**NOTES**

* Le switch `--long` affiche également la taille prise par le site. Cette information est utile mais peut être longue à obtenir
* Le résultat de la commande collecte est un fichier au format csv il peut être facilement ouvert avec un tableur
* La colonne picadmin permet de connaître les sites qui ont un compte "picadmin": par convention, ces sites (généralement spip ou wordpress) sont mis à jour par l'hébergeur, qui peut par ailleurs se connecter pour aider au support technique

### spip

`picgs` comprend plusieurs commandes permettant de gérer les installations du CMS spip.

#### Installation de spip:

*documentation à écrire*

#### Mise à jour de spip:

*documentation à écrire*

### Wordpress

picgs comprend plusieurs commandes permettant de gérer les installations du CMS wordpress

#### Installation de wordpress :

*documentation à écrire*

#### Mise à jour de wordpress:

*documentation à écrire*

### Galette:

picgs permet de gérer l'installation et la mise à jour de galette

#### Préparation:

Avant de pouvoir installer ou mettre à jour galette, il faut qu'un admin récupère le fichier tar du logiciel et le dépose dans le répertoire /home (voir ci-dessous la documentation de toctoc):

```
toctoc envoyer galette-0.9.4.tar.bz2
```

#### Installation de galette:

Lors de la commande picgs ajout, si le type d'espace web (second paramètre) est `galette`, l'application galette est automatiquement installée:

```
picgs ajout poetes galette
```

#### Mise à jour de Galette:

On peut mettre à jour galette avec :

```
picgs galettemaj poetes galette 1
```

### Dolibarr:

picgs permet de gérer l'installation et la mise à jour de Dolibarr

#### Préparation:

Avant de pouvoir installer ou mettre à jour dolibarr, il faut qu'un admin récupère le fichier tar du logiciel et le dépose dans le répertoire /home (voir ci-dessous la documentation de toctoc):

```
toctoc envoyer dolibarr-12.0.5.tgz
```

#### Installation de dolibarr:

Lors de la commande picgs ajout, si le type d'espace web (second paramètre) est `galette`, l'application galette est automatiquement installée:

```
picgs ajout poetes dolibarr
```

#### Mise à jour de Dolibarr:

On peut mettre à jour dolibarr avec :

```
picgs galettemaj poetes dolibarr 1
```

### Archivage, désarchivage:

picgs permet d'archiver un site, c'est-à-dire mettre les fichiers dans un tar et sauvegarder la base de données au format mysql, et aussi de faire l'opération inverse. Il est utile de faire cela avant une mise à jour d'un CMS

#### Archivage:

```
picgs archivage poetes site 1
```

Les données archivées se trouvent dans le répertoire: `/home/archives/poetes-st1`

#### Récupération :

On peut réinstaller un site à partir de l'archivage avec la commande:

```
picgs recharge poetes site 2
```

`picgs` demande quelle archive on veut utiliser pour recharger le site. Si l'archivage a été effectué à partir des commandes ci-dessus, `picgs` sera en mesure de récupérer tous les fichiers et de remettre dessus les propriétaires corrects.

**NOTES :**

-  On peut recharger partiellement une archive : soit la base de données, soit les fichiers
-  Si on recharge la base de données, **on écrase totalement l’ancienne**
-  Si on recharge les fichiers, on écrase les fichiers existants s’ils ont le même nom seulement. Il est quand-même plus simple de faire le ménage avant de recharger !

## toctoc

`toctoc` (demander poliment à entrer dans le conteneur) permet d’aller travailler sur les fichiers des utilisateurs, en entrant dans le conteneur `pic-ssh`. C'est un "wrapper" au-dessus de la commande docker exec ou docker cp, qui est plus simple à utiliser pour des personnes non habituées à utiliser docker.

### Entrer:

```
toctoc entrer
```

On se retrouve dans le répertoire  `/home` avec les droits `root`.

```
toctoc entrer poetes-st1
```

On se retrouve dans le répertoire /home/poetes-st1 avec les droits `poetes-st1`: exactement les mêmes conditions qu'une personne connectée par ssh (port 2200) en tant que `poetes-st1` 

### Envoyer un fichier ou un répertoire

La commande suivante envoie un fichier ou un répertoire dans le conteneur ssh:

```
toctoc envoyer mon-repertoire
```

Le répertoire `mon-repertoire` se retrouve dans le conteneur, `/home/mon-repertoire`. Il suffit alors d’entrer  dans le conteneur (cf. ci-dessus) pour le mettre à sa place (ne pas  oublier les `chown` éventuels car par défaut le fichier appartient à root)

### Récupérer un fichier ou un répertoire

```
toctoc recevoir un-repertoire
```

Le répertoire `/home/un-repertoire` se retrouve copié dans le répertoire courant.

**ATTENTION** Ces deux commandes font des copies de fichiers ou de répertoires sur le serveur, ne pas oublier de faire le ménage ensuite !

