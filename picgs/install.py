#! /usr/bin/python3

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# Installing picgs
#
# Usage: PYTHONPATH="lib" ./install.py 
#

import subprocess
import os
import os.path
import shutil

from sysutils import SysUtils
from picconfig import getConf
import version
from affiche import afficheRouge, afficheJaune
sys = SysUtils()

if os.getenv('PICGSDIR') == None:
    print ("Merci de configurer houaibe en éditant le fichier houaibe.conf")
    print ("Ensuite, l'installation de picgs se fait comme ça:")
    print ("sudo ./installpicgs.sh")
    exit(1)

# Pour la prod on fait un zipapp .pyz
# Pour déboguer on fait un .py
ZIPAPP = True

# Pour tester les wrappers se terminent par N
TEST = False

# 3.0.0 => 3.0
vermaj   = f"{version.MAJOR}.{version.MINOR}"

# Calcul du nom de répertoire 
# Toutes les version 3.x.y seront mises par défaut dans le répertoire
# /usr/local/houaire/picgs/3.x
# Cela permet de conserver les anciennes versions lors du passage de 3.x à 3.x1
# Le passage de 3.x.y à 3.x.y1 n'est qu'une correction de patches et ne devrait pas poser de problèmes
PREFIX   = os.getenv('PICGSDIR')
PREFIXPY = PREFIX + '/' + vermaj + '/'
BINPY    = PREFIXPY + 'bin/'
LIB      = PREFIXPY + 'lib/'
LIBPICGSHELP = PREFIXPY + 'lib/picgshelp'
LIBPICWPGSHELP = PREFIXPY + 'lib/picwpgshelp'
LIBPICPKGSHELP = PREFIXPY + 'lib/picpkgshelp'
LIBTOCTOCHELP = PREFIXPY + 'lib/toctochelp'

# On déposera des petits scripts d'appel ("wrappers") dans /usr/local/bin qui est normalement dans le PATH
# Pour les mettre ailleurs que dans /usr/local/bin il suffit de definir la variable d'environnement PREFIXSH
if os.getenv('PREFIXSH') == None:
    PREFIXSH = "/usr/local/"
else:
    PREFIXSH = os.getenv('PREFIXSH')
    
BINSH    = PREFIXSH + 'bin/'

# Les exécutables .py...
BINFILESPY = ['picgs.py',
            'picwpgs.py',
            'picpkgs.py',
            'toctoc.py',
            'picbloc.py',
            'picwpmigrate.py']

# ...les scripts d'appel correspondants, ou des executables bash
BINFILESSH = ['picgs',
              'picwpgs',
              'picpkgs',
              'toctoc',
              'picbloc',
              'health',
              'picscript',
              'picwpmigrate']

if TEST:
    print (afficheJaune(f"EN MODE TEST - LES BINAIRES CREES SERONT: {[ x.removesuffix('.py')+'N' for x in BINFILESPY ]}"))

LIBFILES = ['apache.py',
            'nginx.py',
            'webserver.py',
            'cert.py',
            'compte.py',
            'dns.py',
            'erreur.py',
            'affiche.py',
            'fin.py',
            'wpfin.py',
            'homedir.py',
            'wphomedir.py',
            'mysql.py',
            'ouinon.py',
            'params.py',
            'main.py',
            'version.py',
            'picconfig.py',
            'picgestion.py',
            'picgsldap.py',
            'plugin.py',
            'debut.py',
            'wpdebut.py',
            'sysutils.py',
            'paheko.py'];

LIBPICGSHELPFILES = [ 'help.txt',
                 'info.txt',
                 'collecte.txt',
                 'modification.txt',
                 'ajout.txt',
                 'demenagement.txt',
                 'suppression.txt',
                 'mdp.txt',
                 'desactiver.txt',
                 'reactiver.txt',
                 'inactifs.txt',
                 'ssh.txt',
                 'nossh.txt',
                 'production.txt',
                 'canon.txt',
                 'cert.txt',
                 'https.txt',
                 'nohttps.txt',
                 'archivage.txt',
                 'recharge.txt',
                 'spipinstall.txt',
                 'spipclone.txt',
                 'spipmaj.txt',
                 'mutuajout.txt',
                 'mutusuppr.txt',
                 'mutuarchivage.txt',
                 'muturecharge.txt',
                 'mutuimporte.txt',
                 'mutuexporte.txt',
                 'mutuvidelescaches.txt',
                 'mutuupgrade.txt',
                 'mutuprod.txt',
                 'mutuinfo.txt',
                 'mutumodif.txt',
                 'mutucollecte.txt',
                 'mutusauveplugins.txt',
                 'wpinstall.txt',
                 'wpmaj.txt',
                 'wpclone.txt',
                 'dolibarrmaj.txt',
                 'dolibarrclone.txt',
                 'galettemaj.txt',
                 'galetteclone.txt',
                 'sauvebd.txt',
                 'mutusauvebd.txt',
                 'statique.txt',
                 'nostatique.txt'
                 ]

LIBPICWPGSHELPFILES = [ 'help.txt',
                        'liste_mails.txt',
                        'liste_themes.txt',
                        'liste_refthemes.txt',
                        'liste_core.txt',
                        'liste_plugins.txt',
                        'liste_users.txt',
                        'liste_refplugins.txt',
                        'maj_core.txt',
                        'maj_plugins.txt',
                        'maj_themes.txt'
                      ]

LIBPICPKGSHELPFILES = [ 'help.txt',
                        'liste.txt',
                        'ajout.txt',
                        'suppr.txt',
                        'videlecache.txt'
                      ]

LIBTOCTOCHELPFILES = [ 'help.txt',
                       'entrer.txt',
                       'envoyer.txt',
                       'recevoir.txt',
                       'liste.txt',
                       'restart.txt',
                       'start.txt',
                       'stop.txt',
                       'verif.txt'
                 ]
                 
HELPFILES = {}
HELPFILES [ 'picgs.py'] = LIBPICGSHELPFILES
HELPFILES [ 'picwpgs.py'] = LIBPICWPGSHELPFILES
HELPFILES [ 'picpkgs.py'] = LIBPICPKGSHELPFILES
HELPFILES [ 'toctoc.py'] = LIBTOCTOCHELPFILES

# ===========================================
def makeZipapp(srcdir, dstdir, f):
    ''' Take the {f} file (executable python3 file) from directory f"{srcdir}/bin" 
        Take the *.py module python files from directory f"{srcdir}/lib" 
        Build a .pyz file, install it in dstdir
        We work in /tmp
        '''
    src = f"{srcdir}/bin/{f}"
    obj = f + 'z'
    libdir = f"{srcdir}/lib/"
    hlpdir = f"{srcdir}/lib/{f.partition('.py')[0]}help/"
    
    sys._system(f"rm -rf /tmp/{f} && mkdir -p /tmp/{f}/help")
    src += ' ' + ' '.join([ libdir + l for l in LIBFILES ])
    sys._system(f"cd /tmp && install -o root -m 444 -t {f} {src}")

    if f in HELPFILES:
        hlp = ' '.join([ hlpdir + h for h in HELPFILES[f] ])
        sys._system(f"cd /tmp && install -o root -m 444 -t {f}/help {hlp}")
    
    sys._system(f"cd /tmp/{f} && mv {f} __main__.py")
    sys._system(f"cd /tmp && python3 -m zipapp -p /usr/bin/python3 {f}")
    sys._system(f"cd /tmp && install -o root -m 444 -t {dstdir} {obj}")
    sys._system(f"rm -r /tmp/{f} /tmp/{obj}")   

# === main ===================================

# Create the directories
dir = [PREFIX,PREFIXSH, PREFIXPY, BINSH, BINPY, LIB, LIBPICGSHELP, LIBPICPKGSHELP, LIBPICWPGSHELP, LIBTOCTOCHELP ]

for d in dir:
    try:
        os.mkdir(d)
    except FileExistsError as e:
        pass

# Create picgs, toctoc etc bash wrappers
# Or if there is nothing to wrap, simply copy a bash file
for f in BINFILESSH:
    if TEST:
        fn = BINSH + f + 'N'
    else:
        fn = BINSH + f

    if ZIPAPP:
        fpy = BINPY + f + '.pyz'
        env = ""
    else:
        fpy = BINPY + f + '.py'
        env = f"PYTHONPATH={LIB} "
        
    # If there is not already a wrapper, we create a standard wrapper
    if not os.path.isfile(f"bin/{f}"):
        with open(fn, 'w') as fd:
            fd.write("#! /bin/bash\n")
            fd.write(f". {os.getenv('HOUAIBEDIR')}/etc/houaibe.conf\n")
            fd.write(f"{env}python3 {fpy} \"$@\"\n")
    else:
        shutil.copyfile(f"bin/{f}", fn)
        
    os.chmod(fn,0o555)

if ZIPAPP:
    # Make a zipapp for each executable file
    srcdir = os.getcwd()
    dstdir = BINPY
    for f in BINFILESPY:
        makeZipapp(srcdir, dstdir, f)
        print (f"{f}z créé")
        
    # Préparation de l'installation de picwpmigrate.py dans pic-maint et pic-ssh
    # cf. ../images/maint/Dockerfile
    print ("Copie de picwpmigrate.pyz dans images/ssh pour intégration au conteneur pic-ssh")
    sys._system(f'cp -a {dstdir}/picwpmigrate.pyz ../images/ssh')
    print ('')

else:
    # Install py executable files
    print ("Copie dans " + BINPY)
    src = ' '.join([ 'bin/' + f for f in BINFILESPY ])
    cmd = 'install -o root -m 444 -t ' + BINPY + ' '+ src
    sys._system(cmd)

# lib and help files are always installed, event in ZIPAPP mode
# This can be useful with external scripts using picgs lib
# Install py lib files
print ("Copie dans " + LIB)
src = ' '.join([ 'lib/' + f for f in LIBFILES ])
cmd = 'install -o root -m 444 -t ' + LIB + ' ' + src
sys._system(cmd)

# Install libhelp files
print ("Copie dans " + LIBPICGSHELP)
src = ' '.join([ 'lib/picgshelp/' + f for f in LIBPICGSHELPFILES ])
cmd = 'install -o root -m 444 -t ' + LIBPICGSHELP + ' ' + src
sys._system(cmd)

# Install libhelp files
print ("Copie dans " + LIBPICWPGSHELP)
src = ' '.join([ 'lib/picwpgshelp/' + f for f in LIBPICWPGSHELPFILES ])
cmd = 'install -o root -m 444 -t ' + LIBPICWPGSHELP + ' ' + src
sys._system(cmd)

# Install libhelp files
print ("Copie dans " + LIBPICPKGSHELP)
src = ' '.join([ 'lib/picpkgshelp/' + f for f in LIBPICPKGSHELPFILES ])
cmd = 'install -o root -m 444 -t ' + LIBPICPKGSHELP + ' ' + src
sys._system(cmd)

# Install libhelp files
print ("Copie dans " + LIBTOCTOCHELP)
src = ' '.join([ 'lib/toctochelp/' + f for f in LIBTOCTOCHELPFILES ])
cmd = 'install -o root -m 444 -t ' + LIBTOCTOCHELP + ' ' + src
sys._system(cmd)
print ('')

# Install template files
containers = getConf('DKRSRVWEB')
# Il faut utiliser le chemin sur le host car dans le conteneur c'est read-only !
#dst        = getConf('APACHE_TEMPL')

for c in containers:
    if c == '':
        continue
    dst = f"{getConf('CONTAINERSDIR')}/{c}/sites-available/pic/template"
    print (f"Copie des templates pour {c}")
    src = f"conf/template-{c}"
    if not sys._dkr_fexists(src):
        print (afficheRouge(f"ERREUR - Le template de configuration Apache {src} n'existe pas"))
    else:
        sys._dkr_cpto(src,dst)
        
print ("Thats'all Folks !")
