#! /usr/bin/python3

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

import argparse
import time
import os
import datetime
import ipaddress
import sqlite3

from erreur import PicErreur
from picconfig import getConf
from plugin import SysUtils

#
# Télécharge la blacklist de abuseip.com
# Vérifie qu'elle est bien constituée de 10000 adresses IP (ipv4 ou ipv6)
# Si non:
#       Envoie un mail à l'admin
# Si oui:
#       Met à jour la base de données blocip.sqlite
#

# Jour 0 = 30 Décembre 1899 !
REF_DATE = datetime.datetime(1899,12,30,0,0)

# Max jours = Nombre de jours pour laisser une IP en blocage alors qu'elle n'est plus fournie par AbuseIPdb
MAX_JOURS = 5

def date2Int(date):
    '''Convertit la date depuis un datetime vers un entier'''

    duree = date - REF_DATE
    j     = duree.days  
    
    return j

def int2Date(j):
    '''Convertit la date depuis un entier vers une date'''
    
    duree= datetime.timedelta(days=j)
    date = REF_DATE + duree
    return date
    
class install_blacklist(SysUtils):
    
    def __init__(self, version='ipv4', verbose=False):
        '''Essaie de trouver la clé, envoie une exception si clé pas trouvée, et initialise quelques trucs'''
    
        today          = datetime.datetime.today()
        self.__date    = today.strftime("%Y-%m-%d")
        self.__jour    = date2Int(today)
        self.__verbose = verbose
        
        # On s'occupe d'adresses IPv4 ou IPv6 ?
        if version=='ipv4':
            self.__IPvXAddress = ipaddress.IPv4Address
            self.__table       = 'ipv4'
            self.__ipset_new   = '{}-new'.format(getConf('IPSETNAME'))
            self.__ipset       = '{}'.format(getConf('IPSETNAME'))
            family             = 'inet'
        elif version=='ipv6':
            self.__IPvXAddress = ipaddress.IPv6Address
            self.__table       = 'ipv6'
            self.__ipset_new   = '{}-new'.format(getConf('IP6SETNAME'))
            self.__ipset       = '{}'.format(getConf('IP6SETNAME'))
            family             = 'inet6'
        else:
            raise PicErreur("ERREUR - Version inconnue {}".format(version))

        self.__version     = version
        self.__ipset_file  = '{}/{}.sve'.format(getConf('IPSETBLACKDIR'),self.__ipset)

        # On essaie de récupérer la clé
        f_apikey = getConf('ABUSKEYFILE')
        if f_apikey == None:
            raise PicErreur("ERREUR - LA CLE D'API DE ABUSEIPDB EST INTROUVABLE")
            
        with open(getConf('ABUSKEYFILE')) as file:
            apikey = file.readline()
        
        self.__apikey   = apikey.strip()

        # Le nom du fichier blacklist qui sera téléchargé de AbuseIpDb
        self.__blacknew = '{}/blacklist.txt.{}'.format(getConf('IPSETBLACKDIR'),self.__date)
        
        # Le nom du fichier blacklist.local
        self.__blacklcl = '{}/blacklist.local'.format(getConf('IPSETBLACKDIR'))

        # Création des tables de la base de donnée, si nécessaire
        try:
            self.__buildDatabase()
        except:
            pass

        # Création des deux ipsets, si nécessaire
        try:
            cmd = 'ipset create {} hash:ip family {}'.format(self.__ipset_new, family)
            self._system(cmd)
        except:
            pass
            
        try:
            cmd = 'ipset create {} hash:ip family {}'.format(self.__ipset, family)
            self._system(cmd)
        except:
            pass

    def getBlacklist(self):
        '''Télécharge si besoin la blacklist de abuseip.com dans self.__blacknew
           et appelle buildNewset()'''
    
        key        = self.__apikey
        apiurl     = getConf('ABUSAPIURL')
        confidence = getConf('ABUSCONFID')
        IPvXAddress= self.__IPvXAddress

        if not os.path.exists(self.__blacknew):
            if self.__verbose:
                print("=== Téléchargement de la liste noirefournie par AbuseIpDb...")
                
            cmd = 'curl -G {0} -d confidenceMinimum={1} -H "Key: {2} " -H "Accept: text/plain" > {3}'.format(apiurl,confidence,key,self.__blacknew)
            self._system(cmd)
        
        return self.buildNewset()
        
    def buildNewset(self):
        '''Construit le set self.__newset à partir du fichier self.__blacknew
           Renvoie le nombre d'adresses lues
        '''

        IPvXAddress = self.__IPvXAddress

        # Le fichier d'AbuseIpDb
        if self.__verbose:
            print("=== Analyse de la liste noire")
        
        with open(self.__blacknew, 'r') as file:
            lines = file.read().split("\n")
        
        # Le fichier local
        if os.path.exists(self.__blacklcl):
            if self.__verbose:
                print("=== Prise en compte de la liste noire locale")
            with open(self.__blacklcl,'r') as file:
                lcllines = file.read().split("\n")

            lines += lcllines    

        if self.__verbose:
            print("=== Filtrage des IP, on ne garde que les adresses {}".format(self.__version))

        cnt = 0    
        newset = set()
        for l in lines:
            try:
                l = l.strip()
                ip = IPvXAddress(l)
                newset.add(str(ip))
                cnt += 1
            except:
                continue
        
        self.__newset = newset
        
        return cnt

    def checkBlacklist(self):
        '''Vérifie self.__newset: N lignes constituées chacune de 1 adresse ip (v4 ou v6)
           Renvoie True ou False
        '''
        
        if self.__verbose:
            print ("=== Vérification des données")
            
        for ip in self.__newset:
            ip = ip.strip()
            if ip == "":
                print ("Houps,une ligne vide. Terminé à la ligne {0}".format(cnt))
                return False
            
            try:
                ipaddress.ip_address(ip)
                
            except:
                print ("{0} n'est pas une adresse ip".format(ip))
                return False
            
    def getOldBlacklist(self):
        '''Lit la BD et construit le set self.__oldset'''
        
        ipsetdb = getConf('IPSETDB')
        conn     = sqlite3.connect(ipsetdb)
        c        = conn.cursor()
        table    = self.__table

        if self.__verbose:
            print ("=== Lecture de la base de données actuelle...")

        # On lit la table si elle existe, on laisse oldset vide sinon
        oldset = set()
        try:
            for row in c.execute('SELECT addr FROM {}'.format(table)):
                try:
                    ip = row[0]
                    ip.strip()
                    oldset.add(ip)
                except:
                    continue
        except:
            pass
            
        self.__oldset = oldset

        
    def __buildDatabase(self):
        '''Si le fichier sqlite n'existe pas, on le crée et on contruit le schéma'''
        
        ipsetdb = getConf('IPSETDB')
        conn     = sqlite3.connect(ipsetdb)
        c        = conn.cursor()
        c.execute('''CREATE TABLE ipv4 (date text, jour integer, addr text)''')
        c.execute('''CREATE TABLE ipv6 (date text, jour integer, addr text)''')
        conn.commit()
        conn.close()

    def fillDatabase(self):
        '''Ajoute les adresses de self.__newset - self.__oldset
           Actualise la date des adresses de self.__newset inter self.__oldset
           Renvoie le nombre d'adresses ajoutées et actualisées
        '''
        
        ipsetdb = getConf('IPSETDB')
        
        if self.__verbose:
            print ("=== Ajout des nouvelles IP dans la base de données, et actualisation des anciennes...")

        conn = sqlite3.connect(ipsetdb)
        c    = conn.cursor()
        
        newset = self.__newset
        oldset = self.__oldset
        to_add = newset.difference(oldset)
        to_actu= newset.intersection(oldset)
        date = self.__date
        jour = self.__jour
        table= self.__table

        tuples = []
        for a in to_add:
            tuples.append( (date, jour, a) )
        
        c.executemany('INSERT INTO {} VALUES (?,?,?)'.format(table), tuples)
        conn.commit()
        
        cnt_add = len(tuples)

        # Pour les adresses toujours bannies on actualise seulement le jour
        # Sera utilisé pour le ménage
        # La date est la date à laquelle l'IP a été bannie
        cnt_actu = 0
        for a in to_actu:
            c.execute('UPDATE {} SET jour=? WHERE addr=?'.format(table),(jour,a))
            cnt_actu += 1
        conn.commit()
        
        conn.close()
        
        return (cnt_add, cnt_actu)

    def cleanDatabase(self):
        '''Retire les enregistrements non actualisés depuis plus de MAX_JOURS jours'''
        
        ipsetdb = getConf('IPSETDB')
        if not os.path.exists(ipsetdb):
            return

        conn = sqlite3.connect(ipsetdb)
        c    = conn.cursor()
        table= self.__table
        min_jour = self.__jour - MAX_JOURS
        
        if self.__verbose:
            print ("=== Suppression des ip non signalées depuis au moins {} jours".format(MAX_JOURS))
            
        c.execute('SELECT count(*) FROM {}'.format(table))
        (before,) = c.fetchone()
        
        c.execute('DELETE FROM {} WHERE jour < ?'.format(table), (min_jour,))
        conn.commit()
        
        c.execute('SELECT count(*) FROM {}'.format(table))
        (after,) = c.fetchone()

        return (after - before, after)
        
    def buildIpset(self):
        '''Construit et sauvegarde la base Ipset'''
        
        conn = sqlite3.connect(getConf('IPSETDB'))
        c    = conn.cursor()
        table      = self.__table
        ipset_new  = self.__ipset_new
        ipset      = self.__ipset
        ipset_file = self.__ipset_file

        if self.__verbose:
            print ("=== Construction de l'ipset")
            
        # Vidage du ipset_new existant
        cmd = "ipset flush {}".format(ipset_new)
        self._system(cmd)
        
        # Remplissage du ipset_new
        for row in c.execute('SELECT addr FROM {}'.format(table)):
            try:
                adr = row[0]
                adr.strip()
                cmd = "ipset add {} {} -exist".format(ipset_new,adr)
                self._system(cmd)

            except:
                continue

        #
        # Swap des ipset/ipset_new
        # 
        cmd = 'ipset swap {} {}'.format(ipset,ipset_new)
        self._system(cmd)
        
        #
        # Sauvegarde du ipset
        #
        cmd = 'ipset save {} -file {}'.format(ipset,ipset_file)
        self._system(cmd)
        
        #
        # Suppression du ipset_new
        #
        cmd = 'ipset destroy {}'.format(ipset_new)
        self._system(cmd)
        
class Stats(object):
    
    def __init__(self,verbose=False):

        today          = datetime.datetime.today()
        self.__date    = today.strftime("%Y-%m-%d")
        self.__jour    = date2Int(today)
        self.__verbose = verbose
        
        ipsetdb = getConf('IPSETDB')
        if not os.path.exists(ipsetdb):
            return

        conn     = sqlite3.connect(ipsetdb)
        c        = conn.cursor()

        # On lit les tables 'ipv4' et 'ipv6'
        # On compte le nombre de fois que chaque data apparaît
        jours = {}
        for table in ['ipv4','ipv6']:
            try:
                for row in c.execute('SELECT jour FROM {}'.format(table)):
                    j = row[0]
                    if not j in jours:
                        jours[j] = 0
                    jours[j] += 1
            except:
                continue 

        self.__jours = jours

        # ips = {}
        # for table in ['ipv4','ipv6']:
            # for row in c.execute('SELECT addr FROM {}'.format(table)):
                # ip = row[0]
                # if not ip in ips:
                    # ips[ip] = 0
                # ips[ip] += 1
        
        # for ip in ips:
            # print ('{}   {}'.format(ip,ips[ip]))
            
    def printStats(self):
        
        jours = self.__jours
        total = 0;
        for j in sorted(jours):
            d = int2Date(j).strftime("%Y-%m-%d")
            total += jours[j]
            print ("{0:10s}{1:10d}".format(d, jours[j]))
            
        print (20*'=')
        print ("{0:10s}{1:10d}".format("TOTAL",total))

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description="picbloc - Construit deux ipset pour bloquer les IP envoyées par AbuseIpDb, ou détectées localement")
    parser.add_argument('-v', action="store_true", default = False, dest="verbose", help="Affiche plusieurs commentaires de déboguage")
    parser.add_argument('-s', action="store_true", default = False, dest="stats", help="Affiche quelques stats à propos de la base de données")
    options = parser.parse_args()

    today = datetime.datetime.today().strftime("%Y-%m-%d")
    
    if options.stats:
        s = Stats()
        s.printStats()
    
    else:
        for v in ['ipv4','ipv6']:
            ib  = install_blacklist(v, options.verbose)
            got = ib.getBlacklist()
            ok  = ib.checkBlacklist()
            if ok == False:
                print ('{} - checkBlackList renvoie False !'.format(today))
                exit(1)
            ib.getOldBlacklist()
            (added,kept) = ib.fillDatabase()
            (removed,remaining) = ib.cleanDatabase()
            ib.buildIpset()
            print ( '{} - {} - {} adresses dans la liste noire (dont {} nouvelles et {} déjà connues), {} supprimées de la base et {} conservées'.format(today,v,got,added,kept,removed,remaining))
    
