#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

import sys
import re
import time
import socket
import subprocess

#from wppicgestion import WpPicGestion
from main import LoopAlgo, StdAlgo
from picgestion import PicGestion
from erreur import PicErreur
from plugin import *
from params import *
from affiche import *


LOOPALGO = LoopAlgo()
STDALGO = StdAlgo()

PLUGINS = [ 'Wpdebut','Compte','WpHomeDir','WpFin' ]
ACTIONS = { 'liste_mails': (LOOPALGO, PLUGINS),
            'liste_core': (LOOPALGO, PLUGINS),
            'liste_plugins': (LOOPALGO, PLUGINS),
            'liste_refplugins': (STDALGO, ['WpFin']),
            'liste_themes': (LOOPALGO, PLUGINS),
            'liste_refthemes': (STDALGO, ['WpFin']),
            'liste_users': (LOOPALGO, PLUGINS),
            'maj_core': (LOOPALGO, PLUGINS),
            'maj_plugins': (LOOPALGO, PLUGINS),
            'maj_themes': (LOOPALGO, PLUGINS)
          }

#
# picwpgs = PIC Gestion Systeme
#           Mise à jour de tous les sites wordpress hébergés (core ou plugins)
#
erreur = False
try:
    
    # PROGRAMME PRINCIPAL
    prms = Params('picwpgs', sys.argv, action_nom=True)

#    import pprint
#    print(prms.action)
#    print(prms.nom)
#    print (prms.fonction)
#    print(prms.version)
#    pprint.pprint(prms.options)
    
    (ALGO, PLUGS) = ACTIONS[prms.action]
    ALGO(prms, PLUGS)
    
except KeyboardInterrupt:
    print (afficheJaune("\n\n===> Action interrompue par un CTRL-C"))

except PicErreur as x:
    erreur = True
    print(afficheRouge(x))

except Exception as x:
    erreur = True
    print(x)
    if 'PICGS_DEBUG' in os.environ:
        traceback.print_exception(x)
        
if erreur:
    sys.exit(1)
