#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

# Migration d'un site wordpress depuis un autre hébergerment sur le PIC
#
# Prérequis:
#   La base de données du site original (fichier .sql, .sql.zip ou .sql.gz) se trouve dans le répertoire priv
#   Les fichiers du site oiginal se trouvent dans le répertoire www
#   Les identifiants de connexion se trouvent dans priv (fichier ident.json) ils ont été créés par picgs ajout
#
# Ce qui est fait:
#   1/ Poser les permissions correctes sur les sous-répertoires de www
#   2/ Régénération du fichier wp-config.php en utilisant les données de ident.json
#   3/ Chargement de la base de données sql (ATTENTION ! L'ancienne B.D. est donc supprimée sans préavis)
#   4/ Installation des plugins ssh-sftp-updater et easy-update-manager
#   5/ Ajout du compte picadmin
#   6/ Changement du paramètre siteurl afin de le mettre sur l'adresse technique, et modif des permaliens
#
# Utilise certains modules de picgs

import sys
import re
import time
import socket
import os
import pwd
import subprocess
import traceback

import json

from params import *
from picgestion import PicGestion
from erreur import PicErreur
from ouinon import ouiNon
from picconfig import getConf
from plugin import *
from main import *
from sysutils import SysUtils
from affiche import *

from homedir import *
from mysql import *

class MysqlDefaut(Mysql):
    def execute(self):
        pg = self.picgestion
        user = pg.user
        domain = getConf('DOMAINNAME')

        # Mise à jour de siteurl
        print ("=== Mise à jour de siteurl")
        dst_siteurl = f"https://{user}.{domain}"
        self._siteUrlPourWp(dst_siteurl)

        
class HomeDirDefault(HomeDirInstallation):

    def __isInstalled(self, wpplugin):
        '''renvoie True si le plugin wordpress est déja installé'''

        pg = self.picgestion
        www = pg.www
        user = pg.user
        conteneur = getConf('DKRMAINT')
        
        cmd = f"cd {www} && wp plugin list --name={wpplugin} --format=json"
        out = self._system(cmd, conteneur,False,user)
        if len(out) > 1:
            import pprint
            print(afficheRouge(f"ATTENTION - La commande:\n{cmd}\nretourne un truc pas clair (erreur ou warning php)\n"))
            print(afficheRouge(pprint.pformat(out)))
            print(afficheRouge("\nCorrigez et recommencez\n"))
            raise Exception

        jsout = json.loads(out[0])
        if len(jsout) > 0:
            return True
        else:
            return False
        
    def __wpConfig(self, idt):
        '''Modification du fichier wpConfig(), les clés-valeurs sont dans le dictionnaire idt'''
        
        pg = self.picgestion
        priv = pg.priv
        www = pg.www
        conteneur = getConf('DKRMAINT')
        user = pg.user
        
        # Modifier les paramètres de wp-config.php
        for v in idt:
            try:
                cmd = f"cd {www} && wp config set {v} {idt[v]}"
                self._system(cmd, conteneur, False, user)
            except PicErreur:
                cmd += " --anchor=EOF && echo >> wp-config.php"
                self._system(cmd, conteneur, False, user)                
   
    def __validerPrerequis(self):
        '''Valide l'existence du fichier priv/ident.json, priv/*.sql (sql.gz ou sql.zip), et www/wp-config.php
           Lance une PicErreur si non validé
           Renvoie le nom du fichier sql si tout est validé'''
        
        pg = self.picgestion
        priv = pg.priv
        www = pg.www
        conteneur = getConf('DKRMAINT')
        domaine = getConf('DOMAINNAME')
        user = pg.user

        if domaine == None:
            raise PicErreur (f"ERREUR - La variable d'env DOMAINNAME n'est pas définie")
            
        if not self._dkr_fexists(f"{priv}/ident.json", conteneur):
            raise PicErreur (f"ERREUR - Le fichier priv/ident.json n'existe pas")

        if not self._dkr_fexists(f"{www}/wp-config.php", conteneur):
            raise PicErreur (f"ERREUR - Le fichier www/wp-config.php n'existe pas")

        try:
            cmd = f"cd {priv} && ( ls *.sql || ls *.sql.gz || ls *.sql.zip ) 2>/dev/null"
            fsql = self._system(cmd, conteneur, False, user)
            
        except PicErreur:
            raise PicErreur("Fichier priv/*.sql, priv/*.sql.zip ou priv/*.sql.gz manquant")

        print ("=== Prérequis validés: var $DOMAINNAME, fichiers priv/ident.json, priv/*.sql, www/wp-config.php trouvés")
        
        if len(fsql) == 0:
            return ""
        else:
            return fsql[0]

    def __rechargeBd(self, fsql):
        ''' Recharge la BD à partir du fichier sql'''

        pg = self.picgestion
        www = pg.www
        priv = pg.priv
        user = pg.user
        db = pg.database
        conteneur = getConf('DKRMAINT')

        if fsql.endswith('.gz'):
            self._system(f"cd {priv} && gunzip {fsql}", conteneur, False, user )
            fsql = fsql.partition('.gz')[0]
            
        if fsql.endswith('.zip'):
            self._system(f"cd {priv} && unzip {fsql}", conteneur, False, user )
            fsql = fsql.partition('.zip')[0]

        print(f"=== Importation de la base de donnees à partir du fichier priv/{fsql}")
        fsql = f"{priv}/{fsql}"
        self._system(f"cd {www} && wp db --yes drop", conteneur, False, user)
        self._system(f"cd {www} && wp db create", conteneur, False, user)
        self._system(f"cd {www} && wp db import {fsql}", conteneur, False, user)

    def execute(self):

        pg = self.picgestion
        www = pg.www
        priv = pg.priv
        user = pg.user
        conteneur = getConf('DKRMAINT')
        domain = getConf('DOMAINNAME')

        # 0.1/ Valider les prérequis
        try:
            fsql = self.__validerPrerequis()
            
        except PicErreur as e:
            print (afficheRouge(f"ERREUR - Prérequis non vérifiés:\n{e.message}"))
            raise e

        # 0.2/Lire le fichier ident.json
        cmd = f"cd {priv} && cat ident.json"
        idt = json.loads(self._system(cmd, conteneur)[0])
            
        # 1/ Poser les permissions correctes sur les sous-répertoires de www
        print("=== Permissions du répertoire www")
        try:
            if not os.getenv('DOCKER_RUNNING'):
                self._system(f"chown -R {user}:{user} {www}", conteneur)
            self._chmodForWordpress()
        except Exception:
            pass

        # 2/ Régénération du fichier wp-config.php en utilisant les données de ident.json
        print("=== Modification de www/wp-config.php à partir de priv/ident.json")
        self.__wpConfig(idt)

        # 3/ Recharger la B.D.
        self.__rechargeBd(fsql)

        # 4/ Installer et configurer trois plugins utiles pour notre hébergement
        print("=== Installation de ssh-sftp-updater-support, stops-core-theme-and-plugin-updates et cache-control")
        self._pluginsForWordpress(['ssh-sftp-updater-support', 'stops-core-theme-and-plugin-updates','cache-control'])
        self._configEasyUpdateManager()
        
        # Pour l'instant on désactive le plugin update pour éviter l'envoi de mails intempestifs
        print("=== Désactivation de stops-core-theme-and-plugin-updates pour éviter l'envoi de mails ")
        print(afficheJaune("    PENSEZ A LE REACTIVER LORSQUE L'INSTALLATION DE WORDPRESS SERA TERMINEE"))
        self._system(f"cd {www} && wp plugin deactivate stops-core-theme-and-plugin-updates", conteneur, False, user)
        
        # 5/ Ajout du compte picadmin (si pas déjà là))
        try:
            self._system(f"cd {www} && wp user create {getConf('ADMIN')} {getConf('ADMINMAIL')} --role=administrator", conteneur, False, user)
            print("=== Ajout d'un compte picadmin")
            
        except PicErreur:
            pass
                    
# PROGRAMME PRINCIPAL
erreur = False
try:
    helpMsg = "\npicwpmigrate monasso site 1\n             Sur le host\n\npicwpmigrate\n             Sur pic-ssh\n\n             termine l'installation d'un site wordpress si les prérequis sont satisfaits"
    prms = Params('picwpmigrate', sys.argv, prod=True, default=True, helpMsg=helpMsg)
    fonction = prms.fonction
    version = prms.version
    
    if fonction != 'site':
        raise PicFatalErreur("Cette commande ne s'applique qu'aux sites web" )

    if version == None:
        raise PicFatalErreur("Il faut spécifier le numéro de version !")
    
    algo = StdAlgo()
    plugins = ['Debut','Compte',HomeDirDefault,MysqlDefaut,'Fin']
    algo(prms, plugins)

except KeyboardInterrupt:
    print (afficheJaune("\n\n===> Action interrompue par un CTRL-C"))

except PicErreur as x:
    erreur = True
    print(afficheRouge(x))

except Exception as x:
    erreur = True
    print(x)
    if 'PICGS_DEBUG' in os.environ:
        import traceback
        traceback.print_exception(x)

if erreur:
    sys.exit(1)
