#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

import sys
import re
import time
import socket
import subprocess

from main import NVStdAlgo
from picgestion import PicGestion
from erreur import PicErreur
from plugin import *
from params import *
from affiche import *

#
# picpkgs = PIC, Paheko Gestion Systeme
#

ACTIONS = [ 'liste', 'ajout', 'suppr', 'videlecache' ]
PLUGINS = [ 'Paheko' ]

erreur = False
try:
    
    # PROGRAMME PRINCIPAL
    Params.setLmn(64)
    prms = Params('picpkgs',sys.argv)
    action = prms.action
    nom = prms.nom
    fonction = prms.fonction
    version = prms.version
    options = prms.options
    #print (action)
    #print (nom)
    #print (fonction)
    #print (version)
    #print (options)
    #print ()

    algo = NVStdAlgo()
    algo(prms, PLUGINS)

except KeyboardInterrupt:
    print (afficheJaune("\n\n===> Action interrompue par un CTRL-C"))

except PicErreur as x:
    erreur = True
    print(afficheRouge(x))

except Exception as x:
    erreur = True
    print(x)
    if 'PICGS_DEBUG' in os.environ:
        import traceback
        traceback.print_exception(x)
