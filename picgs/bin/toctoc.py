#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

import sys
import re
import os.path
import subprocess

from erreur import PicErreur
from ouinon import ouiNon
from picconfig import getConf
from sysutils import SysUtils
from params import *
from picgsldap import *
from affiche import *

import version

#
# TOCTOC = Travailler sur les conteneurs
#          toctoc qui est la !
#

sysu = SysUtils()

def isRunning(conteneur):
    '''Renvoie True si le conteneur passé en paramètres est en cours d'exécution
       Renvoie False si le conteneur n'existe pas ou s'il est arrêté'''
       
    cmd = f'docker ps --filter=name=^{conteneur}$ --format={{.Names}}'
    rvl = sysu._system(cmd)
    if len(rvl) == 2:
        return True
    else:
        return False
 
def run_stop_cont(conteneur,stop=False,cli='',dryrun=False):
    '''Demarre ou arrete un conteneur a l'aide du script run.sh ou stop.sh'''
    
    stdout = None
    cont_dir = f"{getConf('CONTAINERSDIR')}/{conteneur}"
    if not sysu._dkr_fexists(cont_dir):
        raise PicErreur(f"ERREUR - Le conteneur {conteneur} est inconnu")

    if stop:
        print (afficheVert(f"Arrêt de {conteneur}"))
        if not dryrun:
            cmd = f"{cont_dir}/stop.sh"
            if not sysu._dkr_fexists(cmd):
                raise PicErreur(f"ERREUR - {conteneur} ne peut pas être arrêté de cette manière")
            sysu._system(cmd, None, True)

    else:
        print (afficheVert(f"Redémarrage de {conteneur}"))
        if not dryrun:
            cmd = f"{cont_dir}/run.sh"
            if not sysu._dkr_fexists(cmd):
                raise PicErreur(f"ERREUR - {conteneur} ne peut pas être démarré de cette manière")
            sysu._system(f"{cmd} {cli}", None, True)

def run_stop_image(image, stop=False, cli='',dryrun=False):
    '''Démarre ou arrête tous les conteneurs ayant une image donnée '''
    
    # On recherche les images de tous les utilisateurs
    utils = chercheUtilisateurs(None, None, None, True)
    
    conteneurs = []
    for u in utils:
        gecos = u[3]
        ci = gecos.split(',')[1]
        cia = ci.split('#') # Conteneur et Image dans un Array
        if len(cia) > 1:
            c = cia[0]
            i = cia[1]
            if i == image:
                conteneurs.append(c)
    
    for c in conteneurs:
        if c != "":
            run_stop_cont(c, stop, cli, dryrun)
            print()

def run_stop(conteneur, stop=False, cli='', dryrun=False):
    '''Appelle run_stop_cont ou run_stop_image'''

    images = getConf ('DKRSRVIMG')
    
    if conteneur in images:
        try:
            run_stop_image(conteneur, stop, cli, dryrun)
        except PicErreur as e:
            print (afficheRouge(f"L'un des conteneurs de type {conteneur} n'existe pas ou n'a pas réussi à démarrer/s'arrêter !"))
        
    else:
        try:
            # enlever les suffixes s'ils existent pour arrêter/démarrer l'ensemble des deux conteneurs
            conteneur = conteneur.removesuffix('-nginx-1').removesuffix('-php-1')
            run_stop_cont(conteneur, stop, cli, dryrun)
        except PicErreur as e:
            print (afficheRouge(f"Le conteneur {conteneur} n'existe pas ou n'a pas réussi à démarrer/s'arrêter !"))        

def recevoir(src):
    conteneur = getConf('DKRMAINT')
    src = src.rstrip('/')
    dst = os.path.basename(src)
    while os.path.exists(dst):
        dst += '.n'
        
    print("Reception du fichier " + conteneur + ":" + src + " vers " + dst)
    sysu._dkr_cpfrom(src,dst,conteneur)
    
def envoyer(src):
    conteneur = getConf('DKRMAINT')
    dst = f"/home/{os.path.basename(src.rstrip('/'))}"
    while sysu._dkr_fexists(dst,conteneur):
        dst += '.n'
        
    print(f"Envoi du fichier {src} vers {conteneur}:{dst}")
    sysu._dkr_cpto(src,dst,conteneur)

def entrerSsh(user=None):
    '''Entrer dans le conteneur de maintenance en tant que user, ou root si user=None'''

    conteneur = getConf('DKRMAINT')
    
    if user != None:
        home = f'/home/{user}'
    else:
        home = '/home'
        user = 'root'

    msg = f'"{user}@{conteneur}:{home} - exit ou CTRL-D pour sortir"'
    cmd = f'cd {home} && echo {msg} && sh -c "bash; exit 0"'
    sysu._system(cmd,conteneur,True,user)

def entrerCont(cont):
    '''Entrer dans le conteneur cont en tant que root. Essaie d'appeler bash, puis sh'''

    conteneur = cont
    msg = f'"\$(id -nu 2>/dev/null)@{conteneur}:\$(pwd) exit ou CTRL-D pour sortir"'
    cmd = f'echo {msg} && if which bash >/dev/null; then bash; else sh; fi; exit 0'
    sysu._system(cmd,conteneur,True,None,[],None,'sh')

def entrer (nom, version):
    '''Détermine le conteneur à utiliser et Entre dedans '''

    ok = False
    
    # Si version != None, ça ressemble à un user, on essaie de rentrer en tant que ce user
    if version != None:
        user = str(nom) + str(version)
        try:
            entrerSsh(user)
            ok = True
        except Exception:
            if nom != None:
                nom += str(version)
        
    # Si pas de paramètres du tout, on rentre en tant que root
    if not ok and nom == None:

        try:
            entrerSsh()
            ok = True
        except Exception:
            pass

    # Sinon c'est peut-être un conteneur ?
    if not ok:
        
        # Si le conteneur est up, on entre
        if isRunning(nom):
            entrerCont(nom)

        # Sinon, on essaie d'ajouter -nginx-1, de fois que ce serait nginx-fpmXX
        else:
            nom_cont = nom + '-nginx-1'
            if isRunning(nom_cont):
                entrerCont(nom_cont)

            else:
                # On laisse tomber !
                raise PicErreur(f"ERREUR - {nom} n'est ni un utilisateur, ni un conteneur")

def lister():
    '''Faire la liste des conteneurs pouvant etre arrêtés ou démarrés avec toctoc'''
    
    cmd = 'ls ' + getConf('CONTAINERSDIR')
    rep = sysu._system(cmd)

    print ()
    print ("Conteneurs pouvant être redémarrés par toctoc restart:")
    print ("=====================================================")
    for r in rep:
        run = f"{getConf('CONTAINERSDIR')}/{r}/run.sh"
        if sysu._dkr_fexists(run):
            print(r)
    
    print ()
    print ("Conteneurs pouvant être arrêtés par toctoc stop:")
    print ("===============================================")
    for r in rep:
        run = f"{getConf('CONTAINERSDIR')}/{r}/stop.sh"
        if sysu._dkr_fexists(run):
            print(r)
            
def verifier():
    
    # On recherche les images de tous les utilisateurs
    utils = chercheUtilisateurs(None, None, None, True)

    # On ne garde que les conteneurs pour lesquels une image est spécifiée
    # A condition que cette image soit dans dkrsrvimg
    dkrsrvimg = getConf('DKRSRVIMG')
    i_conteneurs = set()
    c_utils = {}
    for u in utils:
        user = u[0]
        gecos = u[3]
        ci = gecos.split(',')[1]
        cia = ci.split('#') # Conteneur et Image dans un Array
        if len(cia) > 1:
            c = cia[0]
            i = cia[1]
            if i in dkrsrvimg:
                i_conteneurs.add(c)
                c_utils[c] = u
    
    # On regarde les conteneurs up
    cmd = "docker ps --format='{{ .Names}}'"
    rvl = sysu._system(cmd)
    r_conteneurs = set([ c.removesuffix('-nginx-1').removesuffix('-php-1') for c in rvl if c != ''])
    
    # On fait la diff entre les conteneurs et les conteneurs qui tournent
    d_conteneurs = i_conteneurs.difference(r_conteneurs)
    
    # On vérifie que les conteneurs qui ne tournent pas sont bien désactivés
    containersdir = getConf('CONTAINERSDIR')
    a_conteneurs = set()
    for c in d_conteneurs:
        fich_inac = f"{containersdir}/{c}/DESACTIVE"
        if not sysu._dkr_fexists(fich_inac):
            a_conteneurs.add(c)

    if len(a_conteneurs) > 0:
        print ("Les conteneurs suivants devraient être RUNNING")
        for c in a_conteneurs:
            gecos = c_utils[c][3]
            img = gecos.split(',')[1].split('#')[1]
            print(f"{img} --> {c}")
    
    return

# PROGRAMME PRINCIPAL
try:
    try:
        prms = Params('toctoc',sys.argv)
        (action,nom,fonction,version,options) = prms.getData()
    except PicErreur as e:
        sys.exit(1)

    #print (action)
    #print (nom)
    #print (fonction)
    #print (version)

    if '--dryrun' in options:
        dryrun = True
    else:
        dryrun = False

    if action == 'entrer':
        entrer (nom, version)

    elif action == 'restart' or action == 'start':
        cli = ''
        if len(sys.argv) > 3:
            cli = sys.argv[3]
        run_stop(sys.argv[2], False, cli, dryrun)

    elif action == 'stop':
        run_stop(sys.argv[2], True, '', dryrun)

    elif action == 'recevoir':
        recevoir(sys.argv[2])

    elif action == 'envoyer':
        envoyer(sys.argv[2])
        
    elif action == 'verif':
        verifier()

    elif action == 'liste':
        lister()

    else:
        print("Pas encore fait !")
    
except KeyboardInterrupt:
    print (afficheJaune("\n\n===> Action interrompue par un CTRL-C"))
    
except PicErreur as x:
    print(afficheRouge(x.message))
    sys.exit(1)

