#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

import sys
import re
import time
import socket
import subprocess
import traceback

from picgestion import PicGestion
from erreur import PicErreur
from ouinon import ouiNon
from picconfig import getConf
from plugin import *
from params import *
from sysutils import SysUtils
from affiche import *
from main import *

# L'ordre des plugins est important car il pilote l'ordre des questions posees a l'utilisateur !
# Il peut aussi exister des dépendances entre plugins
# Actions commençant par mutu
MUTU_PLUGINS = ['Debut','Apache','Compte','HomeDir','Mysql','Cert','Fin']

# Actions de clonage, aussi production, statique et nostatique
CLONE_PLUGINS = ['Debut','Apache','Nginx','Compte','HomeDir','Mysql','Fin']

# Autres actions
PLUGINS      = ['Debut', 'Compte', 'Mysql', 'HomeDir', 'Apache', 'Nginx', 'Cert', 'Fin']

STD_ALGO = StdAlgo()
AJSTD_ALGO = AjStdAlgo()
LOOP_ALGO = LoopAlgo()
MUTU_ALGO = StdMutuAlgo()
LOOP_MUTU_ALGO = LoopMutuAlgo()

# actions possibles, reliées au tuple (ALGO, PLUGINS)
ACTIONS =   {
            'ajout':            (AJSTD_ALGO, PLUGINS),
            'adduser':          (STD_ALGO, PLUGINS),
            'deluser':          (STD_ALGO, PLUGINS),
            'modification':     (STD_ALGO, PLUGINS),
            'demenagement':     (STD_ALGO, PLUGINS),
            'mdp':              (STD_ALGO, PLUGINS),
            'desactiver':       (STD_ALGO, PLUGINS),
            'reactiver':        (STD_ALGO, PLUGINS),
            'inactifs':         (STD_ALGO, PLUGINS),
            'info':             (LOOP_ALGO, PLUGINS),
            'suppression':      (STD_ALGO, PLUGINS),
            'collecte':         (LOOP_ALGO, PLUGINS),
            'ssh':              (STD_ALGO, PLUGINS),
            'nossh':            (STD_ALGO, PLUGINS),
            'cert':             (STD_ALGO, PLUGINS),
            'https':            (STD_ALGO, PLUGINS),
            'nohttps':          (STD_ALGO, PLUGINS),
            'production':       (STD_ALGO, CLONE_PLUGINS),
            'canon':            (STD_ALGO, PLUGINS),
            'archivage':        (STD_ALGO, PLUGINS),
            'recharge':         (STD_ALGO, PLUGINS),
            'spipmaj':          (STD_ALGO, PLUGINS),
            'spipinstall':      (STD_ALGO, PLUGINS),
            'spipclone':        (STD_ALGO, CLONE_PLUGINS),
            'mutuajout':        (MUTU_ALGO, MUTU_PLUGINS),
            'mutusuppr':        (MUTU_ALGO, MUTU_PLUGINS),
            'mutuarchivage':    (MUTU_ALGO, MUTU_PLUGINS),
            'muturecharge':     (MUTU_ALGO, MUTU_PLUGINS),
            'mutuvidelescaches':(MUTU_ALGO, MUTU_PLUGINS),
            'mutuimporte':      (MUTU_ALGO, MUTU_PLUGINS),
            'mutuexporte':      (MUTU_ALGO, MUTU_PLUGINS),
            'mutuprod':         (MUTU_ALGO, MUTU_PLUGINS),
            'mutuupgrade':      (MUTU_ALGO, MUTU_PLUGINS),
            'mutuinfo':         (MUTU_ALGO, MUTU_PLUGINS),
            'mutumodif':        (MUTU_ALGO, MUTU_PLUGINS),
            'mutucollecte':     (LOOP_MUTU_ALGO, MUTU_PLUGINS),
            'mutusauveplugins': (MUTU_ALGO, MUTU_PLUGINS),
            'sauvebd':          (LOOP_ALGO, MUTU_PLUGINS),
            'mutusauvebd':      (MUTU_ALGO, MUTU_PLUGINS),
            'wpinstall':        (STD_ALGO, MUTU_PLUGINS),
            'wpclone':          (STD_ALGO, CLONE_PLUGINS),
            'wpmaj':            (STD_ALGO, PLUGINS),
            'galettemaj':       (STD_ALGO, PLUGINS),
            'galetteclone':     (STD_ALGO, CLONE_PLUGINS),
            'dolibarrmaj':      (STD_ALGO, PLUGINS),
            'dolibarrclone':    (STD_ALGO, CLONE_PLUGINS),
            'statique':         (STD_ALGO, CLONE_PLUGINS),
            'nostatique':       (STD_ALGO, CLONE_PLUGINS)
            }
#
# picgs = PIC Gestion Systeme
#         Ouverture, modification, suppression de comptes pour les membres du pic
#

# VARIABLES D'ENVIRONNEMENT
# ===========================

# PICGS_DEBUG = 1
#               Afficher les programmes externes utilisés
#               Afficher les piles d'appel en cas d'exception

# PICGS_PROFIL = 1
#                Afficher les informations de profilage si pas d'exception)'

# PROGRAMME PRINCIPAL
erreur = False
try:
    # Changer le umask pour les fichiers créés sur le host
    # umask u=rwx,g=rwx,o=r
    os.umask(0o002)

    prms = Params('picgs', sys.argv)
    (action,nom,fonction,version,options) = prms.getData()

    # action est-il correct ? En principe oui, sinon params l'a déjà détecté
    #if not action in ACTIONS:
    #    raise PicErreur(f"ERREUR INTERNE - Action = {action} inconnue")

    (ALGO,PLUGS) = ACTIONS[action]
    ALGO(prms, PLUGS)

except KeyboardInterrupt:
    print (afficheJaune("\n\n===> Action interrompue par un CTRL-C"))

except PicErreur as x:
    erreur = True
    print(afficheRouge(x))
    if 'PICGS_DEBUG' in os.environ:
        traceback.print_exception(x)

except Exception as x:
    erreur = True
    print(x)
    if 'PICGS_DEBUG' in os.environ:
        traceback.print_exception(x)
        
if erreur:
    sys.exit(1)
