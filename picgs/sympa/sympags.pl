#! /usr/bin/perl -X

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# ce script fait la liste des listes SYMPA, et pour chacun d'entre elle envoie un mail à ses 
# proprios en leur donnant quelques informations importantes sur leur liste
# Il est un peu quick-and-dirty, je n'ai pas reussi a utiliser completement l'API
#

use strict;
use IO::File;
use Encode;
use Cwd;

# utile seulement pour deboguer
use Data::Dumper;    

# Charger l'API sympa
use lib '/usr/share/sympa/lib';
use lib '/usr/share/sympa/lib/Sympa';
use Conf;
use List;

# qq constantes
use constant CONF     => '/etc/sympa/sympa/sympa.conf';
use constant ARCHIVES => '/var/lib/sympa/arc/';
use constant SYMPAGSDIR => '/srv/datadisk01/sympags/';

#use constant DEBUG    => 1;
#use constant DEBUG1   => 1;
#use constant DEBUG2   => 1;
#use constant DEBUG3   => 1;
#use constant DEBUG_CMDLINE => 1;


# Analyse de la ligne de commande
# todo - Ecrire cela un peu mieux, bordel !
my @A_KEYS = ( "Nom de liste","Status","sujet","Proprietaires","Privilegies","Moderateurs","Visibilite",
				"Type de liste","Acces a la liste des abonnes","Date du dernier courrier",
				"Nombre d'abonnes", "Documents Partages: Depot","Documents Partages: Acces","Documents Partages: Taille",
				"Archives","Acces aux archives","Premieres archives le ","Dernieres archives le ","Reponse a" );
				
my %H_COMMANDES = ( "collecte"=> 1, "info" => 1, "robots" => 1 );

my $envoyer_email = 0;
my $strict = 0;

if (! exists $H_COMMANDES{$ARGV[0]} ) { usage() };
my $commande   = $ARGV[0];
my $robot      = $ARGV[1];
my $info_liste = '';

if ($commande eq 'info') {
	if (@ARGV <=2) {
		usage(); 
	} else {
		$info_liste = $ARGV[2];
		if (@ARGV==4 && $ARGV[3] eq '--strict') {
			$strict = 1;
		}
	}
}
if ($commande eq 'collecte') {		
	if (@ARGV <2) {
		usage();
	} elsif (@ARGV >2) {
		if($ARGV[2] eq '--envoyer-email') {
			$envoyer_email = 1;
		}
	}
}

if (defined &DEBUG_CMDLINE) {
	print "$robot\n";
	print "$commande\n";
	print "$info_liste\n";
	print "$envoyer_email\n";
	print "$strict\n";
	print "bye\n";
	exit(0);
}

# Le repertoire de listes
our $LISTES = '/var/lib/sympa/list_data/' . $robot;

# lire la conf a partir du fichier de conf
&Conf::load(&CONF);

# la commande robots: juste un petit appel a l'API et on sort !
# (donc ça va très vite)
if ($commande eq 'robots') {
	print "--------------------------------------\n";
	print " ROBOTS SYMPA DISPONIBLES SUR CE SERVEUR\n";
	print "--------------------------------------\n";
	my $ra_robots = Conf::get_robots_list();
	foreach my $r (@$ra_robots) {
		print "$r\n";
	}
	exit(0);
}

# le tableau @$ra_listes contient la liste des listes, pour chaque liste tous ses parametres
# Commande collecte: on prend toutes les listes
# Commande info: on filtre
my $ra_listes = '';
if ($info_liste ne '') {
	my $name=($strict==1)?'name':'%name%';
	$ra_listes = &Sympa::List::get_lists($robot,first_access => 1, filter => [$name => $info_liste]);
} else {
	$ra_listes = &Sympa::List::get_lists($robot,{first_access => 1});
}

# DEBUG !!!
if (defined &DEBUG1) {
	print "DEBUG 1\n";
	my $d = Data::Dumper->new([ $ra_listes ]);
	print $d->Dump();
	exit(1);
} 

# A partir de $ra_liste (array), on fait un rh_listes (hash)
# Cle = Le nom de liste
# Val = Les infos utiles sur la liste
my $rh_listes = &listesParNom($ra_listes,$robot);

# DEBUG !!!
if (defined &DEBUG3) {
	print "DEBUG 3\n";
	my $d = Data::Dumper->new([$rh_listes]);
	print $d->Dump();
	exit(1);
}

if ($commande eq 'info') {
	my @a_conf = values(%$rh_listes);
	commandeInfo(\@a_conf, $robot, $info_liste);
}

if ($commande eq 'collecte') {
	if ($envoyer_email==1) {
		# Sauve les infos dans le repertoire SYMPAGSDIR et envoie un mail à chaque proprio à propos de ses listes
		# le hash $rh_owners contient pour chaque owner un array contenant les parametres des listes dont il est owner
		my $rh_owners = &trouveOwners($rh_listes);
	
		# DEBUG !!!
		if (defined &DEBUG2) {
			print "DEBUG 2\n";
			my $d = Data::Dumper->new([$rh_owners]);
			print $d->Dump();
			exit(1);
		}
		
		# imprime (pour debogguer) pour chaque owner les listes dont il est proprio
		&imprimeInfoProprios($rh_owners,$rh_listes,$robot);
	
		########printf "ENVOYER_EMAIL DESACTIVE !\n";
		&mailInfo($rh_owners,$rh_listes);
	} else {
		# Imprime quelques infos dans un TSV
		&imprimeTsv($robot,$rh_listes);
	}
}

# Fini !
exit(0);


# Imprime les listes dans un TSV
sub imprimeTsv {
	my ($robot,$rh_listes) = @_;
	
	print "Listes du robot $robot à la date du ".impDate()."\n";
	print "Liste\tProprio\tArchives: taille\tArchives: quota\tDocuments\n";
	foreach my $l (keys %$rh_listes) {
		my $conf=$rh_listes->{$l};
		print $l."\t";
		print $conf->{'Proprietaires'}."\t";
		print $conf->{"Archives"}."\t";
		my $quota_arc = $conf->{"Archives: quota"};
		if (!defined $quota_arc) { $quota_arc = "NON"; };
		print $quota_arc."\t";
		print $conf->{"Documents Partages: Taille"}."\n";
	}
}

# Traitement de la commande info
# params = Array de toutes les listes correspondant à la requête
#          Le robot
#          Liste dont on veut sortir les infos
#
sub commandeInfo {
	my ($ra_listes, $robot, $liste) = @_;
	my $rh_conf;
	if (@$ra_listes != 0) {
		foreach my $l (@$ra_listes) {
			print _impConf($l);
		}
	} else {
		print "ERREUR - Le robot $robot est inconnu, ou alors il ne connait pas la liste $liste\n";
	}
}
	
#
# Cree un hash de hash, la cle est le nom de liste, a partir de ra_listes
# La valeur est un hash avec des cles en francais crees a partir de $ra_listes
# et aussi a partir d'autres fonctions
#
sub listesParNom
{
	my ($ra_listes,$robot) = (@_);
	my %h_listes;
	
	foreach my $l (@$ra_listes)
	{
		if (defined &DEBUG) 
		{
			next if ($l->{'name'} ne 'picca');
			print "DEBUG ".$l->{name}."\n";
			my $d = Data::Dumper->new([$l]);
			print $d->Dump();
		}

		my $ra_owners = $l -> get_admins('owner');
		if (defined &DEBUG) 
		{
			print "DEBUG owners\n";
			my $d = new Data::Dumper([$ra_owners]);
			print $d->Dump();
		}
		
		my @a_owner_names;
		foreach my $o (@$ra_owners)
		{
			my $email = $o->{email};
			my $gecos = $o->{gecos};
			if (defined $gecos)
			{
				push @a_owner_names, "$email ($gecos)";
			}
			else
			{
				push @a_owner_names, $email;
			}
		}
	
		if (defined &DEBUG) 
		{
			print "DEBUG a_owner_names\n";
			my $d = new Data::Dumper([\@a_owner_names]);
			print $d->Dump();
		}

		my $ra_editors = $l -> get_admins('editor');	
		my @a_editor_names;
		foreach my $e (@$ra_editors)
		{
			my $email = $e->{email};
			my $gecos = $e->{gecos};
			if ( defined $gecos)
			{
				push @a_editor_names, "$email ($gecos)";
			}
			else
			{
				push @a_editor_names, $email;
			}
		}

		if (defined &DEBUG) 
		{
			print "DEBUG a_editor_names\n";
			my $d = new Data::Dumper([\@a_editor_names]);
			print $d->Dump();
		}

		my ($arc_size,$arc_first,$arc_last) = archInfo($l,$robot);
		my $doc_size = docInfo($l);

		# Si on peut on utilise les accesseurs de List
		# Sinon on va piocher directement dans les champs de l'objet
		my %h_list_params;
		$h_list_params{"Proprietaires"} = join ',',@a_owner_names;
		$h_list_params{"Owners"}        = $ra_owners;
		$h_list_params{"Privilegies"}   = _trouvePriv($l);
		$h_list_params{"Moderateurs"}   = join ',',@a_editor_names;
		
		$h_list_params{"Nom de liste"}  = $l->{'name'}.'@'.$robot;
		$h_list_params{"Visibilite"}    = $l->{admin}->{visibility}->{name};
		$h_list_params{"Type de liste"} = $l->{admin}->{send}->{name};

		$h_list_params{"Date du dernier courrier"} = &POSIX::strftime("%d %b %Y", localtime($l->get_latest_distribution_date()*3600*24));
		$h_list_params{"Taille d'un message max"}  = $l->get_max_size();
		$h_list_params{"Reponse a"}                = $l->{admin}->{reply_to_header}->{value};
		$h_list_params{"Nombre d'abonnes"}         = $l->get_total();

		$h_list_params{"Archives activees"}        = $l->is_archiving_enabled();
		$h_list_params{"Archives: quota"}          = $l->{admin}->{archive}->{quota};
		$h_list_params{"Archives"}                 = $arc_size;
		$h_list_params{"Premieres archives le "}   = $arc_first;
		$h_list_params{"Dernieres archives le "}   = $arc_last;
		$h_list_params{"Acces aux archives"}       = $l->{admin}->{web_archive}->{access}->{name};

		$h_list_params{"Status"}                   = $l->{admin}->{status};
		$h_list_params{"Acces a la liste des abonnes"}= $l->{admin}->{review}->{name};
		
		$h_list_params{"sujet"}                    = lireInfo($l->{name});
		
		$h_list_params{"Documents Partages: Depot" } = $l->{admin}->{shared_doc}->{d_edit}->{name};
		$h_list_params{"Documents Partages: Acces" } = $l->{admin}->{shared_doc}->{d_read}->{name};

		$h_list_params{"Documents Partages: Taille"} = ( $doc_size ne '0' ) ? $doc_size : 'N/A';

		$h_listes{$l->{name}} = \%h_list_params;
	}
	return \%h_listes;
}
		
		
#
# renvoie une ref sur un hash:
#    cle = adresse mail d'un owner
#    val = array, l'ensemble des noms de listes dont il est proprio (owner)
#

sub trouveOwners
{
	my ($rh_listes) = @_;
	my %h_owners;

	foreach my $n (sort keys %$rh_listes)
	{
		my $ra_owners = $rh_listes->{$n}->{Owners};
		foreach my $o (@$ra_owners)
		{
			my $email = $o->{email};
			if (!exists $h_owners{$email}) {
				$h_owners{$email} = [];
			}
			my $ra_ = $h_owners{$email};
			push(@$ra_,$n);
		}
	}
	return \%h_owners;
}

# Appelle get_admins('privileged_owner') et renvoie (chaine, pour affichage) la liste des mails correspondants
sub _trouvePriv
{
	my ($l) = @_;
	my @a_priv = $l->get_admins('privileged_owner');
	my @a_priv_mail = ();
	foreach my $o (@a_priv) {
		push @a_priv_mail, $o->{email};
	}
	return join ',',@a_priv_mail;
}


#
# imprime pour chaque proprio les listes dont il est le proprio, ainsi que tous les parametres importants
#
sub imprimeInfoProprios
{
	my ($rh_owners,$rh_listes,$robot) = @_;

	my $date = impDate();
	$date =~ s/-//g;
	my $dir = &SYMPAGSDIR.'/'.$robot.'/proprio-'.$date;
	mkdir $dir;
	foreach my $o (sort keys %$rh_owners)
	{
		my $fname = $dir . '/' . $o;
		my $fh_o = new IO::File ($fname,'w') or die ("Peux pas ecrire $fname");
		my $ra_liste_noms = $rh_owners->{$o};
		foreach my $l (@$ra_liste_noms)
		{
			my $rh_conf = $rh_listes->{$l};
			print $fh_o _impConf($rh_conf);
		}
	}
}

sub impDate {
	my @time=localtime();
	my $year = sprintf("%02d", $time[5] % 100);
	my $mon  = sprintf("%02d", $time[4] + 1);
	my $mday = sprintf("%02d", $time[3]);
	return "$mday-$mon-$year";
}

# Renvoie la conf passée en paramètres
# Param  = $rh_conf Le hash qui donne la conf
# Return = L'impression sous forme de chaine de caractères
#
sub _impConf
{
	my ($rh_conf) = @_;
	my $rvl = '';
	my @a_keys = @A_KEYS;
	
	my $k1 = shift @a_keys;
	$rvl .= ('-'x60)."\n";
	$rvl .= 'Liste ';
	$rvl .= $rh_conf->{$k1}."\n";
	$rvl .= ('-'x60)."\n";

	$k1 = shift @a_keys;
	if ( $rh_conf->{$k1} ne 'open' )
	{
		$rvl .= "ATTENTION - Liste Fermée\n";
	}

	$k1 = shift @a_keys;
	$rvl .= TronqueLine($rh_conf->{$k1}) . "\n";
	$rvl .= "\nPROPRIETAIRES:\n";
	$k1 = shift @a_keys;
	$rh_conf->{$k1} =~ s/,/\n/g;
	$rvl .= $rh_conf->{$k1} . "\n";

	$rvl .= "\nPROPRIETAIRES PRIVILEGIES:\n";
	$k1 = shift @a_keys;
	$rh_conf->{$k1} =~ s/,/\n/g;
	$rvl .= $rh_conf->{$k1} . "\n";

	$rvl .= "\nMODERATEURS:\n";
	$k1 = shift @a_keys;
	$rh_conf->{$k1} =~ s/,/\n/g;
	$rvl .= $rh_conf->{$k1} . "\n";

	$rvl .= "\nPRINCIPAUX PARAMETRES:\n";
	foreach my $k (@a_keys)
	{
		if ( defined ($rh_conf->{$k}) && $rh_conf->{$k} ne '' )
		{
			$rvl .= sprintf "%30s => %22s\n",$k => $rh_conf->{$k};
		}
	}

	$rvl .= "\n";
	$rvl .= "COMMENTAIRES\n";
	$rvl .= "============\n";
	$rvl .= CommVisibilite($rh_conf);
	$rvl .= CommTypeDeListe($rh_conf);
	$rvl .= CommAccesAbonnes($rh_conf);
	$rvl .= CommAccesArchives($rh_conf);
	$rvl .= CommAccesDocs($rh_conf);
	$rvl .= CommReponseA($rh_conf);
	$rvl .= "\n\n";

	return $rvl;
}

sub CommVisibilite
{
	my ($rh_conf) = @_;
	my %comm;
	$comm{conceal} = "Cette liste n'apparaitra QUE aux personnes abonnées à la liste.\n";
	$comm{noconceal}="Cette liste apparaitra sur l'espace de gestion des listes sympa, même pour les personnes non abonnées.\n";
	return $comm{$rh_conf->{'Visibilite'}};
}

sub CommTypeDeListe
{
	my ($rh_conf) = @_;
	my %comm;
	$comm{closed} = "Seuls les abonnés peuvent envoyer des messages à la liste.\n";
	$comm{privateoreditorkey}="Tout le monde peut envoyer des messages, ceux-ci seront modérés si l'expéditeur n'est pas abonné.\n";
	$comm{editorkey}="Liste modérée.\n";
	$comm{editorkeyonly} = $comm{editorkey};
	$comm{editorkeyonlyauth} = $comm{editorkey};
	$comm{privateandeditorkey} = $comm{editorkey};
	$comm{privatekeyandeditorkeyonly} = $comm{editorkey};
	$comm{newsletter}="Newsletter, seuls les modérateurs ont le droit de poster.\n";
	$comm{newsletterkeyonly} = $comm{newsletter};
	$comm{public}="ATTENTION !!! LISTE OUVERTE A TOUT LE MONDE SANS MODERATION - RISQUE DE SPAM.\n";

	my $rvl;
	if ( exists $comm{$rh_conf->{'Type de liste'}} )
	{
		$rvl = $comm{$rh_conf->{'Type de liste'}};
	}
	else
	{
		return '';
	}
	#if ( $rh_conf->{'Type de liste'} =~ /editorkey/i || $rh_conf->{'Type de liste'} =~ /newsletter/i )
	#{
	#	if ( !defined ($rh_conf->{'Moderateurs'}) || $rh_conf->{'Moderateurs'} eq '' )
	#	{
	#		$rvl .="ATTENTION !!!!! PAS DE MODERATEUR DECLARE SUR CETTE LISTE\n";
	#	}
	#}
	return $rvl;
}
sub CommAccesAbonnes
{
	my ($rh_conf) = @_;
	my %comm;
	$comm{open} = "ATTENTION !!! TOUT LE MONDE PEUT AVOIR LA LISTE DES ABONNES !!!\n";
	$comm{private}="Seuls les abonnés peuvent avoir la liste des adresses abonnées.\n";
	$comm{closed}="Personne n'a accès à la liste des abonnés.\n";
	$comm{owner}="Seuls les propriétaires ont accès à la liste des abonnés.\n";
	if ( exists $comm{$rh_conf->{'Acces a la liste des abonnes'}} )
	{
		return $comm{$rh_conf->{'Acces a la liste des abonnes'}};
	}
	else
	{
		return '';
	}
}

sub CommAccesDocs
{
	my ($rh_conf) = @_;
	my (%comm_read,%comm_edit);
	$comm_read{public} = "ATTENTION !!! TOUT LE MONDE PEUT LIRE LES DOCUMENTS PARTAGES !!!\n";
	$comm_read{private}="Seuls les abonnés peuvent lire les documents partagés.\n";
	$comm_edit{public} = "ATTENTION !!! TOUT LE MONDE PEUT DEPOSER DES DOCUMENTS !!!\n";
	$comm_edit{private}="Seuls les abonnés peuvent déposer des documents.\n";
	my $r1 = $comm_read{$rh_conf->{'Documents Partages: Acces'}};
	my $r2 = $comm_edit{$rh_conf->{'Documents Partages: Depot'}};
	return $r1.$r2;
}

sub CommAccesArchives
{
	my ($rh_conf) = @_;
	my %comm;
	$comm{public} = "ATTENTION !!! TOUT LE MONDE PEUT LIRE LES ARCHIVES !!!\n";
	$comm{private}="Seuls les abonnés peuvent lire les archives.\n";
	$comm{owner}="Seuls les propriétaires peuvent lire les archives.\n";
	$comm{closed}="PERSONNE NE PEUT LIRE LES ARCHIVES.\n";
	if ( exists $comm{$rh_conf->{'Acces aux archives'}} )
	{
		return $comm{$rh_conf->{'Acces aux archives'}};
	}
	else
	{
		return '';
	}
}
sub CommReponseA
{
	my ($rh_conf) = @_;
	my %comm;
	$comm{list}="La réponse sera envoyée à la liste -> risque de trafic élevé, mais possibilité de débats sur la liste.\n";
	$comm{sender}="La réponse sera envoyée à l'expéditeur -> Peu de risque de trafic trop gros, mais sans doute peu de débats.\n";
	if ( exists $comm{$rh_conf->{'Reponse a'}} )
	{
		return $comm{$rh_conf->{'Reponse a'}};
	}
	else
	{
		return '';
	}
}

#
# pour chaque proprio, envoie l'info sur ses listes par mail
sub mailInfo
{
	my ($rh_owners) = @_;

	my $subject = "Les listes SYMPA dont vous etes l heureux proprietaire";
	###my %h_owners_test = ( 'emmanuel.courcelle@laposte.net' => 1 );
	
    foreach my $o (sort keys %$rh_owners)
    {
		# seulement pour deboguage, on evite de spammer tous les proprios
		###next unless exists $h_owners_test{$o};
		my $fh_o = new IO::File ("|mail -a 'Content-Type: text/plain; charset=UTF-8' -s '$subject' $o");
		my $ra_liste_noms = $rh_owners->{$o};
		foreach my $l (@$ra_liste_noms)
		{
			my $rh_conf = $rh_listes->{$l};
			print $fh_o _impConf($rh_conf);
		}
    }
}

#
# TronqueLine - Met des \n pour eviter d'avoir des lignes trop longues dans les mails
#               Met les \n a la place des espaces autant que faire se peut
#

sub TronqueLine() {
	my ($line) = @_;
	chomp $line;

	my $lenbig = 100;	# longueur a partir de laquelle un ligne est "big"
	my $lenrab = 30;	# on autorise quelques caracteres de plus pour aller au premier espace
	my $lenmax = $lenbig + $lenrab;	# Pas question de depasser cette longueur

	my $line_tronquee;
	 while (length($line)>$lenmax) {
		my ($deb, $milieu, $fin) = ($line =~ /(.{$lenbig})(.{$lenrab})(.*)/);
		if ( $milieu =~ / / ) {
			my ($milieu1, $milieu2) = ($milieu =~ /([^ ]*) (.*)/);
			$line_tronquee .= "$deb$milieu1\n";
			$line           = "$milieu2$fin";
		}
		else {
			$line_tronquee .= "$deb\n";
			$line = "$milieu$fin";
		}
	}
	$line_tronquee .= "$line";
	return $line_tronquee;
}

#
# DocInfo
#
# renvoie des infos sur les documents partages et associes a cette liste. (taille)
#

sub docInfo
{
	my ($liste) = @_;
	my $dir = $LISTES . '/' . $liste->{'name'} . '/' . 'shared';
	if ( ! -d $dir )
	{
		return 0;
	}
	my $size   = `du -hs $dir|cut -f1`;
	chomp $size;
	return $size;
}

#
# archInfo
#
# renvoie des infos sur les archives de cette liste: taille prise par l'archive, premier mois, dernier mois archives
#

sub archInfo
{
	my ($liste,$robot) = @_;
	my $dir = &ARCHIVES . $liste->{'name'} . '@' . $robot;
	if ( ! -d $dir )
	{
		return (0,'','');
	}

	my $size   = `du -hs $dir|cut -f1`;
	#my $size=0;
	chomp $size;
	my @a_mois = `ls -t $dir`;
	chomp @a_mois;
	my $first  = $a_mois[-1];
	my $last   = $a_mois[0];
	return ($size,$first,$last);
}

#
# lireInfo
#
# lit le fichier info a partir du nom de la liste
#

sub lireInfo
{
	my ($name) = @_;
	my $info = $LISTES . '/' . $name . '/info';
	my $fh_info = new IO::File($info) || return '';
	my @rvl = <$fh_info>;
	chomp @rvl;
	return join('',@rvl);
}

sub usage {
	print "Usage: sympags robots\n";
	print "        Donne la liste des robots disponibles\n";
	print "\n";
###	print "       sympags collecte robot [--envoyer-email]\n";
	print "       sympags collecte robot\n";
	print "        robot: le nom du robot virtuel, l'un de ceux renvoyé par la commande robots\n";
	print "        collecte: Collecte des informations sur toutes les listes du robot considéré\n";
###	print "                  --envoyer-email: Pour spammer les proprios, NE PAS UTILISER EN TEMPS NORMAL !!!\n";
	print "\n";
	print "       sympags info robot fcpe_liste [--strict]\n";
	print "        robot: le nom du robot virtuel, l'un de ceux renvoyé par la commande robots\n";
	print "        info:  Informations sur liste se trouvant dans robot\n";
	print "        fcpe_liste: Le nom de la liste, on aura toutes les listes blabla_fcpe_liste_bloublou\n";
	print "                    --strict : cette fois on aura SEULEMENT fcpe_liste\n";
	exit(1);
}
	 
__END__


#
# 

foreach my $l (@$ra_listes)
{
	print "======\n";
	print "PROPRIETAIRES\n";
	my $ra_owners = $l->get_owners();
	foreach my $owner (@$ra_owners)
	{
		my $gecos = $owner->{gecos};
		my $email = $owner->{email};
		print $email;
		if (defined $gecos)
		{
			print "( $gecos)\n";
		}
		else
		{
			print "\n";
		}
	}
	print $l->{'name'}."\n";
	print &POSIX::strftime("%d %b %Y", localtime($l->get_latest_distribution_date()*3600*24))."\n";
	
}
