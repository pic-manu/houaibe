## Tests unitaires pour picgs

### 1/ Démarrer l'environnement de tests:

```
cd picgs/tests/docker
docker-compose build
docker-compose up -d
```

### 2/ Exécuter les tests:

```
docker exec py4tests python3 /picgs/tests/TestPicgsLdap.py
docker exec py4tests python3 /picgs/tests/TestParams.py
```

Il peut être intéressant d'utiliser PICGS_DEBUG sur les tests ReadOnly:

### 3/ Détruire l'environnmeent de tests:

```
docker-compose down
```
