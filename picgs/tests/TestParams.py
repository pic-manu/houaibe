#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

import copy
import re
import os
import getpass
import socket
import unittest

from erreur import PicErreur
from picconfig import getConf
from picgsldap import Ldap
from params import *

# TestParams
#
# Teste le module Params, c'est-à-dire:
#
#   La fonction chercheUtilisateurs QUI SE TROUVE EN FAIT DANS Ldap
#   On fait le test ici car c'est plus simple
#
# TEST_READONLY
#   Si la variable d'environnement TEST_READONLY existe et vaut True (ie PAS "")
#   On crée le module ldap en MODE READONLY
#   setUp et tearDown appellent un python externe inliner pour initialiser l'environnement de tests
#   On doit procéder de cette manière car Ldap() est un singleton, il vit jusqu'à la fin du programme
#

#@unittest.skip("bouh")
class TestChercheUtilisateurs(unittest.TestCase):
    '''Teste la fonction ChercheUtilisateur qui se trouve dans le module picgsldap
       Teste aussi la fonction chercherVersion qui se trouve, elle, dans le module params'''
       
    def setUp(self):
        attribs = [ {'cn':'user','uidNumber':'10010','gidNumber':'10100','homeDirectory':'/home'},
                    {'cn':'user1-st1','uidNumber':'5000','gidNumber':'5000','homeDirectory':'/home'},
                    {'cn':'user2','uidNumber':'10000','gidNumber':'10000','homeDirectory':'/home'},
                    {'cn':'user3-st1','uidNumber':25000,'gidNumber':'25000','homeDirectory':'/home'},
                    {'cn':'user4-st1','uidNumber':'10001','gidNumber':'10001','homeDirectory':'/home'},
                    {'cn':'user5-st1','uidNumber':'10002','gidNumber':'10002','homeDirectory':'/home'},
                    {'cn':'user4-st2','uidNumber':'10003','gidNumber':'10003','homeDirectory':'/home'},
                    {'cn':'user5-st4','uidNumber':'10008','gidNumber':'10008','homeDirectory':'/home'}
                  ]
                    
        ldap = Ldap()
        for a in attribs:
            try:
                ldap.addUser(a)
            except:
                pass

    def tearDown(self):
        users = ['user',
                 'user1-st1',
                 'user2',
                 'user3-st1',
                 'user4-st1',
                 'user5-st1',
                 'user4-st2',
                 'user5-st4' ]

        ldap = Ldap()
        for u in users:
            ldap.delUser(u)

    #@unittest.skip
    def test_chercheUtilisateurs(self):
        '''Teste chercheUtilisateurs'''
        
        users = chercheUtilisateurs(extended = False)
        users.sort()

    #@unittest.skip
    def test_version(self):
        '''Teste chercheVersion'''
        
        # Pas de user0 -> exception
        prms = Params('picgs',['picgs','info','user0']); self.assertRaises(PicErreur,prms.chercheVersionPourInfo)
        prms = Params('picgs',['picgs','info','user0','site','1']); self.assertRaises(PicErreur,prms.chercheVersionPourInfo)
        
        # user1-st1 a un uid et gid < 10000 -> exception
        prms = Params('picgs',['picgs','info','user1-st']); self.assertRaises(PicErreur,prms.chercheVersionPourInfo)


        # Pas de nom, pas de versions !
        self.assertEqual(Params('picgs',['picgs','cert']).chercheVersionPourInfo(),[0,0])

        # Si non spécifiée, choisit la version la plus haute, mais on peut forcer une autre version
        self.assertEqual(Params('picgs',['picgs','info','user4','site','2']).chercheVersionPourInfo(),[0,2])
        self.assertEqual(Params('picgs',['picgs','info','user4','site','1']).chercheVersionPourInfo(),[0,1])
        self.assertEqual(Params('picgs',['picgs','info','user4','site']).chercheVersionPourInfo(),[0,2])
        
        # info: n'accepte pas le forçage en version user6-st1 ni user4-st3 car ces versions n'existent pas
        prms = Params('picgs',['picgs','info','user6','site','1']); self.assertRaises(PicErreur,prms.chercheVersionPourInfo)
        prms = Params('picgs',['picgs','info','user4','site','3']); self.assertRaises(PicErreur,prms.chercheVersionPourInfo)

        # Pour ajouter, il propose la version 3 de user4
        self.assertEqual(Params('picgs',['picgs','ajout','user4','site']).chercheVersionPourAjouter(),[2,3])

        # ajouter: propose la version 5 de user5
        self.assertEqual(Params('picgs',['picgs','ajout','user5','site']).chercheVersionPourAjouter(),[4,5])
        
        # ajouter: on force la version 2 de user5
        self.assertEqual(Params('picgs',['picgs','ajout','user5','site','2']).chercheVersionPourAjouter(),[4,2])
        
        # ajouter: propose la version 1, mais accepte qu'on force la version 1 ou 2 car elles n'existent pas
        self.assertEqual(Params('picgs',['picgs','ajout','user6','site']).chercheVersionPourAjouter(),[0,1])
        self.assertEqual(Params('picgs',['picgs','ajout','user6','site','1']).chercheVersionPourAjouter(),[0,1])
        self.assertEqual(Params('picgs',['picgs','ajout','user6','site','2']).chercheVersionPourAjouter(),[0,2])

        # ajouter: n'accepte pas le forçage en version 1 de user4, car elle existe déjà
        prms = Params('picgs',['picgs','info','user4','site','1']); self.assertRaises(PicErreur,prms.chercheVersionPourAjouter)

        
    #@unittest.skip
    def test_getData(self):
        '''Teste getData'''

        # Pour picgs: action/nom/fonction/version/options
        self.assertEqual(Params('picgs',['picgs','ajout','user4','site']).getData(),('ajout','user4-st','site',None,{}))
        self.assertEqual(Params('picgs',['picgs','info','user4','site','1']).getData(),('info','user4-st','site',1,{}))
        self.assertEqual(Params('picgs',['picgs','info','user4-st1']).getData(),('info','user4-st','site',1,{}))
        self.assertEqual(Params('picgs',['picgs','info','user4-st1','--toto=4']).getData(),('info','user4-st','site',1,{'--toto':'4'}))

        # prod=False empêche d'envoyer le message d'aide, puisqu'on est en tests ça ne sert à rien et ça embrouille la sortie
        self.assertRaises(PicErreur, Params, 'picgs', ['picgs','ajout','user4'], prod=False)
        self.assertRaises(PicErreur, Params, 'picgs', ['picgs','ajout'], prod=False)
        self.assertRaises(PicErreur, Params, 'picgs', ['picgs'], prod=False)
        self.assertRaises(PicErreur, Params, 'picgs', ['picgs','nawak'], prod=False)
        self.assertRaises(PicErreur, Params, 'picgs', '--help', prod=False)

        # Pour picwpgs: action_nom vaut True, donc l'action est la concaténation des deux premiers arguments
        self.assertEqual(Params('picwpgs',['picwpgs','liste','core'],action_nom = True).getData(),('liste_core',None,None,None,{}))
        self.assertRaises(PicErreur, Params, 'picwpgs', ['picwpgs','nawak'], action_nom = True, prod=False)
        self.assertRaises(PicErreur, Params, 'picwpgs', ['picwpgs','liste','nawak'],action_nom = True, prod=False)

        # Pour des scripts simples, pas d'action (ou plutôt action vaut default)
        self.assertEqual(Params('monscript', ['monscript','user4-st1','--machin=truc'], default=True, helpMsg="voici le message d'aide").getData(),('default','user4-st','site',1,{'--machin':'truc'}))
        self.assertRaises(PicErreur, Params, 'monscript', ['monscript','--help'], default=True, helpMsg="Voici le message d'aide de monscript")

    #@unittest.skip
    def test_lmn(self):
        '''Teste setLmn et getLmn'''

        # On ne peut pas diminuer en-dessous de la valeur par défaut, soit 32
        self.assertEqual(Params.getLmn(),32)
        self.assertEqual(Params.setLmn(10),None)
        self.assertEqual(Params.getLmn(),32)
        # Mais on peut augmenter
        self.assertEqual(Params.setLmn(35),None)
        self.assertEqual(Params.getLmn(),35)

        # 35 caractères ça passe
        self.assertEqual(Params('picgs',['picgs','ajout','u1234567890123456789012345678901234','site']).getData(),('ajout','u1234567890123456789012345678901234-st','site',None,{}))
        # 36 caractères ça casse
        self.assertRaises(PicErreur, Params, 'picgs', ['picgs','ajout','u12345678901234567890123456789012345'], prod=False)
        
        self.assertEqual(Params.setLmn(32),None)
        self.assertEqual(Params.getLmn(),32)
        
    def test_helpMinMax(self):
        
        #self.assertEqual(Params._Params__helpMinMax(None,['nom','fonction','version'],0,4),['[action]','[nom]','[fonction]','[version]'])
        self.assertEqual(Params._Params__helpMinMax(None,['nom','fonction','version'],1,4),['[nom]','[fonction]','[version]'])
        self.assertEqual(Params._Params__helpMinMax(None,['nom','fonction','version'],2,4),['nom','[fonction]','[version]'])
        self.assertEqual(Params._Params__helpMinMax(None,['nom','fonction','version'],3,4),['nom','fonction','[version]'])
        self.assertEqual(Params._Params__helpMinMax(None,['nom','fonction','version'],4,4),['nom','fonction','version'])

        #self.assertEqual(Params._Params__helpMinMax(None,['nom','fonction','version'],0,3),['[action]','[nom]','[fonction]'])
        self.assertEqual(Params._Params__helpMinMax(None,['nom','fonction','version'],1,3),['[nom]','[fonction]'])
        self.assertEqual(Params._Params__helpMinMax(None,['nom','fonction','version'],2,3),['nom','[fonction]'])
        self.assertEqual(Params._Params__helpMinMax(None,['nom','fonction','version'],3,3),['nom','fonction'])
        
        self.assertRaises(ValueError, Params._Params__helpMinMax, None, ['nom','fonction','version'], 0, 4)
        self.assertRaises(ValueError, Params._Params__helpMinMax, None, ['nom','fonction','version'], 1, 5)
        self.assertRaises(ValueError, Params._Params__helpMinMax, None, ['nom','fonction','version'], 4, 1)


if __name__ == '__main__':
    os.chdir ('/picgs/tests')
    if os.getenv('TEST_READONLY',False):
        print ("On teste Ldap en mode READONLY")
        
    unittest.main()
