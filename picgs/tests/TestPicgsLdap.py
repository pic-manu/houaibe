#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

import copy
import re
import os
import getpass
import socket

from sysutils import *
from erreur import PicErreur
from picgsldap import Ldap

import unittest

#
#   Tests de pigsldap.py
#
#   Un fichier picconfig.py simplifié doit se trouver dans le répertoire /tests/picgs
#   Méthodes de bas niveau = Méthodes qui manipulent le gecos ou qui récupèrent la connection
#   Méthodes de haut niveau= Méthodes pour ajouter/retirer utilisateurs et groupes, changer les groupes
#                            Méthodes qui manipulent nomn, mail, tél, nom de conteneur, stoquées dans le gecos
#
# TestLowLevelxxx = Test des méthodes bas niveau
#                   buildGecos, parseGecos, modifyGecos/setGecos, getGecos, getConnection
#
# TestHighLevelxx = Test des méthodes haut niveau
#                   searchGroup, searchUser
#                   addGroup, addUser, addUserToGroup, delGroup, delUser, delUserFromGroup
#                   modify/setLoginShell
#                   changeUserPassord
#                   getNomLong, getMail, getTel, getCont, setNomLong, setMail, setTel, setCont
#
# Chaque test est effectué d'abord en mode ReadWrite puis en mode ReadOnly
#

#
# initLdap() : Cette fonction est appelée par les setUp(), elle peuple le serveur ldap
# cleanLdap(): Cette fonction est appelée par les tearDown(), elle vide complètement le serveur ldap
#

GROUPS = [
            {'cn':'group1','gidNumber':'1000'},
            {'cn':'group2','gidNumber':'2000'},
            {'cn':'group3','gidNumber':'3000'},
            {'cn':'group4','gidNumber':'4000'},
            {'cn':'group5','gidNumber':'5000'},
            {'cn':'group6','gidNumber':'6000'},
            {'cn':'group7','gidNumber':'7000'},
            {'cn':'group8','gidNumber':'8000'}            
          ]

USERS = [
            {'cn':'user1','uidNumber':'1100','gidNumber':'1000','homeDirectory':'/home/user1','userPassword':'azerty','loginShell':'/bin/bash'},
            {'cn':'user2','uidNumber':'2100','gidNumber':'2000','homeDirectory':'/home/user2','userPassword':'azerty','loginShell':'/bin/zsh','gecos':'gecos de user2'},
            {'cn':'user3','uidNumber':'3100','gidNumber':'3000','homeDirectory':'/home/user3','userPassword':'azerty','gecos':'nom long de user3 (u3@u.fr),pic-itk00,,01-02'},
            {'cn':'user4','uidNumber':'4100','gidNumber':'4000','homeDirectory':'/home/user4','userPassword':'azerty','gecos':'nom long de user4 (u4@u.fr),pic-itk00,,03-04'},
            {'cn':'user5','uidNumber':'5100','gidNumber':'5000','homeDirectory':'/home/user5','userPassword':'azerty','gecos':'nom long de user5 (u5@u.fr),pic-itk12,,05-06'},
            {'cn':'user6','uidNumber':'6100','gidNumber':'8000','homeDirectory':'/home/user6','userPassword':'azerty','gecos':'nom long de user6 (u6@u.fr),pic-ex-st1#nginx-fpm34#2,,06-07'}          
        ]


def initLdap():
    '''Le serveur ldap est vide, On met dedans des enregistrements pour utilisateurs et groupes'''
    
    ldap      = Ldap()
    ldap_conn = ldap.getConnection()
    
    # Les groupes
    ldap_baseg = getConf('LDAPBASEGRP')
    ldap_conn.add(ldap_baseg, 'organizationalUnit')
    
    for g in GROUPS:
        dn='cn='+g['cn']+','+ldap_baseg
        ldap_conn.add(dn, 'posixGroup', g)

    # Les users
    ldap_baseu = getConf('LDAPBASEUSR')
    ldap_conn.add(ldap_baseu, 'organizationalUnit')
    
    for u in USERS:
        dn='cn={0},{1}'.format(u['cn'],ldap_baseu)
        u['objectClass'] = ['account','posixAccount']
        u['uid'] = u['cn']
        ldap_conn.add(dn, 'posixAccount', u)

def cleanLdap():
    '''Supprimer tous les utilisateurs et tous les groupes du serveur ldap'''

    ldap = Ldap()
    ldap_conn = ldap.getConnection()

    # D'abord les utilisateurs
    ldap_baseu = getConf('LDAPBASEUSR')
    ldap_conn.search(ldap_baseu,'(objectclass=account)',attributes=[])
    users = ldap_conn.response
    for u in users:
        dn = u['dn']
        ldap_conn.delete(dn)
    ldap_conn.delete(ldap_baseu)

    # Puis les groupes
    ldap_baseg = getConf('LDAPBASEGRP')
    ldap_conn.search(ldap_baseg,'(objectclass=posixGroup)',attributes=[])
    groups = ldap_conn.response
    for g in groups:
        dn = g['dn']
        ldap_conn.delete(dn)
    ldap_conn.delete(ldap_baseg)

def checkLdap():
    '''Vérifier que le serveur ldap est bien vide, avant de démarrer les tests'''
    
    with open(getConf('LDAPPASSWDFILE'),'r') as fh_pwd:
        pwd    = fh_pwd.readline().partition('\n')[0]
    
    ldapadmin = getConf('LDAPADMIN')
    base = ldapadmin.partition(',')[2]
    ldapuri = getConf('LDAPURI')

    cmd = "ldapsearch -x -L -b {0} -w {1} -D {2} -H {3}".format(base,pwd,ldapadmin,ldapuri)
    res = SysUtils()._system(cmd)
    
    if res[-2] != '# numEntries: 2' or res[-3] != '# numResponses: 3':
        print ("ATTENTION ! Le serveur ldap n'est pas très propre:")
        print ("\n".join(res))

class TestGetCn(unittest.TestCase):
    def setUp(self):
        initLdap()
        
    def tearDown(self):
        cleanLdap()

    def testGetUserCnRw(self):
        
        ldap=Ldap()
        
        self.assertRaises(PicErreur, ldap.getUserCn, 'averell')
        self.assertEqual(ldap.getUserCn('user1'),['user1'])
        self.assertRaises(PicErreur, ldap.getUserCn, {})
        self.assertRaises(PicErreur, ldap.getUserCn, {'attributes':1})

        # Pas terrible mais devrait marcher !
        group = ldap.searchGroup('group6')[0]
        self.assertEqual(ldap.getUserCn(group),['group6'])

        self.assertEqual(ldap.getUserNomCourt('user1'),'user1')
        self.assertRaises(PicErreur, ldap.getUserNomCourt, 'averell')
        self.assertRaises(PicErreur,ldap.getUserNomCourt,'group5')

    def testGetUserCnRo(self):
        
        ldap=Ldap(True)
        self.assertRaises(PicErreur, ldap.getUserCn, 'averell')
        self.assertEqual(ldap.getUserCn('user1'),['user1'])
        self.assertRaises(PicErreur, ldap.getUserCn, {})
        self.assertRaises(PicErreur, ldap.getUserCn, {'attributes':1})

        # PAs terrible mais devrait marcher !
        group = ldap.searchGroup('group6')[0]
        self.assertEqual(ldap.getUserCn(group),['group6'])

        self.assertEqual(ldap.getUserNomCourt('user1'),'user1')
        self.assertRaises(PicErreur, ldap.getUserNomCourt, 'averell')
        self.assertRaises(PicErreur,ldap.getUserNomCourt,'group5')

    def testGetGroupCnRw(self):
        
        ldap=Ldap()
        self.assertRaises(PicErreur, ldap.getGroupNomCourt, 'daltons')
        self.assertEqual(ldap.getGroupNomCourt('group7'),'group7')
        self.assertRaises(PicErreur, ldap.getGroupNomCourt, {})
        self.assertRaises(PicErreur, ldap.getGroupNomCourt, {'attributes':1})

    def testGetGroupCnRo(self):
        
        ldap=Ldap(True)
        self.assertRaises(PicErreur, ldap.getGroupNomCourt, 'daltons')
        self.assertEqual(ldap.getGroupNomCourt('group7'),'group7')
        self.assertRaises(PicErreur, ldap.getGroupNomCourt, {})
        self.assertRaises(PicErreur, ldap.getGroupNomCourt, {'attributes':1})

class TestLowLevelRw(unittest.TestCase):
    def setUp(self):
        initLdap()
        
    def tearDown(self):
        cleanLdap()

    def testGecos(self):
        '''Teste setGecos, getGecos, parseGecos, buildGecos'''
        
        ldap = Ldap()
        self.assertEqual(ldap.isReadOnly(),False)
        
        # parseGecos
        user = ldap.searchUser('user5',['gecos'])
        gecos = user[0]['attributes']['gecos']
        (nom,mail,tel,cont,image,archi) = ldap.parseGecos(gecos)
        
        self.assertEqual(nom,'nom long de user5')
        self.assertEqual(mail,'u5@u.fr')
        self.assertEqual(tel, '05-06')
        self.assertEqual(cont, 'pic-itk12')
        self.assertEqual(image,'')
        self.assertEqual(archi,1)
        
        (nom,mail,tel,cont,image,archi) = ldap.parseGecos('nom long')
        self.assertEqual(nom,'nom long')
        self.assertEqual(mail,'')
        self.assertEqual(tel, '')
        self.assertEqual(cont, '')
        self.assertEqual(image,'')
        self.assertEqual(archi,1)

        # Nouveaux conteneurs
        user = ldap.searchUser('user6',['gecos'])
        gecos = user[0]['attributes']['gecos']
        (nom,mail,tel,cont,image,archi) = ldap.parseGecos(gecos)
        self.assertEqual(nom,'nom long de user6')
        self.assertEqual(mail,'u6@u.fr')
        self.assertEqual(tel, '06-07')
        self.assertEqual(cont, 'pic-ex-st1')
        self.assertEqual(image,'nginx-fpm34')
        self.assertEqual(archi,2)
        
        # buildGecos
        gecos = ldap.buildGecos(('nom long','v@v.fr','0123','pic-itk00','',''))
        self.assertEqual(gecos,'nom long (v@v.fr),pic-itk00##,,0123')
        gecos = ldap.buildGecos(('nom long','v@v.fr','0123','pic-exemple-st1','nginx-fpm12','2'))
        self.assertEqual(gecos,'nom long (v@v.fr),pic-exemple-st1#nginx-fpm12#2,,0123')
        
        self.assertRaises(PicErreur,ldap.buildGecos,('nom long'))
        
        # getGecos
        gecos=ldap.getGecos('user4')
        self.assertEqual(gecos,'nom long de user4 (u4@u.fr),pic-itk00,,03-04')
        
        # setGecos
        ldap.setGecos('user4', 'nom long de user4 (u4@u.fr),pic-itk00,,03-04-05-06')
        gecos=ldap.getGecos('user4')
        self.assertEqual(gecos,'nom long de user4 (u4@u.fr),pic-itk00,,03-04-05-06')
        
#@unittest.skip("bouh")
class TestLowLevelRo(unittest.TestCase):
    def setUp(self):
        initLdap()
        
    def tearDown(self):
        cleanLdap()

    def testGecos(self):
        '''Teste setGecos, getGecos, parseGecos, buildGecos'''
        
        ldap = Ldap(True)
        self.assertEqual(ldap.isReadOnly(),True)

        # parseGecos
        user = ldap.searchUser('user5',['gecos'])
        gecos = user[0]['attributes']['gecos']
        (nom,mail,tel,cont,image,archi) = ldap.parseGecos(gecos)
        
        self.assertEqual(nom,'nom long de user5')
        self.assertEqual(mail,'u5@u.fr')
        self.assertEqual(tel, '05-06')
        self.assertEqual(cont, 'pic-itk12')
        self.assertEqual(image,'')
        self.assertEqual(archi,1)
        
        (nom,mail,tel,cont,image,archi) = ldap.parseGecos('nom long')
        self.assertEqual(nom,'nom long')
        self.assertEqual(mail,'')
        self.assertEqual(tel, '')
        self.assertEqual(cont, '')
        
        # Nouveaux conteneurs
        user = ldap.searchUser('user6',['gecos'])
        gecos = user[0]['attributes']['gecos']
        (nom,mail,tel,cont,image,archi) = ldap.parseGecos(gecos)
        self.assertEqual(nom,'nom long de user6')
        self.assertEqual(mail,'u6@u.fr')
        self.assertEqual(tel, '06-07')
        self.assertEqual(cont, 'pic-ex-st1')
        self.assertEqual(image,'nginx-fpm34')
        self.assertEqual(archi,2)
        
        # buildGecos
        gecos = ldap.buildGecos(('nom long','v@v.fr','0123','pic-itk00','',''))
        self.assertEqual(gecos,'nom long (v@v.fr),pic-itk00##,,0123')
        gecos = ldap.buildGecos(('nom long','v@v.fr','0123','pic-exemple-st1','nginx-fpm12','2'))
        self.assertEqual(gecos,'nom long (v@v.fr),pic-exemple-st1#nginx-fpm12#2,,0123')
        
        self.assertRaises(PicErreur,ldap.buildGecos,('nom long'))
        
        # getGecos
        gecos=ldap.getGecos('user4')
        self.assertEqual(gecos,'nom long de user4 (u4@u.fr),pic-itk00,,03-04')
        
        # setGecos
        self.assertRaises(PicErreur, ldap.setGecos,'user4', 'nom long de user4 (u4@u.fr),pic-itk00,,03-04-05-06')
        gecos=ldap.getGecos('user4')
        self.assertEqual(gecos,'nom long de user4 (u4@u.fr),pic-itk00,,03-04')

#@unittest.skip("bouh")
class TestHighLevelRw(unittest.TestCase):
    def setUp(self):
        initLdap()
        
    def tearDown(self):
        cleanLdap()

    def testCreationGroup(self):
        '''Teste searchGroup, addGroup,delGroup'''
        
        # Recherche de groupes
        ldap = Ldap()
        groups = ldap.searchGroup()
        self.assertEqual(len(groups),8)
        self.assertEqual(groups[5]['attributes']['cn'],['group6'])
        
        groups = ldap.searchGroup(None, ['gidNumber'])
        self.assertEqual(len(groups),8)
        self.assertEqual(groups[3]['dn'].partition(',')[0],'cn=group4')
        self.assertEqual(groups[3]['attributes']['gidNumber'],4000)

        groups = ldap.searchGroup('group5', ['gidNumber'])
        self.assertEqual(len(groups),1)
        self.assertEqual(groups[0]['attributes']['gidNumber'],5000)

        groups = ldap.searchGroup('group100', ['gidNumber'])
        self.assertEqual(len(groups),0)

        # Ajouter un groupe
        ldap.addGroup({'cn':'group9','gidNumber':'9000'})
        groups = ldap.searchGroup()
        self.assertEqual(len(groups),9)
        groups = ldap.searchGroup('group9')
        self.assertEqual(len(groups),1)
        self.assertEqual(groups[0]['attributes']['gidNumber'],9000)
        self.assertEqual(ldap.getGroupNomCourt(groups[0]),'group9')

        # Ajouter le même groupe
        attrs = {'cn':'group9','gidNumber':'9000'}
        self.assertRaises(PicErreur, ldap.addGroup, {'cn':'group9','gidNumber':'9000'})
        groups = ldap.searchGroup()
        self.assertEqual(len(groups),9)

        # Cas limites
        attrs = {'cn':'some_group'}
        self.assertRaises(PicErreur,ldap.addGroup,attrs)
        groups = ldap.searchGroup()
        self.assertEqual(len(groups),9)

        attrs = {'gidNumber':'501'}
        self.assertRaises(PicErreur,ldap.addGroup,attrs)
        groups = ldap.searchGroup()
        self.assertEqual(len(groups),9)

        attrs = {}
        self.assertRaises(PicErreur,ldap.addGroup,attrs)
        groups = ldap.searchGroup()
        self.assertEqual(len(groups),9)

        # Supprimer un groupe
        ldap.delGroup('group4')
        groups = ldap.searchGroup()
        self.assertEqual(len(groups),8)
        
        # Supprimer un groupe qui n'existe pas
        self.assertRaises(PicErreur,ldap.delGroup,'group555')

    def testCreationUser(self):
        '''Teste searchUser, addUser, delUser'''

        # Recherche de users
        ldap = Ldap()
        users = ldap.searchUser()
        self.assertEqual(len(users),6)
        self.assertEqual(users[4]['attributes']['cn'],['user5'])
        
        users = ldap.searchUser(None, ['uidNumber','gidNumber'])
        self.assertEqual(len(users),6)
        self.assertEqual(users[2]['dn'].partition(',')[0],'cn=user3')
        self.assertEqual(users[2]['attributes']['uidNumber'],3100)
        self.assertEqual(users[2]['attributes']['gidNumber'],3000)

        users = ldap.searchUser('user5', ['uidNumber'])
        self.assertEqual(len(users),1)
        self.assertEqual(users[0]['attributes']['uidNumber'],5100)

        users = ldap.searchGroup('user100', ['gidNumber'])
        self.assertEqual(len(users),0)

        # Ajouter un user
        attrs = {'cn':'user7','gidNumber':'7000','uidNumber':'7100','homeDirectory':'/home/user7','gecos':'user7 (u7@u.fr),99-99,,pic-itk00'}
        ldap.addUser(attrs)
        users = ldap.searchUser()
        self.assertEqual(len(users),7)
        users = ldap.searchUser('user7')
        self.assertEqual(len(users),1)
        self.assertEqual(users[0]['attributes']['uidNumber'],7100)

        # Ajouter le même user
        self.assertRaises(PicErreur, ldap.addGroup, attrs)
        users = ldap.searchUser()
        self.assertEqual(len(users),7)

        # Cas limites
        attrs = {'cn':'some_user','uidNumber':'10001'}
        self.assertRaises(PicErreur,ldap.addUser,attrs)
        attrs = {'cn':'rototo'}
        self.assertRaises(PicErreur,ldap.addUser,attrs)
        attrs = {'uidNumber':'10000'}
        self.assertRaises(PicErreur,ldap.addUser,attrs)
        attrs = {}
        self.assertRaises(PicErreur,ldap.addUser,attrs)

        # Supprimer un user
        ldap.delUser('user1')
        users = ldap.searchUser()
        self.assertEqual(len(users),6)
        
        # Supprimer un user qui n'existe pas
        self.assertRaises(PicErreur,ldap.delUser,'user555')

    def testgetset(self):
        '''Teste get/setNomCourt, get/setCont, get/setMail, get/setNomLong, get/setTel'''

        # Recherche de users
        ldap = Ldap()
        users = ldap.searchUser()
        self.assertEqual(len(users),6)

        self.assertEqual(ldap.getUserCn(users[0]), ['user1'])
        self.assertEqual(ldap.getUserNomCourt(users[0]),'user1')

        user = ldap.getUserNomCourt(users[2])

        #import logging
        #from ldap3.utils.log import set_library_log_detail_level, OFF, BASIC, NETWORK, EXTENDED
        #logging.basicConfig(filename='toto.log', level=logging.DEBUG)
        #set_library_log_detail_level(EXTENDED)
        
        # set/get
        self.assertEqual(ldap.getNomLong(user), 'nom long de user3')
        self.assertEqual(ldap.getMail(user),'u3@u.fr')
        self.assertEqual(ldap.getTel(user),'01-02')
        self.assertEqual(ldap.getCont(user),'pic-itk00')

        self.assertEqual(ldap.setNomLong(user, 'nom long modifie de user3'),None)
        self.assertEqual(ldap.setMail(user, 'uu3@u.fr'),None)
        self.assertEqual(ldap.setTel(user, '01-02-03'),None)
        self.assertEqual(ldap.setCont(user, 'pic-itk02'),None)

        self.assertEqual(ldap.getNomLong(user), 'nom long modifie de user3')
        self.assertEqual(ldap.getMail(user),'uu3@u.fr')
        self.assertEqual(ldap.getTel(user),'01-02-03')
        self.assertEqual(ldap.getCont(user),'pic-itk02')
        
        # cas limites
        user = ldap.getUserNomCourt(users[0])
        self.assertEqual(ldap.getNomLong(user), '')
        self.assertEqual(ldap.getMail(user),'')
        self.assertEqual(ldap.getTel(user),'')
        self.assertEqual(ldap.getCont(user),'')
        
        self.assertEqual(ldap.setNomLong(user, 'nom long de user1'),None)
        self.assertEqual(ldap.setMail(user, 'u1@u.fr'),None)
        self.assertEqual(ldap.setTel(user, '01-01-01'),None)
        self.assertEqual(ldap.setCont(user, 'pic-itk33'),None)

        self.assertEqual(ldap.getNomLong(user), 'nom long de user1')
        self.assertEqual(ldap.getMail(user),'u1@u.fr')
        self.assertEqual(ldap.getTel(user),'01-01-01')
        self.assertEqual(ldap.getCont(user),'pic-itk33')
        

#@unittest.skip("bouh")
class TestHighLevelRo(unittest.TestCase):
    def setUp(self):
        initLdap()
        
    def tearDown(self):
        cleanLdap()

    def testCreationGroup(self):
        '''Teste searchGroup, addGroup,delGroup'''
        
        # Recherche de groupes
        ldap = Ldap(True)
        groups = ldap.searchGroup()
        self.assertEqual(len(groups),8)
        self.assertEqual(groups[5]['attributes']['cn'],['group6'])
        
        groups = ldap.searchGroup(None, ['gidNumber'])
        self.assertEqual(len(groups),8)
        self.assertEqual(groups[3]['dn'].partition(',')[0],'cn=group4')
        self.assertEqual(groups[3]['attributes']['gidNumber'],4000)

        groups = ldap.searchGroup('group5', ['gidNumber'])
        self.assertEqual(len(groups),1)
        self.assertEqual(groups[0]['attributes']['gidNumber'],5000)

        groups = ldap.searchGroup('group100', ['gidNumber'])
        self.assertEqual(len(groups),0)

        # Ajouter un groupe
        self.assertRaises(PicErreur,ldap.addGroup,{'cn':'group8','gidNumber':'8000'})
        groups = ldap.searchGroup()
        self.assertEqual(len(groups),8)
        groups = ldap.searchGroup('group8')
        self.assertEqual(len(groups),1)

        # Cas limites
        attrs = {'cn':'some_group'}
        self.assertRaises(PicErreur,ldap.addGroup,attrs)
        groups = ldap.searchGroup()
        self.assertEqual(len(groups),8)

        attrs = {'gidNumber':'501'}
        self.assertRaises(PicErreur,ldap.addGroup,attrs)
        groups = ldap.searchGroup()
        self.assertEqual(len(groups),8)

        attrs = {}
        self.assertRaises(PicErreur,ldap.addGroup,attrs)
        groups = ldap.searchGroup()
        self.assertEqual(len(groups),8)

        # Supprimer un groupe
        self.assertRaises(PicErreur,ldap.delGroup,'group4')
        groups = ldap.searchGroup()
        self.assertEqual(len(groups),8)
        
        # Supprimer un groupe qui n'existe pas
        self.assertRaises(PicErreur,ldap.delGroup,'group555')

    def testCreationUser(self):
        '''Teste searchUser, addUser, delUser'''

        # Recherche de users
        ldap = Ldap(True)
        users = ldap.searchUser()
        self.assertEqual(len(users),6)
        self.assertEqual(users[4]['attributes']['cn'],['user5'])
        
        users = ldap.searchUser(None, ['uidNumber','gidNumber'])
        self.assertEqual(len(users),6)
        self.assertEqual(users[2]['dn'].partition(',')[0],'cn=user3')
        self.assertEqual(users[2]['attributes']['uidNumber'],3100)
        self.assertEqual(users[2]['attributes']['gidNumber'],3000)

        users = ldap.searchUser('user5', ['uidNumber'])
        self.assertEqual(len(users),1)
        self.assertEqual(users[0]['attributes']['uidNumber'],5100)

        users = ldap.searchGroup('user100', ['gidNumber'])
        self.assertEqual(len(users),0)

        # Ajouter un user
        attrs = {'cn':'user6','gidNumber':'6000','uidNumber':'6100','homeDirectory':'/home/user6','gecos':'user6 (u6@u.fr),99-99,,pic-itk00'}
        self.assertRaises(PicErreur, ldap.addUser, attrs)
        users = ldap.searchUser()
        self.assertEqual(len(users),6)
        users = ldap.searchUser('user6')
        self.assertEqual(len(users),1)

        # Supprimer un user
        self.assertRaises(PicErreur, ldap.delUser, 'user1')
        users = ldap.searchUser()
        self.assertEqual(len(users),6)
        
        # Supprimer un user qui n'existe pas
        self.assertRaises(PicErreur,ldap.delUser,'user555')

    def testgetset(self):
        '''Teste get/setNomCourt, get/setCont, get/setMail, get/setNomLong, get/setTel'''

        # Recherche de users
        ldap = Ldap(True)
        users = ldap.searchUser()
        self.assertEqual(len(users),6)

        self.assertEqual(ldap.getUserCn(users[0]), ['user1'])
        self.assertEqual(ldap.getUserNomCourt(users[0]),'user1')

        user = ldap.getUserNomCourt(users[2])

        # set/get
        self.assertEqual(ldap.getNomLong(user), 'nom long de user3')
        self.assertEqual(ldap.getMail(user),'u3@u.fr')
        self.assertEqual(ldap.getTel(user),'01-02')
        self.assertEqual(ldap.getCont(user),'pic-itk00')

        self.assertRaises(PicErreur,ldap.setNomLong,user, 'nom long modifie de user3')
        self.assertRaises(PicErreur,ldap.setMail, user, 'uu3@u.fr')
        self.assertRaises(PicErreur,ldap.setTel, user, '01-02-03')
        self.assertRaises(PicErreur,ldap.setCont,user, 'pic-itk02')

        self.assertEqual(ldap.getNomLong(user), 'nom long de user3')
        self.assertEqual(ldap.getMail(user),'u3@u.fr')
        self.assertEqual(ldap.getTel(user),'01-02')
        self.assertEqual(ldap.getCont(user),'pic-itk00')

#@unittest.skip("bouh")
class TestLdapUserGroupRw(unittest.TestCase):
    '''Teste addUserToGroup'''
    
    def setUp(self):
        initLdap()
        
    def tearDown(self):
        cleanLdap()

    def test_user_group(self):
        '''Teste addUserToGroup, getUsersInGroup et delUserFromGroup'''

        ldap = Ldap()

        # Ajout/suppression d'un user qui existe, ou pas à un groupe qui existe, ou pas
        self.assertRaises(PicErreur,ldap.addUserToGroup,'daltons','user1')

        self.assertRaises(PicErreur,ldap.addUserToGroup,'group7','averell')
        self.assertEqual(ldap.getUsersInGroup('group7'),[])

        self.assertRaises(PicErreur,ldap.addUserToGroup,'daltons','averell')
        self.assertRaises(PicErreur,ldap.delUserFromGroup,'daltons','user1')
        self.assertEqual(ldap.delUserFromGroup('group7','averell'),None)
        self.assertRaises(PicErreur,ldap.delUserFromGroup,'daltons','averell')
        
        # Rechercher la composition d'un groupe qui n'existe pas
        self.assertRaises(PicErreur,ldap.getUsersInGroup, 'daltons')

        # Recherche de group6, groupe vide
        groups = ldap.searchGroup('group6',['memberUid'])
        self.assertEqual(len(groups),1)
        self.assertEqual('memberUid' in groups[0]['attributes'], True)
        self.assertEqual(ldap.getUsersInGroup('group6'),[])
        
        # Ajout de user1 à group6
        ldap.addUserToGroup('group6','user1')
        groups = ldap.searchGroup('group6',['memberUid'])
        self.assertEqual(len(groups),1)
        self.assertEqual(len(groups[0]['attributes']['memberUid']),1)
        self.assertEqual(ldap.getUsersInGroup('group6'),['user1'])

        # Ajout du même user à group6: cela ne change rien
        ldap.addUserToGroup('group6','user1')
        groups = ldap.searchGroup('group6',['memberUid'])
        self.assertEqual(len(groups),1)
        self.assertEqual(len(groups[0]['attributes']['memberUid']),1)
        self.assertEqual(ldap.getUsersInGroup('group6'),['user1'])

        # Ajout de user2 à group6
        ldap.addUserToGroup('group6','user2')
        groups = ldap.searchGroup('group6',['memberUid'])
        self.assertEqual(len(groups),1)
        self.assertEqual(len(groups[0]['attributes']['memberUid']),2)
        self.assertEqual(sorted(ldap.getUsersInGroup('group6')),['user1','user2'])

        # Suppression d'un user qui n'est pas dans le groupe
        ldap.delUserFromGroup('group6','user4')
        groups = ldap.searchGroup('group6',['memberUid'])
        self.assertEqual(len(groups),1)
        self.assertEqual(len(groups[0]['attributes']['memberUid']),2)
        self.assertEqual(sorted(ldap.getUsersInGroup('group6')),['user1','user2'])

        # Suppression du groupe de user2 puis user1
        ldap.delUserFromGroup('group6','user2')
        groups = ldap.searchGroup('group6',['memberUid'])
        self.assertEqual(len(groups),1)
        self.assertEqual(len(groups[0]['attributes']['memberUid']),1)
        self.assertEqual(sorted(ldap.getUsersInGroup('group6')),['user1'])

        ldap.delUserFromGroup('group6','user1')
        groups = ldap.searchGroup('group6',['memberUid'])
        self.assertEqual(len(groups),1)
        self.assertEqual('memberUid' in groups[0]['attributes'], True)
        self.assertEqual(sorted(ldap.getUsersInGroup('group6')),[])

        # group1 est le groupe de login de user1
        self.assertEqual(ldap.getUsersInGroup('group1'),['user1'])

        # on ajoute user3 a group1
        ldap.addUserToGroup('group1','user3')
        self.assertEqual(sorted(ldap.getUsersInGroup('group1')),['user1','user3'])

        # on retire user3 de group1
        ldap.delUserFromGroup('group1','user3')
        self.assertEqual(ldap.getUsersInGroup('group1'),['user1'])
        
        # on ne PEUT PAS retirer user1 car group1 est son groupe de login
        # Mais il n'y a pas d'exception générée
        ldap.delUserFromGroup('group1','user1')
        self.assertEqual(ldap.getUsersInGroup('group1'),['user1'])

        # on supprime user1, group1 et group7 sont vides
        ldap.addUserToGroup('group7','user1')
        self.assertEqual(ldap.getUsersInGroup('group7'),['user1'])
        ldap.delUser('user1')
        self.assertEqual(ldap.getUsersInGroup('group1'),[])
        self.assertEqual(ldap.getUsersInGroup('group7'),[])

#@unittest.skip("bouh")
class TestLdapLoginShell(unittest.TestCase):
    '''Teste get/setLoginShell'''
    
    def setUp(self):
        initLdap()
        
    def tearDown(self):
        cleanLdap()

    def testLoginShellRw(self):

        ldap = Ldap()
        self.assertEqual(ldap.getLoginShell('user1'),'/bin/bash')
        self.assertEqual(ldap.getLoginShell('user2'),'/bin/zsh')
        self.assertEqual(ldap.getLoginShell('user3'),'')
        self.assertRaises(PicErreur, ldap.getLoginShell,'user9')
        
        self.assertEqual(ldap.setLoginShell('user1','/bin/csh'),None)
        self.assertEqual(ldap.setLoginShell('user2','/bin/zozosh'), None)
        self.assertEqual(ldap.setLoginShell('user3','/bin/bash'), None)
        self.assertEqual(ldap.getLoginShell('user1'),'/bin/csh')
        self.assertEqual(ldap.getLoginShell('user2'),'/bin/zozosh')
        self.assertEqual(ldap.getLoginShell('user3'),'/bin/bash')
        self.assertRaises(PicErreur, ldap.setLoginShell,'user9', '/bin/bash')

    def testLoginShellRo(self):

        ldap = Ldap(True)
        self.assertEqual(ldap.getLoginShell('user1'),'/bin/bash')
        self.assertEqual(ldap.getLoginShell('user2'),'/bin/zsh')
        self.assertEqual(ldap.getLoginShell('user3'),'')
        self.assertRaises(PicErreur, ldap.getLoginShell,'user9')
        
        self.assertRaises(PicErreur, ldap.setLoginShell,'user1','/bin/csh')
        self.assertEqual(ldap.getLoginShell('user1'),'/bin/bash')
        self.assertRaises(PicErreur, ldap.setLoginShell,'user9', '/bin/bash')


# On commence par un chdir dans /picgs/tests afin de charger le picconfig.py réservé aux tests !
if __name__ == '__main__':
    os.chdir ('/picgs/tests')

    #checkLdap()
    unittest.main()
    #cleanLdap()
    #initLdap()
    #checkLdap()
    
    
