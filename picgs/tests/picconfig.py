#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# Un picconfig.py minimaliste qui remplace le VRAI picconfig pour les tests
#
import sys
from erreur import PicErreur

def __LONG_MAX_NOM() -> int:
    ''' We define a constant, giving the max length of the user name part: if the user is monasso-st1, the name part is monasso'''
    return 32    


CONFIG  = {
    'FIRSTUID'    :10000,
    'FIRSTUID2'   :20000,
    'LASTUID'     :10999 ,
    'LDAPURI'     : 'ldap://ldap4tests',
    'LDAPADMIN'   : 'cn=admin,dc=example,dc=com',
    'LDAPBASEUSR' : 'ou=Users,dc=example,dc=com',
    'LDAPBASEGRP' : 'ou=Groups,dc=example,dc=com',
    'LDAPFLTSFTP' : '(gidNumber=500)',   # Filtre ldap groupe sftponly
    'LDAPPASSWDFILE' : 'slapd_password',
    'HOMES'       : '/home/',
    'FONCTIONS'   : ['site','galette','dolibarr','autre'],
    'CODEFCT'     : ['st','gt','db','au'],
    'LONG_MAX_NOM': __LONG_MAX_NOM(), 
    'DKRSRVWEB'   : []
}

def getConf(prm):
    '''renvoie la valeur du parametre de config passe en parametres, ou None'''
    try:
        return CONFIG[prm]

    except:
        return None

# TEST
if __name__ == '__main__':
    print(getConf('FIRSTUID'))
    print(getConf('NAWAK'))

