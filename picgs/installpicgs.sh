#! /bin/bash

# Script d'installation de picgs, en environnement houaibe
#
# Usage: sudo ./installpicgs.sh
#

[[ -z "$HOUAIBEDIR" ]] && HOUAIBEDIR="/usr/local/houaibe"

. ${HOUAIBEDIR}/etc/houaibe.conf

export PYTHONPATH="lib"
export HOUAIBEDIR

[[ ! -d $PICGSDIR ]] && mkdir $PICGSDIR
python3 install.py

# Appartenance au groupe docker
chgrp -R docker $PICGSDIR
