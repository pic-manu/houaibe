#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# MODULE homedir =  Definition de l'objet HomeDir
#

version = '0.1'

import sys
import os
import tempfile
import re
import time
import getpass
import xml
import json
from subprocess import *
from xml.dom import minidom
from plugin import Plugin
from ouinon import entrerFiltrer
from ouinon import ouiNon
from erreur import *
from picconfig import getConf
from affiche import *

class HomeDir(Plugin):

    @property
    def nomplugin(self)->str:
        return 'HomeDir'
    
    def __init__(self,action,nom,version,pic_gestion,options={}):
        self._cms = None
        self._cms_wp = False
        self._cms_wp_mig = False
        
        # Appel du constructeur du parent
        Plugin.__init__(self,action,nom,version,pic_gestion,options)

    def _calc_nom_archive(self, user, arch_path = None, verif = False) -> str:
        '''Retourne le nom du fichier tar ou tgz pour user
           arch_path est le nom du répertoire d'archive, si False il est calculé à partir de user
           Si verif est True, vérifie l'existence du fichier (plusieurs possibilités de nom)
           et si pas trouvé, lance une exception (utile en lecture)
           Si verif est False, ne vérifie pas (utile en écriture)'''

        conteneur = getConf('DKRMAINT')
        if arch_path == None:
            archives = getConf('ARCHIVES')
            arch_path = f"{archives}/{user}"

        s_dump_path0 = f"{arch_path}/{user}.tgz"
        
        # En écriture
        if not verif:
            return s_dump_path0

        # En lecture
        else:
            if self._dkr_fexists(s_dump_path0, conteneur):
                return s_dump_path0

            # Nommage libre
            else:
                cmd = f"cd {arch_path} && ls -1 *.tgz 2> /dev/null || true"
                out = self._system(cmd, conteneur)
                if len(out) == 0:
                    raise PicErreur(f"ERREUR - Pas de fichier .tgz dans le répertoire {os.path.dirname(arch_path)}")
                else:
                    s_dump_path2 = out[0]
                    return s_dump_path2

    # ATTENTION - Les méthodes suivantes ne sont pas des property, à cause de la présence de topdir
    def getCms(self,topdir=None) -> str:
        '''Essaie de determiner s'il y a un CMS dans topdir, ou dans le home - Renvoie un nom de CMS, ou NON DETECTE
           Si topdir vaut None -> on prend le homedir et on garde le résultat dans self.__cms         '''

        if topdir==None:
            if self._cms != None:
                return self._cms
            else:
                pg        = self.picgestion
                topdir    = pg.home
                sauve_cms = True
        else:
            sauve_cms = False

        # spip >= 1.9 ?
        rvl = self.getSpip(topdir)
        if rvl == '':
            # wordpress ?
            rvl = self.getWordpress(topdir)
            if rvl == '':
                rvl = self.getYeswiki(topdir)
                if rvl == '':
                    # drupal ?
                    rvl = self.getDrupal(topdir)
                    if rvl == '':
                        # Joomla! ?
                        rvl = self.getJoomla(topdir)
                        if rvl == '':
                            # galette ?
                            rvl = self.getGalette(topdir)
                            if rvl == '':
                                # dolibarr ?
                                rvl = self.getDolibarr(topdir)
                                if rvl == '':
                                    rvl = 'NON DETECTE'
        
        if sauve_cms:
            self._cms = rvl
        
        return rvl
        
    def getSpip(self,topdir=None) -> str:
        pg = self.picgestion
        if topdir==None:
            topdir = pg.home
            
        conteneur = getConf('DKRMAINT')
        
        # paquet.xml ou svn_revision.xml existe-t-il ?
        tmp_paquet   = '/tmp/paquet.xml'
        src_paquet   = ''
        
        for f in ['/www/ecrire/paquet.xml','/ecrire/paquet.xml']:
            if self._dkr_fexists(topdir+f,conteneur):
                src_paquet = topdir+f
                break

        # Pas trouvé = Pas spip (ou spip installé bizarrement)
        if src_paquet == '':
            return ''
        
        # Trouvé: on rapatrie et on analyse le fichier xml
        self._dkr_cpfrom(src_paquet,tmp_paquet,conteneur)

        # NOTE - Ne detecte QUE spip >= 3 !
        rvl = None

        try:
            xmldoc = xml.dom.minidom.parse(tmp_paquet)
            tags   = xmldoc.getElementsByTagName('paquet')
            if tags.length == 0:
                rvl = ''
            else:
                for tag in tags:
                    rvl = 'spip ' + tag.getAttribute('version')
                    break

        except:
            rvl = ''

        os.unlink(tmp_paquet)
        return rvl

    def isSpip(self, topdir=None) -> bool:
        return self.getSpip(topdir) != ''
        
    def getDrupal(self,topdir) -> str:
        ''' renvoie Drupal x.y si c'est du drupal, la chaine vide sinon'''

        #pg = self.picgestion
        #www_dir   = pg.www
        conteneur = getConf('DKRMAINT')

        # CHANGELOG existe-t-il ?
        tmp_changelog = '/tmp/CHANGELOG.txt'
        changelog     = topdir + '/www/CHANGELOG.txt'
        
        try:
            self._dkr_cpfrom(changelog,tmp_changelog,conteneur)
            
        except:
            return ''

        # Oui: est-ce le CHANGELOG de Drupal ?
        rvl=''
        try:
            fh_rev = open(tmp_changelog)
            i=0
            while i<10:
                ligne  = fh_rev.readline()
                sp = re.match('Drupal +([^ ,]+)',ligne)
                if sp != None:
                    version = sp.group(1)
                    rvl = 'Drupal ' + version
                    break
                i = i + 1

        except:
            rvl = ''
            
        os.unlink(tmp_changelog)
        return rvl

    def getWordpress(self,topdir=None) -> str:
        ''' renvoie WordPress x.y si c'est du Wordress, la chaine vide sinon'''
        pg     = self.picgestion
        user   = pg.user
        conteneur = getConf('DKRMAINT')

        if topdir == None:
            www_dir= pg.www
        else:
            www_dir= topdir + '/www'

        cmd = 'cd ' + www_dir + ' && wp core version 2>/dev/null'
        try:
            out = self._system(cmd,conteneur,False,user)
        except PicErreur as e:
            return ''
            
        return 'Wordpress ' + out[0]

    def isWordpress(self, topdir=None) -> bool:
        return self.getWordpress(topdir) != ''
 
    @property
    def cmswp(self) -> bool:
        return self._cms_wp
    
    @property
    def cmswpmig(self) -> bool:
        return self._cms_wp_mig

    def getYeswiki(self,topdir=None):
        ''' renvoie Yeswiki si c'est du Yeswiki, la chaine vide sinon'''
        
        pg     = self.picgestion
        user   = pg.user
        conteneur = getConf('DKRMAINT')

        if topdir == None:
            www_dir= pg.www
        else:
            www_dir= topdir + '/www'

        tmp_constants = '/tmp/constants.php'
        constants = f"{www_dir}/includes/constants.php";

        try:
            self._dkr_cpfrom(constants,tmp_constants,conteneur)
            
        except:
            return ''

        # Oui: est-ce le constants de YesWiki ?
        rvl=''
        try:
            fh_rev = open(tmp_constants)
            i=0
            while i<10:
                ligne  = fh_rev.readline()
                sp = re.search('YESWIKI_VERSION',ligne)
                if sp != None:
                    rvl = 'Yeswiki'
                    break
                i = i + 1

        except:
            rvl = ''
            
#        os.unlink(tmp_constants)
        return rvl
                   
    def getJoomla(self,topdir) -> str:
        ''' renvoie Joomla! x.y si c'est Joomla!, la chaine vide sinon'''
        #pg = self.picgestion
        www_dir   = topdir + '/www'
        conteneur = getConf('DKRMAINT')

        # On cherche le fichier libraries/src/Version.php, et dedans l'indication de la version de Joomla!
        cmd0 = 'grep PRODUCT ' + www_dir + '/libraries/src/Version.php ' + www_dir + '/joomla/libraries/src/Version.php 2>/dev/null; echo FIN';
        cmd1 = 'grep MAJOR_VERSION ' + www_dir + '/libraries/src/Version.php ' + www_dir + '/joomla/libraries/src/Version.php 2>/dev/null; echo FIN';
        cmd2 = 'grep MINOR_VERSION ' + www_dir + '/libraries/src/Version.php ' + www_dir + '/joomla/libraries/src/Version.php 2>/dev/null; echo FIN';
        cmd3 = 'grep PATCH_VERSION ' + www_dir + '/libraries/src/Version.php ' + www_dir + '/joomla/libraries/src/Version.php 2>/dev/null; echo FIN';
        grepped = '';
        
        # grep retournera un code 2 (fichier non trouve) car il ne doit y en avoir qu'un        
        try:
            grepped = self._system(cmd0,conteneur)
            
        except:
            pass

        if grepped[0]=='FIN':
            return ''
        else:
            v = grepped[0].partition('=')[2]
            product = v.strip(' \';')

        try:
            grepped = self._system(cmd1,conteneur)
            
        except:
            pass

        if grepped[0]=='FIN':
            return ''
        else:
            v = grepped[0].partition('=')[2]
            major = v.strip(' ;')

        try:
            grepped = self._system(cmd2,conteneur)
            
        except:
            pass

        if grepped[0]=='FIN':
            return ''
        else:
            v = grepped[0].partition('=')[2]
            minor = v.strip(' ;')

        try:
            grepped = self._system(cmd3,conteneur)
            
        except:
            pass

        if grepped[0]=='FIN':
            return ''
        else:
            v = grepped[0].partition('=')[2]
            patch = v.strip(' ;')
            
        return product + " " + major + '.' + minor + '.' + patch

    def getGalette(self,topdir=None) -> str:
        ''' renvoie Galette x.y si c'est Galette, la chaine vide sinon'''

        if topdir == None:
            pg     = self.picgestion
            topdir = pg.home

        conteneur = getConf('DKRMAINT')

        # On cherche différents fichiers avec dedans la chaine GALETTE_VERSION afin de détecter la version de galette qu'elle soit < ou > 1.0.0
        fichiers = ['/galette/galette/config/versions.inc.php','/galette/config/versions.inc.php','/galette/galette/includes/galette.inc.php', '/www/includes/galette.inc.php', '/galette/includes/galette.inc.php', '/galette/galette/includes/sys_config/versions.inc.php']
        fichiers_str = ' '.join([ topdir + f for f in fichiers ])
        grepped = ''
        try:
            grepped = self._system(f"grep GALETTE_VERSION {fichiers_str} 2>/dev/null; echo -n ''",conteneur)
        except PicErreur:
            pass

        if grepped[0] == '':
            return ''
        else:
            v = grepped[0].partition(',')[2]
            return "Galette " + v.strip(' ;\')')

    def isGalette(self, topdir=None) -> bool:
        return self.getGalette(topdir) != ''

    def getDolibarr(self,topdir=None) -> str:
        ''' renvoie Dolibarr x.y si c'est Dolibarr, la chaine vide sinon
            top_dir = Le directory dont on part pour rechercher          '''

        if topdir == None:
            pg     = self.picgestion
            topdir = pg.home
        
        conteneur  = getConf('DKRMAINT')

        # On cherche le fichier htdocs/filefunc.inc.php, et dedans l'indication de la version de Dolibarr
        filefunc_inc = ''
        fichiers=['/dolibarr/htdocs/filefunc.inc.php', '/www/dolibarr/htdocs/filefunc.inc.php', '/htdocs/filefunc.inc.php']
        for f in fichiers:
            if self._dkr_fexists(topdir + f,conteneur):
                filefunc_inc = topdir + f
                break
        
        # Fichier pas trouve
        if filefunc_inc == '':
            return ''

        # Fichier trouvé: on recherche la chaine define('DOL_VERSION', '12.0.5')
        cmd = f"grep -o -e define\(\\\'DOL_VERSION\\\'.*\) {filefunc_inc} 2>/dev/null; echo FIN"
        #cmd = 'grep DOL_VERSION ' + filefunc_inc + ' 2>/dev/null; echo FIN';
        #cmd = f"grep -o -e define\(\'DOL_VERSION\'.*\) {filefunc_inc} 2>/dev/null; echo FIN"
        grepped = self._system(cmd,conteneur)
        if grepped[0]=='FIN':
            return ''

        # chaine trouvée: on l'exécute !'
        else:
            phpcmd = f"php -r \"{grepped[0]}; echo DOL_VERSION;\" "
            v = self._system(phpcmd,conteneur)
            if len(v) > 0:
                return f"Dolibarr {v[0]}" 

    def isDolibarr(self, topdir=None) -> bool:
        return self.getDolibarr(topdir) != ''

    @property
    def chkpicadmin(self) -> bool:
        '''Cette fonction est définie pour spip/wordpress/dolibarr/galette
           Elle renvoie True si l'état de picadmin est tel qu'on puisse faire une mise à jour du CMS'''
        
        if self.isSpip():
            return self.__chkPicadminSpip()
        if self.isWordpress():
            return self.__chkPicadminWp()
        if self.isDolibarr():
            return self.__chkPicadminDolibarr()
        if self.isGalette():
            return self.__chkPicadminGalette()
            
        return False
        
    def __chkPicadminWp(self) -> bool:
        '''Précondition: On est dans un site Wordpress - renvoie True si cette instance de wordpress a un compte "picadmin" !'''

        pg        = self.picgestion
        user      = pg.user
        www_dir   = pg.www;
        conteneur = getConf('DKRMAINT')

        cmd = "cd " + www_dir + " && wp user get picadmin"
        try:
            self._system(cmd,conteneur,False,user)
        except PicErreur as e:
            return False
            
        return True

    def __chkPicadminDolibarr(self) -> bool:
        '''Précondition: On est dans un site Dolibarr - renvoie True si cette instance de Dolibarr a un compte "picadmin" !
           Renvoie True tout le temps, car la règle du user "picadmin" ne s'applique pas aux sites Dolibarr'''

        return True

    def __chkPicadminGalette(self) -> bool:
        '''Précondition: On est dans un site Galette - renvoie True si cette instance de Galette a un compte "picadmin" !
           Renvoie True tout le temps, car la règle du user "picadmin" ne s'applique pas aux sites Galette'''

        return True

    def __chkPicadminSpip(self) -> bool:
        '''Précondition: On est dans un site spip - renvoie True si cette instance de spip a un compte "picadmin" 
           OU si on est dans une mutu spip !'''
        
        pg   = self.picgestion
        uid  = pg.uid
        user = pg.user
        www  = pg.www
        conteneur = getConf('DKRMAINT')
        
        # Si mutu = Oui 
        if self._dkr_fexists('{}/sites'.format(www),conteneur):
            return True
        
        cmd = "cd {} && spip auteurs:lister -n --no-ansi | tr -s ' ' | sed -e 's/^ //' | cut -d' ' -f 2|grep picadmin".format(www)
        
        # Si picadmin pas trouvé, cmd renvoie 1 ce qui génère une exception
        rvl = True
        try:
            res = self._system(cmd,conteneur,False,user)
        except:
            rvl = False
        
        return rvl
        
    def _selectionnerZip(self,prefixe="spip",extension="zip") -> str:
        ''' Quel fichier zip est cense contenir spip ou autre logiciel ?'''
        
        conteneur = getConf('DKRMAINT')
        les_zips = getConf('HOMES') + prefixe + '-*.*.' + extension
        print ("Quel fichier voulez-vous utiliser ?")
        print
        while True:
            cmd = 'ls -l ' + les_zips
            print ("\n".join(self._system(cmd, conteneur)))
            fichier = input('Entrez un nom de fichier ')
            if self._dkr_fexists(fichier,conteneur):
                break
            print ("Ce fichier n'existe pas !")
        
        return fichier
        
    def _homeProprio(self) -> None:
        ''' Fixe le proprio et les permisisons pour le home directory'''

        pg = self.picgestion
        home = pg.home
        user = pg.user
        maint = getConf('DKRMAINT')
        www = home + '/www' 
        log = home + '/log'
        priv= home + '/priv'
        db  = home + '/db'

        self._system(f"chmod g+s {home} {www} {db} {priv} {log}", maint)
        # {user} renvoie le user unix, qui est AUSSI le group unix
        self._system(f"chown -R {user}:{user} {www} {priv}",maint)
        self._system(f"chown root:{user} {home} {db} {log}",maint)

    def _videLeCache(self, sites:str|list, silence=False) -> None:
        '''Vide le cache du ou des sites spip passé(s) en parametres
           Initialement écrit pour la mutu, fonctionne aussi avec les sites spip hors mutu
           sites = str (si un seul site) ou list (si plusieurs)'''

        pg = self.picgestion
        www = pg.www
        user = pg.user
        user_mutu = getConf('MUTUNOM') + getConf('MUTUVER')
        if isinstance(sites, str):
            sites = [ sites ]
        conteneur  = getConf('DKRMAINT')

        for site in sites:
            if user == user_mutu:
                sitedirtmp = f"{www}/sites/{site}/tmp"
                sitedirlocal = f"{www}/sites/{site}/local"
            else:
                sitedirtmp = f"{www}/tmp"
                sitedirlocal = f"{www}/local"
                
            if not silence:
                print (f"=== Vidage du cache de  {site}")
    
            # On supprime simplement les fichiers de cache par un rm
            # La commande spip cache:vider ne fonctionne pas s'il y a un gros souci de cache !
            cmd = f"cd {sitedirtmp} && rm -rf plugin_xml_cache.gz meta_cache.php cache/*"
            self._system(cmd,conteneur)
        
            # On retire les caches qui se trouvent dans local
            cmd = f"cd {sitedirlocal} && rm -rf cache-css cache-gd2 cache-js cache-vignettes cache-responsive cache-centre-image cache-rainette config.txt"
            self._system(cmd,conteneur)
        
            # Finalement on fait un chown pour être sur de ne pas avoir de fichiers appartenant à root
            cmd = f"chown -R {user}:{user} {sitedirtmp} && chmod -R g+w {sitedirtmp}"
            self._system(cmd,conteneur)

    def _exclusionListe2String(self, l_excl) -> str:
        '''l_excl = Un tableau de sous-répertoires à exclure
           Retourne un string utilisable pour tar, du, ...'''
        
        return ' '.join([ '--exclude='+x for x in l_excl])
        
    def _Taille(self, dir="", exclusion=[]) -> str:
        '''Renvoie la taille prise par le repertoire passé en paramètres, home par défaut
           exclusion est une liste d'exclusion, permettant d'exclure des sous-répertoires'''
        
        pg = self.picgestion
        if dir == "":
            dir = pg.home

        conteneur= getConf('DKRMAINT')

        if not self._dkr_fexists(dir,conteneur):
            return "0"

        else:
            cmd = f'du -sh {self._exclusionListe2String(exclusion)} {dir}'
            taille = self._system(cmd, conteneur)
            return taille[0].split('\t',2)[0]

    def _permissionsPourSpip(self) -> None:
        user     = self.picgestion.user
        user2    = 'w' + user
        group    = user
        spip_inst_dir = self.picgestion.www
        conteneur= getConf('DKRMAINT')

        # Crer les repertoires plugins et plugins/auto
        cmd      = 'mkdir -p ' + spip_inst_dir + '/plugins/auto'
        self._system(cmd,conteneur)

        # Par defaut les fichiers peuvent être lus par le serveur web, mais pas modifiés
        cmd = f'chown -R {user}:{group} {spip_inst_dir}'
        self._system(cmd,conteneur)
        cmd = f'chmod -R o= {spip_inst_dir}'
        self._system(cmd,conteneur)
        cmd = f'chmod -R g-w {spip_inst_dir}'
        self._system(cmd,conteneur)

        # Le serveur web doit être capable d'écrire dans certains répertoires
        cmd      = 'chmod -R g+rwX ' + spip_inst_dir + '/config'
        self._system(cmd,conteneur)
        cmd      = 'chmod -R g+rwX ' + spip_inst_dir + '/IMG' 
        self._system(cmd,conteneur)
        cmd      = 'chmod -R g+rwX ' + spip_inst_dir + '/tmp'
        self._system(cmd,conteneur)
        cmd      = 'chmod -R g+rwX ' + spip_inst_dir + '/local/'
        self._system(cmd,conteneur)
        cmd      = 'chmod -R g+rwX ' + spip_inst_dir + '/plugins/auto'
        self._system(cmd,conteneur)
        
        # vendor est donc accessible en écriture par le serveur web ce qui n'est pas terrible
        # Mais c'est indispensable pour permettre au code d'installation de spip
        # de créer un .htaccess dans le répertoire vendor... et peut-être plus !
        cmd = f'chmod -R g+rwX {spip_inst_dir}/vendor'
        self._system(cmd,conteneur)
        
        # Si on est dans une mutu, il y a d'autres repertoires dans lesquels le serveur doit pouvoir ecrire
        sites_dir = spip_inst_dir + '/sites'
        if self._dkr_fexists(sites_dir,getConf('DKRMAINT')):
            cmd = 'chmod -R ug=rwX ' + sites_dir
            self._system(cmd,conteneur)
        
    def _CacheTag(self,spip_inst_dir=None) -> None:
        '''Cree un fichier CACHEDIR.TAG dans les repertoires de cache afin que tar ne sauvegarde pas le cache'''

        if spip_inst_dir==None:
            spip_inst_dir=self.picgestion.www

        user=self.picgestion.user
        group=user
        conteneur = getConf('DKRMAINT')
        
        if re.match('spip 3.*',self.getCms(),re.I) != None:
            tag='''Signature: 8a477f597d28d172789f06886806bc55
# Ce fichier est une etiquette creee par le PIC pour archiver le site.
# Il peut etre efface sans probleme
# For information about cache directory tags, see:
#   http://www.brynosaurus.com/cachedir/'''
            f_tag  = spip_inst_dir +'/local/CACHEDIR.TAG' 
            f_tag_1= spip_inst_dir + '/tmp/cache/CACHEDIR.TAG'

            [fd_tag, tmp_tag] = tempfile.mkstemp();
            out = os.fdopen(fd_tag,'w')
            out.write(tag)
            out.close();
            
            self._dkr_cpto(tmp_tag,f_tag,conteneur)
            self._dkr_cpto(tmp_tag,f_tag_1,conteneur)

            cmd = 'chown ' + user + ':' + group + ' ' + f_tag + ' ' + f_tag_1
            self._system(cmd,conteneur)

            os.unlink (tmp_tag)

    def __str__(self):
        rvl = 'plugin homedir\n'
        rvl += '-----------------------------------------------\n'
        return rvl


# Contient les méthodes pour installation automatique des CMS
class HomeDirInstallation(HomeDir):

    def _getAdminMails(self) -> list[str]:
        '''Renvoie dans un tableau les mails des administrateurs'''
        pg = self.picgestion
        user = pg.user
        www = pg.www
        conteneur = getConf('DKRMAINT')

        cmd = f"cd {www} && wp user list --role=administrator --field=user_email --format=json 2>/dev/null"
        try:
            out = self._system(cmd, conteneur, False, user)
            conf = json.loads(out[0])
            return conf
            
        except PicErreur as e:
            print(afficheJaune(f"{cmd} ne renvoie pas un json correct"))
            return []
                        
    def _configEasyUpdateManager(self) -> None:
        pg = self.picgestion
        user = pg.user
        www = pg.www
        conteneur = getConf('DKRMAINT')
        #mail = pg.mail
        #adminmail = getConf('ADMINMAIL')
        cmd = f"cd {www} && wp option get MPSUM --format=json 2>/dev/null"
        
        try:
            out = self._system(cmd, conteneur, False, user)
            conf = json.loads(out[0])
            
        except PicErreur as e:
            afficheJaune(f"{cmd} ne renvoie pas un json correct, Easy Update Manager est-il correctement installé ?")
            return

        mail_admins = self._getAdminMails()
        conf['core']['email_addresses'] = mail_admins
        conf['core']['core_updates'] = 'automatic'
        conf['core']['notification_core_update_emails'] = 'on'
        conf['core']['plugin_auto_updates_notification_emails'] = 'on'
        conf['core']['plugin_updates'] = 'automatic'
        conf['core']['theme_auto_updates_notification_emails'] = 'on' 
        conf['core']['theme_updates'] = 'automatic'
        conf['core']['translation_auto_updates_notification_emails'] = 'on'
        conf['core']['translation_updates'] = 'automatic'
        
        cmd = f"cd {www} && echo '{json.dumps(conf)}' | wp option update MPSUM --format=json"
        
        try:
            out = self._system(cmd, conteneur, False, user)
            
        except PicErreur as e:
            afficheJaune(f"{cmd} renvoie une erreur")
            return

    def _configSftp(self, user, ftppass, ftphost) -> None:
        '''Wordpress: Configuration de ssh-sftp...'''
        
        pg = self.picgestion
        www= pg.www
        conteneur = getConf('DKRMAINT')

        params =  f'''"
/* CONFIGURATION DU PLUGIN ssh-sftp-updater-support PAR HOUAIBE */
define('FS_METHOD', 'ssh2');
define('FTP_USER', '{user}');
define('FTP_PASS', '{ftppass}');
define('FTP_HOST', '{ftphost}');
define('FTP_SSL', true);
"'''
        
        cmd = f"echo {params} >>{www}/wp-config.php"
        self._system(cmd, conteneur, False, user)

    def _chmodForWordpress(self) -> None:
        '''Positionne correctement les permissions dans www pour un site wordpress'''
        
        pg = self.picgestion
        www = pg.www
        conteneur = getConf('DKRMAINT')

        print ("=== Mise en place des permissions pour wp-content et .htaccess")
        cmd = f"touch {www}/.htaccess && chmod -R u=rwX,g=rX,o= {www}"
        self._system(cmd, conteneur)

        cmd = f"chmod -R g+w {www}/wp-content {www}/.htaccess"
        self._system(cmd, conteneur)

    def _pluginsForWordpress(self, plugins) -> None:
        '''Installe les plugins passés dans le paramètre plugins (un tableau)'''        

        pg = self.picgestion
        www = pg.www
        conteneur = getConf('DKRMAINT')
        user = pg.user
                
        for pl in plugins:
            self._systemWithWarning(f"cd {www} && wp plugin install {pl} --activate", conteneur, False, user)
        
        self._systemWithWarning(f"cd {www} && wp language plugin install --all fr_FR", conteneur, False, user)

    def _installWordpress(self) -> None:
        ''' D'après https://github.com/plaurent75/WordpressInstaller'''

        pg     = self.picgestion
        home   = pg.home
        www    = pg.www
        dbname = pg.database
        dbuser = pg.user
        dbhost = getConf('HSTMYSQL')
        user   = pg.user
        ftppass= pg.password
        ftphost= f"sftp.{getConf('DOMAINNAME')}:2200"
        mail   = pg.mail
        dbpass = pg.dbpassword
        #picpass= self._picpass
        picpass = self._genPassword()
        wpurl  = f"https://{user}.{getConf('DOMAINNAME')}"
        wptitle="TITRE"

        conteneur = getConf('DKRMAINT')
        wplang    = 'fr_FR'

        print ("==== Telechargement de wordpress")
        cmd       = f"cd {www} && wp core download --force --locale={wplang}"
        self._system(cmd, conteneur,False,user)
        cmd       = f"cd {www} && wp core version"
        self._system(cmd,conteneur,False,user)
        print ("==== Creation de wp-config.php ....")

#       define('AUTOSAVE_INTERVAL', 300 );
#       define('WP_POST_REVISIONS', false );
#       define( 'WP_AUTO_UPDATE_CORE', true );
#       define( 'WP_DEBUG', false );

        cmd = f"cd {www} && wp core config --dbname={dbname} --dbuser={dbuser} --dbpass={dbpass}"
        if dbhost != 'localhost':
            cmd += f" --dbhost={dbhost}"
        self._system(cmd,conteneur,False,user)
        
        print ("==== Wordpress est en cours d'installation")
        cmd  = f"cd {www} && wp core install --url={wpurl} --title={wptitle} --admin_user={getConf('ADMIN')}"
        cmd += f" --admin_email={getConf('ADMINMAIL')} --admin_password={picpass}"
        self._system(cmd,conteneur,False,user)
        
        print ("==== Installation du language FR")
        self._system(f"cd {www} && wp language core activate fr_FR", conteneur,False,user)
        
        print ("==== Vidage du site")
        self._system(f"cd {www} && wp site empty",conteneur,False,user)
        
        # PARAMETRAGE GENERAL
        print ("==== modification des options en cours")
        self._system(f"cd {www} && wp option update blog_public 0",conteneur,False,user)
        self._system(f"cd {www} && wp option update timezone_string Europe/Paris",conteneur,False,user)
        self._system(f"cd {www} && wp option update date_format 'j F Y'",conteneur,False,user)
        self._system(f"cd {www} && wp option update time_format 'G \h i \m\i\n'",conteneur,False,user)

        # NETTOYAGE
        print ( "==== Un peu de menage.....")
        self._systemWithWarning(f"cd {www} && wp post delete 1 --force", conteneur,False,user) # Article exemple - no trash. Comment is also deleted
        self._systemWithWarning(f"cd {www} && wp post delete 2 --force", conteneur,False,user) # page exemple
        self._systemWithWarning(f"cd {www} && wp plugin delete hello", conteneur,False,user)
        self._systemWithWarning(f"cd {www} && wp theme delete twentytwelve", conteneur,False,user)
        self._systemWithWarning(f"cd {www} && wp theme delete twentythirteen", conteneur,False,user)
        self._systemWithWarning(f"cd {www} && wp theme delete twentyfourteen", conteneur,False,user)

        # CREER UN ADMIN
        print (f"==== Creation de l'admin {user}")
        self._systemWithWarning(f"cd {www} && wp user create {user} {mail} --role=administrator", conteneur, False, user)
                
        # PLUGINS
        print ("==== Installation des plugins .....")
        # Non installés: akismet, cookie-law-info, jetpack, wordpress-seo, really-simple-captcha
        plugins = [ 'contact-form-7','honeypot','ssh-sftp-updater-support','tablepress', 'stops-core-theme-and-plugin-updates', 'cache-control' ]
        self._pluginsForWordpress(plugins)
        
        print ("==== Configuration du plugin ssh-sftp-updater-support")
        self._configSftp(user, ftppass, ftphost)

        print ("==== Configuration du plugin easy-update-manager")
        self._configEasyUpdateManager()
        
        # CHANGER LES PERMISSIONS
        self._chmodForWordpress()
        
        print (f"=== C'est fini, allez donc faire un tour sur {wpurl}/wp-admin")
        print (f"=== Utilisateur = {getConf('ADMIN')} - mdp = {picpass}")
    
    def _migrationWordpress(self) -> None:
        '''Préparer la migration de wordpress en créant le fichier priv/ident.json'''

        pg = self.picgestion
        conteneur = getConf('DKRMAINT')
        user = pg.user
        priv = pg.priv

        prms = {}
        prms['DB_NAME'] = pg.database
        prms['DB_USER'] = pg.user
        prms['DB_HOST'] = getConf('HSTMYSQL')
        prms['DB_PASSWORD'] = pg.dbpassword
        prms['FS_METHOD'] = 'ssh2'
        
        prms['FTP_USER'] = pg.user
        prms['FTP_PASS'] = pg.password
        prms['FTP_HOST'] = f"sftp.{getConf('DOMAINNAME')}:2200"
        
        #prms['src_siteurl'] = self._src_siteurl
        
        Plugin.message += "\n"
        Plugin.message += f"MIGRATION DE WORPRESS\n"
        Plugin.message += "================================================\n"
        Plugin.message += "Pour installer votre site vous devrez:\n"
        Plugin.message += "   - Déposer par sftp les fichiers du site dans le répertoire www\n"
        Plugin.message += "   - Déposer par sftp le fichier .sql, .sql.gz ou .sql.zip dans le répertoire priv \n"
        Plugin.message += "   - Vous connecter via ssh et appeler la commande: picwpmigrate\n"

        cmd = f"cat > {priv}/ident.json"
        self._system(cmd, conteneur, False, user, [], json.dumps(prms))
        cmd = f"chmod go= {priv}/ident.json"
        self._system(cmd, conteneur, False, user)

    def _installGalette(self) -> None:
        '''Installation automatique de Galette'''

        pg               = self.picgestion
        home             = pg.home
        user             = pg.user
        www              = home + '/www' 
        galette_path_zip = self._galette_path
        conteneur        = getConf('DKRMAINT')
        
        print ( "=== Installation des fichiers de galette")
        # Supprimer www (qui vient juste d'etre cree !)
        self._system(f"rm -r {www}",conteneur)

        # Donner provisoirement la permission groupe a home
        self._system(f"chmod g+w {home}", conteneur)
        
        # Deposer les fichiers de galette dans le home
        cmd = f"cd {home} && tar xf {galette_path_zip}"
        self._system(cmd, conteneur, False)

        # Decouvrir le nom du repertoire
        cmd = f"cd {home} && ls -d galette-*"
        galette_dir = self._system(cmd,conteneur)[0]
        
        # Changer le nom pour "galette"
        if galette_dir != "galette":
            self._dkr_rename(f"{home}/{galette_dir}", f"{home}/galette",conteneur)

        # Transférer la propriete de galette_dir
        cmd = f"chown -R root:{user} {home}/galette"
        self._system(cmd,conteneur)

        # www devient un lien symbolique vers galette/webroot
        cmd = f"cd {home} && ln -s galette/galette/webroot www"
        self._system(cmd,conteneur)
        
        # Mettre le repertoire config en rw
        cmd = f"cd {home} && chmod ug=rwx galette/galette/config"
        self._system(cmd, conteneur)
        
        # Mettre les repertoires en-dessous de data en rw, ainsi que le répertoire data lui-même
        cmd = f"cd {home}/galette/galette && chmod ug=rwx data data/attachments data/cache data/exports data/files data/imports data/logs data/photos data/tempimages data/templates_c"
        self._system(cmd,conteneur)
        
        # Retirer la permission donnee precedemment
        self._system(f"chmod g-w {home}", conteneur)
        
    def _installDolibarr(self) -> None:
        '''Installation automatique de Dolibarr'''

        pg             = self.picgestion
        home           = pg.home
        user           = pg.user
        www            = home + '/www' 
        dolib_path_zip = self._dolib_path
        conteneur      = getConf('DKRMAINT')
        
        print ( "=== Installation des fichiers de Dolibarr")
        # Supprimer www (qui vient juste d'etre cree !)
        self._system("rm -r " + www,conteneur)

        # Donner provisoirement la permission groupe a home
        self._system("chmod g+w " + home, conteneur)
        
        # Deposer les fichiers de Dolibarr dans le home
        cmd = "cd " + home + " && tar xf " + dolib_path_zip
        self._system(cmd, conteneur, False)
        
        # Decouvrir le nom du repertoire
        cmd = "cd " + home + " && ls -d dolibarr-*"
        dolibarr_dir = self._system(cmd,conteneur)[0]
        
        # Changer le nom pour "dolibarr"
        if dolibarr_dir != "dolibarr":
            self._dkr_rename(home + '/' + dolibarr_dir, home + "/dolibarr",conteneur)
        
        # Changer la propriété des fichiers
        print ( "=== Changement de propriété des fichiers Dolibarr")
        cmd = f"chown -R '{user}:' {home}/dolibarr"
        self._system(cmd, conteneur)

        # www devient un lien symbolique vers htdocs
        cmd = "cd " + home + " && ln -s dolibarr/htdocs www"
        self._system(cmd,conteneur, False, user)
        
        # Creer le fichier conf.php
        cmd = "cd " + home + " && touch dolibarr/htdocs/conf/conf.php && chmod ug=rw " + "dolibarr/htdocs/conf/conf.php"
        self._system(cmd, conteneur, False, user)
        
        # creer le repertoire documents avec les bons droits
        repdoc = 'dolibarr/documents'
        cmd = "cd " + home + "&& mkdir " + repdoc + " && chmod ug+rw " + repdoc
        self._system(cmd,conteneur,False,user)
        
        # Retirer la permission donnee precedemment
        self._system("chmod g-w " + home, conteneur)
        
    def _installSpip(self) -> None:
        '''Installation automatique de Spip'''
        
        pg = self.picgestion;
        user = pg.user
        spip_zip_path = self._selectionnerZip()
        home_dir = pg.home
        spip_inst_path = pg.www
        spip_dir_path = f"{spip_inst_path}/spip"
        conteneur = getConf('DKRMAINT')

        # Quelques verifications avant de commencer
        if self.isSpip():
            raise PicErreur ("ERREUR - spip deja installe !")
        if not self._dkr_fexists(spip_zip_path,conteneur):
            raise PicErreur ("ERREUR - Le fichier " + spip_zip_path + "n'existe pas, merci de le telecharger !")
        if self._dkr_fexists(spip_dir_path,conteneur):
            raise PicErreur ("ERREUR - Le repertoire " + spip_dir_path + " existe deja ! Supprimez-le et recommencez")

        print()
        print(f"CONTENU ACTUEL DU REPERTOIRE {spip_inst_path}")
        print(f"# ls -l {spip_inst_path}")
        cmd = f"ls -l {spip_inst_path}"
        rvl = self._system(cmd, conteneur)
        print("\n".join(rvl))

        # Extraction du fichier zip
        print(f"Extraction de {spip_zip_path}  dans {spip_dir_path}")
        cmd = f"mkdir {spip_dir_path} && unzip {spip_zip_path} -d {spip_dir_path} >/dev/null 2>&1"
        self._system(cmd,conteneur)
        print("OK")

        print()
        print(f"Version en cours d'installation: {self.getSpip(spip_dir_path)}")

        if not ouiNon("Continuer l'installation ?",False):

            # supprimer le repertoire temporaire spip puis annuler
            cmd = f"rm -r {spip_dir_path}"
            self._system(cmd,conteneur)
            raise PicFatalErreur ("ANNULATION")
    
        # copie des fichiers
        cmd = f"cp -r {spip_dir_path}/* {spip_inst_path}"
        self._system(cmd,conteneur)

        # Mettre les permissions correctes
        self._permissionsPourSpip()

        # supprimer le repertoire temporaire spip
        cmd = f"rm -r {spip_dir_path}"
        self._system(cmd,conteneur)

class HomeDirInfo(HomeDir):
    def execute(self) -> None:
        home_dir = self.picgestion.home
        options = self.options

        www = self.picgestion.www + '/'
        wwws = self.picgestion.wwws + '/'

        t_ttl = self._Taille()
        t_www = self._Taille(www)
        t_wwws = self._Taille(wwws)
        
        print(f"Home        : {home_dir}")
        print(f"CMS         : {self.getCms()}")
        if '--short' in options:
            return
            
        print(f"Taille      : {t_ttl}")
        print(f"Taille www  : {t_www}")
        if t_wwws != "0":
            print(f"Taille wwws : {t_wwws}")
        if self.isSpip() or self.isWordpress():
            if self.chkpicadmin:
                print("Compte picadmin: OUI")
            else:
                print("Compte picadmin: NON")
            

class HomeDirMdp(HomeDir):

    def __executeMutu(self) -> None:
        '''Appelé par execute lors du traitement de la mutu (spip)'''
        
        pg = self.picgestion
        options = self.options
        filtre = ''
        if '--sites' in options:
            filtre = options['--sites']

        www = pg.www
        home = pg.home
        user = pg.user
        conteneur = getConf('DKRMAINT')

        sites = [ s for s in pg.sites if s.startswith(filtre) ]
        
        if len(sites) == 0:
            print (afficheRouge(f"Pas de site dont le nom commence par {filtre} dans la mutu "))
            raise Exception
        
        mdp = self._genPassword()
        for site in sites:
            try:
                sitedir = f"{home}/www/sites/{site}"
                if self._dkr_f_lien(sitedir,conteneur):
                    continue
                
                print (f"Changement de mot de passe picadmin pour {site}")
                cmd = f"cd {www}/sites/{site} && spip auteurs:changer:mdp --login=picadmin --mdp={mdp}"
                self._system(cmd,conteneur,False,user)

            except Exception as e:
                print(afficheRouge(f"ERREUR - {cmd} renvoie une erreur"))

        print (afficheVert(f"Nouveau mot de passe picadmin = {mdp}"))


    def __execute(self,wp, spip) -> None:
        '''Appelé par execute lors du traitement d'un site hors mutu (spip ou wp)'''
        pg = self.picgestion
        options = self.options
        conteneur = getConf('DKRMAINT')
        user = pg.user
        www = pg.www

        if '--sites' in options:
            print (afficheRouge("ERREUR - L'option --sites est réservée à la mutu spip !"))
            raise Exception
                    
        if not self.chkpicadmin:
            print (afficheRouge("ERREUR - Ce site n'a pas de compte picadmin"))
            raise Exception

        else:
            mdp = self._genPassword()
            if spip:
                cmd = f"cd {www} && spip auteurs:changer:mdp --login=picadmin --mdp={mdp}"
            elif wp:
                cmd = f"cd {www} && wp user update picadmin --user_pass={mdp}"
            else:
                return

            try:
                self._system(cmd,conteneur,False,user)
                print (afficheVert(f"Nouveau mot de passe picadmin = {mdp}"))

            except PicErreur as e:
                raise  Exception(f"ERREUR - {cmd} renvoie une erreur")

    def execute(self) -> None:
        pg = self.picgestion
        user = pg.user
        mutu_user = pg.mutuuser

        spip = None
        wp = None
        mutu = False
        
        # Si c'est la mutu, c'est du spip !
        if user == mutu_user:
            mutu = True
            spip = self.getSpip()
            
        # Hors mutu: on teste d'abord wp puis spip si besoin
        else:
            wp = self.getWordpress()
            if wp == '':
                spip = self.getSpip()

        if spip == '' and wp == '':
            raise (PicErreur("ERREUR - Ce n'est pas un site web sous spip ni sous wordpress"))
        
        if mutu == True:
            self.__executeMutu()
        else:
            self.__execute(wp,spip)
                
class HomeDirModification(HomeDirInstallation):
    '''Si wordpress, mettre à jour le paramètre mot de passe sftp - s'il existe !'''
    
    def execute(self) -> None:
        if self.isWordpress():
            pg     = self.picgestion
            user   = pg.user
            home   = pg.home
            www    = f"{home}/www"
            ftppass= pg.password
            ftphost= f"sftp.{getConf('DOMAINNAME')}:2200"

            conteneur = getConf('DKRMAINT')

            # mot de passe '.' => Pas de changement !
            err = False
            if ftppass != '.':
                cmd       = f"cd {www} && wp config set FTP_PASS {ftppass} --no-add"
                try:
                    self._system(cmd,conteneur,False,user)
      
                # S'il y a une erreur on suppose que cette instance de wordpress n'est pas configurée pour ssh-sftp-support
                # Donc on refait la configuration depuis zéro
                # Mais il peut être intelligent de vérifier
                except PicErreur as e:
                    err = True
                    
                if err:
                    print ("==== ATTENTION - Nouvelle configuration de ssh-sftp-support ! Vous devriez vérifier wp-config.php")
                    self._configSftp(user, ftppass, ftphost)

class HomeDirMutualisation(HomeDir):
    
    def _getSitesPourAction(self,action,tous=True) -> list:
        '''Dialogue avec l utilisateur et renvoie soit tous les sites soit un seul, pour action 
           Si tous == False, ne renvoie pas les sites pour lesquels le repertoire est un lien symbolique
           ATTENTION Si tous == False c'est assez long, sinon ca va tres vite
           Renvoie toujours une liste'''

        pg        = self.picgestion
        tous_sites = pg.sites
        sites     = []

        if tous:
            sites = list(tous_sites)
        else:
            # Rechercher tous les liens symboliques
            conteneur= getConf('DKRMAINT')
            sitesdir = pg.home + '/www/sites/'
            cmd   = 'find ' + sitesdir + ' -maxdepth 1 -type l'
            liens = self._system(cmd,conteneur)
            liens = list(map(os.path.basename,liens))
            for s in tous_sites:
                # On ajoute s dans sites sauf si le repertoire est un lien symbolique
                if not s in liens:
                    sites.append(s)

        sites.sort()
 
        print("SITES MUTUALISES CONNUS ACTUELLEMENT:")
        for s in sites:
            print(s)

        print ('')
        tmp_in = entrerFiltrer(action + " lequel de ces sites (* = TOUS les sites) ? ",'*abcdefghijklmnopqrstuvwxyz0123456789./-_:')
        if tmp_in != '*' and not tmp_in in sites:
            raise PicErreur("ERREUR - Le site " + tmp_in + " n'existe pas dans la mutu")        
        elif tmp_in != '*':
            return [tmp_in]
        else:
            return sites

    def _archivagePlugins(self, sites, copie=True) -> None:
        '''Archive l'état des plugins Spip des sites passés en paramètre
           Un premier fichier est créé dans le tmp de chaque site
           Si copie vaut True il est recopié dans le répertoire d'archivage
        '''

        pg = self.picgestion
        conteneur = getConf('DKRMAINT')
        home = pg.home
        www = pg.www
        user = pg.user
        arch_path = f"{getConf('ARCHIVES')}/{user}"

        print("=== Archivage de l'etat des plugins ===")

        # Pour UN SEUL SITE ou pour CHAQUE SITE de la mutu on cree un fichier tmp/plugins.txt
        cmd = f"cd {www} && spipmu "
        if len(sites) == 1:
            cmd += sites[0]
        else:
            cmd += "'*'"
        cmd += " 'plugins:lister --no-dist --export'"
        self._system(cmd,conteneur,False,user)
        
        # On copie plugins.txt dans les repertoires d'archivage
        if copie:
            for site in sites:
                sitedir = f"{home}/www/sites/{site}"
                
                # Rien a faire si c'est un lien symbolique
                if self._dkr_f_lien(sitedir,conteneur):
                    continue
    
                # On cree le repertoire si necessaire
                site_path = arch_path + '/' + site
                if not self._dkr_fexists(site_path,conteneur):
                    cmd = "mkdir " + site_path
                    self._system(cmd,conteneur,False,user)
    
                plgfile = site_path + '/' + site + '.plugins.txt'
    
                # Supprime le fichier precedent (appartient a root)
                cmd = "rm -f " + plgfile
                self._system(cmd,conteneur)
    
                # Copie le fichier en -a pour garder les bons droits
                cmd = "cp -a " + sitedir + '/tmp/plugins.txt ' + plgfile
                self._system(cmd,conteneur)
 
    @property
    def mutuarchiversite(self)->list:
        '''renvoie le nom du site de la mutu qui sera archive'''
        return self.__MutuarchiverSite

    @property
    def mutuarchiverfichiers(self)->bool:
        return self.__fich_sites
    
    def _demandeParamsArchivage(self) -> None:
        self.__MutuarchiverSite = self._getSitesPourAction('Archiver',False)
        self.__fich_sites = ouiNon("Archivage des fichiers ?",False)
            
class HomeDirMutuajout(HomeDirMutualisation):
    '''Crée un lien symbolique vers un site existant dans le repertoire sites'''
    
    def execute(self) -> None:
        pg = self.picgestion
        
        # Si on veut creer un lien symbolique
        prod_alias = pg.sitealias
        if prod_alias != None:
            www = pg.www
            site = pg.site
            cmd = f"ln -s {prod_alias} {www}/sites/{site}"
            self._system(cmd,getConf('DKRMAINT'))
            
    def demandeParams(self) -> None:
        pass

class HomeDirMutusuppr(HomeDirMutualisation):
    '''Supprime soit le lien symbolique vers un site existant, soit tous les fichiers dans le repertoire sites'''

    def demandeParams(self) -> None:
        pg = self.picgestion
        
        # Est-ce un lien symbolique ?
        site    = pg.site
        if pg.sitealias == None:
            if not ouiNon("Voulez-vous vraiment supprimer tous les fichiers et la base de donnees de " + site + " ?",False):
                raise PicFatalErreur('ANNULATION')
        else:
            self._lien_symbolique = True
            
    def execute(self) -> None:
        pg = self.picgestion
        www = pg.www
        site = pg.site
        sitedir = f"{www}/sites/{site}"

        # Suppression du lien symbolique...
        if pg.sitealias != None:
            print(f"=== Suppression du lien symbolique sites/{site} ===")
            cmd = f"rm {sitedir}"
            self._system(cmd,getConf('DKRMAINT'))
        else:
            if self._dkr_fexists(sitedir,getConf('DKRMAINT')):
                print(f"=== Suppression du repertoire sites/{sitedir} ===")
                cmd = f"rm -r {sitedir}"
                self._system(cmd,getConf('DKRMAINT'))
            else:
                print(f"=== Le repertoire sites/{site} n'existe pas - Rien a supprimer ===")

class HomeDirMutuprod(HomeDirMutualisation):

    def execute(self) -> None:
        pg = self.picgestion
        plugauto = pg.www + '/plugins/auto'
        lib      = pg.www + '/lib'
        wuser    = 'w' + pg.user

        cmd = 'chmod ug-w -R ' + plugauto + ' ' + lib
        self._system(cmd,getConf('DKRMAINT'))
        print("=== La mutu est maintenant en mode Production, on ne peut pas mettre a jour les plugins")
        print("=== Pour repasser en mode upgrade: picgs mutuupgrade")

class HomeDirMutuupgrade(HomeDirMutualisation):
    
    def execute(self) -> None:
        '''Change les droits du repertoire plugins/auto afin de permettre la mise a jour des plugins
           Aussi exporte les plugins de chaque site, au cas ou on aurait oublie mutuarchivage !'''
        pg = self.picgestion
        plugauto = pg.www + '/plugins/auto'
        lib      = pg.www + '/lib'
        wuser    = 'w' + pg.user
        conteneur= getConf('DKRMAINT')

        cmd = 'chmod ug+w -R ' + plugauto + ' ' + lib
        self._system(cmd,getConf('DKRMAINT'))

        # Exportation de l'etat des plugins
        print("=== Exportation de l'etat des plugins")
        cmd = "cd " + pg.home + "/www; spipmu '*' 'plugins:lister --no-dist --export'"
        self._system(cmd,conteneur)

        print("=== La mutu est maintenant en mode Upgrade, on peut ajouter, supprimer, mettre a jour les plugins")
        print() 
        print("=== Avant de mettre a jour les plugins, vous devriez faire un mutuarchivage !")
        print()
        print("=== LORSQUE VOUS AUREZ TERMINE, N'OUBLIEZ PAS picgs mutuprod")
        
class HomeDirMutuarchivage(HomeDirMutualisation):

    def __archivageFichiers(self, sites) -> None:
        ''' Archive les fichiers des sites passés en paramètre'''

        pg = self.picgestion
        conteneur = getConf('DKRMAINT')
        home = pg.home
        www = pg.www
        sitesdir = f"{www}/sites"

        user = pg.user
        arch_path = f"{getConf('ARCHIVES')}/{user}"

        if self.mutuarchiverfichiers:
            print("=== Archivage des fichiers...")
            for site in sites:
                sitedir = f"{home}/www/sites/{site}"
                if self._dkr_f_lien(sitedir,conteneur):
                    cmd = f'cp -d {sitedir} {arch_path}'
                    self._system(cmd,conteneur,False,user)
                    # print "=== site " + site + " est un lien symbolique - Pas de fichiers a archiver !"
                else:
                    print(f"Creation du .tgz pour {sitedir}")
                    print(f"Taille {self._Taille(sitedir)}")
                    print()
                        
                    try:
                        self._CacheTag(sitedir)
                    except:
                        print("Pas les repertoires de spip - Annulation pour " + site)
                        continue
                        
                    # print "=== Creation du fichier .tgz pour " + site + "==="
                    site_path = f"{arch_path}/{site}"
                    if not self._dkr_fexists(site_path,conteneur):
                        cmd = f"mkdir {site_path}"
                        self._system(cmd,conteneur,False,user)
                    
                    l_excl = []
                    if not self.__avec_img:
                        l_excl.append(f"{site}/IMG")
                    cmd = f"tar czf {site_path}/{site}.tgz -C {sitesdir} {self._exclusionListe2String(l_excl)} --exclude-caches-under {site}"

                    try:
                        self._system(cmd,conteneur)
                    # Si tar renvoie 1 on ignore = modification de fichier durant l'archivage
                    except PicErreur as e:
                        if e.code > 1:
                            raise e
        else:
            print("=== Pas d'archivage des fichiers des sites")

    def __archivageCode(self) -> None:
        '''Archive le code de spip, commun à tous les sites'''
        
        pg = self.picgestion
        user = pg.user
        conteneur = getConf('DKRMAINT')
        arch_path = f"{getConf('ARCHIVES')}/{user}"
        
        print("=== Creation du fichier .tgz pour le code de spip ===")
        
        exclusion = [ f'db', f'www/sites' ]
        if not self.__avec_log:
            exclusion.append(f'log')

        site_path = f"{arch_path}/{user}"
        if not self._dkr_fexists(site_path,conteneur):
            cmd = f"mkdir {site_path}"
            self._system(cmd,conteneur)
        
        cmd = f"tar czf {site_path}/{user}.tgz -C {getConf('HOMES')} {self._exclusionListe2String(exclusion)} {user}"
        try:
            self._system(cmd,conteneur)
        # Si tar renvoie 1 on ignore = modification de fichier durant l'archivage
        except PicErreur as e:
            if e.code > 1:
                raise e

    def demandeParams(self) -> None:
        self._demandeParamsArchivage()
        self.__avec_code= ouiNon("Voulez-vous archiver AUSSI le code de spip ? ",True)
        if self.__avec_code:
            self.__avec_log = ouiNon("Voulez-vous archiver AUSSI les logs ? ",True)
        else:
            self.__avec_log = False
        self.__avec_img = ouiNon("Voulez-vous archiver AUSSI le répertoire IMG ? ",True)
            
    def execute(self) -> None:
        sites = self.mutuarchiversite

        self._videLeCache(sites)
        self.__archivageFichiers(sites)
        self._archivagePlugins(sites)
        
        if self.__avec_code:
            self.__archivageCode()

class HomeDirMutuexporte(HomeDirMutualisation):

    def __archivageFichiers(self, site: str) -> None:
        ''' Archive les fichiers du site passé en paramètre'''

        pg = self.picgestion
        conteneur = getConf('DKRMAINT')
        home = pg.home
        www = pg.www
        sitesdir = f"{www}/sites"

        user = pg.user
        arch_path = f"{getConf('ARCHIVES')}/{user}"

        if self.mutuarchiverfichiers:
            print("=== Archivage des fichiers...")
            sitedir = f"{home}/www/sites/{site}"

            if self._dkr_f_lien(sitedir,conteneur):
                raise PicErreur(f"ERREUR - {site} est un alias !")

            print("=== Création du répertoire export")
            root_export = f"{sitedir}/export/www"

            if self._dkr_fexists(root_export, conteneur):
                cmd = f"rm -rf {root_export}"
                self._system(cmd, conteneur)
            
            cmd = f"mkdir -p {root_export}/plugins/auto"
            self._system(cmd, conteneur)
                
            print("=== Exportation des plugins actifs")
            plg_file = f"{sitedir}/tmp/plugins.txt"
            if not self._dkr_fexists(plg_file, conteneur):
                raise PicFatalErreur("ANNULATION")
            
            else:
                cmd = f"cat {plg_file}"
                plugins = self._system(cmd, conteneur)[0].split(' ')

            for p in plugins:
                cmd = f"cd {root_export}/plugins/auto && ln -s ../../../../../../plugins/auto/{p}"
                self._system(cmd, conteneur)
                        
            print("=== Exportation des autres fichiers")
            cmd = f"cd {root_export} && for d in IMG local squelettes; do ln -s ../../\$d; done"
            self._system(cmd, conteneur)

            f_spip = "CHANGELOG.md composer.json composer.lock ecrire htaccess.txt index.php lib LICENSE local README.md SECURITY.md prive plugins-dist plugins-dist.json spip.php spip.png spip.svg squelettes-dist vendor"
            cmd = f"cd {root_export} && for d in {f_spip}; do ln -s ../../../../\$d; done"
            self._system(cmd, conteneur)

            cmd = f"cd {root_export} && mkdir config tmp"
            self._system(cmd, conteneur)
                        
            print(f"=== Creation du .tgz pour {sitedir}")
            print()
                
            try:
                self._CacheTag(sitedir)
            except:
                print("Pas les repertoires de spip - Annulation pour " + site)

            # print "=== Creation du fichier .tgz pour " + site + "==="
            site_path = f"{arch_path}/{site}"
            if not self._dkr_fexists(site_path,conteneur):
                cmd = f"mkdir -p {site_path}"
                self._system(cmd,conteneur)
                
            cmd = f"tar czf {site_path}/{site}.tgz -C {sitedir} --exclude-caches-under -h export"
            try:
                self._system(cmd,conteneur)
            # Si tar renvoie 1 on ignore = modification de fichier durant l'archivage
            except PicErreur as e:
                if e.code > 1:
                    raise e
        else:
            raise PicFatalErreur('ANNULATION')

    def execute(self) -> None:
        
        while(True):
            sites = self.mutuarchiversite
            if len(sites) == 1:
                break
            print ("ERREUR - Entrez UN SEUL SITE")
        site = sites[0]
        self._videLeCache(site)
        self._archivagePlugins(sites, False)
        self.__archivageFichiers(site)

    def demandeParams(self):
        while(True):
            self._demandeParamsArchivage()
            sites = self.mutuarchiversite
            if len(sites) == 1:
                break
            print
            print ("ERREUR - Entrez UN SEUL SITE")
            print
            
class HomeDirMutuinfo(HomeDirMutualisation):
    
    def execute(self) -> None:
        pg = self.picgestion
        conteneur = getConf('DKRMAINT')
        site = pg.site
        if pg.sitealias != None:
            site_reel = pg.sitealias
        else:
            site_reel = site
        sitedir = pg.home + '/www/sites/' + site_reel
            
        # Afficher les plugins actifs
        cmd = "cd " + sitedir + "; spip plugins:lister --no-dist"
        print("\n".join(self._system(cmd,conteneur)))

class HomeDirMutusauveplugins(HomeDirMutualisation):
            
    def execute(self) -> None:
        pg        = self.picgestion
        conteneur = getConf('DKRMAINT')
        sites     = list(pg.sites)
        user      = pg.user
        sitesdir  = pg.home + '/www/sites/'
        
        for site in sites:
            sitedir = sitesdir + site
            
            if self._dkr_f_lien(sitedir,conteneur):
                # Rien a faire si c'est un lien symbolique
                continue

            squeldir= sitedir + '/squelettes'
            plgdir  = squeldir+ '/plugins'
            plgfile = plgdir + '/' + 'plugins-actifs.txt'
            conffile= sitedir + '/local/config.txt'
            
            # Creer le repertoire si besoin, et laisser tomber si pas de répertoire squelette
            if not self._dkr_fexists(plgdir,conteneur):
                try:
                    cmd = "mkdir " + plgdir
                    self._system(cmd,conteneur)
                except:
                    print (f"WARNING - Répertoire {plgdir} non accessible, problème avec ce site ! ")
                    continue
                
            # Archiver en clair l'etat des plugins          
            self._videLeCache(site,True)
            cmd = "cd " + sitedir +"; spip plugins:lister --no-dist --no-ansi >" + plgfile
            try:
                self._system(cmd,conteneur,False,user)
            except PicErreur as e:
                print(e.message)
                            
            # Mettre les permissions pour que le serveur web puisse modifier tous ces fichiers
            cmd = "chmod -R ug+rwX " + squeldir + " " + conffile + " 2>/dev/null"
            try:
                self._system(cmd,conteneur)
            except:
                pass
                
            # On vide a nouveau le cache car il y a des trucs qui appartiennent a root !
            #self._videLeCache(site,True)
            

class HomeDirMuturecharge(HomeDirMutualisation):

    def demandeParams(self) -> None:
        self.__MuturechargerSite = self._getSitesPourAction('Recharger',False)

        # On ne pose plus la question: Pas de recuperation des fichiers ni de la B.D.
        #self.__fichiers     = ouiNon("Recreation des fichiers ?",False);
        self.__fichiers     = False
        self.__spip_plugins = ouiNon("Remise en activation des plugins ?",False)
    
    def execute(self) -> None:
        pg = self.picgestion
        sites      = self.__MuturechargerSite
        user       = pg.user
        arch_path  = getConf('ARCHIVES') + user
        conteneur  = getConf("DKRMAINT")
        
        # On appelle spip plugins:activer pour activer les plugins a partir du fichier ecrit par picgs mutuarchivage
        if self.__spip_plugins:
            sitesdir = pg.home + '/www/sites/'
            for site in sites:
                sitedir = sitesdir + site
                if self._dkr_f_lien(sitedir,conteneur):
                    continue
                site_path = arch_path + '/' + site
                if not self._dkr_fexists(site_path,conteneur):
                    print("=== ERREUR - Le repertoire " + site_path + " est introuvable")
                    continue
                plgfile = site_path + '/' + site + '.plugins.txt'
                cmd = "cd " + sitedir + "; spip plugins:activer -y --from-file " + plgfile
                res = self._system(cmd,conteneur,False,user)
                print(("\n".join(res)))
                self._videLeCache(site)

        return
        
class HomeDirMutuvidelescaches(HomeDirMutualisation):
    
    def execute(self) -> None:
        pg = self.picgestion
        sites         = list(pg.sites)
        user          = pg.user
        arch_path     = getConf('ARCHIVES') + user
        conteneur_ssh = getConf("DKRMAINT")

        for site in sites:
            sitedir = pg.home + '/www/sites/' + site
        
            # Pas de cache a vider si c'est un lien symbolique
            if self._dkr_f_lien(sitedir,conteneur_ssh):
                continue

            self._videLeCache(site)

class HomeDirMutuimporte(HomeDirMutualisation):

    def demandeParams(self) -> None:
        pg = self.picgestion
        site = pg.site
        self.__importeFichiers = False
        tmp_in = ouiNon("Voulez-vous importer les fichiers config,tmp,local,IMG,squelettes ? ",False)
        if tmp_in:
            self.__importeFichiers = ouiNon(f"Vous etes sur ? Cela DETRUIRA tous les fichiers existants de {site}",False)

        self.__importePlugins = ouiNon("Voulez-vous reactiver les plugins (si le fichier .plugins.txt existe) ? ",False)

    def execute(self) -> None:
        pg = self.picgestion
        site = pg.site
        user = pg.user
        www = pg.www
        sitedir  = f"{www}/sites/{site}"
        s_dir = pg.source[0]
        
        # /home/archives/./toto/
        s_fich= s_dir
        
        # /home/archives/./toto
        while s_fich.endswith('/'):
            s_fich = s_fich.rstrip('/')
            
        # toto
        s_fich = os.path.basename(s_fich)
        s_tgz_path= getConf('ARCHIVES') + s_dir + '/' + s_fich + '.tgz' 
        conteneur= getConf("DKRMAINT")
            
        if self.__importeFichiers:
            print("=== Extraction des fichiers ===")
            
            tar_err = False
            # Si on importe un site qui habite dans son user
            srctop   = s_dir + '/www/' 
            src      = srctop + 'config '
            src     += srctop + 'tmp '
            src     += srctop + 'local '
            src     += srctop + 'IMG '
            src     += srctop + 'squelettes '
            
            try:
                cmd = f"tar xzf {s_tgz_path} --keep-directory-symlink --strip-components=2 -C {sitedir} --exclude=connect.php {src}"
                self._system(cmd,conteneur)
    
            except PicErreur as e:
                tar_err = True
                
            if tar_err:
                # Si on importe un site qui habite dans une autre mutu: Pas de www dans la hierarchie
                srctop   = s_dir + '/' 
                src      = srctop + 'config '
                src     += srctop + 'tmp '
                src     += srctop + 'local '
                src     += srctop + 'IMG '
                src     += srctop + 'squelettes '
            try:
                cmd = 'tar xzf ' + s_tgz_path + ' --keep-directory-symlink --strip-components=1 ' + "-C " + sitedir + " "
                cmd += '--exclude=connect.php ' + src
                self._system(cmd,conteneur)
            except PicErreur as e:
                pass
            
            # Changement de proprio
            print("=== Changement de proprio des fichiers extraits")
            uid = str(pg.uid)
            gid = str(pg.gid)
            cmd = 'chown -R ' + uid + ':' + gid + ' ' + sitedir
            self._system(cmd,conteneur)
            
            # Changement de permission
            print("=== Changement de permissions des fichiers extraits")
            cmd = 'chmod -R ug=rwX ' + sitedir
            self._system(cmd,conteneur)
    
            # Vidage du cache
            self._videLeCache(site)

        # Activation des plugins
        if self.__importePlugins:
            plgfile = getConf('ARCHIVES') + s_dir + '/' + s_fich + '.plugins.txt' 
            if self._dkr_fexists(plgfile,conteneur):
                cmd = "cd " + sitedir +"; spip plugins:activer -y \\$(cat " + plgfile + ")"
                print("\n".join(self._system(cmd,conteneur,False,user)), end=' ')
    
                # On finit en vidant le cache car des fichiers ont ete crees avec les droits root
                self._videLeCache(site)
                print()
    
            else:
                print("Attention - Pas de fichier " + plgfile)
        
class HomeDirAjout(HomeDirInstallation):

    @property
    def cmsinstall(self) -> bool:
        return self.__cms_install
        
    def demandeParams(self) -> None:
        '''S'il s'agit d'un compte pour Galette, on demande ou est le fichier'''
        
        options = self.options
        if self.fonction == "galette":
            self.__cms_install = ouiNon("Voulez-vous installer Galette ? ",True)
            if self.__cms_install:
                self._galette_path = self._selectionnerZip('galette','bz2')

        if self.fonction == "dolibarr":
            self.__cms_install = ouiNon("Voulez-vous installer Dolibarr ? ",True)
            if self.__cms_install:
                self._dolib_path = self._selectionnerZip('dolibarr','tgz')
        
        if self.fonction == "site":
            if '--wp' in options:
                self._cms_wp = True
            else:
                self._cms_wp = ouiNon("Voulez-vous installer Wordpress ? ", False)
            if self._cms_wp:
                self._cms_wp_mig = False
            else:
                self._cms_wp_mig = ouiNon("Voulez-vous MIGRER un Wordpress depuis une installation précédente ? ", False)  

    def execute(self) -> None:
        pg = self.picgestion
        home = pg.home
        user = pg.user
        maint = getConf('DKRMAINT')
        www = home + '/www' 
        log = home + '/log'
        priv= home + '/priv'
        db  = home + '/db'

        self._system(f"[ -f {www}  ] || mkdir -p {www}", maint)
        self._system(f"[ -f {log}  ] || mkdir -p {log}", maint)
        self._system(f"[ -f {priv} ] || mkdir -p {priv}", maint)
        self._system(f"[ -f {db}   ] || mkdir -p {db}", maint)

        print ("=== proprios et permissions")
        self._homeProprio()
        self._system(f"chown -R w{user}:{user} {log}", maint)
        self._system(f"chmod -R o= {home}", maint)
         
        # ecriture du fichier index.html
        index = f"{www}/index.html"
        url   = f"{user}/{getConf('DOMAINNAME')}"

        print(f"=== creation du fichier {index}")
        index_contenu  = f"<html><head></head><body><h1>{pg.nomlong} - version du site: {str(self.version)}</h1>"
        index_contenu += f"<p>Espace r&eacute;serv&eacute; &agrave; l'association {pg.nomlong}</p>"
        index_contenu += f"<h2>Base de donn&eacute;es</h2>"
        index_contenu += f"<p><a href=\"{getConf('PHPMYADMIN')}\">Administrer votre base de donn&eacute;es</a> (<b>{pg.database}</b>)</p>"
        index_contenu += f"</body></html>\n"

        cmd = f"cat > {index}"
        self._system(cmd, maint, False, user, [], index_contenu)
        self._system(f"chmod 640 {index}", maint)

        # Installation automatique de Galette
        if self.fonction == 'galette' and self.__cms_install:
            print ("=== Installation automatique de Galette")
            self._installGalette()

        # Installation automatique de Dolibarr
        if self.fonction == 'dolibarr' and self.__cms_install:
            print ("=== Installation automatique de Dolibarr")
            self._installDolibarr()
            
        # Installation automatique de Wordpress
        if self.fonction == 'site' and self._cms_wp:
            print ("=== Installation automatique de Wordpress")
            self._installWordpress()
            
        # Préparer la migration de wordpress avec picwpmigrate
        if self.fonction == 'site' and self._cms_wp_mig:
            print ("=== Migration semi-automatique de Wordpress")
            self._migrationWordpress()
        
class HomeDirSuppression(HomeDir):
    def execute(self) -> None:
        pg   = self.picgestion
        home = pg.home
        print("=== Suppression des fichiers");
        cmd = 'rm -rf ' + home
        self._system(cmd,getConf('DKRMAINT'))

class HomeDirSsh(HomeDir):
    def execute(self) -> None:
        pg = self.picgestion
        home = pg.home
        user = pg.user
        print("changement de proprio du home (appartient a " + user + ")")
        self._system('chown ' + user + ':' + user + ' ' + home,getConf('DKRMAINT'))

class HomeDirNossh(HomeDir):
    def execute(self) -> None:
        print("changement de permissions du home (appartient a root)")
        pg = self.picgestion
        home = pg.home
        user = pg.user
        self._system('chown root:' + user + ' ' + home,getConf('DKRMAINT'))

class HomeDirCollecte(HomeDir):

    def entetes(self) -> str:
        if '--short' in self.options:
            return ""
            
        if '--long' in self.options:
            return "home dir\ttaille\tCMS\tpicadmin\t"
        else:
            return "home dir\tCMS\tpicadmin\t"

    def execute(self) -> None:
        if '--short' in self.options:
            return
            
        pg = self.picgestion
        if pg.filtre:
            return

        home_dir = pg.home
        rvl  = home_dir + "\t"

        if self.chkpicadmin:
            picadmin = 'OUI'
        else:
            picadmin = 'NON'

        if '--long' in self.options:
            rvl += self._Taille() + "\t"
            rvl += self.getCms() + "\t"
            rvl += picadmin + "\t"
        else:
            rvl += self.getCms() + "\t"         
            rvl += picadmin + "\t"
        
        print(rvl, end=' ')

class HomeDirMutucollecte(HomeDirMutualisation):
    
    def entetes(self) -> str:
        if '--long' in self.options:
            return "taille\t"
        else:
            return ""

    def execute(self) -> None:
        pg = self.picgestion
        if pg.filtre:
            return      
        if '--long' in self.options:
            site     = pg.site
            # On vide le cache afin de ne pas le compter dans la taille
            self._videLeCache(site,True)

            site_dir = pg.www + '/sites/' + site
            print(self._Taille(site_dir) + "\t", end=' ')
            
class HomeDirArchivage(HomeDir):

    def demandeParams(self) -> None:
        
        self.__avec_log = ouiNon("Voulez-vous archiver AUSSI les logs ? ",True)
        if self.isSpip():
            self.__avec_img = ouiNon("Voulez-vous archiver AUSSI le répertoire IMG ? ",True)
        else:
            self.__avec_img = True

    def __UidGid(self) -> None:
        '''Garde dans un petit fichier les nombres uid,uid2,gid - Ils seront utilises par la commande Recharge'''
        pg = self.picgestion
        uid = pg.uid
        uid2= pg.uid2
        gid = pg.gid
        
        print("=== creation du fichier " + getConf('PICUSERS'))
        picusers = pg.home + '/' + getConf('PICUSERS')
        [fdout, tmp_picusers] = tempfile.mkstemp();
        out = os.fdopen(fdout,'w')
        out.write(str(uid)+' '+str(uid2)+' '+str(gid));
        out.close();
        
        self._dkr_cpto(tmp_picusers,picusers,getConf('DKRMAINT'))
        os.unlink (tmp_picusers)
            
    def execute(self) -> None:
        pg = self.picgestion
        home_dir = pg.home
        user = pg.user
        arch_path = self._calc_nom_archive(user)
 
        exclusion = [ f'{user}/db' ]       
        if not self.__avec_log:
            exclusion.append(f'{user}/log')

        if not self.__avec_img:
            exclusion.append('www/IMG')
            
        print("Archivage des fichiers...")
        print("Repertoire " + home_dir)
        print("Taille     " + self._Taille(home_dir, exclusion))
        print("CMS        " + self.getCms())

        self._CacheTag()
        self.__UidGid()
        print("=== Creation du fichier .tgz ===")

        cmd = f"tar czf {arch_path} -C {getConf('HOMES')} {self._exclusionListe2String(exclusion)} --exclude-caches-under {user}"
        try:
            self._system(cmd,getConf('DKRMAINT'))
        # Si tar renvoie 1 on ignore = modification de fichier durant l'archivage
        except PicErreur as e:
            if e.code > 1:
                raise e

class HomeDirSpipinstall(HomeDirInstallation):              

    def execute(self) -> None:
        self._installSpip()

class HomeDirRecharge(HomeDir):

    def demandeParams(self) -> None:
        pg = self.picgestion
        [s_user,s_nom,s_version] = pg.source
        try:
            self._s_dump_path = self._calc_nom_archive(s_user, None, True)
            self._rechargeFichiers = ouiNon(f"Recréation des fichiers à partir de {self._s_dump_path} ?", False)

        except PicErreur as e:
            self._rechargeFichiers = False
            print (afficheJaune(f"ATTENTION - Pas de fichier .tgz dans {s_user}"))

    def execute(self) -> None:
        pg = self.picgestion
        home_dir = pg.home
        www = pg.www
        log = f"{home_dir}/log"
        user = pg.user
        user2 = f"w{user}"
        [s_user,s_nom,s_version] = pg.source
        conteneur = getConf('DKRMAINT')

        s_tgz_path = self._s_dump_path

        if self._rechargeFichiers:
            print("=== Extraction des fichiers ===")
            cmd = f'tar xzf {s_tgz_path} --keep-directory-symlink --strip-components=1 --numeric-owner -C {home_dir}'
            self._system(cmd, conteneur)
            
            # Changement de proprio si necessaire
            uid = str(pg.uid)
            uid2= str(pg.uid2)
            gid = str(pg.gid)
            conteneur = getConf('DKRMAINT')
            picusers = f"{home_dir}/{getConf('PICUSERS')}"
            cmd = 'cat ' + picusers
            try:
                user_info = self._system(cmd,conteneur)[0]
                
            # Pas d'information sur les anciens users: on met tout à {user}
            except:
                print ("=== changement de proprio du home")
                self._homeProprio() 
                return
                
            try:
                [ouid,ouid2,ogid] = user_info.split(' ')
            except:
                raise PicErreur("ERREUR - mauvais format pour " + picusers)
            
            #import pprint
            #pprint.pprint([uid,uid2,gid])
            #pprint.pprint([ouid,ouid2,ogid])
            if [uid,uid2,gid] != [ouid,ouid2,ogid]:
                print("=== Changement de proprio ===")
                cmd = f'find {home_dir} -user {ouid} -exec chown {user}:{user}' + ' {} \;'
                self._system(cmd,conteneur)
                cmd = f'find {home_dir} -user {ouid2} -exec chown w{user}:{user}' + ' {} \;'
                self._system(cmd,conteneur)
                cmd = f'find {home_dir} -group {ogid} -exec chgrp {user}' + ' {} \;'
                self._system(cmd,conteneur)

            else:
                print("=== Changement de proprio inutile ===")
                
            # On change la propriété du répertoire log au cas où on passerait avec recharge 
            # d'un hébergement apache ancien vers un hébergement nginx
            cmd = f"chown -R {user2} {log}"
            self._system(cmd,conteneur)

        else:
            print("Extraction des fichiers annulee")

        return

class HomeDirSpipclone(HomeDirRecharge):

    def demandeParams(self) -> None:
        self._rechargeFichiers = True
        super().demandeParams()
        
    # On appelle la methode de la superclasse puis on change les permissions pour spip
    def execute(self) -> None:
        pg = self.picgestion
        home_dir = pg.home
        www = pg.www
        user = pg.user
        domaine = getConf('DOMAINNAME')
        addr = f"https://{user}.{domaine}"
        database = pg.database
        conteneur = getConf('DKRMAINT')

        super().execute()
        #connect = f"{www}/config/connect.php"

        print ("=== Permissions pour spip")
        self._permissionsPourSpip()

        print ("=== Vide les caches")
        self._videLeCache(user, True)

class HomeDirWpclone(HomeDirRecharge):

    def demandeParams(self) -> None:
        self._rechargeFichiers = True
        super().demandeParams()
        
    # On appelle la methode de la superclasse, puis on complete:
    #    - mise à jour de wp-config.php (seulement par rapport à ftp))
    #
    def execute(self) -> None:
        pg = self.picgestion
        home_dir  = pg.home
        www       = pg.www
        user      = pg.user

        ftppass   = pg.password
        ftphost   = f"sftp.{getConf('DOMAINNAME')}:2200"
        [s_user,s_nom,s_version] = pg.source
        conteneur = getConf('DKRMAINT')

        super().execute()
        
        # Modification de wp-config
        self._system(f"cd {www} && wp config set FTP_USER {user}", conteneur, False, user)
        self._system(f"cd {www} && wp config set FTP_PASS {ftppass}", conteneur, False, user)
        self._system(f"cd {www} && wp config set FTP_HOST {ftphost}", conteneur, False, user)

class HomeDirGaletteclone(HomeDirRecharge):

    def demandeParams(self) -> None:
        self._rechargeFichiers = True
        super().demandeParams()

    # On supprime www, qui sera remplacé par un lien symbolique
    # Seulement après, on appelle la méthode de la superclasse
    #
    def execute(self):
        pg = self.picgestion
        user      = pg.user
        www       = pg.www
        conteneur = getConf('DKRMAINT')

        if ouiNon(f"Nous devons supprimer le répertoire {www} et le transformer en lien symbolique: OK ? ", False):
            self._system(f"rm -rf {www}", conteneur)
        else:
            raise PicFatalErreur('ANNULATION')

        HomeDirRecharge.execute(self)

class HomeDirDolibarrclone(HomeDirRecharge):

    def demandeParams(self) -> None:
        self._rechargeFichiers = True
        super().demandeParams()
        
    def execute(self) -> None:
        ''' On supprime www, qui sera remplacé par un lien symbolique
            Seulement après, on appelle la méthode de la superclasse'''

        pg = self.picgestion
        user      = pg.user
        www       = pg.www
        conteneur = getConf('DKRMAINT')

        if ouiNon(f"Nous devons supprimer le répertoire {www} et le transformer en lien symbolique: OK ? ", False):
            self._system(f"rm -rf {www}", conteneur)
        else:
            raise PicFatalErreur('ANNULATION')

        HomeDirRecharge.execute(self)

#
# Installation de wordpress
#
# D'apres https://github.com/plaurent75/WordpressInstaller
#
class HomeDirWpinstall(HomeDirInstallation):
    
    def demandeParams(self) -> None:
        self._picpass = getpass.getpass('mot de passe de picadmin          : ')
        pg   = self.picgestion
        mail = pg.mail
        user = pg.user
        if mail=='':
            print(("ATTENTION l'utilisateur " + user + " n'a PAS DE MAIL - Merci de corriger avec picgs modif"))
            sys.exit(1)

    def execute(self) -> None:
        self._installWordpress()
        
class HomeDirMaj(HomeDir):
    '''Classe parente pour les classes de mise à jour des CMS et applications'''

    def _verifSpecifiques(self) -> None:
        '''AVANT mise à jour: Vérifications spécifiques à chaque CMS - Lève une exception en cas d'échec'''
        return
        
    def _verifGenerales(self) -> None:
        '''AVANT mise à jour: Vérifications communes à tous les CMS - Lève une exception en cas d'échec
           Préconditions
               - self._inst_path
               - self._dir_path
               - self._zip_path
               - self._zip_ext'''

        # Pour s'assurer que les membres sont bien initialisés
        zip_path  = self._zip_path
        zip_ext   = self._zip_ext
        inst_path = self._inst_path
        dir_path  = self._dir_path
        
        try:
            unzip_path= self._unzip_path
        except AttributeError:
            unzip_path = inst_path

        conteneur = getConf('DKRMAINT')
        pg        = self.picgestion
        user      = pg.user
        arch_path = getConf('ARCHIVES') + user

        # Quelques verifications avant de commencer
        if not self._dkr_fexists(arch_path,conteneur):
            if not ouiNon(afficheJaune("ATTENTION - PAS D'ARCHIVAGE TROUVE - On continue tout-de-même ?"), False):
                raise PicErreur ("ERREUR - Pas d'archive, pas de mise à jour ! Utilisez d'abord picgs archivage")
                
        if zip_path != None:
            if not self._dkr_fexists(zip_path,conteneur):
                raise PicErreur (f"ERREUR - Le fichier {zip_path} n'existe pas, merci de le telecharger !")
            if self._dkr_fexists(dir_path,conteneur):
                raise PicErreur (f"ERREUR - Le repertoire {dir_path} existe deja ! Supprimez-le et recommencez")
            if not self.chkpicadmin:
                raise PicErreur ("ERREUR - pas de compte d'administration - mise a jour automatique refusee !")

        # Vérifier la date du dernier archivage
        print()
        cmd = f"ls -l {arch_path}"
        try:
            res = '\n'.join(self._system(cmd,conteneur))
        except PicErreur:
            # Si on est arrivé là, c'est qu'on l'a voulu
            pass
        else:
            print("DERNIER ARCHIVAGE:")
            print(f"# ls -l {arch_path}")
            print(res)
            print()
            if not ouiNon('Continuer la mise à jour ?',False):
                raise PicFatalErreur ("ANNULATION")

        # Extraction du fichier zip
        cmd = f"mkdir {unzip_path}"
        try:
            self._system(cmd,conteneur)
        except PicErreur:
            pass

        if zip_path != None:
            print(f"Extraction de {zip_path} dans {unzip_path}")
            if zip_ext=='zip':
                cmd = f"cd {unzip_path} && unzip {zip_path} >/dev/null 2>&1"
            elif zip_ext=='tgz' or zip_ext=='tar.gz':
                cmd = f"cd {unzip_path} && tar xf {zip_path}"
            elif zip_ext=='tar.bz2':
                cmd = f"cd {unzip_path} && tar xjf {zip_path}"
            else:
                raise PicFatalErreur (f"ERREUR INTERNE - zip_ext={zip_ext} ni .zip ni .tar.gz ni .tar.bz2")
                
            self._system(cmd,conteneur)
            print("OK")
    
            #  Vérification des versions
            vcour = self.getCms()
            vinst = self.getCms(dir_path)
            if vcour > vinst:
                print()
                print("ATTENTION VOUS ETES EN TRAIN DE REVENIR EN ARRIERE !!!!!!!!")
                print("-----------------------------------------------------------")
    
            print()
            print(f"Version courante du site       : {vcour}")
            print(f"Version en cours d'installation: {vinst}")
            print()
    
            if not ouiNon('Continuer la mise à jour ?',False):
                # supprimer le repertoire temporaire (dir_path) puis annuler
                cmd = f"rm -r {dir_path}"
                self._system(cmd,conteneur)
                raise PicFatalErreur ("ANNULATION")
    
    def _copieFichiers(self) -> None:
        '''Copie les fichiers de la nouvelle version à leur place'''
        
        inst_path = self._inst_path
        dir_path  = self._dir_path
        conteneur = getConf('DKRMAINT')
        
        cmd = f"/bin/cp -r {dir_path}/* {inst_path}"
        self._system(cmd,conteneur)

        # supprimer le repertoire temporaire
        cmd = f"rm -r {dir_path}"
        self._system(cmd,conteneur)

    def _permissionsGenerales(self) -> None:
        '''Fait ou refait les permissions (utile pour les nouveaux fichiers)'''
        
        inst_path = self._inst_path
        pg        = self.picgestion
        user      = pg.user
        group     = user
        conteneur = getConf('DKRMAINT')
        
        # Par defaut les fichiers peuvent etre lus par apache, mais pas modifies
        cmd      = f"chown -R {user}:{group} {inst_path}"
        self._system(cmd,conteneur)
        cmd      = f"chmod -R o= {inst_path}"
        self._system(cmd,conteneur)
        cmd      = f"chmod -R g-w {inst_path}"
        self._system(cmd,conteneur)
        
        return

    def _permissionsSpecifiques(self) -> None:
        '''Fait ou refait les permissions - Spécifique à chaque CMS'''
        return
    
    def _majBd(self) -> None:
        '''Mise à jour de la base de données - Par défaut on écrit un message et c'est tout'''

        url   = 'https://' + self.picgestion.user + '.' + getConf('DOMAINNAME')
        print()
        print("MISE A JOUR TERMINEE")
        print("Connectez-vous avec votre navigateur sur " + url + " utilisateur " + getConf('ADMIN') + ',')
        print("pour vérifier que tout fonctionne bien et terminer la mise à jour (base de données)") 
        
    def demandeParams(self) -> None:
        '''Doit être surchargée par les classes dérivées'''
        raise PicFatalErreur("ERREUR INTERNE - Fonction abstraite")
        
    def execute(self) -> None:

        # Faire la mise à jour: copie des fichiers
        self._copieFichiers()
    
        # refaire les permissions correctement
        self._permissionsGenerales()
        self._permissionsSpecifiques()

        # Mise à jour de la base de données
        self._majBd()
        
class HomeDirSpipmaj(HomeDirMaj):
    
    def _verifSpecifiques(self) -> None:
        '''Vérifie que nous sommes bien dans une installation de Spip
           Si une vérification est négative, lève une exception'''
        if not self.isSpip():
            raise PicErreur ("ERREUR - pas de spip ici !")
    
    def _permissionsSpecifiques(self) -> None:
        '''Fait ou refait les permissions - Spécifique à chaque CMS
           TODO - Maj et Install devraient sans doute dériver du même objet...
        '''
        self._permissionsPourSpip()
        return
        
    def demandeParams(self) -> None:
        pg        = self.picgestion
        zip_path  = self._selectionnerZip('spip','zip')
        inst_path = pg.www
        dir_path  = inst_path + '/spip'
        unzip_path= dir_path
            
        self._zip_path = zip_path
        self._zip_ext  = 'zip'
        self._inst_path= inst_path
        self._dir_path = dir_path
        self._unzip_path = unzip_path
        
        # Vérifications et dezippage dans un répertoire temporaire
        self._verifSpecifiques()
        self._verifGenerales()
        
class HomeDirDolibarrmaj(HomeDirMaj):
    '''Mise à jour de Dolibarr'''

    def _permissionsSpecifiques(self) -> None:
        # Mettre les repertoires en-dessous de documents en rw
        
        print ("=== Autorisations d'écriture sur le répertoire documents")
        pg        = self.picgestion
        home      = pg.home
        conteneur = getConf('DKRMAINT')
        cmd  = f"cd {home}/dolibarr && chmod -R ug=rwX documents "
        self._system(cmd,conteneur)

    def _verifSpecifiques(self) -> None:
        '''Vérifie que nous sommes bien dans une installation de Dolibarr faite avec picgs
           Si une vérification est négative, lève une exception'''

        pg        = self.picgestion
        home      = pg.home
        conteneur = getConf("DKRMAINT")
        
        if not self.isDolibarr():
            raise PicErreur ("ERREUR - pas de dolibarr ici !")
        if not self._dkr_fexists(f"{home}/dolibarr",conteneur):
            raise PicErreur ("ERREUR - Dolibarr n'a pas été installé avec picgs !")

    def _majBd(self) -> None:
        '''Supprime le verrou et propose de se connecter pour finir la mise à jour'''
        
        pg        = self.picgestion
        home      = pg.home
        user      = pg.user
        conteneur = getConf("DKRMAINT")

        print ("=== Suppression du fichier install.lock")
        cmd = f"cd {home} && rm -f dolibarr/documents/install.lock"
        self._system(cmd,conteneur,False,user)

        print ("")
        url = f"https://{user}.{getConf('DOMAINNAME')}"
        print (f"Pour terminer la mise à jour de dolibarr, allez maintenant sur: {url}") 
        print (f"Lorsque c'est terminé, revenez sur cet écran et tapez o")
        
        while True:
            if ouiNon("On termine l'installation ?",False):
                break
                
        print ("=== Mise en place du fichier install.lock")
        cmd = f"cd {home} && touch dolibarr/documents/install.lock && chmod a=r dolibarr/documents/install.lock"
        self._system(cmd,conteneur,False,user)

    def demandeParams(self) -> None:
        pg        = self.picgestion
        conteneur = getConf("DKRMAINT")

        zip_path  = self._selectionnerZip('dolibarr','tgz')
        home_dir  = pg.home
        inst_path = pg.home + '/dolibarr'

        # dolibarr.13.0.1.tgz => Dolibarr-dolibarr-a573d57/
        cmd = f"tar tf {zip_path} | head -1"
        dir_path = f"{inst_path}/{self._system(cmd,conteneur)[0].rstrip('/')}"
        
        self._zip_path = zip_path
        self._zip_ext  = 'tgz'
        self._inst_path= inst_path
        self._dir_path = dir_path
        
        # Vérifications
        self._verifSpecifiques()
        self._verifGenerales()

class HomeDirGalettemaj(HomeDirMaj):
    '''Mise à jour de Galette'''

    def _verifSpecifiques(self) -> None:
        '''Vérifie que nous sommes bien dans une installation de Galette faite avec picgs
           Si une vérification est négative, lève une exception'''

        pg        = self.picgestion
        home      = pg.home
        conteneur = getConf("DKRMAINT")
        
        if not self.isGalette():
            raise PicErreur ("ERREUR - pas de galette ici !")
        if not self._dkr_fexists(home + '/galette',conteneur):
            raise PicErreur ("ERREUR - Galette n'a pas été installé avec picgs !")

    def _majBd(self) -> None:
        '''Propose de se connecter pour finir la mise à jour'''
        
        pg        = self.picgestion
        home      = pg.home
        user      = pg.user
        conteneur = getConf("DKRMAINT")
        conf      = self._conf

        print ("=== Autorisation d'écriture du répertoire config")
        cmd = f"cd {home} && chmod -R a+w galette/galette/config"
        self._system(cmd,conteneur)

        print ("=== Paramètres actuels de configuration (vous en aurez besoin !)")
        for l in conf:
            print(l)
            
        print ("")
        url = f"https://{user}.{getConf('DOMAINNAME')}/installer.php"
        print (f"Pour terminer la mise à jour de galette, allez maintenant sur: {url}")
        print (f"Lorsque c'est terminé, revenez sur cet écran et tapez o")
        
        while True:
            if ouiNon("On termine la mise à jour ?",False):
                break

        print ("=== Sauvegarde et suppression du répertoire install")
        cmd = f"cd {home}/galette/galette && tar czf install.tgz install && chown {user} install.tgz && rm -rf install"
        self._system(cmd,conteneur)

        print ("=== Mise en sécurite du fichier de config")
        cmd = f"cd {home} && chmod -R a-w galette/galette/config"
        self._system(cmd,conteneur)
                
    def _permissionsSpecifiques(self) -> None:
        # Mettre les repertoires en-dessous de data en rw
        
        print ("=== Autorisations d'écriture sur le répertoire data")
        pg        = self.picgestion
        home      = pg.home
        conteneur = getConf('DKRMAINT')
        cmd  = "cd " + home + "/galette/galette/data && chmod -R ug=rwX attachments cache exports files imports logs photos tempimages templates_c "
        self._system(cmd,conteneur)

    def demandeParams(self) -> None:
        pg        = self.picgestion
        zip_ext   = 'tar.bz2'
        zip_path  = self._selectionnerZip('galette',zip_ext)
        home_dir  = pg.home
        inst_path = pg.home + '/galette'

        # galette-0.9.3.1.tar.bz2 ==> galette-0.9.3.1
        tmp = os.path.basename(zip_path)
        tmp = tmp.rpartition('.')[0]
        tmp = tmp.rpartition('.')[0]
        dir_path  = inst_path + '/' + tmp
        conteneur = getConf('DKRMAINT')
            
        self._zip_path = zip_path
        self._zip_ext  = zip_ext
        self._inst_path= inst_path
        self._dir_path = dir_path
        
        # Vérifications
        self._verifSpecifiques()
        self._verifGenerales()
        
        # Lecture du fichier de configuration (celui-ci sera détruit par le processus de mise à jour !)
        cmd        = "cd " + home_dir + "/galette/galette/config && cat config.inc.php"
        conf       = self._system(cmd,conteneur)
        self._conf = conf
        
#
# Mise à jour de wordpress
#
class HomeDirWpmaj(HomeDirMaj):
    
    def _verifSpecifiques(self) -> None:
        '''Vérifie que nous sommes bien dans une installation de Wordpress faite avec picgs
           Si une vérification est négative, lève une exception'''

        pg        = self.picgestion
        www_dir   = pg.www
        user      = pg.user
        conteneur = getConf("DKRMAINT")
        mineur    = False
        majeur    = False
        
        # Sommes-nous dans une installation wordpress ?
        if not self.isWordpress():
            raise PicErreur ("ERREUR - pas de wordpress ici !")

        # Y a-t-il un upgrade mineur à faire ?
        cmd = "cd " + www_dir + "&& wp core check-update --minor 2>/dev/null"
        out = self._system(cmd,conteneur,False,user)
        
        if not "\n".join(out).startswith("Success:"):
            mineur = True
            print ("MISES A JOUR MINEURES")
            print ("\n".join(out))
            
        # Y a-t-il un upgrade majeur à faire ?
        cmd = "cd " + www_dir + "&& wp core check-update --major 2>/dev/null"
        out = self._system(cmd,conteneur,False,user)
        
        if not "\n".join(out).startswith("Success:"):
            majeur = True
            print ("MISES A JOUR MAJEURES")
            print ("\n".join(out))

        # Ni l'un ni l'autre, on annule
        if mineur==False and majeur==False:
            print ("PAS DE MISE A JOUR A FAIRE - CETTE INSTALLATION DE WORDPRESS EST A JOUR")
            raise PicFatalErreur ("ANNULATION")

        # Garder en mémoire pour les étapes suivantes
        self.__mineur = mineur
        self.__majeur = majeur

    def _majBd(self) -> None:

        url   = 'https://' + self.picgestion.user + '.' + getConf('DOMAINNAME')
        print()
        print("MISE A JOUR TERMINEE")
        print("Connectez-vous avec votre navigateur sur " + url + " utilisateur " + getConf('ADMIN') + ',')
        print("pour vérifier que tout fonctionne bien !") 
        
    def demandeParams(self) -> None:
        pg        = self.picgestion
        user      = pg.user
        arch_path = getConf('ARCHIVES') + user
        www_dir   = pg.www;
        conteneur = getConf('DKRMAINT')
        mineur    = False
        majeur    = False

        # Vérifications spécifiques à Wordpress
        self._verifSpecifiques()
        
        # On utilise la commande wp pour mettre à jour, PAS le téléchargement classique !
        self._zip_path = None
        self._zip_ext  = None
        self._inst_path= None
        self._dir_path = None

        # Pas de picadmin, on propose d'en créer un 
        if not self.chkpicadmin:
            print ("Il n'y a pas de compte PicAdmin sur ce site.")
            if ouiNon("Voulez-vous créer le compte PicAdmin maintenant ?",False):
                pwd_in = getpass.getpass("Mot de passe PicAdmin: ")

                if pwd_in == "":
                    raise PicErreur("ERREUR - Pas de mot de passe entré")
                mail_in= input("Adresse mail associée: ")
                if mail_in == "":
                    raise PicErreur("ERREUR - Pas de mail associé")
                cmd = "cd " + www_dir + " && wp user create picadmin " +  mail_in + " --role=administrator --user_pass=" + pwd_in + " --description='Compte de maintenance pour le PIC' 2>/dev/null"
                self._system(cmd,conteneur,False,user)
            else:
                print ("ANNULATION car pas de compte picadmin")
                raise PicFatalErreur("ANNULATION")

        else:
            print("Compte PicAdmin OK")

        # Vérifications habituelles
        self._verifGenerales()
        
        # Upgrade majeur ou mineur ?
        mineur = self.__mineur
        majeur = self.__majeur
        
        if mineur and majeur:
            print ("Quel upgrade souhaitez-vous effectuer ? Mineur ou majeur ?")
            if ouiNon ("Voulez-vous effectuer l'upgrade MINEUR ? ",True):
                majeur = False
            else:
                mineur = False
        
        if mineur:      
            if not ouiNon("OK pour faire un upgrade MINEUR (et donc PAS majeur) ?",False):
                raise PicFatalErreur ("ANNULATION")
            majeur = False
        
        if majeur:
            if not ouiNon("OK pour faire un upgrade MAJEUR ? ATTENTION, PEUT ETRE DANGEREUX !!!!",False):
                raise PicFatalErreur ("ANNULATION")
            mineur = False

    def execute(self) -> None:
        pg        = self.picgestion
        user      = pg.user
        www_dir   = pg.www;
        conteneur = getConf('DKRMAINT')
        mineur    = self.__mineur
        majeur    = self.__majeur

        if majeur:
            print("")
            print ("Lancement de la mise à jour MAJEURE en utilisant la commande wp")
            cmd = f"cd {www_dir} && wp core update 2>/dev/null "
        if mineur:
            print("")
            print ("Lancement de la mise à jour MINEURE en utilisant la commande wp")
            cmd = f"cd {www_dir} && wp core update --minor 2>/dev/null" 

        self._system(cmd,conteneur,False,user)

        # Mise à jour de la BD
        cmd = f"cd {www_dir} && wp core update-db"
        self._system(cmd,conteneur,False,user)
        
        
        # Juste afficher le message de victoire 
        self._majBd()
        
#
# Site statique adossé à un site dynamique
#
class HomeDirStatique(HomeDir):

    def execute(self) -> None:
        pg = self.picgestion
        user = pg.user
        wwws_dir = pg.www + 's'
        conteneur = getConf('DKRMAINT')

        cmd = f"mkdir -p {wwws_dir} && chown {user}: {wwws_dir} && chmod g+w {wwws_dir}"
        self._system(cmd, conteneur)
        
# TEST
if __name__ == '__main__':
    c = HomeDir('ajout','jungle',100);
