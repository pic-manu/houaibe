#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# MODULE fin = Ce plugin est appelé en DERNIER
#

version = '0.1'

import os
import re
import time
import socket
import json
import requests
from bs4 import BeautifulSoup
from urllib.parse import urlparse

from plugin import Plugin
from ouinon import ouiNon
from erreur import *
from affiche import *
from picconfig import getConf

class Fin(Plugin):

    @property
    def nomplugin(self)->str:
        return 'Fin'
    
    def __init__(self,action,nom,version,pic_gestion,options={}):

        # Appel du constructeur du parent
        Plugin.__init__(self,action,nom,version,pic_gestion,options)

class FinInactifs(Fin):
    
    def execute(self) -> None:
        print ()
        print("Vous pouvez réactiver les sites inactifs par la commande: picgs reactiver monasso site 1")

class FinInfo(Fin):

    def __detecteRedirections(self, prod_urls: list) -> dict:
        '''Fait un wget sur les adresses de production 
           et détecte celles qui sont redirigées
           Renvoie un dict, un élément par url de prod'''

        rvl = {}
        if len(prod_urls) == 0:
            return ""

        for url in prod_urls:
            # || true assure in status 0 (donc pas d'exception) quoiqu'il arrive
            cmd = f"wget -q -S -O /dev/null --max-redirect=0 https://{url} || true"
            out = {}
            res = self._system(cmd)
            #print (f"lecture de {url}")
            for r in res:
                r = r.strip()
                #print(r)
                if r.startswith('HTTP'):
                    out['code'] = r.split(' ',2)[1].strip() # HTTP/1.1 200 OK => 200
                    out['codeline'] = r

                if r.startswith('Location'):
                    out['location'] = r.split(' ',2)[1].strip()

            if not 'code' in out:
                out['code'] = '-1'
                out['codeline'] = ''
                
            rvl[url] = out
        return rvl

    def execute(self) -> None:
        # On écrit un message si ce user est inactif
        pg = self.picgestion
        if pg.isinactif:
            print ()
            print (afficheRouge('CE SITE EST DESACTIVE - picgs reactiver si nécessaire'))

        else:
            # Détection des adresses de production et des url canoniques
            options = self.options
            user = pg.user

            if not '--short' in options:

                (prod_urls, prod_user) = pg.prod
                
                print()
                print("ADRESSES DE PRODUCTION ET REDIRECTIONS")
                print("======================================")
                
                if prod_user == "":
                    print (f"Prod        : Pas d'adresse de production")
                
                elif prod_user != user:
                    print (f"Prod        : voir {prod_user}")
                
                else:
                    # on teste les adresses de production
                    redirs = self.__detecteRedirections(prod_urls)
                    for url in prod_urls:
                        code = redirs[url]['code']
                        codeline = redirs[url]['codeline']
                        if code == "301":
                            location = redirs[url]["location"]
                            print ("prod" + 8 * ' ' + ': ' + f"{url}: {codeline} -> {location}")
                            
                        elif code == "-1":
                            print ("prod" + 8 * ' ' + ': ' + f"{url}: PAS DE REPONSE")
                            
                        else:
                            print ("prod" + 8 * ' ' + ': ' + f"{url}: {codeline}")
                    
                    print()
                            
                    # On recherche l'adresse canonique
                    print ("ADRESSE CANONIQUE")
                    print ("=================")
                    if len(prod_urls) == 0:
                        canon = self._detecteCanonical(user=user)
                    else:
                        canon = self._detecteCanonical(url=list(prod_urls)[0])
                        
                    if canon == "":
                        print (f"Canonical   : Pas d'adresse canonique spécifiée, il faut sans doute configurer le CMS")
                    else:
                        print (f"Canonical   : {canon}")
                    

        print()
        print ("--------------------------------------")
        print ()

class FinColl(Fin):
    def execute(self) -> None:
        pg = self.picgestion
        if not pg.filtre:
            print('')

class FinCollecte(FinColl):
    pass
class FinMutucollecte(FinColl):
    pass

class FinMuturecharge(Fin):
    def execute(self) -> None:
        # Supprimer les droits o+rx sur archives
        conteneur = getConf('DKRMAINT')
        cmd = "chmod o-rx " + getConf('ARCHIVES')
        self._system(cmd,conteneur)

class FinMutuajout(Fin):
    def execute(self) -> None:
        pg = self.picgestion

        if pg.sitealias != None:
            return
        else:
            print()
            print("Rendez-vous sur https://" + pg.site + "/ecrire pour finir l'installation")

        # Fini !!!

class FinSpipclone(Fin):
    def execute(self) -> None:
        pg = self.picgestion
        url= f"https://{pg.user}.{getConf('DOMAINNAME')}/ecrire"
        print(f"Connectez-vous maintenant a l'URL {url} pour vérifier l'installation")
        print("Le clonage est termine")

class FinWpclone(Fin):
    def execute(self) -> None:
        print("Le clonage est termine")

class FinGaletteclone(Fin):
    def execute(self) -> None:
        print("Le clonage est termine")

class FinDolibarrclone(Fin):
    def execute(self) -> None:
        print("Le clonage est termine")
        
class FinAjout(Fin):
    def execute(self) -> None:
        pg = self.picgestion
        
        if self.fonction == "galette" and pg.cmsinstall:
            bd        = pg.database
            user      = pg.user
            pwd       = pg.dbpassword
            home      = pg.home
            conteneur = getConf('DKRMAINT')
            
            print ("")
            print (f"Pour terminer l'installation de galette, allez maintenant sur: https://{user}.{getConf('DOMAINNAME')}" )
            print (f"Remplissez le formulaire comme expliqué ci-dessous")
            print (f"")
            print (f"HOTE           = {getConf('HSTMYSQL')}")
            print (f"UTILISATEUR    = {user}")
            print (f"MOT DE PASSE   = {pwd}")
            print (f"NOM DE LA BASE = {bd}")
            print (f"")
            print (f"Lorsque vous aurez terminé, mais PAS AVANT,")
            print (f"Revenez sur cet ecran et tapez o")
            
            while True:
                if ouiNon("On termine l'installation ?",False):
                    break
            
            print (f"=== Suppression du répertoire install")
            cmd = f"cd {home}/galette/galette && tar czf install.tgz install && chown {user} install.tgz && rm -rf install"
            self._system(cmd,conteneur)

            print ("=== Mise en sécurité du fichier de config")
            cmd = f"cd {home} && chmod -R a-w galette/galette/config"
            self._system(cmd,conteneur)
            
        if self.fonction == "dolibarr" and pg.cmsinstall:
            bd        = pg.database
            user      = pg.user
            pwd       = pg.dbpassword
            home      = pg.home
            conteneur = getConf('DKRMAINT')
            
            print ("")
            print (f"Pour terminer l'installation de dolibarr, allez maintenant sur: https://{user}.{getConf('DOMAINNAME')}/install")
            print ("Remplissez le formulaire comme explique ci-dessous")
            print ("Ne modifiez pas les champs non cites ci-dessous")
            print ("")
            print (f"Repertoire devant contenir les documents = {home}/dolibarr/documents")
            print (f"Forcer les connexions securisees         = Cocher la case")
            print (f"Nom de la base de donnees                = {bd}")
            print (f"Serveur de base de donnees               = {getConf('HSTMYSQL')}")
            print (f"Identifiant                              = {user}")
            print (f"Mot de passe                             = {pwd}")
            print (f"")
            print (f"Lorsque vous verrez la page de login de Dolibarr, mais PAS AVANT,")
            print (f"Revenez sur cet ecran et tapez o")
            
            while True:
                if ouiNon("On termine l'installation ?",False):
                    break
                    
            print ("=== Mise en place du fichier install.lock")
            cmd = "cd " + home + " && touch dolibarr/documents/install.lock && chmod a=r dolibarr/documents/install.lock"
            self._system(cmd,conteneur,False,user)

            print ("=== Mise en securite du fichier de config")
            cmd = "cd " + home + " && chmod -R a-w dolibarr/htdocs/conf/conf.php"
            self._system(cmd,conteneur)

        if Plugin.message != "":
            msg = f"\n\nVeuillez copier-coller le message ci-dessous et l'envoyer a {pg.mail}\n"
            msg += f"========================================================\n"
            Plugin.message = msg + Plugin.message
            
class FinModification(Fin):
    def execute(self) -> None:
        if Plugin.message != "":
            pg = self.picgestion
            msg = f"\n\nVeuillez copier-coller le message ci-dessous et l'envoyer a {pg.mail}\n"
            msg += f"========================================================\n"
            Plugin.message = msg + Plugin.message
            

class FinSpipinstall(Fin):
    
    def execute(self) -> None:
        # message de fin
        pg = self.picgestion
        user = pg.user
        domaine = getConf('DOMAINAME')
        
        url   = f"https://{user}.{domaine}"
        print()
        print("INSTALLATION DE SPIP TERMINEE")
        print(f"Connectez-vous avec votre navigateur sur: {url}/ecrire") 
        print("pour terminer l'installation et creer les auteurs (en particulier picadmin)") 
