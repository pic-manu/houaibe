#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

import sys
import re
import os.path
import time
import version
import socket
import zipfile
import pathlib
import pwd
import pstats
import io
import traceback

#from functools import reduce

from erreur import *
from affiche import *
from plugin import Plugin
from picgestion import *
from picgsldap import chercheUtilisateurs

#
# picgs = PIC Gestion Systeme
#         Ouverture, modification, suppression de comptes pour les membres du pic
#         Et un paquet d'autres trucs
#
# 

# main.py = Plusieurs fonctions "main" utilisées par picgs et autres scripts

class AlgoCommon:
    '''Methods common to All Algos'''
    
    def _avant(self) -> None:
        '''executed BEFORE the algo'''

        if 'PICGS_DEBUG' in os.environ:
            print(afficheJaune('DEBUG ACTIVE'))
    
        if 'PICGS_PROFIL' in os.environ:
            print(afficheJaune('PROFILAGE ACTIVE'))
            import cProfile, pstats, io, re
            pr = cProfile.Profile()
            pr.enable()
            self.__pr = pr
            
    def _apres(self) -> None:
        '''executed AFTER the algo
           Will NOT be executed if an exception is raised
        '''

        if 'PICGS_PROFIL' in os.environ:
            pr = self.__pr
            pr.disable()
            s = io.StringIO()
            #sortby = SortKey.CUMULATIVE
            sortby = 'cumulative'
            ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
            #ps = pstats.Stats(pr, stream=s)
            ps.print_stats()
            print(s.getvalue())

            
class StdCommon(AlgoCommon):
    '''Methods common to AjStdAlgo and StdAlgo'''
    
    def _stdAlgo(self, prms, plugins, version_p, version_c) -> None:
        (action,nom,fonction,version,options) = prms.getData()
        
        if not '--silence' in options:
            print("=======================================================")
            msg = f"ATTENTION - ACTION={action}" 
            if len(options)>0:
                msg += f" OPTIONS={str(options)}"
            if nom != None:
                msg += f" COMPTE={nom}{str(version_c)}"
            print(msg)
            print("=======================================================")

        # Si l'utilisateur existe deja dans une version precedente, on initialise a partir de la derniere
        # version existante (version_p), puis on change de version pour la suite
        if version_p > 0:
            pg = PicGestion(plugins, action, nom, version_p, options)
            pg.setVersion(version_c)
    
        # S'il s'agit d'un nouvel utilisateur, on part de zero !
        else:
            pg = PicGestion(plugins, action, nom, version_c, options)
    
        # Remise à zéro du message
        Plugin.message = ""
        pg.agir()
    
        if Plugin.message != "":
            print(Plugin.message)


class AjStdAlgo(StdCommon):
    '''The algo "nearly standard", used for adding an user: computes a new version, then calls picgestion.agir'''
    
    def __call__(self, prms, plugins) -> None:
        # version_c is a "new" version, does not exist yet
        # version_p is the preceding one
        
        self._avant()
        [version_p,version_c] = prms.chercheVersionPourAjouter()
        self._stdAlgo(prms, plugins, version_p, version_c)
        self._apres()

class StdAlgo(StdCommon):
    ''' The algo "standard" is the algorithm used by most picgs actions: selects the version, then calls picgestion.agir'''
    
    def __call__(self, prms, plugins) -> None:
        # version_c is the last version of this user
        # version_p is the preceding one
        
        self._avant()
        [version_p,version_c] = prms.chercheVersionPourInfo()
        self._stdAlgo(prms, plugins, version_p, version_c)
        self._apres()

class NVStdAlgo(StdCommon):
    ''' The algo "standard", but with No version at all - used by picpkgs'''
    
    def __call__(self, prms, plugins) -> None:
        self._avant()
        self._stdAlgo(prms, plugins, 0, 0)
        self._apres()
        
class LoopAlgo(AlgoCommon):
    '''This algo loops over all users, ignores some users according to filters, and calls picgestion.agir for each remaining user'''

    def __call__(self, prms, plugins) -> None:

        self._avant()
        action = prms.action
        nom = prms.nom
        codefct = prms.codefct
        version = prms.version
        options = prms.options

        if '--cont' in options:
            cont = options['--cont']
        else:
            cont = None

        filtre = []
        if '--filtre' in options:
            filtre = options['--filtre'].replace(' ','').split(',')
            
        utilisateurs = chercheUtilisateurs(user=nom, version=version, codefct=codefct, conteneur=cont)   # Recherche des utilisateurs dans ldap

        # PICGS_LOOPALGO_MIN and  _MAX are very useful for debugging !
        i_min = int(os.getenv('PICGS_LOOPALGO_MIN',1))
        if i_min < 0:
            i_min = 1
        i_max = int(os.getenv('PICGS_LOOPALGO_MAX',len(utilisateurs)))
        if i_max < i_min:
            i_max = i_min
        i = 0
        for u in utilisateurs:
            i += 1
            # print (f"{action} {i_min} <= {i} <= {i_max} {u[0]}{u[1]} {options}")
            if i < i_min:
                continue
            if i > i_max:
                break
            try:
                if 'PICGS_DEBUG' in os.environ:
                    print(f"i={i} -> u={u[0]}{u[1]}")

                # On diffère la création de PicGestion pour améliorer la performance avec l'option --filtre
                # pg = PicGestion(plugins, action, u[0], u[1], options)
                pg = None
                if i == i_min:
                    pg = PicGestion(plugins, action, u[0], u[1], options) if pg == None else pg
                    entetes = pg.entetes()
                    if entetes != '':
                        print(entetes)
 
                if len(filtre) == 0:
                    pg = PicGestion(plugins, action, u[0], u[1], options) if pg == None else pg
                    pg.agir()

                elif f"{u[0]}{u[1]}" in filtre:
                    pg = PicGestion(plugins, action, u[0], u[1], options) if pg == None else pg
                    pg.agir()
                    
                if i == i_max:
                    pg = PicGestion(plugins, action, u[0], u[1], options) if pg == None else pg
                    derniere = pg.derniere()
                    if derniere != '':
                        print(derniere)
            
            # PicFatalErreur = we exit from the loop
            except PicFatalErreur as e:
                raise e        
            
            # Other exception = we stay in the loop, only indicating the error
            except Exception as e:
                print(afficheRouge(f"ERREUR avec l'utilisateur {u[0]}{u[1]} !"))
                print(afficheRouge(e))
                if 'PICGS_DEBUG' in os.environ:
                    traceback.print_exception(e)

        self._apres()

class MutuCommon(AlgoCommon):
    '''Methods common to the mutualisation mutuaware algorithms'''
    
    def _setNom(self) -> str:
        nom = getConf("MUTUNOM")
        if nom == None:
            raise PicErreur("La mutu spip n'est pas configurée - Paramètres MUTUNOM et MUTUVER")

        if not nom.endswith('-st'):
            nom += '-st'

        return nom

    def _setVersion(self) -> str:
        version = getConf('MUTUVER')
        if version == None:
            raise PicErreur("La mutu spip n'est pas configurée - Paramètres MUTUNOM et MUTUVER")

        version = int(version)
        if version <= 0:
            raise PicErreur("Mauvaise configuration de MUTUVER")

        return version
        
class StdMutuAlgo(MutuCommon):
    ''' The algo standard for the spip mutualisation'''

    def __call__(self, prms, plugins) -> None:
        
        self._avant()
        (action,nom,fonction,version,options) = prms.getData()
        
        # remplacer nom et version par ceux de la mutu !
        nom = self._setNom()
        version = self._setVersion()
        
        pg = PicGestion(plugins, action, nom, version, options)
        pg.agir()
        self._apres()
        
class LoopMutuAlgo(MutuCommon):
    '''Same as LoopAlgo, but for the mutu ! '''
    
    def __call__(self, prms, plugins) -> None:
        
        self._avant()
        (action,nom,fonction,version,options) = prms.getData()

        # remplacer nom et version par ceux de la mutu !
        nom = self._setNom()
        version = self._setVersion()
        
        pg = PicGestion(plugins, action, nom, version, options)
        print(pg.entetes())
        
        sites = pg.sites
        for site in sites:
            try:
                pg.setSiteAndAlias(site)
                pg.agir()

            # PicFatalErreur = we exit from the loop
            except PicFatalErreur as e:
                raise e        
            
            # Other exception = we stay in the loop, only indicating the error
            except Exception as e:
                print(afficheRouge(f"ERREUR avec le site {site} !"))
                print(afficheRouge(e))
                if 'PICGS_DEBUG' in os.environ:
                    traceback.print_exception(e)

        self._apres()
