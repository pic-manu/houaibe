#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

import sys
import re
import os.path
import time
import version
import socket
import zipfile
import pathlib
import pwd

from version import VERSION
from sysutils import SysUtils
from erreur import *
from affiche import *
from picconfig import getConf

# La variable LDAPOK est définie par picgsldap
from picgsldap import *

#
# picgs = PIC Gestion Systeme
#         Ouverture, modification, suppression de comptes pour les membres du pic
#

class Params(object):

    # Variables de classe
    __lmn = getConf('LONG_MAX_NOM')

    @classmethod
    def setLmn(cls, long_max_nom: int) -> int:
        '''set the long max nom parameter. Cannot be < LONG_MAX_NOM (default value)'''
        if long_max_nom < getConf('LONG_MAX_NOM'):
            cls.__lmn = getConf('LONG_MAX_NOM')
        else:
            cls.__lmn = long_max_nom

    @classmethod
    def getLmn(cls) -> int:
        '''get the long max nom parameter'''
        return cls.__lmn
    
    def __init__(self, app, argv, prod=True, default=False, helpMsg="", action_nom=False):
        '''Lit le fichier d'aide général: help.txt
           Initialise plusieurs données membres
           app = Nom de l'application, utilisé pour chercher les fichiers d'aide
           argv = Les arguments
           prod = True lorsqu'on est en usage normal, False lorsqu'on est en test, pour éviter d'envoyer plein de messages durant les tests
           default = Si True, action n'est pas spécifié dans les argv, et action vaut default
           helpMsg = Message d'aide utilisé si default = True (optionnel)
           action_nom = Si True, action est la concaténation des DEUX premiers paramètres, nom est dans le switch --nom
                        (utile par exemple pour picwpgs)'''

        self.__app = app
        self.__argv = argv
        self.__prod_flg = prod
        self.__default = default
        if default:
            self.__argv.insert(1,'default')
        self.__helpMsg = helpMsg
        self.__action_nom = action_nom
        
        # If inside a container, it should be pic-ssh: just use current user
        if os.getenv('DOCKER_RUNNING') == '1':
            pwd_info = pwd.getpwuid(os.getuid())
            user = pwd_info.pw_name
            self.__argv.insert(1,user)

        self.__actions = None
        self.__action = None
        self.__min = None
        self.__max = None
         
        self.__sudo = None
        self.__nom = None
        self.__nom_alt = None   # Pour la VERRUE
        self.__fonction = None
        self.__codefct = None
        self.__version = None
        
        self.__readHelp()
        self.__buildOptions()
        self.__buildAction()

        self.__checkArgs()
        
        self.__buildNom()
        self.__buildVersion()
        
        # S'il y a --help, on appelle printUsageAndExit
        if '--help' in self.__options:
            self.__printUsageAndExit()

    def isZipapp(self) -> bool:
        '''Renvoie True si le programme vit dans un zipapp '''
        
        # TODO - Trouver une manière plus robuste de faire !
        prg = sys.path[0]
        return prg.endswith('.pyz')
        
    def __v2int(self) -> int|None:
        ''' Renvoie self.__version convertie en entier (si possible, c-à-d si non None) '''
        if self.__version == None:
            return None
        else:
            return int(self.__version)

    def __findHelp(self, action=None):
        ''' Recherche et renvoie un file handler d'un fichier d'aide
            Si le fichier n'est pas trouvé, renvoie None
        '''

        if action:
            f = f"{action.strip()}.txt"
        else:
            f = "help.txt"

        # Dans un Zipapp: sous-répertoire help
        if self.isZipapp():
            root = sys.path[0]
            #print (f"koukou dans un Zipapp {root} !")
            fn = f"help/{f}"
            path = zipfile.Path(root,at=fn)
            
        # Dans un FS ordinaire: sous-répertoire lib/apphelp
        else:
            if 'HOUAIBEDIR' in os.environ:
                basedir = os.environ['HOUAIBEDIR']
            else:
                basedir = '/usr/local/houaibe'
            fn = f"{basedir}/picgs/{version.MAJOR}.{version.MINOR}/lib/{self.__app}help/{f}"
            path = pathlib.Path(fn)

        return path

    def __readHelp(self) -> None:
        '''Lit le fichier d'aide général et contruit le dictionnaire self.__actions
           Construit aussi le tableau self.__help qui sera utilisé pour afficher l'aide'''
       
        actions = {}
        a_help = []
        path = self.__findHelp()

        try:
            with path.open('r') as fh_help:
                for ligne in fh_help:
                    ligne = ligne.strip()
                    if ligne == '' or ligne.startswith('#'):
                        continue
                        
                    # Calcul des arguments qui peuvent être utilisés après action
                    # NB - Pas utilisé pour l'instant
                    if ligne.startswith('!'):
                        a = ligne.partition('!')[2]
                        arg_names = [ x.strip() for x in a.split(';') ]
                        #self.__args = arg_names
                        continue
    
                    # Ligne de titre - dans help mais pas dans actions
                    if ligne.startswith('='):
                        a_help.append(ligne.strip()) 
                        continue
    
                    # Ligne normalement composée de 4 champs                                                            
                    a_ligne = ligne.split(';',3)
                    if len(a_ligne) != 4:
                        raise PicFatalErreur(f"ERREUR INTERNE - Fichier {n_help} MAL FORME: #{ligne}#")
    
                    # Si action commence par 0, on doit être root et le premier champ vaut True
                    action = a_ligne[0].replace(' ','')
                    if action.startswith('0'):
                        sudo = True
                        action = action.lstrip('0')
                    else:
                        sudo = False
    
                    actions[action] = [sudo,int(a_ligne[1].strip()),int(a_ligne[2].strip()),a_ligne[3].strip()] + arg_names
                    a_help.append(action)
        except FileNotFoundError:
            pass

        # On ajoute l'action par défaut, si nécessaire                
        if self.__default:
            actions['default'] = [False,1,4,self.__helpMsg] + ['nom', 'fonction', 'version']

        self.__actions = actions
        self.__help = a_help
        #import pprint
        #pprint.pprint(actions)
        
    def __getHeader(self) -> str:
        action = self.__action
        h = f"Usage: {self.__app} version VERSION\n"
        return h

    def __helpGeneral(self) -> str:
        '''Renvoie le message d'aide général, construit à partir de self.__actions'''

        actions = self.__actions
        a_help = self.__help
        u = ""

        # Les arguments possibles
        #args = self.__args
        #long_args = len(reduce(lambda x, y: x + y, args)) + 3 * len(args)

        for a in a_help:
            
            # Une ligne de titre
            if a.startswith('='):
                u += "\n" + afficheGras(a) + "\n"
            else:
                args = actions[a][4:]
                a_min = actions[a][1]
                a_max = actions[a][2]
                if self.__action_nom:
                    a_min -= 1
                    a_max -= 1
                desc= actions[a][3]
                l = f"{self.__app} {a.replace('_',' ')} "

                # Afficher les arguments de cette action, avec des [] si l'argument est facultatif
                l += " ".join(self.__helpMinMax(args,a_min,a_max))
                l += ' [--help]'
                u += l.ljust(50) 
                u += f"{desc}\n"

        return u
    
    def __helpMinMax(self,args,a_min,a_max) -> list:
        '''Renvoie une copie de args avec les paramètres optionnels entre crochets'''

        rvl = []

        if a_min <= 0:
            raise ValueError(f"{args[0]} {a_min} {a_max} a_min <= 0")

        if a_max > len(args) + 1:
            raise ValueError(f"{args[0]} {a_min} {a_max} a_max > {len(args)+1}")

        if a_max < a_min:
            raise ValueError(f"{args[0]}  a_max ({a_max}) < a_min ({a_min})")
            
        # L'action ne figure pas dans args !
        a_min -= 1
        a_max -= 1
        
        # Les paramètres obligatoires
        i = 0
        for j in range(a_min):
            rvl.append(args[i])
            i += 1

        # Les paramètres optionnels
        for j in range(a_max-a_min):
            rvl.append(f"[{args[i]}]")
            i += 1

        return rvl
                
    def __helpAction(self) -> str:
        '''Renvoie le message d'aide sur une action en particulier'''
        
        action = self.__action
        if action == None:
            raise PicFatalErreur("ERREUR INTERNE - Action vaut None")

        path = self.__findHelp(action)
        usge = ""
        try:
            with path.open('r') as fh_help:
                for ligne in fh_help:
                    if ligne.startswith('#'):
                        continue
                    if not ligne.startswith(' '):
                        usge += afficheGras(ligne.strip()) + "\n"
                    else:
                        usge += ligne 
        except FileNotFoundError:
            if action == 'default':
                usge += self.__helpMsg
            else:
                usge += f"Pas d'aide pour l'action {action}"
            
        return usge
        
    def __printUsageAndExit(self, msg='') -> str:
        '''Affiche l'aide et lève une exception'''
        
        if self.__prod_flg and msg != '':
            print (afficheRouge(msg))
            print('----------------------------------------------------')

        usge = self.__getHeader()
        usge = usge.replace('VERSION',VERSION)
        
        if self.__action == None:
            usge += self.__helpGeneral()
        else:
            usge += self.__helpAction()
            
        if self.__prod_flg:
            print (usge)

        raise(PicErreur(" "))

    def __buildAction(self) -> None:
        '''Calcule une version normalisée de action, ou appelle printUsageAndExit
           PREREQUIS: __buildOptions a déjà été appelé, donc il n'y a plus de --xxx dans argv
        '''

        argv = self.__argv
        if len(argv) == 1 or self.__action_nom and len(argv) == 2:
            if '--help' in self.__options:
                msg = ''
            else:
                msg = f"ERREUR - Pas d'action spécifiée !"
            self.__printUsageAndExit(msg)
    
        action = argv[1].lower()
        if self.__action_nom:
            action += '_' + argv[2].lower()

        actions = self.__actions
        actions_possibles = { a:actions[a] for a in actions if a.startswith(action) }

        # Aucune action, ERREUR
        if len(actions_possibles) == 0:
            msg = f"ERREUR - Pas d'action dont le nom commence par {action}"
            return self.__printUsageAndExit(msg)
        
        # Une seule action trouvée: on la stocke et on la renvoie
        #import pprint
        #pprint.pprint(actions_possibles)
        if len(actions_possibles) == 1:
            a = list(actions_possibles.keys())[0]
            self.__action = a
            self.__sudo = actions_possibles[a][0]
            self.__min = actions_possibles[a][1]
            self.__max = actions_possibles[a][2]

        else:
            # Plusieurs actions, ERREUR
            msg = "ERREUR - Plusieurs actions possibles: "
            for a in actions_possibles.keys():
                msg += a + " "
            self.__printUsageAndExit(msg)

    def __buildNom(self) -> None:
        '''Calcule self.__nom
           PREREQUIS = __checkArgs et __buildOptions ont déjà été appelés'''
        
        argv = self.__argv
        
        # Depuis v4.0.0: Si le flag self.__action_nom est mis, action vaut argv[1] + '_' + argv[2]
        #                Dans ce cas, nom peut être fixé par le switch --nom
        # Depuis v3.14.5: On passe nom en minuscules sinon problème avec le serveur ldap !
        if self.__action_nom:
            if '--nom' in self.__options:
                self.__nom = self.__options['--nom']
            else:
                self.__nom = None
        else:
            if len(argv) >= 3:
                self.__nom = argv[2].lower()

        # On n'utilise pas ldap ici'
        #if LDAPOK == False:
        #    return
            
        # Si le paramètre qui suit l'action ne s'appelle pas nom, on retourne tout de suite
        a = self.__action
        actions= self.__actions
        
        if actions[a][4] != 'nom':
            return
            
        # Si --prod, le nom n'est pas limité car c'est un nom de domaine
        #if not '--prod' in self.__options:
        if self.__nom != None:
            if len(self.__nom) > Params.getLmn():
                self.__printUsageAndExit(f'ERREUR - Le nom doit avoir {Params.getLmn()} caracteres au plus')

            if len(argv) >= 4:
                self.__fonction = argv[3].lower()
                fonctions = getConf('FONCTIONS')
                codefcts  = getConf('CODEFCT')

                if not self.__fonction in fonctions:
                    self.__printUsageAndExit(f"ERREUR - Fonction = {self.__fonction} - devrait être {str(getConf('FONCTIONS'))}" )

                # site -> st etc.
                fctind = fonctions.index(self.__fonction)
                self.__codefct = codefcts[fctind]
                

                # VERRUE - self.__nom_alt prend l'ancien nom (seulement si 12 caractères))
                #          12 caractères est l'équivalent de LONG_MAX_NOM jusqu'à la version 3.12
                #          Sinon on laisse à la valeur par défaut, ie None
                #          Exemple: azertyazerty => azertyazertyst1 jusqu'à 3.12
                #                                   azertyazerty-st1 à partir de 3.13
                # La VERRUE consiste à gérer les deux systèmes: l'existant d'un côté, les nouveaux site de l'autre !
                if len(self.__nom) == 12:
                    self.__nom_alt = self.__nom + self.__codefct

                # Ajout de la fonction au nom
                self.__nom += '-'
                self.__nom += self.__codefct

    def __buildVersion(self) -> None:
        '''Calcule self.__version
        PREREQUIS = __buildNom a déjà été appelé'''
        argv = self.__argv
        if len(argv) >= 5:
            self.__version = argv[4].lower()

    def __buildOptions(self) -> None:
        '''Construit le dictionnaire options, à partir des paramètres --xxx
           Supprime ces paramètres de argv, qui est donc modifié
           S'il y a --version, on affiche la version et on sort'''

        options = {}
        argv = self.__argv
        
        i=len(argv) - 1
        while i>0:
            if argv[i].startswith('--'):
                o = argv[i].partition('=')
                options[o[0]] = o[2]
                argv.remove(argv[i])
            i -= 1
    
        if '--version' in options:
            print("Version " + VERSION)
            exit(0)
            
        self.__options = options

    def __checkCompte(self) -> None:
        ''' Essaie d'analyser l'argument pour extraire nom, fonction, version 
            Modifie self.__argv pour faire comme si on avait tapé 3 paramètres
            TODO - ça c'est pas très propre'''
        
        # Pas d'utilisation de ldap ici
        #if LDAPOK == False:
        #    return
            
        argv = self.__argv
        
        # Si le paramètre qui suit l'action ne s'appelle pas nom, on retourne tout de suite
        a = self.__action

        actions= self.__actions

        #import pprint
        #pprint.pprint(actions[a])
        #pprint.pprint(argv)
        if actions[a][4] != 'nom':
            return
        
        # Si c'est un nom de compte, pas d'autres arguments
        if len(argv) != 3:
            return
            
        # cf. picgsldap.py chercheUtilisateurs - on prend les mêmes regex
        # modele2 est inutilisé, pour gérer les noms genre azertyazertyst1 on utilisera SEULEMENT la syntaxe azertyazerty site 1
        modele1 = re.compile('^([a-z0-9-_]{1,' + str(Params.getLmn()) + '}-[a-z][a-z])([0-9]+)$')

        compte = argv[2]
        matched = modele1.match(compte)

        # Si pas de match ce n'est pas un nom de compte
        if matched == None:
            return

        nom = matched.group(1)

        # Si ne se termine pas par un nombre, ce n'est pas un nom de compte            
        try:
            version = matched.group(2)
        
        except ValueError as e:
            return
            
        codefct = nom[-2:]
                        
        # Si la fonction n'est pas reconnue, ce n'est pas un nom de compte
        codefcts = getConf('CODEFCT')
        fonctions= getConf('FONCTIONS')
        if not codefct in codefcts:
            return
        else:
            fonction = fonctions[codefcts.index(codefct)]

        # On modifie argv car on on est sûr que c'est un nom de compte 
        argv[2] = nom[0:-3]
        argv.append(fonction)
        argv.append(version)
        
    def __checkProd(self):
        '''Recherche le nom technique correspondant au nom de production passé en paramètres
           Stocké dans argv[2] en remplacement du nom donné initialement
           Si aucun ou plusieurs noms sont trouvés, lève une exception'''
           
        # TODO - Ne marche PAS avec les conteneurs nginx-fpmxx !!!!!!!
                
        if '--prod' in self.__options:
            sysutils = SysUtils()
            nom = self.__argv[2]
            cmd = f"cd {getConf('APACHE_AVAIL')} && grep -lri 'serveralias *{nom}' *{getConf('APACHE_CEXT')} 2>/dev/null| sort -u"
            fichiers=[]
            for c in getConf('DKRSRVWEB'):
                try:
                    fichiers += sysutils._system(cmd,c)
                except:
                    1;
    
            comptes = [ f.partition(getConf('APACHE_CEXT'))[0] for f in fichiers if f != '' ]
            if len(comptes) == 0:
                raise PicErreur(f"ERREUR - Aucun site avec {nom} dans l'adresse de production")
            
            elif len(comptes) > 1:
                #print(comptes)
                raise PicErreur(f"ERREUR - Plusieurs adresses contiennent {nom}")

            else:
                self.__argv[2] = comptes[0]

    def __checkArgs(self) -> None:
        ''' Vérifie que la taille de argv est compatible avec ce qui est déclaré, sinon appelle l'aide et sort 
            PREREQUIS: __buildAction a déjà été appelé'''
        
        # On a peut-être entré directement un nom de production !
        # __buildOptions doit avoir été appelé avant
        self.__checkProd()

        # On a peut-etre entré directement un nom de compte
        self.__checkCompte()

        argv = self.__argv        
        nb_args = len(argv) - 1
        action = self.__action

        min = self.__min
        max = self.__max
        if nb_args < min or nb_args > max:
            self.__printUsageAndExit()

    def checkSudo(self) -> None:
        if self.__sudo and os.getuid() != 0:
            raise PicErreur (f"ERREUR - il faut être root pour utiliser picgs {self.__action} !")
            
    def getData(self) -> tuple[str, str, str, int, dict]:
        ''' En cas de sudo, vérifie si on est bien root
            Renvoie action, nom,     fonction, version,  options
                    str     str|None srt|None  int|None  dict'''
        
        self.checkSudo()
            
        # Renvoie les données
        return (self.__action, self.__nom, self.__fonction, self.__v2int(), self.__options)

    @property
    def action(self) -> str:
        self.checkSudo()
        return self.__action

    @property
    def nom(self) -> str:
        self.checkSudo()
        return self.__nom

    @property
    def fonction(self) -> str:
        self.checkSudo()
        return self.__fonction

    @property
    def codefct(self) -> str:
        self.checkSudo()
        return self.__codefct

    @property
    def version(self) -> int|None:
        self.checkSudo()
        return self.__v2int()
        
    @property
    def options(self) -> dict:
        self.checkSudo()
        return self.__options
        
    # VERRUE - A cause de la verrue, self.__nom a peut-être changé dans chercheVersionPourInfo 
    #          du coup on doit pouvoir récupérer la nouvelle valeur !
    def getNom(self) -> str:
        return self.__nom
           
    def __versionsExistantes(self, nom) -> list[int]:
        '''Retourne une liste triée d'entiers correspondant aux numéros de versions existants pour un nom d'utilisateur donné'''

        if LDAPOK == False:
            return []
            
        # on recherche a partir des utilisateurs nomX dans ldap
        modele = re.compile(nom + '([0-9]+)$')

        first_uidn = int(getConf('FIRSTUID'))
        last_uidn  = int(getConf('LASTUID'))

        # Par défaut en readonly, on repassera readwrite si nécessaire
        ldap = Ldap(True)
        u_list = ldap.searchUser(None,attrs=['uid','uidNumber'])
        
        versions=[]
        for l in u_list:
            u         = l['attributes']['uid'][0]
            uidNumber = l['attributes']['uidNumber']
        
            matched = modele.match(u)
            if matched != None and uidNumber >= first_uidn and uidNumber < last_uidn:
                versions.append(int(matched.group(1)))
        
        # on trie
        versions.sort()

        return versions

    def chercheVersionPourInfo(self) -> list[int]:
        '''Cherche le numero de version pour info
        Si self.__version vaut None, on recherche le numero de version le plus grand
        Sinon on recherche si version existe En cas de pb, on lève une exception
        On renvoie [0,cour] puisqu'on se fiche de la version precedente dans ce cas
        C est un tableau de int
        ATTENTION - Si on appelle la verrue, on peut modifier self.__nom (c'est bien une verrue)
        '''

        # Pas de nom, pas de versions !
        if self.__nom == None:
            return [0,0]

        version = self.__v2int()
         
        # If living in a container, return the current version number
        if os.getenv('DOCKER_RUNNING') == '1':
            return [0, version]

        # Liste des versions existantes
        versions=self.__versionsExistantes(self.__nom)
        
        # VERRUE - DEBUT
        versions_alt = []
        if self.__nom_alt != None:
            versions_alt = self.__verrueChercheVersions(versions)
            versions += versions_alt
            versions.sort()
        # VERRUE - FIN     

        # import pprint
        # pprint.pprint(versions)

        if len(versions)==0 and len(versions_alt)==0:
            raise PicErreur(f"ERREUR - PAS d'utilisateur ayant pour nom {self.__nom}X")
        
        else:
            # Pas de version, on renvoie la denière version existante
            if version == None:
                return [0,versions[len(versions)-1]]
                
            # Version spécifiée, correspond à une version existante
            if version in versions:
                # VERRUE - DEBUT - version correspond à nom_alt (ancien nommage)
                if version in versions_alt:
                    # Hou la vilaine verrue on CHANGE self.__nom
                    self.__nom = self.__nom_alt
                    return[0,version]
                # VERRUE - FIN
                else:
                    return [0,version]
            
            # Version spécifiée, rien de trouvé
            else:
                raise PicErreur(f"ERREUR - PAS d'utilisateur ayant pour nom {self.__nom}{version}")

    def chercheVersionPourAjouter(self) -> list:
        '''Cherche le numero de version courante pour un nouvel utilisateur
        Si version est passée par parametre, on essaiera de prendre version comme version courante
        Si version est passée par parametre, on essaiera de prendre version comme version courante
        Renvoie [version_precedente,version_courante] si version_precedente n'existe pas, le remplace par 0
        Algo = je recherche le numero le plus élevé qui existe deja et j'ajoute 1
        C un tableau de int'''

        #self.getData()
        # Liste des versions existantes
        versions=self.__versionsExistantes(self.__nom)

        # VERRUE - DEBUT
        if self.__nom_alt != None:
            versions_alt = self.__verrueChercheVersions(versions)
            versions += versions_alt
            versions.sort()
        # VERRUE - FIN

        # valeurs de retour par defaut
        prec=0
        cour=1
    
        # si on a trouve des versions, on garde celle avec le plus haut numero
        if len(versions)>0:
            prec = versions[-1]

        #import pprint
        #pprint.pprint(versions)

        # si on a demande un numero de version precis, on essaie de le prendre
        version = self.__v2int()
        if version != None:
            if version in versions:
                raise PicErreur(f"ERREUR - version {version} existe deja !!!")
            else:
                cour = version
    
        # sinon on incremente le dernier trouve
        else:
            cour = prec+1
            
        return [prec,cour]

    def __verrueChercheVersions(self, versions) -> list:
        '''Appelé par chercheVersion... lorsque self.__nom a 12 caractères
           pour gérer les anciens noms de 12 charactères: azertyazerty ==> azertyazertyst1 au lieu de azertyazerty-st1 !
           C'est ça la verrue'''

        versions_alt = []
        if self.__nom_alt != None:
            versions_alt = self.__versionsExistantes(self.__nom_alt)

        # On ne peut pas avoir un numéro de version avec ancien nom ET nouveau nom !            
        v  = set(versions)
        va = set(versions_alt)
        if len(v.intersection(va)) != 0:
            raise PicErreur(f"Il y a des numéros de version communs à {self.__nom} et {self.__nom_alt}")

        return versions_alt
