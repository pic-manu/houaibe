# 
# Message d'aide pour l'action nohttps
#         Les lignes débutant à la colonne 0 seront écrites en gras
#
sudo picgs nohttps
      Supprime un certificat, aussi bien de Dehydrated que du reverse proxy
      Utile lorsqu'un nom DNS disparaît, par exmple avant de fermer un site

      ATTENTION - Il faut être root
                  Cette action est relativement dangereuse car le site peut devenir inaccessible
                  Mais aisément réversible (grâce à la commande http)
