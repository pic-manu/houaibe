# 
# Message d'aide pour l'action collecte
#         Les lignes débutant à la colonne 0 seront écrites en gras
#
picgs collecte [nom] [--long][--short] [fonction] [version] [--filtre=monsite-st1,tonsite-st2]> FICHIER.tsv
      Collecte des informations sur tous les sites hébergés et envoie sur la sortie standard
      un fichier tsv (séparateur = TAB) qui pourra être repris par un tableur
      [--long] donne une colonne de plus avec la taille du site, mais ça dure plus longtemps
      [--short] donne moins d'infos, mais va très vite
      
picgs collecte [nom] --cont=pic-itk83 [--long][--short] [nom] [fonction] [version] > FICHIER.tsv
      Se limite aux sites web qui se trouvent dans le conteneur pic-itk73
      
picgs collecte [nom] --cont=nginx-fpm83 [--long][--short] [nom] [fonction] [version] > FICHIER.tsv
      Se limite aux sites web qui utilisent les images nginx et fpm83

picgs collecte --filtre=monsite-st1,tonsite-st2 [--long][--short] [nom] [fonction] [version] > FICHIER.tsv
      Se limite aux sites explicitement spécifiés

picgs collecte monsite [--long][--short] [nom] [fonction] [version] > FICHIER.tsv
      Se limite aux sites dont le nom commence par monsite

      ATTENTION - Un site dans la mutu spip ne sera pas concerné par cette commande.
                  Allez voir mutucollecte
