# 
# Le message d'aide général - Contient la liste des actions utilisables par ce programme
#
# FORMAT DE FICHIER: csv (séparateur ;)
#
# colonne 1 = Nom de l'action (si commence par 0, on doit être root !)
# colonne 2 = Nombre minimal de paramètres positionnels en tenant compte de action (int)
# colonne 3 = Nombre maximal de paramètres positionnels en tenant compte de action (int)
# colonne 4 = Brève description 
#
# Les lignes blanches ou  commençant par # sont ignorées

# Les lignes de ce style sont des sous-titres, utilisés dans l'affichage de l'aide:
# ========= texte du sous-titre
# La ligne commençant par ! est une ligne csv donnant le nom des arguments qui suivent action

====== Actions générales            
! nom ; fonction ; version
ajout      ; 3; 4; Ajoute un compte et un espace web
modification; 4; 4; Modifie une info sur un site
demenagement; 4; 4; Change de conteneur un site
suppression ; 4; 4; Supprime un compte et un espace web, efface les données
mdp         ; 4; 4; Genere un mot de passe pour le compte picadmin (spip ou wp)

====== Afficher l'information            
collecte    ; 1; 4; Collecte les informations générales sur tous les sites hébergés
info        ; 2; 4; Affiche les infos sur un site particulier

====== Accès ssh            
ssh         ; 4; 4; Donne un accès ssh
nossh       ; 4; 4; Supprime un accès ssh précédemment donné

====== Adresses de production, redirections et certificats
production  ; 4; 4; Donne une adresse de production à un compte
0canon      ; 4; 4; root: Identifie une des adresses de production comme canonique et met en place les redirections 
0cert       ; 1; 1; root: Visualise l'état des certificats
0https      ; 1; 1; root: Génère un certificat pour une adresse de production
0nohttps    ; 1; 1; root: Supprime un certificat pour une adresse de production

====== Sites statiques adossés à un site dynamique
statique    ; 4; 4; Ajoute un site statique à côté du site dynamique (Expérimental)
nostatique  ; 4; 4; Supprime la configuration statique (mais garde intacts les fichiers du site statique)

====== Sauvegardes, restitutions,clonages
sauvebd     ; 1; 4; Sauvegarde la base de données de chaque compte dans le répertoire db
archivage   ; 4; 4; Archive les données d'un espace web 
recharge    ; 4; 4; Recharge (éventuellement sous un autre compte) des données archivées
spipclone   ; 4; 4; Clône un site sous spip
wpclone     ; 4; 4; Clône un site sous Wordpress
galetteclone; 4; 4; Clône une galette
dolibarrclone; 4; 4; Clône une installation Dolibarr

====== SPIP (hors mutualisation)
spipinstall ; 4; 4; Installe spip
spipmaj     ; 4; 4; Met à jour spip

====== Activer, désactiver
desactiver  ; 4; 4; Désactive un site
reactiver   ; 4; 4; Réactive un site
inactifs    ; 1; 1; Liste les sites désactivés
 
====== MUTUALISATION SPIP
mutuajout   ; 1; 1; Ajoute un site à la mutu
mutusuppr   ; 1; 1; Supprime un site de la mutu
mutuarchivage ; 1; 1; Archive un site mutualisé (spip n'est PAS archivé)
muturecharge  ; 1; 1; Récupère un site de la mutu archivé
mutuimporte       ; 1; 1; Importe un nouveau site dans la mutu
mutuexporte       ; 1; 1; Exporte un site pour le sortir de la mutu
mutuvidelescaches ; 1; 1; Vide les caches de tous les sites mutualisés
mutuupgrade      ; 1; 1; Passe la mutu en mode "upgrade de plugins"
mutuprod         ; 1; 1; Repasse la mutu en mode "production" (plugins protégés)
mutuinfo         ; 1; 1; Affiche les infos sur un site de la mutu
mutumodif        ; 1; 1; Modifie les infos d'un site de la mutu
mutucollecte     ; 1; 1; Collecte et affiche les infos sur tous les sites de la mutu
mutusauveplugins ; 1; 1; Copie la liste des plugins activés dans l'espace web de chaque site
mutusauvebd      ; 1; 1; Sauvegarde la base de données de chaque site dans le répertoire squelettes

====== WORDPRESS
wpinstall        ; 4; 4; Installe wordpress
wpmaj            ; 4; 4; Met à jour wordpress

====== DOLIBARR
dolibarrmaj      ; 4; 4; Met à jour Dolibarr (picgs ajout pour installer Dolibarr)

====== GALETTE
galettemaj       ; 4; 4; Met à jour Galette (picgs ajout pour installer Galette)

