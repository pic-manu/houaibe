# 
# Message d'aide pour l'action archivage
#         Les lignes débutant à la colonne 0 seront écrites en gras
#
picgs archivage monsite site 1
picgs archivage monsite galette 1
picgs archivage monsite dolibarr 1
picgs archivage monsite autre 1
picgs archivage monsite-st1
      Met tous les fichiers dans un tar.gz, et extrait la base de données dans un .sql
      Les répertoires dans lesquels on a le fichier ou répertoire CACHEDIR ne seront pas archivés
      
      Il convient d'exécuter cette commande avant toute mise à jour de CMS
      L'archivage se fera dans un sous-répertoire de /home/archives, accessible avec toctoc
