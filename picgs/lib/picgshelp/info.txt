# 
# Message d'aide pour l'action info
#         Les lignes débutant à la colonne 0 seront écrites en gras
#
picgs info monsite [--short]
      Donne des informations sur tous les comptes qui commencent par monsite
      --short: Affiche moins d'informations, mais va plus vite'
      
picgs info monsite.exemple.org --prod [--short]
      Considère que monsite.exemple.org est une adresse de production
      Recherche le compte correspondant et affiche les informations
