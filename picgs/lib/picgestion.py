#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# MODULE picgestion = Definition de l'objet PicGestion
#                     PicGestion contient une liste de plugins
#                     Quelques methodes permettent d'exporter de l'information, celle-ci se trouve dans les plugins


version = '0.1'

import sys
import os
from erreur import *
from picconfig import getConf
from plugin import Factory
from affiche import afficheRouge

import subprocess
import re

class PicGestion:
    # Ces tableaux publics peuvent être utilisés par des plugins pour y stoker des données utilisables d'une itération à l'autre (LOOP algorithms)
    # adata = Array, ddata = Dictionary
    adata = []
    ddata = {}
    
    def __init__(self,plg,action,nom,version,options={}):
        '''Constructeur: On construit et initialise tout un tas de plugins'''

        # Construction de tous les plugins à partir de plg
        # Attention dans plg il peut y avoir soit des noms soit des classes
        # Si ce sont des classes elles doivent dériver de Plugin !
        #self._noms_plugins = noms_plugins;
        self._noms_plugins = []
        self._plugins = {}
        for p in plg:
            pl = Factory.createPlugin(p,action,nom,version,options,self)
            nom_pl = pl.nomplugin
            self._noms_plugins.append(nom_pl)
            self._plugins[nom_pl] = pl

        # initialise completement le constructeur en echangeant des donnees entre plugins
        for p in self._noms_plugins:
            self._plugins[p].initialise()

    def __str__(self) -> str:
        rvl = ""
        for p in self._plugins:
            rvl += str(p)
        return rvl

    def agir(self) -> None:
        '''Appelle demandeParams puis execute pour chaque plugin, puis imprime un \n '''

        for k in self._noms_plugins:
            self._plugins[k].demandeParams()

        # Voir main.py pour le traitement d'erreur
        for k in self._noms_plugins:
            try:
                #print (k)
                self._plugins[k].execute()

            except PicFatalErreur as e:
                raise e

            except:
                print(afficheRouge(f"PLUGIN {k} - Erreur rencontree - Vérifiez que tout est normal"))
                raise
        
    def entetes(self) -> str:
        '''Appelle entetes pour chaque plugin '''
        e = ''
        for k in self._noms_plugins:
            e += self._plugins[k].entetes()
        return e

    def derniere(self) -> str:
        '''Appelle derniere pour chaque plugin '''
        d = ''
        for k in self._noms_plugins:
            d += self._plugins[k].derniere()
        return d

    def setVersion(self,version) -> None:
        '''Change le numero de version - Appelle tous les plugins car ils peuvent avoir des trucs a recalculer'''
        for k,p in self._plugins.items():
            p.version = version

    @property
    def filtre(self) -> bool:
        return self.__proxy('Debut', 'filtre')

    # La plupart des "property" ci-dessous utilisent un des plugins pour
    # récupérer la valeur de la property
    def __proxy(self, nom_plg, attr):
        if nom_plg in self._plugins:
            return getattr(self._plugins[nom_plg], attr)
        else:
            raise PicFatalErreur(f'ERREUR - plugin {nom_plg} inconnu')

    @property
    def mutuuser(self):
        return self.__proxy('Compte', 'mutuuser')
        
    @property
    def user(self):
        return self.__proxy('Compte', 'user')

    @property
    def uid(self) -> int:
        return self.__proxy('Compte', 'uid')

    @property
    def uid2(self) -> int:
        return self.__proxy('Compte', 'uid2')

    @property
    def gid(self) -> int:
        return self.__proxy('Compte', 'gid')

    @property
    def password(self) -> str:
        return self.__proxy('Compte', 'password')

    @property
    def home(self) -> str:
        return self.__proxy('Compte', 'home')
        
    @property
    def www(self) -> str:
        return f"{self.home}/www"

    @property
    def wwws(self) -> str:
        return f"{self.home}/wwws"

    @property
    def priv(self) -> str:
        return f"{self.home}/priv"

    @property
    def mail(self) -> str:
        return self.__proxy('Compte', 'mail')
        
    @property
    def nomlong(self) -> str:
        return self.__proxy('Compte', 'nomlong')

    @property
    def site(self) -> str:
        return self.__proxy('Debut', 'site')

    def setSiteAndAlias(self,site) -> None:
        '''Utile en cas de mutu !'''
        if 'Debut' in self._plugins:
            return  self._plugins['Debut'].setSiteAndAlias(site)
        else:
            raise PicErreur('ERREUR - plugin debut non encore defini')

    @property
    def sitealias(self) -> str:
        return self.__proxy('Debut', 'sitealias')

    # On recherche l'info sur le plugin Apache ET sur le plugin Nginx
    # On lance une exception si:
    #    - Aucun des deux plugins n'existe
    #    - Les deux plugins ont trouvé une adresse de production
    @property
    def prod(self) -> tuple[set,str]:
        prodApache = []
        prodNginx = []
        apu = ''
        npu = ''
        if 'Apache' in self._plugins:
            prodApache = self._plugins['Apache'].prod

        if 'Nginx' in self._plugins:
            prodNginx = self._plugins['Nginx'].prod

        if len(prodApache) == 0 and len (prodNginx) == 0:
            raise PicFatalErreur('ERREUR - plugin Apache et nginx non encore definis')
            
        if len(prodApache) > 0:
            apu = prodApache[1]

        if len(prodNginx) > 0:
            npu = prodNginx[1]

        if apu != '' and npu != '':
            msg  = 'ERREUR - Plusieurs versions de prod de plusieurs users: ' + "\n" + npu + "\n" + apu + "\n"
            msg += "\ncorrigez cela a la main et reessayez\n"
            raise PicErreur(msg)

        if apu != "":
            return prodApache

        if npu != '':
            return prodNginx
            
        return [set(),'']

    @property
    def prodnext(self) -> str:
        return self.__proxy('Debut', 'prodnext')

    @property
    def sites(self) -> tuple[set]:
        return self.__proxy('Apache', 'sites')

    @property
    def image(self) -> str:
        return self.__proxy('Compte', 'image')

    # Architectures de conteneurs:
    ARCHI_APACHE_MODPHP = 1 # Un conteneur avec apache/mpm_itk/plusieurs vhosts/mod_php
    ARCHI_NGINX_FPM = 2     # Un ensemble de 2 conteneurs avec nginx/fpm, UN SEUL site servi par conteneur
    
    @property
    def archi(self) -> str:
        return self.__proxy('Compte', 'archi')

    @property
    def conteneur(self) -> str:
        return self.__proxy('Compte', 'conteneur')

    @property
    def conteneur_old(self) -> str:
        return self.__proxy('Compte', 'conteneur_old')

    @property
    def image_old(self) -> str:
        return self.__proxy('Compte', 'image_old')

    @property
    def archi_old(self) -> str:
        return self.__proxy('Compte', 'archi_old')

    @property
    def source(self) -> list[str]:
        '''Renvoie [repertoire, user, version source] - Utilisé pour recharge'''
        return self.__proxy('Debut', 'source')

    # TODO - Pas terrible, la méthode est définie par HomeDirMutualisation qui dérive de HomeDir
    #        Si on appelle à partir de la classe HomeDir, ça va planter
    @property
    def mutuarchiversite(self) -> bool:
        return self.__proxy('HomeDir', 'mutuarchiversite')
        
    @property
    def database(self) -> str:
        return self.__proxy('Mysql', 'database')

    @property
    def dbpassword(self) -> str:
        return self.__proxy('Mysql', 'dbpassword')
    
    @property
    def cmswp(self) -> bool:
        '''True = programme l'installation automatique de wp lors de pigs ajout'''
        return self.__proxy('HomeDir', 'cmswp')
            
    @property
    def cmswpmig(self) -> bool:
        '''True = programme la préparation de migration de wordpress lors de picgs ajout'''
        return self.__proxy('HomeDir', 'cmswpmig')

    @property
    def cmsinstall(self) -> bool:
        '''True = Demande l'installation automatique d'un CMS lors de pigs ajout'''
        return self.__proxy('HomeDir', 'cmsinstall')
            
    @property
    def isinactif(self) -> bool:
        ''' Renvoie True si inactif '''

        archi = self.archi
        if archi == self.ARCHI_APACHE_MODPHP:
            plugin = 'Apache'
        else:
            plugin = 'Nginx'
        return self.__proxy(plugin, 'isinactif')

    @property
    def statinf(self) -> dict:
        '''Renvoie le dict statinf'''
        return self.__proxy('Nginx', 'statinf')
            
    # getCms() et les autres: ce ne sont pas des property à cause du paramètre topdir
    def getCms(self) -> str:
        ''' Renvoie le nom du CMS, ou NON DETECTE'''
        if 'HomeDir' in self._plugins:
            return self._plugins['HomeDir'].isCms()
        else:
            raise PicFatalErreur('ERREUR - plugin HomeDir non encore defini')
        
    def getSpip(self) -> str:
        ''' Renvoie la version de Spip, ou la chaine nulle'''
        
        if 'HomeDir' in self._plugins:
            return self._plugins['HomeDir'].getSpip()
        else:
            raise PicFatalErreur('ERREUR - plugin HomeDir non encore defini')

    def isSpip(self) -> bool:
        if 'HomeDir' in self._plugins:
            return self._plugins['HomeDir'].isSpip()
        else:
            raise PicFatalErreur('ERREUR - plugin HomeDir non encore defini')
        
    def isGalette(self) -> bool:
        if 'HomeDir' in self._plugins:
            return self._plugins['HomeDir'].isGalette()
        else:
            raise PicFatalErreur('ERREUR - plugin HomeDir non encore defini')

    def isDolibarr(self) -> bool:
        if 'HomeDir' in self._plugins:
            return self._plugins['HomeDir'].isDolibarr()
        else:
            raise PicFatalErreur('ERREUR - plugin HomeDir non encore defini')
        
    def getWordpress(self) -> str:
        ''' Renvoie la version de Wordpress, ou la chaine nulle'''
        
        if 'HomeDir' in self._plugins:
            return self._plugins['HomeDir'].isWordpress()
        else:
            raise PicFatalErreur('ERREUR - plugin HomeDir non encore defini')

    def isWordpress(self) -> bool:
        if 'HomeDir' in self._plugins:
            return self._plugins['HomeDir'].isWordpress()
        else:
            raise PicFatalErreur('ERREUR - plugin HomeDir non encore defini')
 
# TEST
if __name__ == '__main__':
    pg = PicGestion('jungle', 100);
