#
# MODULE ldap = encapsuler la connection ldap dans un singleton
#

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

try:
    import ldap3
    from ldap3.utils.hashed import hashed
    import re
    LDAPOK = True

except ImportError:
    LDAPOK = False

from erreur import *
from picconfig import getConf
import re

# Architectures de conteneurs, utilisées par parseGecos pour gérer les ANCIENS FORMATS de gecos
# c-à-d les formats d'AVANT les conteneurs ngix-fpmxx
# Recopié depuis picgestion, tant pis si c'est moche
ARCHI_APACHE_MODPHP = 1 
ARCHI_NGINX_FPM = 2

class Ldap(object):

    _read_only  = None
    _connection = None
    _server     = None

    def __init__(self, ro=False):
        '''Authentification ldap utilisant ldap3, retourne un objet "connection"
           Si ro vaut True, ouvre la connection en mode anonyme (read only)
           Si ro vaut False (défaut) ouvre la connection en mode readwrite
           Si déjà ouvert, on ferme la connection avant de la réouvrir
           En cas d'erreur leve une exception
           Si ldapuri vaut None on ne peut pas utiliser les fonctions Ldap
           mais le module est quand-même correctement utilisé '''

        # Si pas de module ldap3, on lance une exception
        if not LDAPOK:
            raise PicErreur("ERREUR - Le module ldap3 n'a pas été importé")

        if Ldap._server == None:
            ldapuri = getConf('LDAPURI')
            if ldapuri != None:
                Ldap._server = ldap3.Server(ldapuri)

        if Ldap._server:
            if ro==True and Ldap._read_only != True:
                Ldap._read_only = True
                self.__initConn()
    
            if ro==False and Ldap._read_only != False:
                Ldap._read_only = False
                self.__initConn()
        
    def isLdapOk(self) -> bool:
        '''Renvoie True si le serveur est opérationnel'''
        return Ldap._server != None
                
    def __initConn(self) -> None:
        '''Initialise la connection, appelé par le constructeur, setReadOnly ou setReadWrite'''

        if Ldap._server == None:
            return
            
        if Ldap._read_only:
            pwd = None
            usr = None
        else:
            with open(getConf('LDAPPASSWDFILE'),'r') as fh_pwd:
                pwd    = fh_pwd.readline().partition('\n')[0]
            usr = getConf('LDAPADMIN')

        # On est passé de ro vers rw ou l'inverse
        if Ldap._connection != None:
            conn = Ldap._connection
            conn.unbind()
            if not conn.rebind(user=usr, password=pwd):
                msg = "Serveur ldap = {0} Nouvelle connexion impossible (rebind renvoie False) ".format(getConf('LDAPURI'))
                raise PicErreur(msg)
                
        # Initialisation
        else:
            conn = ldap3.Connection(Ldap._server,usr,pwd,auto_bind=True)
            if conn == False:
                Ldap._server     = None
                Ldap._connection = None
                msg = "Serveur ldap = " + getConf('LDAPURI') + " Connexion impossible"
                raise PicErreur(msg,1)
            else:
                Ldap._connection = conn

    def isReadOnly(self) -> bool:
        '''Sommes-nous en read only ?'''
        return Ldap._read_only

    def getConnection(self):
        '''Renvoie la connection, utile surtout pour les tests
           Les modules utilisent normalement les methodes de plus haut niveau'''
        return Ldap._connection
        
    def addUser(self, attrs) -> None:
        '''Crée et ajoute un user
           input = attrs, les attributs
           Attributs requis = cn, uidNumber, gidNumber, homeDirectory
           loginShell vaut /bin/false (pas de ssh par defaut !)
           retourne True
           '''

        if Ldap._server == None:
            raise PicErreur("ERREUR - Pas de serveur ldap")
            
        if Ldap._read_only:
            raise PicErreur("ERREUR - La connection Ldap est en read only")
            
        if not 'cn' in attrs:
            raise PicErreur ("_ldapAddUser: Pas de cn !",1)
        if not 'uidNumber' in attrs:
            raise PicErreur ("_ldapAddUser: Pas de uidNumber !",1)
        if not 'gidNumber' in attrs:
            raise PicErreur ("_ldapAddUser: Pas de gidNumber !",1)
        if not 'homeDirectory' in attrs:
            raise PicErreur ("_ldapAddUser: Pas de homeDirectory !",1)

        # Valeurs par défaut
        if not 'uid' in attrs:
            attrs['uid'] = attrs['cn']
        if not 'loginShell' in attrs:
            attrs['loginShell'] = '/bin/false'

        attrs['objectClass']   = ['account','posixAccount']

        ldap_conn = Ldap._connection
        ldap_base = getConf('LDAPBASEUSR')

        # Verif des attributs 
        if ldap_conn.search(ldap_base,'(uidNumber='+str(attrs['uidNumber'])+')') == True:
            raise PicErreur("Le user uidNumber=" + str(attrs['uidNumber']) + " existe deja !")
        if ldap_conn.search(ldap_base,'(cn='+attrs['cn']+')') == True:
            raise PicErreur("Le user cn=" + attrs['cn'] + " existe deja !")

        # On crée le container Users s'il n'existe pas 
        if ldap_conn.search(ldap_base,'(ou=Users)') == False:
            ldap_conn.add(ldap_base, 'organizationalUnit')
        
        dn='cn={0},{1}'.format(attrs['cn'], getConf('LDAPBASEUSR'))
        ldap_conn.add(dn, 'posixAccount', attrs)

    def modifyGecos(self, user, new_gecos) -> None:
        self.setGecos(user, new_gecos)
        
    def setGecos(self, user, new_gecos) -> None:
        '''Modifie le gecos du user et renvoie None'''

        if Ldap._server == None:
            raise PicErreur("ERREUR - Pas de serveur ldap")
            
        if Ldap._read_only:
            raise PicErreur("ERREUR - La connection Ldap est en read only")
            
        u_searched = self.searchUser(user, [])
        if len(u_searched) != 1:
            raise PicErreur("Le user " + user + " n'existe pas")
            
        ldap_conn = Ldap._connection
        ldap_base = getConf('LDAPBASEUSR')
        dn = 'cn='+user+','+ldap_base
        ldap_conn.modify(dn,{'gecos': [(ldap3.MODIFY_REPLACE, [new_gecos])]})
        
    def getLoginShell(self, user) -> str:
        '''Retourne le loginShell du user'''
        
        users = self.searchUser(user, ['loginShell'])
        if len(users) != 1:
            raise PicErreur("Le user {0} n'existe pas".format(user))

        loginShell = users[0]['attributes']['loginShell']
        # S'il n'y a rien, loginShell vaut [] !
        if isinstance(loginShell,list):
            return ''
        
        else:
            return loginShell

    def modifyLoginShell(self, user, new_shell) -> None:
        self.setLoginShell(user, new_shell)
        
    def setLoginShell(self, user, new_shell) -> None:
        '''Modifie le login shell du user'''

        if Ldap._server == None:
            raise PicErreur("ERREUR - Pas de serveur ldap")
            
        if Ldap._read_only:
            raise PicErreur("ERREUR - La connection Ldap est en read only")
            
        u_searched = self.searchUser(user, [])
        if len(u_searched) != 1:
            raise PicErreur("Le user " + user + " n'existe pas")
            
        ldap_conn = Ldap._connection
        ldap_base = getConf('LDAPBASEUSR')
        dn = 'cn='+user+','+ldap_base
        ldap_conn.modify(dn,{'loginShell': [(ldap3.MODIFY_REPLACE, [new_shell])]})

    def addGroup(self, attrs) -> None:
        '''Ajoute un groupe, les attributs sont dans attrs
           Attributs requis = cn et gidNumber'''

        if Ldap._server == None:
            raise PicErreur("ERREUR - Pas de serveur ldap")
            
        if Ldap._read_only:
            raise PicErreur("ERREUR - La connection Ldap est en read only")
            
        if not 'cn' in attrs:
            raise PicErreur ("_ldapAddGroup: Pas de cn !",1)
        if not 'gidNumber' in attrs:
            raise PicErreur ("_ldapAddGroup: Pas de gidNumber !",1)

        ldap_conn = Ldap._connection
        ldap_base = getConf('LDAPBASEGRP')

        # Verif des attributs 
        if ldap_conn.search(ldap_base,'(GidNumber='+str(attrs['gidNumber'])+')') == True:
            raise PicErreur("Le groupe GidNumber=" + str(attrs['gidNumber']) + " existe deja !")
        if ldap_conn.search(ldap_base,'(cn='+attrs['cn']+')') == True:
            raise PicErreur("Le groupe group=" + attrs['cn'] + " existe deja !")

        # On crée le container Groups s'il n'existe pas 
        if ldap_conn.search(ldap_base,'(ou=Groups)') == False:
            ldap_conn.add(ldap_base, 'organizationalUnit')
        
        dn='cn='+attrs['cn']+','+ldap_base
        ldap_conn.add(dn, 'posixGroup', attrs)

    def delUser(self,user) -> None:
        '''Supprime le user passé en paramètres'''

        if Ldap._server == None:
            raise PicErreur("ERREUR - Pas de serveur ldap")
            
        if Ldap._read_only:
            raise PicErreur("ERREUR - La connection Ldap est en read only")
            
        ldap_conn = Ldap._connection
        ldap_base = getConf('LDAPBASEUSR')
        dn        = 'cn='+user+','+ldap_base
        if ldap_conn.search(ldap_base,'(cn='+user+')') == False:
            raise PicErreur("Le user cn=" + user + " n'existe pas !")
        
        # Commence par retirer le user des groupes auxquels il appartient !
        all_groups = self.searchGroup(None,['memberUid'])
        for g in all_groups:
            if user in g['attributes']['memberUid']:
                self.delUserFromGroup(self.getGroupNomCourt(g),user)
        
        ldap_conn.delete(dn)

    def delGroup(self,cn) -> None:
        '''Supprime le groupe passé en paramètres'''

        if Ldap._server == None:
            raise PicErreur("ERREUR - Pas de serveur ldap")
            
        if Ldap._read_only:
            raise PicErreur("ERREUR - La connection Ldap est en read only")
            
        ldap_conn = Ldap._connection
        ldap_base = getConf('LDAPBASEGRP')
        dn        = 'cn='+cn+','+ldap_base
        if ldap_conn.search(ldap_base,'(cn='+cn+')') == False:
            raise PicErreur("Le groupe group=" + cn + " n'existe pas !")
        
        ldap_conn.delete(dn)

    def searchGroup(self,cn=None,attrs='*'):
        '''Effectue une recherche sur les groupes. Si cn==None recherche tous les groupes
           Sinon recherche un seul groupe
           Renvoie les attributs demandés: par défaut all, sinon on doit passer un array'''

        if Ldap._server == None:
            raise PicErreur("ERREUR - Pas de serveur ldap")
            
        ldap_conn = Ldap._connection
        ldap_base = getConf('LDAPBASEGRP')
        if cn != None:
            ldap_conn.search(ldap_base,'(cn='+cn+')',attributes=attrs)
        else:
            ldap_conn.search(ldap_base,'(objectclass=posixGroup)',attributes=attrs)
        
        return ldap_conn.response

    def searchUser(self,cn=None,attrs='*') -> list[dict]:
        '''Effectue une recherche sur les users. 
           Si cn==None recherche tous les users
           Sinon recherche un seul user par son cn, c-à-d son nom court
           Renvoie un tableau de dictionnaires:
             [ { dn => cn=user1,dc=...
                 attributes => cn    => user1
                               gecos => ...
               },
               ...
             ]
                                    ...
           par défaut on demande tous les attributs, sinon on doit passer un array
           Si on passe attrs=[], on ne renvoie aucun attribut'''
           
        if Ldap._server == None:
            raise PicErreur("ERREUR - Pas de serveur ldap")
            
        ldap_conn = Ldap._connection
        ldap_base = getConf('LDAPBASEUSR')
        if cn != None:
            ldap_conn.search(ldap_base,'(cn={0})'.format(cn),attributes=attrs)
        else:
            ldap_conn.search(ldap_base,'(objectclass=account)',attributes=attrs)
        
        return ldap_conn.response

    def addUserToGroup(self,cng,user) -> None:
        '''Ajoute un user a un groupe (cng=nom du groupe, user=nom du user)
           Leve une exception si le groupe ou si le user n'existe pas
        '''
        
        if Ldap._server == None:
            raise PicErreur("ERREUR - Pas de serveur ldap")
            
        if Ldap._read_only:
            raise PicErreur("ERREUR - La connection Ldap est en read only")
            
        ldap_conn = Ldap._connection
        ldap_base = getConf('LDAPBASEGRP')
        groups = self.searchGroup(cng,['memberUid'])
        if len(groups)==0:
            raise PicErreur("Le groupe {0} n'existe pas !".format(cng))
        users = self.searchUser(user,[])
        if len(users)==0:
            raise PicErreur("Le user {0} n'existe pas !".format(user))

        if 'memberUid' in groups[0]['attributes']:
            members = groups[0]['attributes']['memberUid']
        else:
            members = []
        members.append(user)
        
        dn = 'cn={0},{1}'.format(cng,ldap_base)
        ldap_conn.modify(dn,{'memberUid': [(ldap3.MODIFY_REPLACE, members)]})

    def delUserFromGroup(self,cng,user) -> None:
        '''Retire un user d'un groupe (cng=nom du groupe, user=nom du user)
           Leve une exception si le groupe n'existe pas
           Ne fait rien si le user n'est pas dans le groupe OU si le groupe est un groupe de login du user
        '''
        
        if Ldap._server == None:
            raise PicErreur("ERREUR - Pas de serveur ldap")
            
        if Ldap._read_only:
            raise PicErreur("ERREUR - La connection Ldap est en read only")
            
        ldap_conn = Ldap._connection
        ldap_base = getConf('LDAPBASEGRP')
        groups = self.searchGroup(cng,['memberUid'])
        if len(groups)==0:
            raise PicErreur("Le groupe cn=" + cng + " n'existe pas !")

        if 'memberUid' in groups[0]['attributes']:
            members = groups[0]['attributes']['memberUid']
        else:
            members = []

        if user in members:
            members.remove(user)
            dn = 'cn='+cng+','+ldap_base
            ldap_conn.modify(dn,{'memberUid': [(ldap3.MODIFY_REPLACE, members)]})

    def getUsersInGroup(self,cn) -> list:
        '''Renvoie dans un tableau la liste des utilisateurs appartenant à ce groupe
           On tient compte des groupes de login ET des groupes additionnels !
           Lève une exception si le groupe n'existe pas
           Renvoie un tableau vide si le groupe n'a pas d'utilisateurs
        '''
        
        if Ldap._server == None:
            raise PicErreur("ERREUR - Pas de serveur ldap")
            
        ldap_conn = Ldap._connection
        ldap_base = getConf('LDAPBASEGRP')
        
        # Recherche des memberUid dans le groupe
        groups = self.searchGroup(cn,['memberUid','gidNumber'])
        if len(groups)==0:
            raise PicErreur("Le groupe cn=" + cn + " n'existe pas !")
        
        if 'memberUid' in groups[0]['attributes']:
            members = groups[0]['attributes']['memberUid']
        else:
            members = []

        # Recherche des gidNumber dans les users
        if 'gidNumber' in groups[0]['attributes']:
            gidNumber = groups[0]['attributes']['gidNumber']
            users = self.searchUser(None,['gidNumber','cn'])
            for u in users:
                gid = u['attributes']['gidNumber']
                cn  = u['attributes']['cn'][0]
                if gid == gidNumber:
                    members.append(cn)

        # On retourne members en supprimant les doublons éventuels
        return list(set(members))
        
    def getGecos(self, user) -> str:
        '''user: Le nom court d'un utilisateur (une string)
           Renvoie le gecos correspondant au user
           Si searchUSer renvoie plusieurs users ou à user, envoie une exception'''

        users = self.searchUser(user,['gecos'])
        if len(users) != 1:
            raise PicErreur("ERREUR - {0} utilisateurs correspondent à {1}".format(len(users),user))
        else:
            gecos = users[0]['attributes']['gecos']
            # S'il n'y a rien, gecos vaut [] !
            if isinstance(gecos,list):
                return ''

            # Et sinon gecos est le scalaire
            else:
                return gecos

    @staticmethod
    def parseGecos(gecos) -> tuple[str, str, str, str, str, str]:
        '''Renvoie un tuple de longueur 6:
            (nom_long,contact_mail,contact_tel,conteneur,image,architecture)'''

        try:
            [nom_long_mail,conteneur,d2,contact_tel] = gecos.split(',')

        except ValueError:
            [nom_long_mail,conteneur,d2,contact_tel] = [gecos,'','','']
        
        # 'association machin (machin@truc.fr)' ==> ['association machin','machin@truc.fr']
        if re.search('\(.*\)', nom_long_mail) == None:
            nom_long = nom_long_mail
            contact_mail = ''
        else:
            [nom_long,contact_mail] = nom_long_mail.split(')')[0].split('(')
            
            nom_long = nom_long.strip()
            contact_mail = contact_mail.strip()

        contact_tel = contact_tel.strip()

        # On peut avoir dans le ldap conteneur#image#archi Dans ce cas:
        #   - on sépare
        #   - on renseigne conteneur, image et architecture
        # 
        # Ou alors seulement conteneur (anciennes valeurs)
        #
        conteneur = conteneur.strip()
        image = ''
        architecture = ''
        tmp = conteneur.split('#')
        if len(tmp) == 1:
            conteneur = tmp[0]
            image = ""
            archi = ARCHI_APACHE_MODPHP
        elif len(tmp) == 2:
            conteneur = tmp[0]
            image = tmp[1]
            archi = ARCHI_NGINX_FPM
        elif len(tmp) == 3:
            conteneur = tmp[0]
            image = tmp[1]
            archi = int(tmp[2])
        else:
            raise PicErreur(f"Conteneur - valeur défectueuse {conteneur}")

        return (nom_long,contact_mail,contact_tel,conteneur,image,archi)

    @staticmethod
    def buildGecos(data: tuple[str, str, str, str, str, str]) -> str:
        '''L'inverse de parseGecos: 
            data: Un tuple de longueur 6
            Construit et renvoie le gecos'''
        
        if len(data) != 6:
            raise PicErreur("ERREUR - Le tuple data devrait avoir 4 éléments")
        
        return f"{data[0]} ({data[1]}),{data[3]}#{data[4]}#{data[5]},,{data[2]}"

    #############################################################################
    # Méthodes de plus haut niveau = récupèrent ou modifient le gecos
    #
    # Ces méthodes supposent que le gecos est formatté de la manière suivante:
    # nom (mail),conteneur,,téléphone
    #
    # getXXX: input = une chaîne de caractères (nomCourt) (sauf getXxxCn)
    #         return= une chaîne de caractères (sauf getXxxCn)
    #
    # setXXX: input = chaîne de caractères (nomCourt)
    #                 chaîne de caractères (valeur)
    #         return= None
    #############################################################################
        
    def getUserCn(self, user) -> list:
        '''Renvoie la valeur de 'cn', c'est-à-dire un tableau
           user est soit une string, soit un dictionnaire retourné par searchUser
           Lève une exception si user['attribute']['cn'] n existe pas'''

        if isinstance(user,str):
            users = self.searchUser(user)
            if len(users) != 1:
                raise PicFatalErreur("ERREUR INTERNE - Pas de user ayant comme cn (=nom court) {0}")
            else:
                real_user = users[0]
            
        elif isinstance(user,dict):
            real_user = user
        
        else:
            raise PicFatalErreur("ERREUR INTERNE - user doit être soit un str soit un dict")

        try:
            cn = real_user['attributes']['cn']
            return cn
            
        except Exception:
            raise PicFatalErreur("ERREUR INTERNE - Problème avec les attributs de {0}".format(real_user))

    def getGroupNomCourt(self, group: str|dict) -> list:
        '''Renvoie la valeur de 'cn', c'est-à-dire un tableau
           group est soit une string, soit un dictionnaire retourné par searchGroup
           Lève une exception si group['attribute']['cn'] n existe pas'''

        if isinstance(group,str):
            groups = self.searchGroup(group)
            if len(groups) != 1:
                raise PicFatalErreur("ERREUR INTERNE - Pas de user ayant comme cn (=nom court) {0}")
            else:
                real_group = groups[0]
            
        elif isinstance(group,dict):
            real_group = group
        
        else:
            raise PicFatalErreur("ERREUR INTERNE - user doit être soit un str soit un dict")

        try:
            dn = real_group['dn']
            cn = dn.split(',')[0]
            cn = cn.split('=')[1]
            return cn
            
        except Exception:
            raise PicFatalErreur("ERREUR INTERNE - Problème avec les attributs de {0}".format(real_group))
        
    def getUserNomCourt(self, user) -> str:
        cn = self.getUserCn(user)
        return cn[0]
        
    def getNomLong(self, user) -> str:
        '''Renvoie le "nom long" associé à un utilisateur'''
        return Ldap.parseGecos(self.getGecos(user))[0]
        
    def getMail(self, user) -> str:
        '''Renvoie le contact_mail associé à un utilisateur'''
        return Ldap.parseGecos(self.getGecos(user))[1]
    
    def getTel(self, user) -> str:
        '''Renvoie le contact_tel associé à un utilisateur'''
        return Ldap.parseGecos(self.getGecos(user))[2]
        
    def getCont(self, user) -> str:
        '''Renvoie le nom de conteneur associé à un utilisateur'''
        return Ldap.parseGecos(self.getGecos(user))[3]
        
    def getImage(self, user) -> str:
        '''Renvoie le nom d'image associé à un utilisateur'''
        return Ldap.parseGecos(self.getGecos(user))[4]

    def getArchitecture(self, user) -> str:
        '''Renvoie l'architecture associée à un utilisateur'''
        return Ldap.parseGecos(self.getGecos(user))[5]
                
    def setNomLong(self, user, val) -> str:
        g    = list(Ldap.parseGecos(self.getGecos(user)))
        g[0] = val
        self.modifyGecos(user,Ldap.buildGecos(g))

    def setMail(self, user, val) -> str:
        g    = list(Ldap.parseGecos(self.getGecos(user)))
        g[1] = val
        self.modifyGecos(user,Ldap.buildGecos(g))

    def setTel(self, user, val) -> None:
        g    = list(Ldap.parseGecos(self.getGecos(user)))
        g[2] = val
        self.modifyGecos(user,Ldap.buildGecos(g))

    def setCont(self, user, val) -> None:
        g    = list(Ldap.parseGecos(self.getGecos(user)))
        g[3] = val
        self.modifyGecos(user,Ldap.buildGecos(g))

    def changeUserPassword(self,user,password) -> None:
        '''Changement de mot de passe suivant https://stackoverflow.com/questions/39325089/changing-userpassword-in-openldap-using-ldap3-library'''

        if Ldap._server == None:
            raise PicErreur("ERREUR - Pas de serveur ldap")
            
        if Ldap._read_only:
            raise PicErreur("ERREUR - La connection Ldap est en read only")
            
        ldap_conn = Ldap._connection
        ldap_base = getConf('LDAPBASEUSR')

        users = self.searchUser(user,[])
        if len(users)==0:
            raise PicErreur("Le user cn="+user+" est inconnu")
        if len(users)>1:
            raise PicFatalErreur("ERREUR INTERNE - " + "len(users) users retournés")
        
        dn = "cn=" + user + "," + ldap_base
        hashed_password = hashed(ldap3.HASHED_SALTED_SHA, password)
        changes = {'userPassword': [(ldap3.MODIFY_REPLACE, [hashed_password])]}
        if ldap_conn.modify(dn, changes=changes) == False:
            raise PicErreur("ERREUR - Pas possible de changer le mot de passe pour " + user)

        
def chercheUtilisateurs(user=None, conteneur=None, codefct=None, extended=True, version=None) -> list[tuple[str, str]]:
    '''Cherche les utilisateurs de la forme totoN qui se trouvent dans le serveur ldap (normalement tous)
    Renvoie un tableau de (nom,version), ou un tableau de (nom,version,uidn,gecos) si extended vaut True
    Si le parametre user est different de None, limite aux utilisateurs dont le nom commence par user
    Si le parametre conteneur est different de None, limite aux utilisateurs associés à ce conteneur
    Si le paramètre codefct est différent de None, limite aux utilisateurs avec ce codefct (2 caractères)'''

    if codefct!= None and not codefct in getConf('CODEFCT'):
        raise PicFatalErreur ("ERREUR INTERNE")
        
    utilisateurs = []
    #      Des chiffres et des lettres ou un - suivis de deux lettres (st,gt etc)
    lmn = getConf('LONG_MAX_NOM')
    modele1 = re.compile('^([a-z0-9-_]{1,' + str(lmn) + '}-[a-z][a-z])([0-9]+)')
    modele2 = re.compile('^([a-z0-9-_]{14})([0-9]+)')
    ldap = Ldap(True)
    u_list = ldap.searchUser(None,attrs=['uid','uidNumber','gecos'])
    first_uidn = int(getConf('FIRSTUID'))
    last_uidn  = int(getConf('LASTUID'))

    # Pour le filtre de conteneur
    if conteneur != None:
        p = re.compile(f'{conteneur}($|#)')
    else:
        p = None

    for l in u_list:
        u    = l['attributes']['uid'][0]
        uidn = l['attributes']['uidNumber']
        gecos= l['attributes']['gecos']
        if uidn>=first_uidn and uidn<last_uidn:
            matched = modele1.match(u)
            if matched == None:
                matched = modele2.match(u)
            if matched != None:
                nom  = matched.group(1)
                vers = int(matched.group(2))

                # Filtre en fonction du nom
                if user != None and not nom.startswith(user):
                    continue
                
                # Filtre en fonction du codefct (st,gt,db,au)
                if codefct != None and user != None and not user.endswith(codefct):
                    continue
                    
                # Filtre en fonction du conteneur
                if conteneur != None:
                    cntr = gecos.split(',')[1]
                    if p.search(cntr) == None:
                        continue
                        
                # Filtre en fonction de version
                if version != None and not int(version) == vers:
                    continue

                if extended:
                    utilisateurs.append((nom,vers,uidn,gecos))
                else:
                    utilisateurs.append((nom,vers))
    
    return utilisateurs
