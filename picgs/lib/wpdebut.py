#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# MODULE wpdebut
#

import re
import time

from picgestion import PicGestion
from plugin import Plugin
from ouinon import ouiNon
from ouinon import entrerFiltrer
from erreur import *
from picconfig import getConf



class Wpdebut(Plugin):

    @property
    def nomplugin(self)->str:
        return 'WpSource'
    
    def __init__(self,action,nom,version,pic_gestion,options=[]):

        # Appel du constructeur du parent
        Plugin.__init__(self,action,nom,version,pic_gestion,options)
        
    def execute(self) -> None:
        '''Crée un dict et le met à la suite dans le tableau adata'''
        PicGestion.adata.append({})
        
class WpdebutListe_core(Wpdebut):
    def __init__(self, action, nom, version, pic_gestion, options=[]):

        # Appel du constructeur du parent
        Plugin.__init__(self,action,nom,version,pic_gestion,options)

