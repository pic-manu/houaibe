#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# MODULE erreur =  Definition de l'objet PicErreur et PicFatalErreur
#

class PicErreur(Exception):
    def __init__(self, msg: int, err=0):
        self.__msg = msg
        self.__err = int(err)
    
    @property    
    def message(self) -> str:
        return self.__msg
    
    @property
    def code(self) -> int:
        return self.__err

class PicFatalErreur(PicErreur):
    def __init__(self, msg: int, err=0):
        self.__msg = msg
        self.__err = int(err)
        
    @property    
    def message(self) -> str:
        return self.__msg
    
    @property
    def code(self) -> int:
        return self.__err
