#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# MODULE nginx =  The Nginx object, used to define nginx + fpm containers
#

version = '0.1'

import os
import re
import subprocess
from plugin import Plugin
from erreur import *
from ouinon import ouiNon
from ouinon import entrerFiltrer
from picconfig import getConf
from affiche import *

class Webserver(Plugin):
    ''' Classe de base pour les classes Apache, Nginx'''

    def _executeProduction(self) -> None:
        ''' Appelé par ApacheProduction.execute et NginxProduction.execute '''
        
        pg = self.picgestion
        prod_urls = self._prod_urls
        user = pg.user
        conteneur = pg.conteneur
        prod_next = pg.prodnext

        # Dans certains cas, il n'y a rien à faire
        if len(prod_urls) == 0 and prod_next == '':
            return
        if prod_next in prod_urls:
            return

        # Mettre à jour self._prod_urls et régénérer le fichier de conf Apache
        if prod_next == "":
            prod_urls.clear()
        else:
            prod_urls.add(prod_next)
        
        # La version appelée n'est pas la même si on est avec apache ou avec nginx !'
        self._urls2conf(prod_urls, conteneur, user)

        # Redemarrage du serveur web
        self._restart(conteneur)
        
        # Fini !!!
        return
