#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# MODULE apache =  Definition de l'objet Apache
#

version = '0.1'

import os
import re
import fileinput
###import subprocess
from plugin import Plugin
from webserver import Webserver
from erreur import *
from ouinon import ouiNon
from ouinon import entrerFiltrer
from picconfig import getConf
from affiche import *

def isApache(func):
    '''Teste si on utilise l'architecture ARCHI_APACHE_MODPHP
       Utilisé comme décorateur pour les fonctions execute et demandeParams'''
       
    def mfunc(self):
        if self.isApache():
            func(self)
            
    return mfunc

class Apache(Webserver):

    @property
    def nomplugin(self)->str:
        return 'Apache'
    
    def __init__(self,action,nom,version,pic_gestion,options={}):
        # Appel du constructeur du parent
        Plugin.__init__(self,action,nom,version,pic_gestion,options)
        self._prod_urls = set()
        self._prod_user = ""
    
    def isApache(self, archi=None) -> bool:
        '''Renvoie True si archi est ARCHI_APACHE_MODPHP'''
        pg = self.picgestion
        if not archi:
            archi = pg.archi
        return archi == pg.ARCHI_APACHE_MODPHP
        
    def initialise(self) -> None:
        self._calcProd()

    def _grp_docker(self,f, conteneur) -> None:
        '''Change le uid/gid du fichier f dans le conteneur pour le mettre au groupe docker'''

        # Détermine le gid du groupe docker. Chez moi c'est 999 et chez vous ?
        cmd = "getent group | grep docker | cut -d ':' -f 3"
        tmp = self._system(cmd)
        if len(tmp)>=1:
            gid = tmp[0]
        else:
            raise PicErreur("ERREUR - Ne trouve pas le groupe docker, docker est-il bien installé ?")
        
        # Le fichier appartient à root.docker et est en rw-r-----
        cmd = f"chown root:{gid} {f} && chmod o= {f}"
        self._system(cmd, conteneur)

        
    def _getFichierConfExiste(self, user, conteneur, host=False) -> str:
            '''renvoie le nom du fichier de configuration, il doit exister ou alors lance une exception
               Si host vaut True, on recherche le fichier sur le host'''
               
            if host:
                conf_avail = f"{getConf('CONTAINERSDIR')}/{conteneur}/sites-available/{user}{getConf('APACHE_CEXT')}"
                if self._dkr_fexists(conf_avail)==False:
                    raise PicErreur(f'Le fichier de conf {conf_avail} n\'existe pas sur le host')
            
            else:
                conf_avail = getConf('APACHE_AVAIL') + user + getConf('APACHE_CEXT')
                if self._dkr_fexists(conf_avail,conteneur) == False:
                    raise PicErreur(f'Le fichier de conf {conf_avail} n\'existe pas dans le conteneur {conteneur}')

            return conf_avail

    def _enable(self,conf_avail,conteneur=None) -> None:
        if conteneur == None:
            conteneur = self.picgestion.conteneur
        '''appelle a2ensite pour rendre enabled ce site'''
        site = os.path.basename(conf_avail)
        cmd  = 'a2ensite ' + site
        # a2ensite essaie d'ecrire dans /var/lib/apache2/site/enabled_by_admin pourquoi ? mystere
        # parfois ca ne marche pas la reponse est not found - pourquoi ? Mystere !
        try:
            self._system(cmd,conteneur)
        except:
            pass

    def _disable(self,conf_avail,conteneur=None) -> None:
        '''appelle a2dissite pour rendre disabled ce site'''
        if conteneur == None:
            conteneur = self.picgestion.conteneur
        site = os.path.basename(conf_avail)
        cmd  = 'a2dissite ' + site
        # cf. le commentaire ci-dessus a propos de a2ensite !
        try:
            self._system(cmd,conteneur)
        except:
            pass

    def _copie_tmpl(self,f_src,d_dst,user,user2,group,addr,host,mail,home,log, conteneur) -> str:
        ''' copie template en changeant certains trucs, fait un chgrp et renvoie conf_avail'''
        host1 = host
        if host1.endswith(getConf('DOMAINNAME')):
            host1 = host1.replace('.' + getConf('DOMAINNAME'),'')

        f_src_conteneur = '/etc/apache2/sites-available/pic/template'
        f_src_host      = '/tmp/tmpl_'+user
        self._dkr_cpfrom(f_src_conteneur,f_src_host,self.picgestion.conteneur)
        fh_src = open(f_src_host,'r')
        
        f_dst_conteneur = d_dst + host1 + getConf('APACHE_CEXT')
        f_dst_host      = '/tmp/' + host1 + getConf('APACHE_CEXT')
        fh_dst = open(f_dst_host,'w')
        while True:
            ligne = fh_src.readline()
            if ligne == '':
                    break
            ligne = ligne.replace('ADDRESS',addr)
            ligne = ligne.replace('HOST',host)
            ligne = ligne.replace('MAIL',mail)
            ligne = ligne.replace('HOME',home)
            ligne = ligne.replace('LOG',log)
            ligne = ligne.replace('USER2',user2)
            ligne = ligne.replace('USER',user)
            ligne = ligne.replace('GROUP',group)
            fh_dst.write(ligne)

        fh_dst.close()
        fh_src.close()

        # Copie le fichier et change le groupe du fichier destination
        # C'est important pour que le conteneur puisse redémarrer sans que l'utilisateur soit root
        self._dkr_cpto(f_dst_host,f_dst_conteneur,conteneur)
        self._grp_docker(f_dst_conteneur,conteneur)
        
        # Un peu de ménage
        os.unlink(f_src_host)
        os.unlink(f_dst_host)
        
        return f_dst_conteneur
        
    def _detecteRedirections(self, prod: list) -> dict:
        '''Fait un wget sur les adresses de production 
           et détecte celles qui sont redirigées
           Renvoie un dict, un élément par adresse de prod'''

        rvl = {}
        if len(prod) == 0:
            return ""

        for url in prod:
            # || true assure in status 0 (donc pas d'exception) quoiqu'il arrive
            cmd = f"wget -q -S -O /dev/null --max-redirect=0 https://{url} || true"
            out = {}
            res = self._system(cmd)
            #print (f"lecture de {url}")
            for r in res:
                r = r.strip()
                #print(r)
                if r.startswith('HTTP'):
                    out['code'] = r.split(' ',2)[1].strip() # HTTP/1.1 200 OK => 200
                    out['codeline'] = r

                if r.startswith('Location'):
                    out['location'] = r.split(' ',2)[1].strip()

            if not 'code' in out:
                out['code'] = '-1'
                out['codeline'] = ''
                
            rvl[url] = out
        return rvl
        
    def _executeAjout(self) -> None:
        '''Appelé par ApacheAjout et ApacheDemenagement'''
        
        pg = self.picgestion
        home = pg.home
        user = pg.user
        conteneur = pg.conteneur
        group= user
        user2= 'w' + user
        host = user + '.' + getConf('DOMAINNAME')
        #addr = pg.getAddr()
        addr = '*'
        mail = pg.mail
        if mail == '':
            mail = getConf('ADMINMAIL')
        log  = self.nomcourt

        # Creation du fichier conf_avail
        print(f"=== Création du fichier de conf apache - conteneur {conteneur}")
        conf_avail = self._copie_tmpl(getConf('APACHE_TEMPL'),getConf('APACHE_AVAIL'),user,user2,group,addr,host,mail,home,log,conteneur)

        # Appel de a2ensite
        self._enable(conf_avail)

    @property
    def prod(self) -> tuple[set,str]:
        '''renvoie le nom des sites de production et l'utilisateur de production'''
        return (self._prod_urls, self._prod_user)

    @property
    def sites(self) -> tuple[set]:
        '''En cas de mutualisation = renvoie le nom des sites de production (un set)
           On utilise self._prod_urls pour cela, c'est un hack'''
        return self._prod_urls
                
    def _calcProd(self) -> None:
        ''' recherche les infos sur le user de production et garde le resultat dans self: 1 user, plusieurs urls
            Exception si plusieurs users ou si le fichier a un format inattendu
            Cette fonction sert aussi dans le cas de mutualisation spip'''
        
        nom_court = self.nomcourt
        
        # Si nom_court pas initialise, on laisse tomber (commande picgs cert par exemple)
        if nom_court==None:
            return  

        # print (f"ICI Apache._calcProd - {nom_court}")               

        # Recherche les fichiers dans lesquels on a une directive ServerAlias active, mais si on a deux fois la meme on ne la compte qu'une seule fois
        # NB Dans le cas ssl cela peut arriver !
        # On fait la recherche dans tous les conteneurs
        cmd       = 'grep -Hi ServerAlias ' + getConf('APACHE_AVAIL') + nom_court + '*' + getConf('APACHE_CEXT') + ' 2>/dev/null |grep -v \'#\' |sed -e \'s/\s\s*/ /g\'|sort -u'
        
        # Un dictionnaire, keys = nom de conteneur, values = résultat de la cmd sur le conteneur c (un array)
        lignes = {}
        for c in getConf('DKRSRVWEB'):
                l = self._system(cmd,c)
                l.remove('')
                if len(l) > 0:
                    lignes[c] = l
                
        if len(lignes)==0:
            return
        
        # pattern sur le résultat du grep
        # Détecte le nom du fichier et le nom de production
        pattern = '([-.\w]+):\s*ServerAlias +(\S+)'
        
        # u = l'ensemble des users ayant une adresse de production, normalement un singleton !
        # p = l'ensemble des urls de production
        # ct = l'ensemble des conteneurs des éléments de u, normalement un singleton !
        u=set()
        p=set()
        ct=set()

        for c in lignes:
            for l in lignes[c]:
                match = re.search(pattern,l,re.I)
                if ( match == None ):
                    raise PicErreur('Erreur interne, '+l+' ne matche pas avec '+pattern)
                u.add(match.group(1).replace(getConf('APACHE_CEXT'),''))
                p.add(match.group(2))
                ct.add(c)
                        
        if len(u)>1:
            msg  = 'ERREUR - Plusieurs versions de production de plusieurs users: ' + "\n" + "\n".join(lignes)
            msg += "\ncorrigez cela a la main et reessayez\n"
            raise PicFatalErreur(msg)

        if len(ct)>1:
            msg = f"ERREUR INTERNE - Plusieurs conteneurs pour 1 user !!! - {ct}"
            raise PicFatalErreur(msg)
        
        if len(u)==0:
            return
            
        # L'utilisateur qui a la version de production
        # Dans le cas de la mutu, l'utilisateur de la mutu      
        self._prod_user = u.pop()

        # Le conteneur correspondant
        # self._cont_u = ct.pop()
                
        # Les sites de production
        # Dans le cas de la mutu, les sites mutualises
        self._prod_urls   = p
        
        return
    
    def _urls2conf(self, prod_urls, conteneur, user) -> None:
        ''' Supprime et recrée les lignes ServerAlias à partir de prod_urls
        '''
        pg = self.picgestion
        conteneur = pg.conteneur
        

        conf_avail = self._getFichierConfExiste(user,conteneur, host=True)
        
        # 1/ Supprimer tout ce qui commence par ServerAlias
        for line in fileinput.FileInput(conf_avail, inplace=True):
            if line.strip().startswith('ServerAlias'):
                continue
            else:
                print (line,end='')

        # 2/ Remettre les ServerAlias
        for line in fileinput.FileInput(conf_avail, inplace=True):
            if line.strip().startswith('#ServerAlias PROD'):
                for u in prod_urls:
                    if u == '':
                        continue
                    line += f"    ServerAlias {u}\n"
            print (line,end='')
        
    @property
    def isinactif(self) -> bool:
        ''' Renvoie True si inactif '''

        pg = self.picgestion
        conteneur = pg.conteneur
        user = pg.user

        path = f"{getConf('APACHE_ENABL')}/{user}{getConf('APACHE_CEXT')}"
        return not self._dkr_fexists(path,conteneur)

class ApacheAjout(Apache):

    @isApache
    def execute(self):
        
        pg = self.picgestion
        conteneur = pg.conteneur        
        self._executeAjout()
        self._restart(conteneur)
        
        return

class ApacheDemenagement(Apache):
    
   # def __demenageProd(self,conteneur,prod):
   #     '''prod est un tableau de li'''

    def execute(self) -> None:
        
        pg = self.picgestion
        user = pg.user

        # Si on atterrit sur un Apache
        if self.isApache():
            conteneur = pg.conteneur
            (prod_urls, prod_user) = pg.prod
            self._executeAjout()

        
            # Si l'utilisateur en déménagement a la version de prod,
            # on la récupère dans le nouveau conteneur
            if prod_user == user:
                print("=== Récupération des sites de production")
                self._urls2conf(prod_urls, conteneur, user)
        
            self._restart(conteneur)

        # Si on part d'un apache
        archi_old = pg.archi_old
        if self.isApache(archi_old):
            try:
                conteneur_old = pg.conteneur_old
                
                print("=== Suppression du fichier de conf apache - conteneur " + conteneur_old)
                conf_avail = getConf('APACHE_AVAIL') + user + getConf('APACHE_CEXT')
                self._disable(conf_avail,conteneur_old)
    
                print("    mv du fichier de conf -> .bkp")
                conf_avail_bkp = conf_avail + '.bkp'
                cmd = 'mv ' + conf_avail + ' ' + conf_avail_bkp
                self._system(cmd,conteneur_old)
            
                # Redemarrage d'Apache sur le conteneur de depart
                self._restart(conteneur_old)

            except:
                pass
        
        return

class ApacheSuppression(Apache):

    @isApache
    def execute(self) -> None:
        pg = self.picgestion
        conteneur = pg.conteneur

        print("=== Suppression du fichier de conf apache")
        user = pg.user
        conf = getConf('APACHE_AVAIL') + user + getConf('APACHE_CEXT')

        try:
            self._disable(conf)

            # Redemarrage d'apache
            self._restart(conteneur)
            print("=== Redémarrage d'Apache ===")

        except:
            pass
        
class ApacheDesactiver(Apache):
    
    @isApache
    def execute(self) -> None:
        pg = self.picgestion
        user = pg.user

        print(f"=== Suppression du fichier de conf apache de {user} ===")
        conf = f"{getConf('APACHE_AVAIL')}{user}{getConf('APACHE_CEXT')}"
        self._disable(conf)
        
        self._restart(pg.conteneur)

class ApacheReactiver(Apache):
    
    @isApache
    def execute(self) -> None:
        pg = self.picgestion
        user = pg.user

        print(f"=== Remise en place du fichier de conf apache de {user} ===")
        conf = f"{getConf('APACHE_AVAIL')}{user}{getConf('APACHE_CEXT')}"
        self._enable(conf)
        self._restart(pg.conteneur)
 
class ApacheInactifs(Apache):
    
    #@isApache
    def execute(self) -> None:
        pg = self.picgestion
        tmp = []
        conteneurs = getConf('DKRSRVWEB')
        
        # Détermination des fichiers de conf dans sites-available
        cmd = f"cd {getConf('APACHE_AVAIL')} && ls *{getConf('APACHE_CEXT')}"
        for c in conteneurs:
            try:
               tmp.append(self._system(cmd, c))
            except PicErreur as e:
                continue
            
        # Aplatissement du tmp et suppression des fichiers non significatifs
        available = set([item for sub_list in tmp for item in sub_list if not item.startswith('000-default') and item != ""])

        # Détermination des fichiers de conf dans sites-enabled
        cmd = f"cd {getConf('APACHE_ENABL')} && ls *{getConf('APACHE_CEXT')}"
        tmp = []
        for c in conteneurs:
            try:
                tmp.append(self._system(cmd, c))
            except PicErreur as e:
                continue
            
        # Aplatissement du tmp et suppression des fichiers non significatifs
        enable = set([item for sub_list in tmp for item in sub_list if not item.startswith('000-default') and item != ""])
        
        inactifs = available.difference(enable)
        print()
        if len(inactifs) > 0:
            print ("Côté apache:")
            suffix = getConf('APACHE_CEXT')
            for s in inactifs:
                print (s.removesuffix(suffix))
            print()
        
        else:
            print ("Aucun site désactivé actuellement dans un conteneur apache")
                
class ApacheMutualisation(Apache):
        
    def initialise(self) -> None:
        Apache.initialise(self)
        
        user = self.picgestion.user
        if user != self._prod_user:
            msg = 'ERREUR - Sites mutualises trouves sur ' + self._prod_user + '.'
            msg += ' Mutualisation = ' + user + '. Faudrait savoir !'
            raise PicErreur(msg)
    

class ApacheMutuajout(ApacheMutualisation):

    @isApache
    def execute(self) -> None:
        # pg est l'objet PicGestion dans lequel nous habitons
        pg = self.picgestion
        conteneur = pg.conteneur
        user = pg.user
        urls = self._prod_urls
        urls.add(pg.site)
        self._urls2conf(urls, conteneur, user)

        # Redemarrage d'apache
        self._restart(pg.conteneur)
        
        return
        
class ApacheMutusuppr(ApacheMutualisation):
    '''Appelle le constructeur d'ApacheMutualisation'''
    pass
            
    @isApache
    def execute(self) -> None:
        pg = self.picgestion
        (prod_urls,prod_user) = pg.prod
        site = pg.site
        user = pg.user
        conteneur = pg.conteneur
        prod_urls.remove(site)
        self._urls2conf(prod_urls, conteneur, user)

        # Redemarrage d'apache
        self._restart(conteneur)
        
        return

class ApacheMutuarchivage(ApacheMutualisation):
    '''Appelle le constructeur d'ApacheMutualisation'''
    pass
                    
class ApacheMuturecharge(ApacheMutualisation):
    '''Appelle le constructeur d'ApacheMutualisation'''
    pass

class ApacheMutuvidelescaches(ApacheMutualisation):
    '''Appelle le constructeur d'ApacheMutualisation'''
    pass

class ApacheMutuimporte(ApacheMutualisation):
    '''Appelle le constructeur d'ApacheMutualisation'''
    pass

class ApacheMutuinfo(ApacheMutualisation):
    '''Appelle le constructeur d'ApacheMutualisation'''
    pass

class ApacheMutumodif(ApacheMutualisation):
    '''Appelle le constructeur d'ApacheMutualisation'''
    pass

class ApacheMutumodif(ApacheMutualisation):
    pass

class ApacheProduction(Apache):
    
    @isApache
    def execute(self):
        self._executeProduction()

class ApacheCollecte(Apache):

    def entetes(self) -> str:
        if '--short' in self.options:
            return ""
        
        return "prod\t"

    @isApache
    def execute(self) -> None:
        if '--short' in self.options:
            return ""
        # pg est l'objet PicGestion dans lequel nous habitons
        pg = self.picgestion
        
        if pg.filtre:
            return
        
        (prod_urls,prod_user) = pg.prod
        user = pg.user
        mutu_user = pg.mutuuser
        
        # On n'imprime pas les sites de la mutu !
        if prod_user == mutu_user:
            print("\t", end=' ')
            return
            
        if len(prod_urls) != 0 and prod_user == pg.user:
            print(','.join(prod_urls) + "\t", end=' ')
        else:
            print("\t", end=' ')

class ApacheMutucollecte(ApacheMutualisation):
    
    def entetes(self) -> str:
        return "Aliase vers\t"

    @isApache
    def execute(self) -> None:
        pg = self.picgestion
        if pg.filtre:
            return

        if pg.sitealias != None:
            print(pg.sitealias + "\t", end=' ')
                    
class ApacheWpclone(Apache):
    '''Important pour que la fonction initialise soit appelée '''
    pass
