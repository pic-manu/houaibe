#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# MODULE cert =  Differents trucs concernant les certificats
#

version = '0.1'

import os
import re
import json
import subprocess

#  pip3 install  python-dateutil
from dateutil.parser import parse
import datetime

from plugin import Plugin
from erreur import *
from ouinon import ouiNon,entrerFiltrer
from picconfig import getConf
from affiche import afficheJaune

class Cert(Plugin):

    @property
    def nomplugin(self) -> str:
        return 'Cert'
    
    def __init__(self,action,nom,version,pic_gestion,options={}):
        # Appel du constructeur du parent
        Plugin.__init__(self,action,nom,version,pic_gestion,options)
        self._uid       = os.getuid()
        
        if self._uid == 0:
            self._conteneur = None
            self._proxycert = getConf('PROXYCERT')
        else:        
            self._conteneur = getConf('DKRMAINT')
            self._proxycert = "/etc/nginx/certs"

    def _cert_2_dircert(self,cert) -> str:
        '''Renvoie le nom du repertoire dans lequel se trouvent les fichiers de certificat, ou '' s'il n'existe pas'''
        
        if cert == '':
            return ''
            
        dir_cert = getConf('CERTDIR') + '/' + cert
        if self._dkr_f_dir(dir_cert,self._conteneur):
            return dir_cert
        else:
            i = 100
            while i>0:
                d = dir_cert + '-' + '{:04d}'.format(i)
                if self._dkr_f_dir(d,self._conteneur):
                    return d
                i -= 1
            return ''
                
    def _cert_2_files(self,cert) -> tuple[str, str, str]:
        '''Renvoie les fichiers (certificat,fullchain,privkey), ou ('','','') si le repertoire n'existe pas'''

        dircert = self._cert_2_dircert(cert)
        if dircert == '' or not self._dkr_f_dir(dircert,self._conteneur):
            print (dircert)
            return ('','','')
        else:
            return (dircert+'/cert.pem',dircert+'/fullchain.pem',dircert+'/privkey.pem')

    def __nettoie_certs(self,certs) -> list:    
        ''' Renvoie la liste apres suppression des fichiers .key et suppression des .crt '''

        cert_dir = getConf('CERTDIR')
        n_certs  = []
        for c in certs:
            p = cert_dir + "/" + c + "/cert.pem"
            if self._dkr_fexists(p,self._conteneur):
                n_certs.append(c)
        return n_certs

    def _readDomain(self) -> list[dict]:
        '''Lit domains.txt et renvoie un tableau de dict'''
        
        # Lecture de domains.txt
        domains = getConf('CERTPROGDIR') + '/domains.txt'
        with open(domains,'r') as fh_dom:
            domains_str = fh_dom.read()
            
        # Dans un tableau de dict indexés par le premier mot (nom du certificat)
        dom_dict = {}
        for l in domains_str.split('\n'):
            if l.strip()=='':
                continue
            noms = [ m for m in l.split(' ') if m!=' ']
            dom_dict[noms[0]] = noms
            
        return dom_dict

    def _writeDomain(self, dom_dict) -> None:
        '''Ecrit dom_dict dans domains.txt'''

        # Sauvegarde du fichier actuel
        domains = getConf('CERTPROGDIR') + '/domains.txt'
        dom_bkp = domains + '.bkp'
        self._dkr_cpto(domains,dom_bkp)

        # On écrit domains.txt
        with open(domains,'w') as fh_dom:
            for i in dom_dict:
                fh_dom.write(' '.join(dom_dict[i])+'\n')

    def _liste_certs(self, cert_dir=None, only_one = False) -> None:
        ''' Affiche la liste de certificats connus et demande de faire un choix. Le resultat est dans self._certs'''

        if cert_dir == None:
            cert_dir  = getConf('CERTDIR')
        
        if not self._dkr_f_dir(cert_dir):
            certs = []
        else:
            cmd = 'ls -1 ' + cert_dir
            certs = self._system(cmd,self._conteneur)
            certs = self.__nettoie_certs(certs)
            print("\n".join(certs))

        self._tous_certs=certs
        
        if not only_one:
            tmp_in = entrerFiltrer('Entrez un nom de certificat (* pour tous, rien pour verifier simplement la coherence)','abcdefghijklmnopqrstuvwxyz0123456789-.*')
        else:
            tmp_in = entrerFiltrer('Entrez un nom de certificat ', 'abcdefghijklmnopqrstuvwxyz0123456789-.*')
            
        source = tmp_in
        if tmp_in=='*':
            self._certs = certs
        elif tmp_in=='':
            self._certs = []
        elif tmp_in in certs:
            self._certs      = [tmp_in]
        else:
            raise PicErreur('ERREUR - ' + source + ' n\'est pas un certificat connu de nos services.')
            
        if only_one:
            if len(self._certs) != 1:
                raise PicErreur('ERREUR - vous devez spécifier un et une seul certificat')

    def _infos_cert(self,cert,cert_file) -> dict:
        '''Renvoie les infos importantes au sujet du certificat: domaines,date limite '''

        # La commande depend du format de fichier, donc du proxy
        cmd  = ' openssl x509 -in ' + cert_file + ' -text -noout|grep -e "DNS:" -e "Not After" -e "Subject: CN"'
        
        out = {}
        cn_dom  = None
        dns_dom = None
        lignes = self._system(cmd, self._conteneur)
        for l in lignes:
            if l != "":
                ls = l.strip()
                # Recherche du CN (nom du certificat, du domaine principal)
                if ls.startswith('Subject: CN'):
                    cn_dom = ls.partition('=')[2].strip()
                    
                # Recherche des autres domaines certifiés
                if ls.startswith('DNS'):
                    domaines = ls.split(',')
                    dom = ""
                    for d in domaines:
                        dom += " " + d.partition(':')[2].strip()
                    dns_dom=dom.strip()
                    
                # Recherche de la date
                if ls.startswith('Not After'):
                    lp = l.partition(':')
                    k = lp[0].strip()
                    v = lp[2].strip()
                    #d = datetime.datetime.strptime(v,'%c').date()
                    d = parse(v).date()
                    out['date'] = d
        
        # Si cn_dom n'existe pas il y a un GROS SOUCI avec le certificat !
        if cn_dom == None:
            raise PicErreur("ERREUR - {0} n'a pas un format x509 normal - Il manque la ligne CN".format(cert_file))
        
        # On s'arrange pour mettre top_dom en tête des noms de domaine
        else:
            if dns_dom == None:
                out['DNS'] = cn_dom
            else:
                dns_dom = dns_dom.split(" ")
                dns_dom.remove(cn_dom)
                dns_dom.insert(0,cn_dom)
                out['DNS'] = " ".join(dns_dom)
        
        return out
        
class CertCert(Cert):

    def demandeParams(self) -> None:
        '''Demande les parametres a l'utilisateur, c-a-d le nom du certificat a verifier'''
        
        print("Quel certificat voulez-vous verifier ?")
        print()
        self._liste_certs()
        
        # self._liste_certs a initialise self._certs
        if len(self._certs) > 0:
            self._cert_complet = ouiNon("Voulez-vous imprimer le certificat complet ? ",False)

    def __sync_certs(self,c,cert_files) -> None:
        '''Assure la synchro des certificats entre répertoire dehydrated et répertoire du proxy
           Testé seulement avec dehydrated'''
        
        #import pprint
        #print (c)
        #pprint.pprint(cert_files)
        
        ng_cert = f"{self._proxycert}/{c}.crt"
        ng_key  = f"{self._proxycert}/{c}.key"
        
        # Copier fullchain et clé, et s'assurer que les permissions sont correctes
        cmd1 = f"cp {cert_files[1]} {ng_cert}"
        cmd2 = f"cp {cert_files[2]} {ng_key}"
        self._system(cmd1, self._conteneur)
        self._system(cmd2, self._conteneur)
        self._system(f'chmod 600 {ng_cert} {ng_key} ', self._conteneur)
        
    def __check_certs(self) -> None:
        '''Vérifie que les certificats utilises par nginx sont bien les certificats geres par dehydrated'''
        
        check = True
        corrige = False
        proxycert = self._proxycert
        
        for c in self._tous_certs:
            cert_files = self._cert_2_files(c)
            le_cert = cert_files[0]
            
            # Pas un certificat (README par exemple)
            if le_cert == '':
                continue
                
            cmd = 'head -c 500 ' + le_cert
            le_char = "\n".join(self._system(cmd, self._conteneur))
            
            c1 = re.split('-\d\d\d\d\Z',c)[0] # toto-0001 -> toto
            #ng_cert = getConf('PROXYCERT') + '/' + c1 + '.crt'
            ng_cert = proxycert + '/' + c1 + '.crt'
            try:
                cmd = 'head -c 500 ' + ng_cert
                ng_char = "\n".join(self._system(cmd, self._conteneur))
                
            except PicErreur:
                print()
                print(("************* ATTENTION => CERTIFICAT " + c))
                print ("                                      Pas connu de nginx !")
                if ouiNon("On corrige ?",False):
                    self.__sync_certs(c,cert_files)
                    corrige = True
                check = False
                continue

            if le_char != ng_char:
                print()
                print(("************* ATTENTION => CERTIFICAT " + c))
                print ("                                      Defaut de synchronisation")
                if ouiNon("On corrige ?",False):
                    self.__sync_certs(c,cert_files)
                    corrige = True
                check = False
                continue
                
        if check:
            print ("Les certificats de la conf nginx sont synchronises avec les certificats letsencrypt")
        else:
            print()
            print ("***** ATTENTON => INCOHERENCE ENTRE LES CERTIFICATS DE letsencrypt ET CEUX DE nginx")
            print(("                  Verifiez les repertoires: " + getConf('PROXYCERT')))
            print(("                                         et " + getConf('CERTDIR')))
            if corrige:
                print("\n")
                print("***** Si vous avez corrigé les incohérences, n'oubliez pas de REDEMARRER LE REVERSE PROXY:")
                print("***** toctoc restart pic-proxy")
            
        
    def execute(self) -> None:
        '''Appelle openssl sur le(s) certificat(s) afin de savoir ce qu'il a dans le ventre'''
        
        sep = ""
        if len(self._certs)>1:
            sep = "======================================================"

        certificats_u = []
        if len(self._certs)>0:
            for cert in self._certs:
                if cert=='':
                    continue

                [cert_file,cert_chain,cert_key] = self._cert_2_files(cert)
                if cert_file == '':
                    continue
    
                infoscert = self._infos_cert(cert,cert_file)

                c = {}
                c['nom']   = cert
                c['date']  = infoscert['date']
                c['dns']   = infoscert['DNS']
                c['file']  = cert_file
                
                certificats_u.append(c)
                
            certificats = sorted(certificats_u, key=lambda c: c['date'])
            
            today = datetime.datetime.today().date()
            for c in certificats:
                print()
                print("INFORMATIONS UTILES SUR LE CERTIFICAT " + c['nom'])
                print("----------------------------------------------")
                print()
                d        = c['date']
                delta    = d - today
                delta_j  = delta.total_seconds() / (24*3600)
                if delta_j <= 30:
                    print ("ATTENTION - CERTIFICAT A RENOUVELER PROCHAINEMENT !")
                    print ("***************************************************")
                    print ()
                print("DOMAINES           : " + c['dns'])
                #date_str = str(d.year) + '-' + str(d.month) + '-' + str(d.day)
                #date_str = d.strftime('%c')
                print("DATE DE PEREMPTION : " + d.strftime('%c'))
                print("-------------------------")
                print()
                if self._cert_complet:
                    print("PLUS D'INFOS AVEC LA COMMANDE:")
                    print("------------------------------")
                    print("openssl x509 -in " + c['file'] + " -noout -text")
                print(sep)
            
        # S'il y a un proxy, vérifie que tous les certificats sont bien en place (s'il n'y a pas de proxy on ne sait pas quoi vérifier !)
        if getConf('PROXYNAME') == None:
            print (afficheJaune("Pas de proxy connu, vérifiez vous-même si les certificats sont bien à leur place !"))
        else:
            self.__check_certs()

class CertNohttps(Cert):

    def demandeParams(self) -> None:

        print("Quel certificat voulez-vous desactiver ?")
        print()
        self._liste_certs(None, True)
        
        certs = self._certs
        if len(certs) != 1:
            print ("Vous ne pouvez desactiver qu'un seul certificat a la fois !")
            raise PicFatalErreur("ANNULATION")
        else:
            self._cert     = self._certs[0]
        
        print(("ATTENTION - Le ou les sites certifies par " + self._cert + " vont REPASSER en http"))
        if not ouiNon("            Etes-vous sur de vouloir faire cela ?", False):
            raise PicFatalErreur("ANNULATION")

    def __buildDomain(self, del_domaines) -> None:
        '''Construit et sauvegarde le fichier domains.txt utilisé par dehydrated
           del_domaines: domaines à retirer
        '''
        
        dom_dict = self._readDomain()
        
        # On retire del_domaines de dom_dict
        k = del_domaines.split(' ')[0]
        try:
            dom_dict.pop(k)
            self._writeDomain(dom_dict)
        except KeyError:
            print (f"ATTENTION - {k} n'est pas dans domains.txt ! Donc je ne le retire pas")
        except e: 
            raise e

    def execute(self) -> None:
        '''Suppression du certificat deploye. Ne fonctionne QUE SI ON EST ROOT !'''
        
        cert = self._cert
        
        # Supression du certificat
        print ("=== Suppression du certificat et de sa cle privee...")
        dst_cert = getConf('PROXYCERT') + '/' + cert + '.crt'
        dst_key  = getConf('PROXYCERT') + '/' + cert + '.key'
        os.unlink(dst_cert)
        os.unlink(dst_key)
        
        # Supression du fichier de conf de pic-proxy
        print ("=== Suppression (avant regeneration) du fichier de configuration du proxy")
        os.unlink(getConf('PROXYCONFFILE'))
                
        # On redemarre le proxy !
        print ("=== Redemarrage du reverse proxy...")
        cmd = f"docker restart {getConf('PROXYNAME')}"
        self._system(cmd)

        # Suppression du certificat dans dehydrated
        self.__buildDomain(cert)
        dircert = self._cert_2_dircert(cert)
        if dircert != '':
            cmd = f"rm -r {dircert}"
            self._system(cmd)
        
        print("C'est fait, n'oubliez pas de tester !")

class CertHttps(Cert):
    
    __ch = None
    
    def __init__(self,action,nom,version,pic_gestion,options={}):
        super().__init__(action,nom,version,pic_gestion,options)

    def demandeParams(self) -> None:
        tmp_in = entrerFiltrer('Entrez le nom du "top domaine" a certifier (exemple.com, PAS www.exemple.com)')
        top_dom= tmp_in
        pictop_dom = getConf('DOMAINNAME')
        if top_dom == pictop_dom:
            print(pictop_dom + " ? NON - N'utilisez pas picgs pour cela")
            print("Mais SI vous vouliez dire asso." + pictop_dom + " c'est possible, entrez asso." + pictop_dom)
            raise PicFatalErreur("ANNULATION")
        self._cert = tmp_in
        [self._cert_file,self._cert_chain,self._cert_key] = self._cert_2_files(self._cert)
        
        domaines = []
        if os.path.exists(self._cert_file):
            dom = self._infos_cert(self._cert,self._cert_file)['DNS']
            domaines = dom.split(" ")
            #print(str(domaines))
            print("Le certificat " + self._cert + " existe deja")
            print()
            print("CETTE COMMANDE NE SERT PAS A RENOUVELER UN CERTIFICAT")
            print()
            print("Par contre si vous souhaitez AJOUTER DES DOMAINES ou DEPLOYER A NOUVEAU les certificats, on peut continuer")
            print("Les domaines suivants sont deja certifies: " + dom)
            if not (ouiNon("On continue ?",False)):
                raise PicFatalErreur("ANNULATION")
            
        # Entrer les domaines a certifier
        # NB - new_domaines est une copie profonde de domaines !
        new_domaines = domaines[:]
        if len(new_domaines) == 0:
            new_domaines.append(top_dom)
            
        while(True):
            tmp_in = entrerFiltrer("Nouveau sous-domaine a certifier (ex: www), Entrer pour sortir: ")
            if tmp_in == '':
                break
            dom = tmp_in + '.' + top_dom
            if dom in domaines:
                print("Non, le domaine " + dom + " est DEJA certifie")
            else:
                new_domaines.append(dom)
            
        # Finalement avons-nous entre un nouveau domaine ?
        if len(domaines)==len(new_domaines):
            print ("Pas de nouveau domaine a certifier")
            self._domaines_a_certifier = False
            #self._deployer = ouiNon ("Voulez-vous deployer a nouveau les certificats ?",False)
        else:
            self._domaines_a_certifier = True
        
        # On recapitule
        print("Les domaines suivants seront donc certifies par ce certificat:")
        for d in new_domaines:
            print("   " + d)
            
        if not ouiNon("On y va ?",True):
            raise PicFatalErreur("ANNULATION")
            
        self._new_domaines = new_domaines
        
    def execute(self) -> None:
        '''Generation et deploiement du certificat'''
        
        new_domaines         = self._new_domaines
        domaines_a_certifier = self._domaines_a_certifier

        if domaines_a_certifier==False:
            print ("Pas de nouveau domaine, pas de certificat a redeployer. Bye.")
            raise PicFatalErreur("ANNULATION")
        
        if domaines_a_certifier:
            # Appel de dehydrated
            print("Appel de dehydrated pour obtenir le nouveau certificat...")
            
            # Sauvegarde du domains.txt actuel car dehydrated peut faire des betises
            domains = getConf('CERTPROGDIR') + '/domains.txt'
            dom_bkp = domains + '.bkp0'
            self._dkr_cpto(domains,dom_bkp)
            
            cmd = 'cd ' + getConf('CERTPROGDIR') + ' && ./dehydrated --force -c --domain "' + ' '.join(new_domaines) + '" --hook ' + getConf('CERTPROGDIR') + '/hooks.bash'
    
            try:
                dehydrated_out = self._system(cmd, self._conteneur)
                print("\n".join(dehydrated_out))
                print()
                print ("Régénération du fichier domains.txt pour le renouvellement")
                self.__buildDomain(new_domaines)
                print("C'est fait, n'oubliez pas de tester !")
            
            except PicErreur as e:
                print (e.message)
    
                
    def __buildDomain(self, new_domaines) -> None:
        '''Construit et sauvegarde le fichier domains.txt utilisé par dehydrated
           NOTE - On écrit directement dans domains.txt et domains.txt.bkp donc ces deux fichiers 
                  doivent appartenir au groupe docker, mode 660
        '''
        
        dom_dict = self._readDomain()
        
        # On entre new_domaines dans dom_dict
        dom_dict[new_domaines[0]] = new_domaines
        
        self._writeDomain(dom_dict)
