#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

import os
import fileinput

from picgestion import PicGestion
from erreur import PicErreur
from ouinon import ouiNon
from picconfig import getConf
from plugin import *
from params import *
from sysutils import SysUtils
from affiche import afficheRouge

class Paheko(Plugin):
    def __init__(self,action, nom, d3, d4, d5):

        # Appel du constructeur du parent
        Plugin.__init__(self, action, nom, d3, d4, d5)

    def _nom2dir(self, nom) -> tuple[str, str]:
        '''Calcule répertoire et url à partir de nom'''

        domaine = getConf('DOMAINNAME')
        datadir = "/src/data/users"
        suffix = f".{domaine}"

        url = self._http_suppr(nom)
        if url.endswith(suffix):
            userdir = f"{datadir}/{url.removesuffix(suffix)}"
        else:
            userdir = f"{datadir}/{url}"
            
        return (userdir, url)
    
class PahekoListe(Paheko):
    
    @property
    def nomplugin(self)->str:
        return 'Paheko'
    
    def __init__(self, d1, d2, d3, d4, d5):
        Paheko.__init__(self, d1, d2, d3, d4, d5)

    def execute(self) -> None:
        conteneur = getConf('PAHEKONAME')
        domaine = getConf('DOMAINNAME')
        datadir = "/src/data/users"
        cmd = f"ls {datadir}"
        instances = self._system(cmd, conteneur)
        print ("LISTE DES INSTANCES PAHEKO")
        print ("==========================")
        for i in instances:
            if i == '':
                continue
            if '.' in i:
                print (f"https://{i}")
            else:
                print (f"https://{i}.{domaine}")

class PahekoMaj(Paheko):
    
    @property
    def nomplugin(self)->str:
        return 'maj'
    
    def __init__(self,d1,d2,d3,d4,d5):
        Paheko.__init__(self, d1, d2, d3, d4, d5)
    
    def execute(self) -> None:
        conteneur = getConf('PAHEKONAME')
        domaine = getConf('DOMAINNAME')
        datadir = "/src/data/users"
        print ("KOUKOU MAJ")

class PahekoAjout(Paheko):

    @property
    def nomplugin(self)->str:
        return 'ajout'
    
    def __init__(self,d1,d2,d3,d4,d5):
        Paheko.__init__(self, d1, d2, d3, d4, d5)

    def execute(self) -> None:
        containersdir = getConf('CONTAINERSDIR')
        conteneur = getConf('PAHEKONAME')

        nom = self.nomcourt
        if not '.' in nom:
            raise PicErreur("Vous devez entrer une url, genre paheko.exemple.fr")

        (userdir, url) = self._nom2dir(nom)
        if self._dkr_f_dir(userdir,conteneur):
            raise PicErreur(f"L'instance {nom} existe déjà !")

        cmd = f"mkdir {userdir} && chown 33:33 {userdir}"
        self._system(cmd, conteneur)

        apache_conf = f"{containersdir}/{conteneur}/sites-available/000-default.conf"
        for line in fileinput.FileInput(apache_conf, inplace=True):
            if line.strip().startswith('#ServerAlias PROD'):
                line += f"    ServerAlias {url}\n"
            print (line,end='')

        # redémarrage du conteneur
        self._restart(conteneur)

class PahekoSuppr(Paheko):

    @property
    def nomplugin(self)->str:
        return 'suppr'
    
    def __init__(self,d1,d2,d3,d4,d5):
        Paheko.__init__(self, d1, d2, d3, d4, d5)

    def demandeParams(self) -> None:
        if not ouiNon(f"Voulez-vous VRAIMENT supprimer l'instance {self.nomcourt} ? ", False):
            raise PicFatalErreur("ANNULATION")

    def execute(self) -> None:
        containersdir = getConf('CONTAINERSDIR')
        conteneur = getConf('PAHEKONAME')

        nom = self.nomcourt
        if not '.' in nom:
            raise PicErreur("Vous devez entrer une url, genre paheko.exemple.fr")

        (userdir, url) = self._nom2dir(nom)
        if not self._dkr_f_dir(userdir,conteneur):
            raise PicErreur(f"L'instance {self._nom} n'existe pas !")

        cmd = f"rm -r {userdir}"
        self._system(cmd, conteneur)

        apache_conf = f"{containersdir}/{conteneur}/sites-available/000-default.conf"
        for line in fileinput.FileInput(apache_conf, inplace=True):
            if line.strip().startswith(f'ServerAlias {url}'):
                continue
            print (line,end='')

        # redémarrage du conteneur
        self._restart(conteneur)

class PahekoVidelecache(Paheko):
    
    @property
    def nomplugin(self)->str:
        return 'videlecaache'
    
    def __init__(self,d1,d2,d3,d4,d5):
        Paheko.__init__(self, d1, d2, d3, d4, d5)

    def execute(self) -> None:
        containersdir = getConf('CONTAINERSDIR')
        conteneur = getConf('PAHEKONAME')
        cachedirs = f"{containersdir}/{conteneur}/paheko-cacheroot/cache/*"
        
        # Vérifie que le conteneur est arrêté
        #cmd = f"docker exec {conteneur} hostname"
        try:
            running = False
            self._system('hostname', conteneur)
            running = True
        except:
            pass
        
        if running:
            raise PicFatalErreur(f"Vous devez arrêter le conteneur AVANT de vider le cache: toctoc stop {conteneur}")
        
        # Le conteneur est à l'arrêt, on peut continuer
        cmd = f"rm -rf {cachedirs}"
        self._system(cmd)
        
        print (afficheVert(f"Le cache de paheko est vide.\nRedémarrez paheko par: toctoc start pic-paheko"))
