#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# MODULE compte =  Definition de l'objet Compte

import copy
import re
import os
import pwd
import getpass
import socket

from erreur import *
from picconfig import getConf
from picgsldap import Ldap
from plugin import Plugin
from ouinon import ouiNon
from ouinon import entrerFiltrer

# CONFIGURATION
# Les uid des assos sont obligatoirement entre FIRSTUID et LASTUID
# Les uid supplementaires (w-) commencent a FIRSTUID2. Par exemple on a 2055, 20055
# SUPPGROUP est important pour proftp

# Les noms courts user doivent se conformer a cette regex
NC_REGEX = '^[a-z0-9\-]{2,}$'

class Compte(Plugin):

    @property
    def nomplugin(self)->str:
        return 'Compte'
    
    def __init__(self, action, nom, version, pic_gestion, options={}):

        # Appel du constructeur du parent
        Plugin.__init__(self, action, nom, version, pic_gestion, options)

        self._ssh = False
        pg = self.picgestion
        
        # Si on tourne dans un conteneur, on part simplement du user courant
        if os.getenv('DOCKER_RUNNING') == '1':
            pwd_info = pwd.getpwuid(os.getuid())
            user = pwd_info.pw_name
            if user != nom + str(version):
                raise PicFatalErreur(f'ERREUR INTERNE - nom={nom} version={version} user={user}')
            
            self._user = user
            (self._nom_long,self._contact_mail,self._contact_tel,self._conteneur) = Ldap.parseGecos(pwd_info.pw_gecos) 
            self._home = pwd_info.pw_dir
            self._uid = os.getuid()
            self._gid = os.getgid()
            
            return
            
        # Ouvrir la connexion au serveur ldap, en readonly par défaut
        ldap = Ldap(True)
        if not ldap.isLdapOk():
            return

        # rechercher le user ainsi que le max_uid compris entre FIRSTUID et LASTUID
        user_trouve = False
        if nom != None:
            user_recherche = nom + str(version)
            max_uid = getConf('FIRSTUID')
            uid = max_uid
            u_list = ldap.searchUser(None, ['uid', 'uidNumber', 'gidNumber', 'gecos', 'homeDirectory', 'loginShell'])
            for cn in u_list:
                u = cn['attributes']
                user = u['uid'][0]
                uid = u['uidNumber']
                gid = u['gidNumber']
                if 'gecos' in u:
                    gecos = u['gecos']
                else:
                    gecos = ''
                home = u['homeDirectory']
                shell = u['loginShell']

                if user == user_recherche:
                    user_trouve = True
                    self._user = user
                    self._home = home
                    self._uid = uid
                    self._gid = gid

                    # Détecte si le user a les accès ssh
                    if user in ldap.getUsersInGroup('SFTPONLY'):
                        self._ssh = False
                    else:
                        self._ssh = True

                    # Le gecos
                    (self._nom_long, self._contact_mail, self._contact_tel, self._conteneur, self._image, self._archi) = Ldap.parseGecos(gecos)

                    self._passwd = ""

                uid = int(uid)
                if uid >= max_uid and uid <= getConf('LASTUID'):
                    max_uid = uid
            
            # garder last_uid, ce sera peut-etre utile plus tard
            self._last_uid = max_uid

        # Si le user n'existe pas:
        if user_trouve == False:
            self._setVersion(version)
            self._nom_long = ""
            self._contact_mail = ""
            self._contact_tel = ""
            self._passwd = ""
            self._conteneur = ""
            self._image = ""
            self._archi = 0

        # demandeParams() met la-dedans la liste des parametres modifies par l'utilisateur
        self._parametres_modifies = []

        # Si le groupe SFTPUSER n'existe pas, on le crée
        sftpgrp = getConf('SFTPONLY')
        if len(ldap.searchGroup(cn=sftpgrp, attrs=[])) == 0:
            sftpgid = getConf('SFTPONLYGID')

            # Connection read-write
            ldap = Ldap(False)
            ldap.addGroup({'cn': sftpgrp, 'gidNumber': sftpgid})

            # Connection read-only
            ldap = Ldap(True)

    def _inputConteneur(self) -> None:
        '''Initialise self._conteneur'''
        
        pg = self.picgestion
        user = pg.user
        options = self.options

        contngx = self.getConteneurFromUser(user)
        conteneurs = getConf('DKRSRVWEB') + [contngx]

        if '--cont' in options:
            tmp_in = options['--cont']
            if tmp_in != '':
                if tmp_in in conteneurs:
                    self._conteneur = tmp_in
                else:
                    raise PicErreur(f"ERREUR - Le conteneur {tmp_in} n'existe pas")
        else:
            while True:
                def_cont = self.conteneur

                # Conteneurs NGINX_FPM = Le com de conteneur change avec le user !
                if self.archi == pg.ARCHI_NGINX_FPM:
                    def_cont = self.getConteneurFromUser(user)
                    
                msg = f'Entrer le conteneur (parmi {conteneurs}) [{def_cont}]'
                tmp_in = input(msg)
                if tmp_in != '':
                    if tmp_in in conteneurs:
                        self._conteneur = tmp_in
                        break
                else:
                    if def_cont in conteneurs:
                        self._conteneur = def_cont
                        break
                        
    def _inputImage(self) -> None:
        '''Initialiser self._image, si nécessaire'''        

        pg = self.picgestion
        user = pg.user
        options = self.options
        contngx = self.getConteneurFromUser(user)
        if self._conteneur == contngx:
            images = getConf('DKRSRVIMG')

            if '--img' in options:
                tmp_in = options['--img']
                if tmp_in != '':
                    if tmp_in in images:
                        self._image = tmp_in
                    else:
                        raise PicErreur(f"ERREUR - L'image {tmp_in} n'existe pas")
            else:
                while True:
                    msg = f"Entrer l'image (parmi {images}) [{self._image}]"
                    tmp_in = input(msg)
                    if tmp_in != '':
                        if tmp_in in images:
                            self._image = tmp_in
                            break
                    else:
                        if self._image in images:
                            break
            self._archi = pg.ARCHI_NGINX_FPM

        else:
            self._image = ''
            self._archi = pg.ARCHI_APACHE_MODPHP
        
    def _setVersion(self, v) -> None:
        '''redefinition de _setVersion, cf. plugin - recalcule user et home'''
        if self.nomcourt != None:
            self._user = self.nomcourt + str(v)
            self._home = getConf('HOMES') + self._user
        else:
            self._user = None
            self._home = None

    @property
    def user(self) -> str:
        return self._user
        
    @property
    def mutuuser(self) -> str:
        return f"{getConf('MUTUNOM')}-st{getConf('MUTUVER')}"
        
    @property
    def uid(self) -> int:
        return int(self._uid)

    @property
    def uid2(self) -> int:
        return int(getConf('FIRSTUID2')) + (int(self._uid) - int(getConf('FIRSTUID')))

    @property
    def gid(self) -> int:
        return int(self._gid)
    
    @property
    def password(self) -> str:
        return self._passwd

    @property
    def home(self) -> str:
        return self._home
    
    @property
    def mail(self) -> str:
        return self._contact_mail

    @property
    def tel(self) -> str:
        return self._contact_tel

    @property
    def nomlong(self) -> str:
        return self._nom_long

    @property
    def gecos(self) -> str:
        return Ldap.buildGecos( (self._nom_long.strip(), self._contact_mail, self._contact_tel, self._conteneur, self._image, self._archi) )

    @property
    def conteneur(self) -> str:
        '''renvoie le nom du conteneur'''
        return self._conteneur

    @property
    def archi(self) -> str:
        '''renvoie l'architecture de conteneurs choisie'''
        return self._archi

    def getConteneurFromUser(self,user) -> str:
        '''Utilisé avec ARCHI_NGINX_FPM'''
        return 'pic-' + user

    @property
    def image(self) -> str:
        return self._image

    @property
    def conteneur_old(self) -> str:
        '''renvoie le nom du conteneur de depart en cas de demenagement'''
        return self._conteneur_old

    @property
    def image_old(self) -> str:
        return self._image_old
    
    @property
    def archi_old(self) -> str:
        return self._archi_old

    def __str__(self):
        rvl = 'nom court: ' + self.nomcourt + '\n'
        rvl += 'version  : ' + str(self.version) + '\n'
        rvl += 'user     : ' + self._user
        return rvl

class CompteAjoutModif(Compte):
    '''Methodes communes a Ajout et Modif'''

    def demandeParams(self) -> None:
        '''demande les parametres a l'utilisateur 
           pour chaque parametre, test s'il a ete modifie et si oui met son nom dans self._parametres_modifies'''
        pg = self.picgestion
        user = pg.user
        
        # Si on doit ajouter ou modifier, entrer les parametres
        options = self.options
        while True:
            tmp_in = ''
            if '--nom' in options:
                tmp_in = options['--nom']
            else:
                tmp_in = entrerFiltrer('Entrer le nom long     [' + self._nom_long + ']')
            if tmp_in != '':
                self._nom_long = tmp_in
                self._parametres_modifies.append('_gecos')

            if '--mail' in options:
                tmp_in = options['--mail']
            else:
                tmp_in = ''
                while tmp_in == '':
                    tmp_in = entrerFiltrer('Entrer le contact mail [' + self._contact_mail + ']')
                    if tmp_in != '':
                        self._contact_mail = tmp_in
                        self._parametres_modifies.append('_gecos')
                    if self._contact_mail != '':
                        break

            if '--tel' in options:
                tmp_in = options['--tel']
            else:
                tmp_in = entrerFiltrer('Entrer le contact tel  [' + self._contact_tel + ']')
                if tmp_in != '':
                    self._contact_tel = tmp_in
                    self._parametres_modifies.append('_gecos')

            # Attention pour modifier le conteneur on doit demenager
            if self.isAjout():
                self._inputConteneur()
                self._inputImage()

            # Pas de mot de passe pour la mutu !
            if not self.isMutu():
                pwd = self._genPassword()
                if not '--autopwd' in options:
                    msg = 'Entrer le mot de passe (' + pwd + ') '
                    if self.isModification():
                        msg += '(entrer \'.\' pour laisser le mot de passe inchange) '
                    tmp_in = getpass.getpass(msg)
                    if self.isModification() and tmp_in != '.':
                        if not ouiNon('ATTENTION - Voulez-vous VRAIMENT changer le mot de passe sftp/ssh ?', False):
                            tmp_in = '.'
                    if tmp_in == '':
                        tmp_in = pwd
                else:
                    tmp_in = pwd

                self._passwd = tmp_in
                self._parametres_modifies.append('_passwd')

            # S'il y a un paramètre passé par options, on ne propose pas de recommencer'
            if options != {}:
                break
            if not ouiNon('Recommencer ?', False):
                break

class CompteAjout(CompteAjoutModif):
    def execute(self) -> None:
        '''Ajout d un utilisateur'''

        user = self.user
        group = user
        user2 = 'w' + user
        home = self.home
        passwd = self._passwd
        gecos = self.gecos

        # AJOUTER DEUX UTILISATEURS: user et user2
        # S'assurer que les caracteres de nom court sont acceptables (minuscules+chiffres+-, c'est tout)
        user_tmpl = re.compile(NC_REGEX)
        if user_tmpl.match(self.nomcourt) == None:
            raise PicErreur(
                'ERREUR - ' + self.nomcourt + ' ne colle pas avec la liste des caracteres admissibles: ' + NC_REGEX)

        self._uid = self._last_uid + 1
        self._gid = self._uid
        uid = self._uid
        gid = self._gid
        uid2 = self.uid2

        if uid > getConf('LASTUID'):
            raise PicErreur("ERREUR - Plus d'uid disponibles - changez la valeur de LASTUID (actuellement " + str(
                getConf('LASTUID')) + ")")

        # Passage de ldap en rw
        ldap = Ldap()

        # On cree le groupe puis les deux users, en ignorant les erreurs (peut-etre qu'ils existent dejà)
        print("=== creation du groupe " + group)
        try:
            ldap.addGroup({'cn': group, 'gidNumber': str(gid)})
        except PicErreur as e:
            pass

        print("=== creation de l'utilisateur " + user)
        try:
            ldap.addUser({'cn': user,
                          'uidNumber': str(uid),
                          'gidNumber': str(gid),
                          'gecos': gecos,
                          'loginShell': '/bin/false',
                          'homeDirectory': home})
        except PicErreur as e:
            print("ATTENTION - " + e.message)

        # Ajout de user au groupe sftponly
        ldap.addUserToGroup(getConf('SFTPONLY'), user)

        print("=== creation de l'utilisateur " + user2)
        try:
            ldap.addUser({'cn': user2,
                          'uidNumber': str(uid2),
                          'gidNumber': str(gid),
                          'gecos': gecos,
                          'loginShell': '/bin/false',
                          'homeDirectory': home})
        except PicErreur as e:
            print("ATTENTION - " + e.message)

        print("=== mise en place du mot de passe")
        try:
            ldap.changeUserPassword(user, passwd)
        except PicErreur as e:
            print("ATTENTION - " + e.message)

        Plugin.message += f"Acces web: adresse      = https://{user}.{getConf('DOMAINNAME')}\n"
        Plugin.message += f"Acces sftp: adresse     = sftp://{getConf('SFTP')}\n"
        Plugin.message += f"                   port = {str(getConf('SFTPPORT'))}\n"
        Plugin.message += f"           utilisateur  = {user}\n"
        Plugin.message += f"           Mot de passe = {passwd}\n"
        Plugin.message += "Plus d'infos sur https://www.le-pic.org/mon-hebergement-au-pic\n"

class CompteMutumodifinfo(CompteAjoutModif):

    def _lireMutuInfo(self, f_info) -> str:
        '''Lire les infos depuis le fichier .info.txt - Ce fichier DOIT exister'''

        cmd = 'cat ' + f_info
        info = self._system(cmd, getConf('DKRMAINT'))
        for ligne in info:
            if ligne.startswith('NOM_LONG'):
                self._nom_long = ligne.partition('=')[2]
            if ligne.startswith('CONTACT_MAIL'):
                self._contact_mail = ligne.partition('=')[2]
            if ligne.startswith('CONTACT_TEL'):
                self._contact_tel = ligne.partition('=')[2]

    def _ecrireMutumodifinfo(self, f_info) -> None:
        '''Ecrire les infos dans le fichier .info.txt - Cree le fichier si necessaire'''

        # edition dans un fichier temporaire
        pg = self.picgestion
        site = pg.site

        f_info_tmp = '/tmp/' + os.path.basename(f_info)
        with open(f_info_tmp, 'w') as fh_info:
            fh_info.write('NOM_COURT=' + self.nomcourt + "\n")
            fh_info.write('SITE=' + site + "\n")
            fh_info.write('NOM_LONG=' + self.nomlong + "\n")
            fh_info.write('CONTACT_MAIL=' + self.mail + "\n")
            fh_info.write('CONTACT_TEL=' + self.tel + "\n")

        # envoi dans le conteneur
        self._dkr_cpto(f_info_tmp, f_info, getConf('DKRMAINT'))

        # menage
        os.unlink(f_info_tmp)

class CompteMutuajout(CompteMutumodifinfo):
    def demandeParams(self) -> None:
        pg = self.picgestion
        if pg.sitealias != None:
            return
        else:
            CompteMutumodifinfo.demandeParams(self)

    def execute(self) -> None:
        pg = self.picgestion
        if pg.sitealias != None:
            return
        else:
            site = pg.site;
            f_info = pg.priv + '/' + site + '.info.txt'
            self._ecrireMutumodifinfo(f_info)


class CompteMutuinfo(CompteMutumodifinfo):
    def demandeParams(self) -> None:
        '''On evite l'appel de CompteAjoutModif.demandeParams !'''
        pass

    def execute(self) -> None:
        pg = self.picgestion
        site = pg.site
        if pg.sitealias != None:
            site_reel = pg.sitealias
        else:
            site_reel = site

        f_info = pg.priv + '/' + site_reel + '.info.txt'
        if self._dkr_fexists(f_info, getConf('DKRMAINT')):
            self._lireMutuInfo(f_info)
            print("Informations pour " + site)
            print("==========================")
            if pg.sitealias != None:
                print("ALIAS VERS " + site_reel)
                print()
            print("NOM LONG     = " + self._nom_long)
            print("CONTACT MAIL = " + self._contact_mail)
            print("CONTACT TEL  = " + self._contact_tel)
        else:
            print("Pas d'informations pour " + site)
            print("Vous pouvez les entrer avec picgs mutumodif")


class CompteMutumodif(CompteMutumodifinfo):
    def demandeParams(self) -> None:
        pg = self.picgestion
        site = pg.site
        f_info = pg.priv + '/' + site + '.info.txt'
        if self._dkr_fexists(f_info, getConf('DKRMAINT')):
            self._lireMutuInfo(f_info)

        CompteMutumodifinfo.demandeParams(self)

    def execute(self) -> None:
        pg = self.picgestion
        site = pg.site
        f_info = pg.priv + '/' + site + '.info.txt'
        self._ecrireMutumodifinfo(f_info)


class CompteMutusuppr(Compte):
    def execute(self) -> None:
        pg = self.picgestion
        # site = pg.prod[2]
        site = pg.site
        siteinfo = f"{pg.home}/priv/{site}.info.txt"

        print("=== Suppression du fichier d'informations de " + site)
        cmd = f"rm -f {siteinfo}"
        self._system(cmd, getConf('DKRMAINT'))

class CompteInfo(Compte):
    def execute(self) -> None:
        pg = self.picgestion
        
        print("Utilisateur : " + self.user)
        print("Nom court   : " + self.nomcourt)
        print("Version     : " + str(self.version))
        print("Nom         : " + self.nomlong)
        print("Contact mail: " + self._contact_mail)
        print("Contact tel : " + self._contact_tel)
        print("Conteneur   : " + self.conteneur)
        print("Image       : " + self.image)
        if self._archi == pg.ARCHI_APACHE_MODPHP:
            srvr = 'Apache'
        elif self._archi == pg.ARCHI_NGINX_FPM:
            srvr = 'nginx'
        else:
            srvr = "INCONNU"
            
        print("Serveur     : " + srvr)
        if self._ssh:
            print("Accès ssh   : Oui")
        else:
            print("Accès ssh   : Non")
        print()


class CompteModifDem(CompteAjoutModif):

    # MODIFIER UN UTILISATEUR
    def execute(self) -> None:

        user = self._user
        group = user
        user2 = 'w' + user
        home = self._home
        passwd = self._passwd

        # Modification du nom long, etc.
        gecos = self.gecos

        # Passage de ldap en rw
        ldap = Ldap()

        if '_gecos' in self._parametres_modifies:
            # Modifier le gecos pour user
            ldap.setGecos(user, gecos)

            # Modifier le gecos pour wuser1, pour la coherence
            ldap.setGecos(user2, gecos)

        # Modification du mot de passe
        if '_passwd' in self._parametres_modifies and passwd != '.':
            ldap.changeUserPassword(user, passwd)

            Plugin.message += "Acces sftp: adresse     = sftp://" + getConf('SFTP') + "\n"
            Plugin.message += "            port        = " + str(getConf('SFTPPORT')) + "\n"
            Plugin.message += "           utilisateur  = " + user + "\n"
            Plugin.message += "           Mot de passe = " + passwd + "\n"
            Plugin.message += "Plus d'infos sur https://www.le-pic.org/mon-hebergement-au-pic\n"


class CompteModification(CompteModifDem):
    pass


class CompteDemenagement(CompteModifDem):
    '''Changement de conteneur'''

    def demandeParams(self) -> None:
        '''demande les parametres a l'utilisateur '''

        pg = self.picgestion
        conteneur = pg.conteneur
        image = pg.image
        archi = pg.archi
        if archi == pg.ARCHI_APACHE_MODPHP:
            srvr = 'apache'
        elif archi == pg.ARCHI_NGINX_FPM:
            srvr = 'nginx'
        else:
            srvr = '?'
        print(f"Le conteneur actuel est   : {self.conteneur}")
        if image != "":
            print(f"L'image actuelle est      : {self.image}")
        print(f"Le serveur web actuel est : {srvr}")
        
        self._inputConteneur()
        self._inputImage()
        
        if conteneur == self.conteneur and image == self.image:
            print("Pas besoin de demenager, vous y etes dejà !")
            raise PicFatalErreur('ANNULATION')
        
        else:
            self._conteneur_old = conteneur
            self._image_old = image
            self._archi_old = archi
            self._parametres_modifies.append('_gecos')

class CompteSuppression(Compte):

    def execute(self) -> None:
        # SUPPRIMER UN UTILISATEUR

        user = self._user
        group = user
        gid = self._gid
        user2 = 'w' + user
        home = self._home
        # gecos    = self._nom_long.strip() + ' (' + self._contact_mail + '),,,' + self._contact_tel

        # Passage de ldap en rw
        ldap = Ldap()

        # Supprimer user et user2           
        print("=== Suppression des utilisateurs principaux");
        try:
            ldap.delUser(user)
        except PicErreur as e:
            pass
        try:
            ldap.delUser(user2)
        except PicErreur as e:
            pass

        try:
            ldap.delGroup(group)
        except PicErreur as e:
            pass

        # Suppression du user du groupe sftponly
        print("=== Retrait de cet utilisateur du groupe sftponly");
        ldap.delUserFromGroup(getConf('SFTPONLY'), user)


class CompteCollecte(Compte):

    def entetes(self) -> str:
        return "Nom court\tVersion\tFonction\tNom long\tContact mail\tContact tel\tConteneur\t"

    def execute(self) -> None:
        # IMPRIMER DES INFOS
        pg = self.picgestion

        if pg.filtre:
            return

        if self._contact_tel == '':
            tel = '            '
        else:
            tel = self._contact_tel
        print(self.nomcourt + "\t" + str(
            self.version) + "\t" + self.fonction + "\t" + self._nom_long + "\t" + self._contact_mail + "\t" + tel + "\t" + self._conteneur + "\t",
              end=' ')


class CompteMutucollecte(CompteMutumodifinfo):

    def demandeParams(self) -> None:
        pass

    def entetes(self) -> str:
        return "Nom long\tContact mail\tContact tel\t"

    def execute(self) -> None:
        pg = self.picgestion
        if pg.filtre:
            return

        site = pg.site
        if '--nom' in self.options:
            if not site.startswith(self.options['--nom']):
                return

        f_info = pg.priv + '/' + site + '.info.txt'
        if self._dkr_fexists(f_info, getConf('DKRMAINT')):
            self._lireMutuInfo(f_info)
            print(self._nom_long + "\t" + self._contact_mail + "\t" + self._contact_tel + "\t", end=' ')
        else:
            print("\t\t\t", end=' ')


class CompteSsh(Compte):
    def execute(self) -> None:
        user = self._user

        # Passage de ldap en rw
        ldap = Ldap()

        # Suppression du groupe sftponly
        ldap.delUserFromGroup(getConf('SFTPONLY'), user)

        # Modif de l'entree de user, login shell
        ldap.modifyLoginShell(user, '/bin/bash')


class CompteNossh(Compte):
    def execute(self) -> None:
        user = self._user

        # Passage de _ldap en rw
        ldap = Ldap()

        # Ajout de user au groupe sftponly
        ldap.addUserToGroup(getConf('SFTPONLY'), user)

        # Modif de l'entree de user, login shell
        ldap.modifyLoginShell(user, '/bin/false')

class CompteWpinstall(Compte):
    
    def demandeParams(self) -> None:
        self._passwd = getpass.getpass('mot de passe sftp                 : ')

class CompteWpclone(Compte):
    
    def demandeParams(self) -> None:
        self._passwd = getpass.getpass('mot de passe sftp                 : ')


# TEST
if __name__ == '__main__':
    import sys

    nom = sys.argv[1]
    v = sys.argv[2]

    c1 = Compte('ajout', nom, int(v) - 1, 0)
    print(c1)

    c1.version = 5
    print(c1)
