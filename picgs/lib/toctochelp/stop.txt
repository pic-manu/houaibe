# 
# Message d'aide pour l'action top
#         Les lignes débutant à la colonne 0 seront écrites en gras
#

toctoc stop conteneur
toctoc stop nginx-fpm82 [--dryrun]
      Arrête le conteneur, ou les conteneurs 
      
      Dans le cas de nginx-fpm82, tous les conteneurs dont l'"image" est marquée nginx-fpm82 sont arrêtés
      --dryrun Rien ne se passe, on affiche juste ce qui devrait se passer
      
