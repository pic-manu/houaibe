# 
# Message d'aide pour l'action entrer
#         Les lignes débutant à la colonne 0 seront écrites en gras
#
toctoc entrer
       Entrer dans le conteneur ssh pour accéder aux fichiers des sites
       On arrive dans le répertoire /home
       CTRL-D pour ressortir
ATTENTION - Dans le conteneur, on est identifié comme root
       
toctoc entrer monasso site 5
toctoc entrer monasso dolibarr 5
toctoc entrer monasso galette 5
toctoc entrer monasso autre 5
toctoc entrer monasso-st5
       Entrer dans le conteneur ssh pour accéder à un seul site
       On arrive dans le répertoire /home/monasso-st5
       CTRL-D pour ressortir
ATTENTION - Dans le conteneur, on est identifié comme monasso-st5

toctoc entrer pic-itk80
       Entrer dans le conteneur pic-itk80
       Le répertoire d'arrivée dépend du conteneur
       CTRL-D pour ressortir
ATTENTION - Dans le conteneur, on est identifié comme root
