# 
# Message d'aide pour l'action restart
#         Les lignes débutant à la colonne 0 seront écrites en gras
#

toctoc restart conteneur
toctoc restart nginx-fpm82 [--dryrun]
      Redémarre ou démarre le conteneur, ou les conteneurs 
      NOTE - start est un alias de restart, donc start et restart font la même chose
      
      Dans le cas de nginx-fpm82, tous les conteneurs dont l'"image" est marquée nginx-fpm82 sont redémarrés
      --dryrun Rien ne se passe, on affiche juste ce qui devrait se passer
      
