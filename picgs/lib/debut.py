#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# MODULE Debut = Le premier plugin appelé
#

version = '0.1'


import re
import time

from plugin import Plugin
from ouinon import ouiNon
from ouinon import entrerFiltrer
from erreur import *
from picconfig import getConf
from urllib.parse import urlparse
from affiche import *

class Debut(Plugin):

    @property
    def nomplugin(self)->str:
        return 'Debut'
    
    def __init__(self,action,nom,version,pic_gestion,options=[]):

        # Appel du constructeur du parent
        Plugin.__init__(self,action,nom,version,pic_gestion,options)

        self._prod_next = ""
        self._site = ""
        self._site_alias = ""
        self._source = ""
        self._source_user = ""
        self._source_version = ""
        
        # Utilisé pour Collection
        self.filtre = False
        
    @property
    def source(self) -> list[str]:
        return [self._source,self._source_user,self._source_version]
        
    @property
    def prodnext(self) -> str:
        return self._prod_next

    @property
    def site(self) -> str:
        '''renvoie le nom du site sur lequel on travaille (dans le cas de la mutu)'''
        return self._site

    @property
    def sitealias(self) -> str:
        '''renvoie le nom du site vers lequel on veut faire un alias (utilise pour la mutu)'''
        return self._site_alias
    
    def setSiteAndAlias(self, site:str) -> None:
        '''Initialise les variables _site et _site_alias.
           PREREQUIS = Le site est DEJA CREE
           Si le répertoire du site est un lien symbolique, on initialise _site_alias
           Sinon on initialise _site_alias à None'''
        
        pg = self.picgestion
        www = pg.www
        sitedir = f"{www}/sites/{site}"
        conteneur = getConf('DKRMAINT')
        
        # Lien symbolique !
        if self._dkr_f_lien(sitedir, conteneur):
            cmd = f"readlink {sitedir}"
            self._site_alias = self._system(cmd, conteneur)[0]
            self._site = site
        
        # Répertoire !
        elif self._dkr_f_dir(sitedir, conteneur):
            self._site_alias = None
            self._site = site
        
        # Nawak
        else:
            raise PicFatalErreur(f"ERREUR - {sitedir} n'est ni un répertoire ni un lien symbolique")

class DebutCanon(Debut):

    def demandeParams(self) -> None:
        pg = self.picgestion
        
        user = pg.user
        (prod_urls, prod_user) = pg.prod

        if len(prod_urls) <= 1:
            raise PicErreur ( "Il y a moins de DEUX adresses de production, il n'y a pas de redirection à poser !")

        print(f"Version de production actuelle  : {prod_user}")
        print(f"Adresses de production actuelles: {str(list(prod_urls))}")
        msg=""
        print()
        if not ouiNon("Voulez-vous supprimer toutes les redirections pour ces adresses de production ?", False):
            
            canon = self._detecteCanonical(user)
            if canon == "":
                print ()
                print ("Pas d'adresse canonique détectée!")
                tmp_in = entrerFiltrer (f"Entrez l'adresse de redirection: ",'abcdefghijklmnopqrstuvwxyz0123456789./-_:')

            else:
                print ()
                print (f"Adresse canonique détectée: {canon}")
                url_psed = urlparse(canon)

                redir = url_psed.netloc
                
                if ouiNon (f"Etes-vous d'accord pour rediriger toutes les adresses de production vers {redir} ?",False):
                    tmp_in = redir
                else:
                    tmp_in = entrerFiltrer (f"Entrez l'adresse de redirection: ",'abcdefghijklmnopqrstuvwxyz0123456789./-_:')
        
        else:
            tmp_in = ""
        
        self._redir = tmp_in

    def execute(self) -> None:
        pg = self.picgestion
        proxy = getConf('PROXYNAME')
        confdir = f"{getConf('CONTAINERSDIR')}/{proxy}/vhost.d"
        redir = self._redir

        user = pg.user
        (prod_urls, prod_user) = pg.prod

        # Déterminer s'il est nécessaire de redémarrer le proxy !
        redem = False
        
        # Suppression des éventuels fichiers de redirection
        for p in list(prod_urls):
            vhconf = f"{confdir}/{p}"
            if self._dkr_fexists(vhconf):
                self._system(f"rm {vhconf}")
                redem = True

        if redir != "":
            # Création de nouvelles redirections
            for p in list(prod_urls):
                # Pas de redirection pour l'url canonique
                if p == redir:
                    continue
                vhconf = f"{confdir}/{p}"
                cmd = f"echo 'return 301 https://{redir}\$request_uri;' >{vhconf}"
                self._system(cmd)
                redem = True 
        
        # Redémarrage du proxy
        if redem:
            self._restart(proxy)

class DebutInactifs(Debut):
    def execute(self) -> None:
        print("SITES INACTIFS:")
        print("===============")
    
class DebutRecharge(Debut):
    def demandeParams(self) -> None:
        '''Demande les parametres a l'utilisateur, c-a-d le nom du fichier source'''
        
        archives  = getConf('ARCHIVES')
        conteneur = getConf('DKRMAINT')
        
        print("Quelle archive voulez-vous recharger ?")
        print()
        cmd = 'ls -l ' + archives
        print("\n".join(self._system(cmd,conteneur)))
        
        #cmd = ['docker', 'exec', 'pic-ssh', 'ls','-l',archive]
        #print subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()[0]
        tmp_in = entrerFiltrer('Entrez un nom de repertoire ','abcdefghijklmnopqrstuvwxyz0123456789-.')
        source = tmp_in
        if self._dkr_fexists(f"{archives}/{source}",conteneur):
            self._source = source
        else:
            raise PicErreur(f"ERREUR - {source} n'existe pas !")
        
        m = re.match('^([a-z0-9\-]{2,})([0-9]+)$',source)
        if m:
            self._source_user    = m.group(1)
            self._source_version = m.group(2)
        else:
            self._source_user = None
            self._source_version = None

class DebutSpipclone(DebutRecharge):
    pass
    
class DebutWpclone(DebutRecharge):
    def demandeParams(self) -> None:
        raise PicErreur("UTILISEZ PLUTOT picwpmigrate !!!!")

class DebutGaletteclone(DebutRecharge):
    pass

class DebutDolibarrclone(DebutRecharge):
    pass

class DebutArchivage(Debut):
    def demandeParams(self) -> None:
        '''Verifie la place restante sur le disque avant archivage'''
        
        pg   = self.picgestion
        user = pg.user
        arch_root = getConf("ARCHIVES")
        arch_path = f"{arch_root}/{user}"
        conteneur = getConf('DKRMAINT')

        print("ESPACE DISQUE RESTANT: ")
        print()
        cmd = f"df -h {arch_root}"
        print("\n".join(self._system(cmd, conteneur)))

        arch_path= getConf('ARCHIVES') + user
        print("Archivage dans " + arch_path)

        if self._dkr_fexists(arch_path,conteneur):
            cmd = f"cd {arch_path} && ls -l ."
            print("\n".join(self._system(cmd, conteneur)))
            print            
            if not ouiNon("Les fichiers d'archivage de ce site seront ECRASES: OK ? ",False):
                raise PicFatalErreur('ANNULATION')
        else:       
            cmd = 'mkdir ' + arch_path
            self._system(cmd, conteneur)

class DebutMutuajout(Debut):
    
    def demandeParams(self) -> None:
        pg = self.picgestion
        sites = pg.sites
        www = pg.www
        conteneur = getConf('DKRMAINT')

        print("SITES MUTUALISES CONNUS ACTUELLEMENT:")
        for s in list(sites):
            print(s)

        tmp_in = entrerFiltrer("Nouvelle adresse de site mutualise: ",'abcdefghijklmnopqrstuvwxyz0123456789./-_:')
        tmp_in = tmp_in.lower()
        site = self._http_suppr(tmp_in)

        if site in sites:
            raise PicErreur(f'ERREUR - Le site {site} est DEJA déclaré')
                        
        if ouiNon("Est-ce un alias vers un site mutualisé existant ?",False):
            tmp_in = entrerFiltrer("Lequel ? ",'abcdefghijklmnopqrstuvwxyz0123456789./-_:')

            site_alias = self._http_suppr(tmp_in)
            sitedir_alias = f"www/sites/{site_alias}"

            if not site_alias in sites:
                msg = f"ERREUR - Le site {site_alias} n'est PAS un site mutualise connu."
                raise PicErreur(msg)
                
            if self._dkr_f_lien(sitedir_alias, conteneur):
                msg = f"ERREUR - {site_alias} est DEJA un lien symbolique !"
                raise PicErreur(msg)

            self._site_alias = site_alias
            self._site = site
                
        else:
            self._site_alias = None
            self._site = site
        
        # Note - On n'utilise pas setSiteAndAlias ici car par définition le site n'exite pas encore

class DebutMutuarchivage(DebutArchivage):
    '''demandeParams pose les memes question pour Mutuarchivage que pour Archivage'''
    
    def execute(self) -> None:
        pg   = self.picgestion
        user = pg.user
        arch_path = getConf('ARCHIVES') + user
        conteneur = getConf('DKRMAINT')
        
        # Si le repertoire n'existe pas, il sera cree
        cmd       = "mkdir -p " + arch_path
        self._system(cmd, conteneur)
        
        # Il appartient a la mutu, important pour pouvoir faire l'archivage sous le user de la mutu
        # (sinon on a des tas d'ennuis avec spip)
        cmd       = "chown -R " + user + ':' + user + " " + arch_path
        self._system(cmd,conteneur)
        
        # archives doit avoir les droits o, pour la meme raison
        cmd       = "chmod o+rx " + getConf('ARCHIVES')
        self._system(cmd,conteneur)


class DebutMutuimporte(Debut):
    def demandeParams(self) -> None:
        '''Demande les parametres a l'utilisateur, c-a-d le nom du fichier Debut et le site de destination'''

        pg = self.picgestion
        sites = pg.sites

        # A partir d'où importer ?
        archives  = getConf('ARCHIVES')
        conteneur = getConf('DKRMAINT')
        print("Quelle archive voulez-vous importer ?")
        print()
        cmd = "cd " + archives + "; find . -type d -ls|tr -s ' ' |cut -d' ' -f 8-"
        print("\n".join(self._system(cmd,conteneur)))
        tmp_in = entrerFiltrer('Entrez un nom de repertoire ','abcdefghijklmnopqrstuvwxyz0123456789-./')
        # Virer le ./ entre parce qu'on a fait un copie-colle !
        if tmp_in.startswith('./'):
            tmp_in = tmp_in.lstrip('./')
        if not self._dkr_fexists(archives + '/' + tmp_in,conteneur):
            raise PicErreur("ERREUR - " + tmp_in + " n'existe pas")
        source = tmp_in
        self._source         = source
        self._source_user    = None
        self._source_version = None

        # Où importe-t-on ?
        print("SITES MUTUALISES CONNUS ACTUELLEMENT:")
        sites = pg.sites
        for s in list(sites):
            print(s)
        tmp_in = entrerFiltrer("Importer dans lequel de ces sites ? ",'abcdefghijklmnopqrstuvwxyz0123456789./-_:')
        if not tmp_in in sites:
            raise PicErreur("ERREUR - Vous devez importer dans un site existant - Peut-etre devriez-vous commencer par pigs mutuajout ?")       
        else:
            self._site = tmp_in

        print()
        print()

class DebutColl(Debut):
    
    def entetes(self) -> str:
        """ Renvoie une ligne avec la date du jour"""
        a_tme = time.localtime()
        year  = a_tme[0]
        mon   = a_tme[1]
        day   = a_tme[2]
        rvl = "date = " + str(day) + '-' + str(mon) + '-' + str(year) + "\n"
        
        #rvl += "Site\t"
        return rvl
                
    def derniere(self) -> str:
        return ""

class DebutCollecte(DebutColl):

    def initialise(self) -> None:
        pg      = self.picgestion
        options = self.options
        if '--conteneur' in options:
            conteneur_cible = options['--conteneur']

            if not conteneur_cible in getConf('DKRSRVWEB'):
                raise PicErreur('ERREUR - Conteneur=' + conteneur_cible + ' Conteneurs possibles = ' + str(getConf('DKRSRVWEB')))

            if conteneur_cible == pg.conteneur:
                self.filtre = False
            else:
                self.filtre = True
        else:
            self.filtre = False

class DebutMutucollecte(DebutColl):

    def execute(self) -> None:
        """Met le filtre sur True/False suivant l'option nom. Ne peut pas ici etre mis dans la fonction Initialise"""
        pg   = self.picgestion
        site = pg.site
        if '--nom' in self.options:
            if not site.startswith(self.options['--nom']):
                # Signale aux autres plugins que celui-ci est filtre
                self.filtre = True
            else:
                # signale aux autres plugins que celui-ci n'est PAS filtre
                self.filtre = False
        else:
            self.filtre = False
            
        if not self.filtre:
            print(site + "\t", end=' ')
    
class DebutMuturecharge(Debut):
    def demandeParams(self) -> None:
        '''Met en garde l'utilisateur'''
        
        print("ATTENTION: ")
        print("    - Si vous souhaitez importer un NOUVEAU site dans la mutu utilisez picgs mutuimporte")
        print("    - Si vous souhaitez recharger un site precedemment archive par picgs mutuarchivage cette commande est pour vous")
        print("    - Si vous souhaitez recharger TOUS les sites de la mutu, cette commande est pour vous egalement")
        print()
        
        if not ouiNon("OK ?",False):
            raise PicFatalErreur("ANNULATION")
            
    def execute(self) -> None:
        pg   = self.picgestion
        user = pg.user
        arch_path = getConf('ARCHIVES') + user
        conteneur = getConf('DKRMAINT')
        
        # Il appartient a la mutu, important pour pouvoir recuperer les plugins sous le user de spip
        # pareil pour tous les repertoires et fichiers   de la hierarchie
        # (sinon on a des tas d'ennuis avec spip)
        cmd = "chown -R " + user + ':' + user + " " + arch_path
        self._system(cmd,conteneur)
        
        # archives doit avoir les droits o, pour la meme raison
        cmd = "chmod o+rx " + getConf('ARCHIVES')
        self._system(cmd,conteneur)

class DebutMutuinfo(Debut):

    def demandeParams(self) -> None:
        pg = self.picgestion
        sites = pg.sites

        print("SITES MUTUALISES:")
        for s in list(sites):
            print(s)

        print()
        tmp_in = entrerFiltrer("Site a afficher: ",'abcdefghijklmnopqrstuvwxyz0123456789./-_:')
        print()
        site = (self._http_suppr(tmp_in))
        
        if not site in sites:
            raise PicErreur(f"ERREUR - {tmp_in} n'est pas connu des sites mutualises")
        
        self.setSiteAndAlias(site)

class DebutMutumodif(Debut):

    def demandeParams(self) -> None:
        pg = self.picgestion
        sites = pg.sites

        print("SITES MUTUALISES:")
        for s in list(sites):
            print(s)

        print()
        tmp_in = entrerFiltrer("Site a modifier: ",'abcdefghijklmnopqrstuvwxyz0123456789./-_:')
        print()
        site = self._http_suppr(tmp_in)

        self.setSiteAndAlias(site)
        site_alias = pg.sitealias

        if site_alias != None:
            print(afficheJaune(f"{site} est un lien, il pointe vers {site_alias} - On modifie donc {site_alias}"))
            site = site_alias
            self.setSiteAndAlias(site)
            site_alias = pg.sitealias
            if site_alias != None:
                raise PicFatalErreur(f"ERREUR INTERNE")
            
        if not site in sites:
            raise PicErreur(f"ERREUR - {site} n'est pas connu des sites mutualises")
            
class DebutMutusuppr(Debut):
    
    def demandeParams(self) -> None:
        pg = self.picgestion
        sites = pg.sites

        print("SITES MUTUALISES CONNUS ACTUELLEMENT:")
        for s in list(sites):
            print(s)

        tmp_in = entrerFiltrer("Site a supprimer: ",'abcdefghijklmnopqrstuvwxyz0123456789./-_:')
        site = self._http_suppr(tmp_in)

        if not site in sites:
            raise PicErreur("ERREUR - " + tmp_in + " n'est pas connu des sites mutualises")
            
        self.setSiteAndAlias(site)

class DebutSuppression(Debut):
    
    def demandeParams(self) -> None:
        '''Demande confirmation car cette commande est super-dangereuse !!!'''
        
        # Si on a mis le switch --oui, pas de confirmation demandée
        options = self.options
        if not '--oui' in options:
            if not ouiNon(f"Voulez-vous VRAIMENT supprimer les fichiers et la B.D. de {self.nomcourt} version {self.version} ?",False):
                raise PicFatalErreur ("ANNULATION")
    
    def execute(self) -> None:
        ''' Arrêt du conteneur seulement dans le cas NGIX_FPM
            NOTE - On ne peut pas le faire dans la calss Nginx car le compte 
                   a déjà disparu lorsque le execute de cette classe est exécuté'''
        pg = self.picgestion
        if pg.archi == pg.ARCHI_NGINX_FPM:
            conteneur = pg.conteneur
            self._stop(conteneur)

class DebutProduction(Debut):
    def demandeParams(self) -> None:
        if self.nomcourt == getConf('MUTUNOM')+'-st':
            msg = 'ERREUR - Sur une mutualisation, utilisez picgs mutuAjout'
            raise PicErreur(msg)
            
        pg = self.picgestion
        user = pg.user
        (prod_urls, prod_user) = pg.prod
        # print (f"koukou {len(prod_urls)} - {prod_user}")
        inf = pg.statinf
        if inf['prod2stat'] == False and inf['prod2dyn'] == False and len(inf['staturl']) == 0 and len(inf['dynurl']) == 0:
            statdyn = False
        else:
            statdyn = True

        if statdyn and inf['prod2stat'] == False and inf['prod2dyn'] == False:
            msg = "ERREUR - La configuration actuelle du site statique empêchera les adresses de production de fonctionner.\n"
            msg+= "         Vous devez commencer par SUPPRIMER la partie statique: picgs nostatique {user} "
            print(afficheRouge(msg))
            raise PicErreur("ANNULATION")

        # Oups - user de production différente de la version actuelle, on arrête
        if prod_user != "" and prod_user != user:
            print (afficheRouge(f"ERREUR - Version de production actuelle = {prod_user}\n         Vous devez d'abord appeler picgs production {prod_user} pour SUPPRIMER cette version de production"))
            raise PicFatalErreur("ANNULATION")

        # On a déjà un user de production            
        if prod_user != "":
            print(f"Adresses de production actuelles: {str(list(prod_urls))}")
            
        # On n'en a pas encore: ce sera le user courant !'
        else:
            prod_user = user
            self._prod_user = user

        # Demander la nouvelle url de production
        msg=""
        tmp_in = entrerFiltrer(f"Nouvelle adresse de production (ENTREE pour tout supprimer): ",'abcdefghijklmnopqrstuvwxyz0123456789./-_:')
        prod_next = self._http_suppr(tmp_in)
        if len(prod_urls) == 0 and prod_next == "":
            raise PicFatalErreur('ANNULATION')

        # On a entré "": Supprimer les anciennes adresses de production
        if len(prod_urls) != 0 and prod_next == "":
            msg = ""
            if statdyn:
                msg = "\n"
                msg += f"ATTENTION - Utilisateur {user} Il existe un site statique adossé au site dynamique.\n"
                msg += "             Si vous supprimez les adresses de production, vous supprimerez AUSSI ce site statique.\n"
                msg += "             Cependant, les fichiers ne SERONT PAS SUPPRIMES\n"
                msg += "             Vous pourrez aisément le recréer avec la commande picgs statique {user} \n\n"
                msg = afficheJaune(msg)
            msg += f"Supprimer {str(list(prod_urls))} -> {prod_user} (O/n) ?"
            
        # On a entré quelque chose: Ajouter une url de production
        if len(prod_urls) != 0 and prod_next != "":
            msg = f"Adresses de prod: {str(list(prod_urls))} -> {prod_user} Ajouter {prod_next} -> {prod_user} (O/n) ?"

        # Pas encore d'adresse de production: Première adresse de production
        if len(prod_urls) == 0 and prod_next != "":
            msg = f"Configurer {prod_next} -> {prod_user} (O/n) ?"
        
        if msg == "":
            raise PicFatalErreur('ERREUR INTERNE')
            
        # Dernière chance de laisser tomber
        if not ouiNon(msg,True):
            raise PicFatalErreur('ANNULATION')
        
        # Sauvegarder prod_next
        self._prod_next = prod_next        
        
        return

class DebutStatique(Debut):
    def demandeParams(self) -> None:
        pg = self.picgestion
        if pg.archi != pg.ARCHI_NGINX_FPM:
            raise PicErreur("ERREUR - picgs statique doit être utilisé avec un hébergement nginx-fpm")
        
class DebutNostatique(Debut):
    def demandeParams(self) -> None:
        pg = self.picgestion
        if pg.archi != pg.ARCHI_NGINX_FPM:
            raise PicErreur("ERREUR - picgs nostatique doit être utilisé avec un hébergement nginx-fpm")
