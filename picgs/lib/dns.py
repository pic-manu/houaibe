#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# MODULE dns = Definition de l'objet Dns
#

version = '0.1'

# CONSTANTES
ADDR    = 'A FAIRE SI NECESSAIRE'

import os
import re
import time
import socket
from plugin import Plugin
from ouinon import ouiNon
from erreur import *
from picconfig import getConf

class Dns(Plugin):

    @property
    def nomplugin(self)->str:
        return 'Dns'
    
    def __init__(self,action,nom,version,pic_gestion,options={}):

        # Appel du constructeur du parent
        Plugin.__init__(self,action,nom,version,pic_gestion)
        
    # renvoie l'adresse IP du serveur
    def getAddr(self):
        return ADDR
