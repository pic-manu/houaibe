#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# MODULE plugin =  Definition de l'objet Plugin (classe abstraite)
#

import os
import tempfile
import re
import inspect

from sysutils import *
from erreur import *
from picconfig import getConf

version = '0.1'
            
class Plugin(SysUtils):
    
    # Chaque plugin pourra contribuer à ce message
    # class variable
    message = ""

    @property
    def nomplugin(self)->str:
        '''Renvoie le nom de la classe qui dérive EN PREMIER de Plugin
           Ex. classe HomeDirAjout  --> renverra HomeDir
        '''
        raise PicFatalErreur('ERREUR INTERNE - Fonction abstraite')
        
    def __init__(self,action,nom_court,version,pic_gestion,options={}):
        '''teste la validite des parametres du constructeur et initialise les champs prives'''
        
        if nom_court != None and not isinstance(nom_court,str):
            raise PicFatalErreur('ERREUR INTERNE - type(nom_court) = ' + str(type(nom_court)))
        
        if version != None and not isinstance(version,int):
            raise PicFatalErreur('ERREUR INTERNE - version = ' + str(type(version)) + ' au lieu de int')

        self.__pic_gestion = pic_gestion
        self.__nom_court   = nom_court
        self.__version     = version
        self.__action      = action
        self.__options     = options

        return self

    def _genPassword(self) -> str:
        cmd = 'openssl rand -base64 30'
        return self._system(cmd)[0]
    
    @property
    def picgestion(self):
        return self.__pic_gestion
    
    @property
    def nomcourt(self) -> str:
        return self.__nom_court
        
    @property
    def fonction(self) -> str:
        modele  = re.compile('([a-z][a-z])$')
        matched = modele.search(self.nomcourt)
        if matched != None:
            fct =  str(matched.group(1))
        else:
            fct = ''
        if fct != '':
            codefct   = getConf('CODEFCT')
            fonctions = getConf('FONCTIONS')
            if fct in codefct:
                fctind = codefct.index(fct)
                return fonctions[fctind]
        return ''

    @property
    def version(self):
        return self.__version
    
    @property
    def options(self) -> dict:
        return self.__options

    def isSuppression(self) -> bool:
        return self.__action == 'suppression'
    def isAjout(self) -> bool:
        return self.__action == 'ajout'
    def isCollecte(self) -> bool:
        return self.__action == 'collecte'
    def isModification(self) -> bool:
        return self.__action == 'modification'
    def isProduction(self) -> bool:
        return self.__action == 'production'
    def isArchivage(self) -> bool:
        return self.__action == 'archivage'
    def isSpipmaj(self) -> bool:
        return self.__action == 'spipmaj'
    def isSpipinstall(self) -> bool:
        return self.__action == 'spipinstall'
    def isRecharge(self) -> bool:
        return self.__action == 'recharge'
    def isMutu(self) -> bool:
        return self.__action.startswith('mutu')
    
    def entetes(self) -> str:
        return ''
    def derniere(self) -> str:
        return self.entetes()    
    #def collecte(self) -> str:
    #    return ''

    @version.setter
    def version(self,v) -> None:
        '''Change la version dans plugin, appelle _setVersion pour synchroniser dans la classe derivee'''
        self.__version = v
        self._setVersion(v)

    def _setVersion(self,v) -> None:
        '''Cette fonction peut etre redefinie dans une classe derivee'''
        pass

    def initialise(self) -> None:
        '''Initialise certaines proprietes qui n'ont pas pu etre initialisees par le constructeur'''
        return
        
    def demandeParams(self) -> None:
        '''Demande des parametres a l'utilisateur, en mode conversationnel'''
        return

    def execute(self) -> None:
        '''Fait l'action'''
        return

class Factory:
    '''L\'usine a plugins, on cree ici les plugins qui seront utilises'''

    @staticmethod
    def isAut(mot) -> None:
        '''Lève une erreur si un caractère interdit est employé dans le mot'''
        
        car_autorises = 'abcdefghijklmnopqrstuvwxyz0123456789_'
        for c in list(mot):
            if not c.lower() in car_autorises:
                raise PicFatalErreur(f"ERREUR INTERNE - {c} n'est pas un caractère autorisé dans un nom de plugin")
                
    @staticmethod       
    def createPlugin(nom_plugin,action,nom,version,options,pic_gestion):
        '''1/ Si nom_plugin est une str, creation d un objet en fonction des parametres nom_plugin et action
           2/ Si nom_plugin est une classe qui dérive de Plugin, creation d'un objet de cette classe 
           3/ Sinon, erreur'''

        Factory:isAut(action)

        # Cas "normal": picgs -> On passe par des eval pour importer la classe et construire le plugin
        if isinstance(nom_plugin,str):
            Factory:isAut(nom_plugin)
            # Importation du module     
            mod = nom_plugin.lower()
            mod_cmd = 'import ' + mod
            exec(mod_cmd)

            # Creer le plugin et le renvoyer
            obj_cmd_base = mod + '.' + nom_plugin + '(action,nom,version,pic_gestion,options)'
            obj_cmd      = mod + '.' + nom_plugin + action.capitalize() + '(action,nom,version,pic_gestion,options)'
    
            # Exemple avec plugin Apache, action Production:
            # On essaie de creer ApacheProduction, qui derive d'Apache
            try:
                #print (obj_cmd)
                obj = eval(obj_cmd)
                
            # Oups, envoi d'une PicErreur: on la propage !
            except PicErreur as x:
                raise x
                
            # Envoi d'une autre erreur (sans doute classe n'existe pas): on cree la classe de base
            except Exception as x:
                #print (x)
                obj = eval(obj_cmd_base)
                #print obj_cmd_base
        
        # Cas "atypique": On passe une classe qui dérive de Plugin
        elif inspect.isclass(nom_plugin) and issubclass(nom_plugin,Plugin):
            obj = nom_plugin(action,nom,version,pic_gestion,options)
            
        # Cas nawak
        else:
            raise PicFatalErreur(f"ERREUR INTERNE - Cette classe de dérive pas de Plugin")

        return obj
