#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# MODULE mysql =  Definition de l'objet Mysql 
#

version = '0.1'

import os
import os.path
import socket
import tempfile
import re
import hashlib
import getpass
import json

from plugin import Plugin
from ouinon import ouiNon, entrerFiltrer
from erreur import *
from picconfig import getConf
from affiche import *
import subprocess

class Mysql(Plugin):

    @property
    def nomplugin(self)->str:
        return 'Mysql'
    
    def __init__(self,action,nom,version,pic_gestion,options={}):

        # Appel du constructeur du parent
        Plugin.__init__(self,action,nom,version,pic_gestion,options)

        self._a_parametres_modifies = []
        
    def _calc_nom_archive(self, user, arch_path = None, verif = False) -> None:
        '''Retourne le nom du fichier de bases de donnée archivé pour user
           arch_path est le nom du répertoire d'archive, si False il est calculé à partir de user
           Si verif est True, vérifie l'existence du fichier (plusieurs possibilités de nom)
           et si pas trouvé, lance une exception (utile en lecture)
           Si verif est False, ne vérifie pas (utile en écriture)'''

        conteneur = getConf('DKRMAINT')
        if arch_path == None:
            archives = getConf('ARCHIVES')
            arch_path = f"{archives}/{user}"

        s_dump_path0 = f"{arch_path}/{user}.sql.gz"
        
        # En écriture
        if not verif:
            return s_dump_path0

        # En lecture
        else:
            if self._dkr_fexists(s_dump_path0, conteneur):
                return s_dump_path0

            # Ancien nommage, pour la compatibilité
            else:
                s_dump_path1 = f"{arch_path}/{user}.mysqldump.gz"
                if self._dkr_fexists(s_dump_path1, conteneur):
                    return s_dump_path1
                    
                # Nommage libre
                else:
                    cmd = f"cd {arch_path} && ls -1 *sql.gz *sql 2> /dev/null || true"
                    out = self._system(cmd, conteneur)
                    if len(out) == 0:
                        raise PicErreur(f"ERREUR - Pas de fichier .sql dans le répertoire {os.path.dirname(arch_path)}")
                    else:
                        s_dump_path2 = out[0]
                        return s_dump_path2

    def _siteUrlPourWp(self, siteurl) -> None:
        '''Modifie le siteUrl dans le cas de wordpress '''

        pg = self.picgestion
        www = pg.www
        user = pg.user
        priv = pg.priv
        conteneur = getConf('DKRMAINT')
        
        # Sauvegarder la base de données !
        fsql = f"{priv}/database.sql"
        print (f"=== Sauvegarder la base de données sur {fsql}")
        cmd = f"cd {www} && wp db export {fsql}"
        self._system(cmd, conteneur, False, user)

        # Récupérer le siteurl actuel
        cmd = f"cd {www} && wp option get siteurl 2>/dev/null"
        old_siteurl = self._system(cmd, conteneur, False, user)[0]

        # changement de siteurl et home
        siteurl = self._https_add(siteurl)
        print (f"=== Positionner siteurl sur {siteurl}")
        cmd = f"cd {www} && wp option update siteurl {siteurl}"
        self._system(cmd, conteneur, False, user)

        # Mise à jour des permalinks
        print (f"=== Mise à jour des permalinks: {old_siteurl} => {siteurl}")
        cmd = f"cd {www} && wp search-replace {old_siteurl} {siteurl} 2>/dev/null"
        self._system(cmd, conteneur, False, user)
        
        # Interversion http <=> https
        if old_siteurl.startswith("http:"):
            old_siteurl = "https:" + old_siteurl.removeprefix("http:")
        elif old_siteurl.startswith("https:"):
            old_siteurl = "http:" + old_siteurl.removeprefix("https:")
        print (f"=== Mise à jour des permalinks: {old_siteurl} => {siteurl}")
        cmd = f"cd {www} && wp search-replace {old_siteurl} {siteurl} 2>/dev/null"
        self._system(cmd, conteneur, False, user)

        # Sans le https://
        old_siteurl = self._http_suppr(old_siteurl)
        siteurl = self._http_suppr(siteurl)
        print (f"=== Mise à jour des permalinks: {old_siteurl} => {siteurl}")
        cmd = f"cd {www} && wp search-replace {old_siteurl} {siteurl} 2>/dev/null"
        self._system(cmd, conteneur, False, user)

    def _siteUrlPourSpip(self,siteurl) -> None:
        '''Modifie le siteUrl dans le cas de spip '''

        pg = self.picgestion
        www = pg.www
        user = pg.user
        conteneur = getConf('DKRMAINT')

        cfg = {}
        cfg['adresse_site'] = siteurl
        cmd = f"cd {www} && cat > tmp/config.json && spip config:ecrire -f tmp/config.json --json"
        self._system(cmd, conteneur, False, user, [], json.dumps(cfg))

    @property
    def database(self) -> str:
        db = self.picgestion.user
        db = db.replace('-','_')
        return db

    @property
    def dbpassword(self) -> str:
        if hasattr(self,'_pwd'):
            return self._pwd;
        else:
            raise ("ERREUR - Le mot de passe de la BD n'est pas connu actuellement")
        
    def getMysqlRootPwd (self) -> str:
        '''Envoie un mysqladmin ping avec authentification root pour savoir si le mot de passe est correct et si le serveur est vivant'''
        host=" --host="+getConf('HSTMYSQL')+' '
        ok = False
        while ok==False:
            mysql_root_pwd = getpass.getpass('mot de passe root mysql')
            cmd = 'mysqladmin' + host + '--password=\'' + mysql_root_pwd + '\' ping '
            res = self._system(cmd,getConf('DKRMYSQL'))[0]
            if res == 'mysqld is alive':
                ok = True
            else:
                print("ERREUR D'IDENTIFICATION MYSQL - Essayez encore un coup !")
        
        return mysql_root_pwd

    def getUserDb(self) -> str:
        '''Renvoie le user attendu par mariadb: 'toto-st1'@'localhost' '''
        
        user = self.picgestion.user
        cli  = getConf('CLIMYSQL')
        return f"'{user}'@'{cli}'"
        
class MysqlAjout(Mysql):
    
    def demandeParams(self) -> None:
        options = self.options
        pwd = self._genPassword()
        if '--autopwd' in options:
            self._pwd = pwd
        else:
            self._pwd = getpass.getpass(f'mot de passe mysql pour ce nouvel utilisateur ({pwd}) ')
            if self._pwd == '':
                self._pwd = pwd
        self._a_parametres_modifies.append('__pwd')

    def execute(self) -> None:
        '''Ajout dun utilisateur'''

        # pg est l'objet PicGestion dans lequel nous habitons
        pg = self.picgestion
        
        # Noms de bases etc
        user      = pg.user
        userdb    = self.getUserDb()
        db        = self.database
        pwd       = self.dbpassword
        conteneur = getConf('DKRMAINT')
        
        privileges  = 'ALTER,CREATE,DELETE,DROP,INDEX,INSERT,SELECT,UPDATE,CREATE TEMPORARY TABLES, SHOW VIEW, CREATE ROUTINE, ALTER ROUTINE, EXECUTE, CREATE VIEW,EVENT, TRIGGER, LOCK TABLES, REFERENCES'
        
        print("=== creation de la base de donnees " + db)
        cmd = 'mysqladmin create ' + db
        self._system(cmd,conteneur)

        if self.fonction == "dolibarr" and pg.cmsinstall:
            print ("=== interclassement utf8_unicode_ci pour Dolibarr")
            req = "ALTER DATABASE " + db + " CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci"
            self._sql(req,"",conteneur)
        
        print ("=== creation de l'utilisateur " + userdb)

        req = "CREATE OR REPLACE USER {};".format(userdb)
        self._sql(req,db,conteneur)

        print("=== Definition des droits sur " + db)
        req = "GRANT {privileges} ON {db}.* TO {userdb};".format(privileges=privileges, db=db, userdb=userdb)
        self._sql(req,db,conteneur)
        
        print("=== Definition du mot de passe pour " + user)
        req = "SET PASSWORD FOR '" + user + "'= PASSWORD('"+ self._pwd +"')";
        req = "SET PASSWORD FOR {userdb}=PASSWORD('{password}');".format(userdb=userdb, password=self._pwd)

        self._sql(req,db,conteneur)

        Plugin.message += "SERVEUR DE BD   = " + getConf('HSTMYSQL') + "\n"
        Plugin.message += "BASE DE DONNEES = " + db + "\n"
        Plugin.message += "utilisateur     = " + user + "\n"
        Plugin.message += "mot de passe    = " + self._pwd + "\n"
        Plugin.message += "Administration  = " + getConf('PHPMYADMIN') + "\n"

class MysqlModification(Mysql):

    def demandeParams(self) -> None:
        pwd = self._genPassword()
        tmp_in =  getpass.getpass('mot de passe mysql ('+pwd+' entrer \'.\' pour laisser le mot de passe inchange)')
        if tmp_in == '':
            tmp_in = pwd
        if tmp_in != '.':
            if ouiNon('ATTENTION - Voulez-vous VRAIMENT changer le mot de passe mysql ?',False):
                self._pwd = tmp_in
                self._a_parametres_modifies.append('__pwd')

    def execute(self) -> None:
        if  '__pwd' in self._a_parametres_modifies and self._pwd != '.':

            # pg est l'objet PicGestion dans lequel nous habitons
            pg = self.picgestion
            
            # Noms de bases etc
            user   = pg.user
            userdb = self.getUserDb()

            db = self.database
            conteneur = getConf('DKRMAINT')
            
            # MODIFICATION: Mot de passe mysql, on ne touche pas aux privileges qui peuvent avoir ete modifies par ailleurs
            print("=== Mot de passe sur " + db)
            
            #req = "SET PASSWORD FOR '" + user + "'= PASSWORD('"+ self._pwd +"')";
            req = "SET PASSWORD FOR {userdb}=PASSWORD('{password}');".format(userdb=userdb,password=self._pwd)
            self._sql(req,db,conteneur) 
            
            Plugin.message += "SERVEUR DE BD   = " + getConf('HSTMYSQL') + "\n"
            Plugin.message += "BASE DE DONNEES = " + db + "\n"
            Plugin.message += "utilisateur     = " + user + "\n"
            Plugin.message += "mot de passe    = " + self._pwd + "\n"
            Plugin.message += "Administration  = " + getConf('PHPMYADMIN') + "\n"

def substr(string, start, length = None) -> str:
    '''Remplace subsr de php - cf http://www.php2python.com'''

    if start < 0:
        start = start + len(string)
    if not length:
        return string[start:]
    elif length > 0:
        return string[start:start + length]
    else:
        return string[start:length]

class MysqlMutualisation(Mysql):
    '''Toutes les classes Mutuxxx derivent de cette classe.'''

    def _compDatabase(self,site) -> str:
        '''Calcule et renvoie le nom de la base de données à partir du nom du site, en reprenant exactement le même
        algorithme que la fonction php mutualiser.php: prefixe_mutualisation'''
        
        max = 15
        install_prefix_db = getConf('MUTUINSTPRFX')
        if install_prefix_db != None and len(install_prefix_db) != 0:
            max = 15 - len(install_prefix_db)
        
        p = re.sub('^www\.|[^a-z0-9]','',site.lower())
        if len(p) > max and p != site:
            p = substr(p, 0, (max-4)) + substr(hashlib.md5(site.encode()).hexdigest(), -4);
    
        if ord(p[0]) < 58:
            p = 'a' + p
            
        return str(install_prefix_db) + p

    def _getDatabase(self,site) -> str|None:
        '''Puisqu il s agit d un spip mutualise, on peut lire connect.php
        pour determiner le nom de la base de donnees - retourne None si pas de bd, ou le nom de la bd'''

        pg = self.picgestion

        # Chemin vers connect.php 
        sitedir = pg.www + '/sites/' + site
        if self._dkr_f_lien(sitedir,getConf('DKRMAINT')):
            return None

        connect = sitedir + '/config/connect.php'
        
        # On utilise cat pour lire le fichier qui se trouve dans le conteneur
        # C'est plus sur que de le rapatrier
        try:
            conteneur = getConf('DKRMAINT')
            cmd = 'cat ' + connect 
            cont_connect = self._system(cmd, conteneur)
        except PicErreur as x:
            print(f"WARNING - Site {site}: Pas de fichier connect.php")
            return None

        for l in cont_connect:
            l = l.strip()
            if l.startswith('spip_connect_db'):
                chps = l.split(',')
                if len(chps)==9:
                    db = chps[4].strip("'")
                    return db
                else:
                    print("ERREUR - Le fichier " + connect + " n'a pas le bon format (spip_connect_db n'a pas le bon nombre de parametres)")
                    return None
        print ("ERREUR - Le fichier " + connect + " n'a pas le bon format (spip_connect_db non trouve)")
        return None

class MysqlMutuajout(MysqlMutualisation):
    '''Création de la base de données: on préfère le faire ici avec les privilèges root que la laisser à spip car cela nécessiterait
       de donner à l'utilisateur de la mutu les privilèges globaux. '''
    
    def execute(self) -> None:
        # pg est l'objet PicGestion dans lequel nous habitons
        pg = self.picgestion

        # Si on crée un alias, il n'y a rien à faire
        if pg.sitealias != None:
            return

        # Noms de bases etc
        user   = pg.user
        userdb = self.getUserDb()

        site = pg.site
        db   = self._compDatabase(site)
        conteneur = getConf('DKRMAINT')
        
        privileges  = 'ALTER,CREATE,DELETE,DROP,INDEX,INSERT,SELECT,UPDATE,CREATE TEMPORARY TABLES, SHOW VIEW, CREATE ROUTINE, ALTER ROUTINE, EXECUTE, CREATE VIEW,EVENT, TRIGGER'
        
        print("=== creation de la base de donnees " + db)
        cmd = 'mysqladmin create ' + db
        self._system(cmd,conteneur)
        
        print("=== Definition des droits de " + user + " sur " + db)
        #req = "GRANT " + privileges + " ON " + db + ".* TO '" + user + "';"
        req = "GRANT {privileges} ON {db}.* TO {userdb};".format(privileges=privileges, db=db, userdb=userdb)

        self._sql(req,db,conteneur)

class MysqlMutusuppr(MysqlMutualisation):
    
    def demandeParams(self) -> None:
        '''Demande si besoin le mot de passe root de mysql'''

        # Determination du nom de la B.D.
        pg = self.picgestion
        #prod_p = pg.prod[2]
        site = pg.site
        self.__db = self._getDatabase(site)

        if self.__db == None:
            print(f"=== Pas de base de donnée detectée pour {site} ===")
            return

    def execute(self) -> None:
        '''Supprime la base de donnees'''
        db = self.__db
        if db != None:  
            print("=== Suppression de la base de donnee " + db + " ===");
            req = "DROP DATABASE IF EXISTS " + db + ';' 
            conteneur = getConf('DKRMAINT')
            self._sql(req,db, conteneur)

class MysqlMutuarchivage(MysqlMutualisation):
    
    def execute(self) -> None:
        '''Dump toutes les bases de donnees des sites archives'''
        
        pg = self.picgestion
        sites = pg.mutuarchiversite
        home = pg.home
        user = pg.user
        archives = getConf('ARCHIVES')
        arch_path_base = f"{archives}/{user}"
        conteneur = getConf('DKRMAINT')
        
        print("=== Archivage des bases de donnees...")
        for site in sites:
            sitedir = f'{home}/www/sites/{site}'
            if self._dkr_f_lien(sitedir,conteneur):
                print(f"=== site {site} est un lien symbolique - Pas de BD a archiver !")
                continue

            db = self._getDatabase(site)
            if db == None:
                continue
            
            arch_path = f'{arch_path_base}/{site}'
            cmd = f"mkdir -p {arch_path}"
            self._system(cmd,conteneur)
            fdump = self._calc_nom_archive(site, arch_path)

            print(f"=== site = {site }  db = {db}")
            
            # je préferre le faire en deux temps car si on a tapé un mauvais mot de passe mysqldump renvoie un code d'erreur
            # et on sait que ça n'a pas marche ! Sinon on aurait pu mettre un pipe
            self._system(f'rm -f {fdump}',conteneur)
            fdump1 = fdump.removesuffix('.gz')

            cmd = f"mysqldump --hex-blob --databases {db} > {fdump1}"
            self._system(cmd, conteneur)
            
            cmd = f'gzip {fdump1}'
            self._system(cmd, conteneur)

class MysqlMutuexporte(MysqlMutuarchivage):
    pass    

class MysqlMutusauvebd(MysqlMutualisation):
    '''Sauve toutes les bases de donnees de la mutu dans le repertoire squelettes/db
       Ne demande aucun parametres, car peut etre utilise dans un cron'''
    
    def execute(self) -> None:
        '''Dump toutes les bases de donnees des sites de la mutu'''
        
        pg          = self.picgestion
        sites       = list(pg.sites)
        user        = pg.user
        conteneur   = getConf('DKRMAINT')

        for site in sites:
            sitedir = pg.home + '/www/sites/' + site
            if self._dkr_f_lien(sitedir,conteneur):
                continue

            db = self._getDatabase(site)
            if db == None:
                continue
            
            site_path = sitedir + '/squelettes/db/'
            dump_path = site_path + site + '.sql'

            # Si on n'a pas encore sauve la bd, le repertoire n'est pas encore cree
            if not self._dkr_fexists(site_path,conteneur):
                try:
                    cmd = "mkdir " + plgdir
                    self._system(cmd,conteneur)
                except:
                    print (f"WARNING - Répertoire {site_path} non accessible, problème avec ce site ! ")
                    continue

            # je préferre le faire en deux temps car si on a tape un mauvais mot de passe mysqldump renvoie un code d'erreur
            # et on sait que ca n'a pas marche !
            self._system(f'mysqldump --hex-blob --databases {db} > {dump_path}',conteneur)
            self._system(f'rm -f {dump_path}.gz', conteneur)
            self._system(f'gzip {dump_path}', conteneur)

            # Mettre le groupe sur l'utilisateur de la mutu
            self._system(f'chgrp -R {user} {site_path} && chmod o= {dump_path}.gz', conteneur)


class MysqlMutuimporte(MysqlMutualisation):
    
    def demandeParams(self) -> None:
        pg = self.picgestion
        site = pg.site
        db   = self._getDatabase(site)
        if db == None:
            raise PicErreur("ERREUR - Pas de base de donnees configuree: avez-vous installe spip sur ce site ?")

        self.__importeBase = False
        tmp_in = ouiNon("Voulez-vous importer la base de donnees ? ",False)
        if tmp_in:
            self.__importeBase = ouiNon("Vous etes sur ? Cela DETRUIRA la base existante du site de destination",False )
            self.__db          = db
        
    def execute(self) -> None:
        if self.__importeBase:
            pg = self.picgestion
            user = pg.user
            site = pg.site

            [s_user,s_nom,s_version] = pg.source
            #s_dump_path= getConf('ARCHIVES') + s_user + '/' + s_user + '.mysqldump.gz'
            s_dump_path = self._calc_nom_archive(s_user, None, True)
            conteneur  = getConf('DKRMAINT')
            db = self.__db
    
            print("=== Suppression et recreation de la base de donnees...")
            cmd = 'mysqladmin --force drop ' + db
            self._system(cmd, conteneur)
    
            cmd = 'mysqladmin create ' + db
            self._system(cmd, conteneur)
            print("=== Remplissage de la base de donnees...")
            cmd = "zgrep -v '^USE' " + s_dump_path + " |grep -v '^CREATE DATABASE'|mysql " + db
            self._system(cmd,conteneur)

            # ATTENTION - La commande spip auteur:mdp ne marche pas ici car spip-cli n'est PAS ENCORE utilisable !
            #             Pour que spip-cli soit utilisable il faut que adresse_site ait une valeur correcte
            #
            print("=== Mise à jour de adresse_site")
            req = f"UPDATE `spip_meta` SET `valeur` = 'https://{site}' WHERE `spip_meta`.`nom` = 'adresse_site'"; 
            self._sql(req, db, conteneur)

class MysqlSuppression(Mysql):
    def execute(self) -> None:
        
        # pg est l'objet PicGestion dans lequel nous habitons
        pg = self.picgestion

        # Noms de bases etc
        user   = pg.user
        userdb = self.getUserDb()
        db     = self.database
        conteneur = getConf('DKRMAINT')
        
        print("=== Suppression des bases de donnees et des utilisateurs mysql");
        req = "DROP DATABASE IF EXISTS " + db + ';'
        req += "DROP USER IF EXISTS {userdb};".format(userdb=userdb)
        self._sql(req,'mysql',conteneur)

class MysqlArchivage(Mysql):
    
    def execute(self) -> None:
        pg = self.picgestion
        
        # Noms de bases etc
        user = pg.user
        db   = self.database

        home_dir = pg.home
        conteneur= getConf('DKRMAINT')

        print("=== Archivage de la base de donnees...")
        fdump = self._calc_nom_archive(user)

        # je préferre le faire en deux temps car si on a tapé un mauvais mot de passe mysqldump renvoie un code d'erreur
        # et on sait que ça n'a pas marche ! Sinon on aurait pu mettre un pipe
        self._system(f'rm -f {fdump}',conteneur)
        fdump1 = fdump.removesuffix('.gz')
        cmd = f"mysqldump --hex-blob --databases {db} > {fdump1}"
        self._system(cmd, conteneur)
        
        cmd = f'gzip {fdump1}'
        self._system(cmd, conteneur)

class MysqlSauvebd(Mysql):
    def execute(self) -> None:
        # pg est l'objet PicGestion dans lequel nous habitons
        pg = self.picgestion
        
        # Noms de bases etc
        user = pg.user
        db   = self.database
        fdump= db + '.sql.gz'
        home_dir = self.picgestion.home
        dump_path= home_dir + '/db/' + fdump

        conteneur= getConf('DKRMAINT')

        # On appelle mysqldump dans le conteneur ssh
        cmd = "mysqldump --hex-blob --databases " + db + "| gzip > " + dump_path

        # On donne à l'utilisateur la propriété du fichier
        cmd1 = f'chown {user} {dump_path} && chmod o= {dump_path}'
        try:
            self._system(cmd,conteneur)
            self._system(cmd1,conteneur)
        except PicErreur as e:
            print(e.message)
            

class MysqlRecharge(Mysql):
    
    def demandeParams(self) -> None:
        pg = self.picgestion
        [s_user,s_nom,s_version] = pg.source

        try:
            self._s_dump_path = self._calc_nom_archive(s_user, None, True)
            self._rechargeBd = ouiNon(f"Recharge de la BD a partir de {self._s_dump_path} ?",False)

        except PicErreur as e:
            self._rechargeBd = False
            print (afficheJaune(f"ATTENTION - Pas de fichier sql dans {s_user}"))
    
    def execute(self) -> None:
        '''Recharge d'un site a partir de l'archivage'''

        pg = self.picgestion
        conteneur = getConf('DKRMAINT')
        s_sql_path = f'{self._s_dump_path}' 
        db = self.database

        try:
            if self._rechargeBd:
                
                print("=== Suppression et recreation de la base de données...")
                cmd = f"mysqladmin --force  drop {db}"
                self._system(cmd,conteneur)
                
                cmd = f"mysqladmin create {db}"
                self._system(cmd,conteneur)
                
                print("=== Remplissage de la base de données...")
                cmd = f"zgrep -v '^USE' {s_sql_path} | grep -v '^CREATE DATABASE' | mysql --binary-mode {db}" 
                self._system(cmd,conteneur)
            else:
                print("=== Pas de rechargement de la base de données")

        except Exception as e:
            print (afficheRouge(f"ERREUR - Chargement de la base de données incomplet - {e}"))

class MysqlProduction(Mysql):
    def __demandeParamsWp(self) -> None:
        pg = self.picgestion
        www = pg.www
        user = pg.user
        conteneur = getConf('DKRMAINT')
        inf = pg.statinf
        
        # Détermination de l'url actuelle du site
        cmd = f"cd {www} && wp option get siteurl 2>/dev/null"
        rvl = self._system(cmd, conteneur, False, user)
        if len(rvl) == 0:
            raise PicFatalErreur("ERREUR INTERNE")
        
        self.__siteurl = rvl[0]
        if pg.prodnext == "":
            self.__siteprod = "https://" + user + "." + getConf("DOMAINNAME")
        else:
            self.__siteprod = "https://" + pg.prodnext
            
        # Si c'est la même que l'adresse de prod on ne change rien !
        if self.__siteurl == self.__siteprod:
            print (f"L'URL de base (siteurl) est déjà {self.__siteurl}")
            self.__setsiteurl = False
            
        # Sinon on propose de la changer
        else:
            # S'il y a un site statique adossé au wordpress dynamique et s'il prend les adresses de prod, surtout ne rien changer
            if inf['prod2stat']:
                self.__setsiteurl = False
            else:
                print (f"Url du site ACTUELLE = {self.__siteurl}")
                self.__setsiteurl = ouiNon(f"Voulez-vous la changer pour {self.__siteprod} ?", False)
            
        print()

    def __demandeParamsSpip(self) -> None:
        pg = self.picgestion
        www = pg.www
        user = pg.user
        conteneur = getConf('DKRMAINT')
        inf = pg.statinf

        # Détermination de l'url actuelle du site
        cmd = f"cd {www} && spip config:lire adresse_site"
        rvl = self._system(cmd, conteneur, False, user)
        if len(rvl) == 0:
            raise PicFatalErreur("ERREUR INTERNE")

        rvl = a=[ x.strip() for x in rvl if x != '' ] # Supprimer les lignes vides et les espaces
        self.__siteurl = rvl[len(rvl)-1].strip() # Il y a une ligne vide en tête de réponse
        #print(rvl)
        if pg.prodnext == "":
            self.__siteprod = "https://" + user + "." + getConf("DOMAINNAME")
        else:
            self.__siteprod = "https://" + pg.prodnext
            
        # Si c'est la même que l'adresse de prod on ne change rien !
        if self.__siteurl == self.__siteprod:
            print (f"L'URL de base (siteurl) est déjà {self.__siteurl}")
            self.__setsiteurl = False
            
        # Sinon on propose de la changer
        else:
            # S'il y a un site statique adossé au wordpress dynamique et s'il prend les adresses de prod, surtout ne rien changer
            if inf['prod2stat']:
                self.__setsiteurl = False
            else:
                print (f"Url du site ACTUELLE = {self.__siteurl}")
                self.__setsiteurl = ouiNon(f"Voulez-vous la changer pour {self.__siteprod} ?", False)
            
        print()
        
    def demandeParams(self) -> None:
        pg = self.picgestion
        www = pg.www
        user = pg.user
        conteneur = getConf('DKRMAINT')

        self.__wordpress = False
        self.__spip = False
        if pg.isWordpress():
            self.__wordpress = True
        elif pg.isSpip():
            self.__spip = True

        if self.__wordpress:        
            print ("\n=== IL S'AGIT D'UN SITE WORDPRESS ===")
            self.__demandeParamsWp()
        if self.__spip:        
            print ("\n=== IL S'AGIT D'UN SITE SPIP ===")
            self.__demandeParamsSpip()

    def __executeWp(self) -> None:
        self._siteUrlPourWp(self.__siteprod)
        print (f"=== Le site a maintenant comme url de base {self.__siteprod}")

    def __executeSpip(self) -> None:
        self._siteUrlPourSpip(self.__siteprod)
        print (f"=== Le site a maintenant comme url de base {self.__siteprod}")
        
    def execute(self) -> None:
        pg = self.picgestion
        if self.__wordpress:
            if self.__setsiteurl:
                self.__executeWp()
        elif self.__spip:
            if self.__setsiteurl:
                self.__executeSpip()

class MysqlStatique(Mysql):
    
    def execute(self) -> None:
        pg = self.picgestion
        user = pg.user
        domaine = getConf('DOMAINNAME')
        tech_url = f'{user}.{domaine}'
        inf = pg.statinf
        
        # Si la prod va sur le site statique, il faut modifier le siteurl
        if inf['prod2stat']:
        
            if pg.isWordpress():
                self._siteUrlPourWp(tech_url)
    
            elif pg.isSpip():
                self._siteUrlPourSpip(tech_url)
            
            else:
                print (afficheJaune("ATTENTION - Peut-être faut-il modifier l'URL de base du CMS: {tech_url}"))
            
class MysqlNostatique(Mysql):

    def demandeParams(self) -> None:
        pg = self.picgestion
        user = pg.user
        prod = pg.prod
        prod_user = prod[1]
        prod_urls = prod[0]
        self.__siteurl = ""
        if prod_user == user:
            inf = pg.statinf
            if inf['prod2stat']:
                print("ATTENTION - Les adresses de prod reviennent sur le site dynamique")
                if pg.isWordpress() or pg.isSpip():
                    while self.__siteurl == "":
                        self.__siteurl = entrerFiltrer(f"Précisez l'url du site parmis {str(prod_urls)} ")
                else:
                    print("             Il faudra changer l'url de site dans votre CMS")

    def execute(self) -> None:
        pg = self.picgestion
        if self.__siteurl != "":
            if pg.isWordpress():
                self._siteUrlPourWp(self.__siteurl)
            elif pg.isSpip():
                self._siteUrlPourSpip(self.__siteurl)
            else:
                raise PicFatalErreur(f"ERREUR INTERNE - Ni wordpress, ni spip mais self.__siteurl={self.__siteurl} !")
                
class MysqlSpipclone(MysqlRecharge):

    def demandeParams(self) -> None:
        super().demandeParams()
        pwd = ''
        while pwd == '':
            pwd = getpass.getpass('mot de passe mysql ')
        self.__pwd = pwd

    def execute(self) -> None:
        pg = self.picgestion
        home_dir = pg.home
        www = pg.www
        user = pg.user
        domaine = getConf('DOMAINNAME')
        addr = f"https://{user}.{domaine}"
        database = pg.database
        conteneur = getConf('DKRMAINT')
        db_host = getConf('HSTMYSQL')
        connect = f"{www}/config/connect.php"

        # On appelle la methode de la superclasse, puis on complete:

        # Recharge de la B.D.
        super().execute()

        # Reconstitution du fichier connect        
        print("=== Spipclone = Recréation du fichier connect.php")     
        cmd = f"rm -f {connect}"
        self._system(cmd, conteneur)

        pwd = self.__pwd
        cmd = f"cd {www} && spip core:install --db-server=mysql --db-host={db_host} --db-pass={pwd} --db-login={user} --db-database={database}"
        self._system(cmd, conteneur, False, user)
        
        # On remet à jour l'URL du site
        self._siteUrlPourSpip(addr)
        
        # core:install a probablement créé un stupide utilisateur appelé Admin. Je le vire
        try:
            cmd = f"cd {www} && spip auteurs:changer:statut --statut=5poubelle --email='admin@spip'"
            self._system(cmd, conteneur, False, user)        
        except:
            pass
        
        # Et enfin on vide le cache
        cmd = f"cd {www} && spip cache:vider"
        self._system(cmd, conteneur, False, user)        

class MysqlWpinstall(Mysql):
    
    def demandeParams(self) -> None:
        self._pwd  = getpass.getpass('mot de passe de la base de donnees: ')
            
class MysqlWpclone(MysqlRecharge):

    def demandeParams(self) -> None:
        self._rechargeBd = True # On force la recharge de la Bd lorsqu'on clone
        self._pwd  = getpass.getpass('mot de passe de la base de donnees: ')
        super().demandeParams()
       
    # On appelle la methode de la superclasse, puis on complete:
    #    - mise à jour de wp-config.php (seulement par rapport à mysql))
    def execute(self) -> None:

        pg = self.picgestion
        home_dir = pg.home
        www      = pg.www
        user     = pg.user
        mail     = pg.mail
        db       = self.database
        pwd      = self.dbpassword
        hostdb   = getConf('HSTMYSQL')
        domaine  = getConf('DOMAINNAME')
        
        [s_user,s_nom,s_version] = pg.source
        [prod_urls, prod_user] = pg.prod
        conteneur= getConf('DKRMAINT')
        
        super().execute()
        
        # Modification de wp-config
        self._system(f"cd {www} && wp config set DB_NAME {db}", conteneur, False, user)
        self._system(f"cd {www} && wp config set DB_USER {user}", conteneur, False, user)
        self._system(f"cd {www} && wp config set DB_PASSWORD {pwd}", conteneur, False, user)
        self._system(f"cd {www} && wp config set DB_HOST {hostdb}", conteneur, False, user)

        # Appel de SearchReplace

        # Si la source a une ou plusieurs adresses de production on les remplace dans la B.D.
        # par la nouvelle adresse technique
        if prod_user == user:
            urls = prod_urls
        else:
            urls = []

        # On remplace aussi l'ancienne adresse technique par la nouvelle
        urls.append(f"{s_user}.{domaine}")
        
        url_new = f"{user}.{domaine}"
        for u in urls:
            print (f"Remplacer {u} par {url_new}")
            self._system(f"cd {www} && wp search-replace http://{u} https://{url_new}", conteneur, False, user)
            self._system(f"cd {www} && wp search-replace https://{u} https://{url_new}", conteneur, False, user)
            entrerFiltrer ("Appuyer sur Entrer pour continuer...")
 
        # changement de siteurl et home
        siteurl = f"https://{user}.{getConf('DOMAINNAME')}"
        print (f"=== Positionner siteurl sur {siteurl}")
        cmd = f"cd {www} && wp option update siteurl {siteurl}"
        self._system(cmd, conteneur, False, user)
        cmd = f"cd {www} && wp option update home {siteurl}"
        self._system(cmd, conteneur, False, user)
           
        # On remplace enfin le chemin absolu vers les fichiers au cas où
        # NOTE - On fait l'hypothèse que le home de source et destination sont "frères"
        #        Par exemple source: /home/monasso-st1 et destination: /home/monasso-st2
        s_home = os.path.join(os.path.dirname(home_dir), s_user)
        print (f"Remplacer {s_home} par {home_dir}")
        self._system(f"cd {www} && wp search-replace {s_home} {home_dir}", conteneur, False, user)
        entrerFiltrer ("Appuyer sur Entrer pour continuer...")
        
        # Ajout d'un nouvel admin
        print (f"==== Creation de l'admin {user}")
        self._systemWithWarning(f"cd {www} && wp user update {s_user} --user_email=none@none.org", conteneur, False, user)
        self._systemWithWarning(f"cd {www} && wp user create {user} {mail} --role=administrator", conteneur, False, user)

        # Suppression de l'ancien admin
        print (f"==== Suppression de l'admin {s_user}")
        self._systemWithWarning(f"cd {www} && wp user delete {s_user} --reassign={user}", conteneur, False, user)

class MysqlGaletteclone(MysqlRecharge):

    def demandeParams(self) -> None:
        self._rechargeBd = True # On force la recharge de la Bd lorsqu'on clone
        super().demandeParams()
        self._pwd  = getpass.getpass('mot de passe de la base de donnees: ')
        
    # On appelle la methode de la superclasse, puis on complete:
    #    - mise à jour de galette/config/config.inc.php
    def execute(self) -> None:

        pg = self.picgestion
        home_dir = pg.home
        www      = pg.www
        user     = pg.user
        mail     = pg.mail
        db       = self.database
        pwd      = self.dbpassword
        hostdb   = getConf('HSTMYSQL')
        domaine  = getConf('DOMAINNAME')
        
        [s_user,s_nom,s_version] = pg.source
        [prod_urls, prod_user] = pg.prod
        conteneur= getConf('DKRMAINT')
        
        super().execute()
        
        # Régénération de config.inc.php !
        # Validé pour galette jusqu'à la version 0.9.6.1, on espère que le format ne va pas changer
        # NOTE - Ne marchera pas si le préfixe n'est pas galette_
        confNom = f"{home_dir}/galette/galette/config/config.inc.php"
        confContenu = f"<?php\n"
        confContenu += f"define('TYPE_DB', 'mysql');\n"
        confContenu += f"define('HOST_DB', '{hostdb}');\n"
        confContenu += f"define('PORT_DB', '3306');\n"
        confContenu += f"define('USER_DB', '{user}');\n"
        confContenu += f"define('PWD_DB', '{pwd}');\n"
        confContenu += f"define('NAME_DB', '{db}');\n"
        confContenu += f"define('PREFIX_DB', 'galette_');\n"

        self._systemWithWarning(f"echo \"{confContenu}\" > {confNom} && chown {user}:{user} {confNom} && chmod 440 {confNom}", conteneur)

class MysqlDolibarrclone(MysqlRecharge):

    def demandeParams(self) -> None:
        self._rechargeBd = True # On force la recharge de la Bd lorsqu'on clone
        super().demandeParams()
        self._pwd  = getpass.getpass('mot de passe de la base de donnees: ')
        
    def execute(self) -> None:
        ''' On appelle la methode de la superclasse, puis on complete:
            mise à jour de dolibarr/htdocs/conf/conf.php'''

        pg = self.picgestion
        home_dir = pg.home
        www      = pg.www
        user     = pg.user
        mail     = pg.mail
        db       = self.database
        pwd      = self.dbpassword
        hostdb   = getConf('HSTMYSQL')
        domaine  = getConf('DOMAINNAME')
        
        [s_user,s_nom,s_version] = pg.source
        #[prod, prod_user, prod_p] = pg.prod
        conteneur= getConf('DKRMAINT')
        super().execute()
        
        # Régénération de conf.php !
        f_conf = f"{home_dir}/dolibarr/htdocs/conf/conf.php"
        url = f"https://{user}.{getConf('DOMAINNAME')}"

        sed_cmd =  f" s!$dolibarr_main_url_root=.*!$dolibarr_main_url_root='{url}';!;";
        sed_cmd += f" s!$dolibarr_main_document_root=.*!$dolibarr_main_document_root='{www}';!;"
        sed_cmd += f" s!$dolibarr_main_document_root_alt=.*!$dolibarr_main_document_root_alt='{www}/custom';!;"
        sed_cmd += f" s!$dolibarr_main_data_root=.*!$dolibarr_main_data_root='{home_dir}/dolibarr/documents';!;"
        sed_cmd += f" s!$dolibarr_main_db_host=.*!$dolibarr_main_db_host='{hostdb}';!;"
        sed_cmd += f" s!$dolibarr_main_db_name=.*!$dolibarr_main_db_name='{db}';!;"
        sed_cmd += f" s!$dolibarr_main_db_user=.*!$dolibarr_main_db_user='{user}';!;"
        sed_cmd += f" s!$dolibarr_main_db_pass=.*!$dolibarr_main_db_pass='{pwd}';!;"

        self._sed(sed_cmd, f_conf, conteneur)
