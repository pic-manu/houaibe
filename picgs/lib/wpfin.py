#! /usr/bin/python

#
# MODULE source = 1/ Definition de la source des donnees, utilise pour recharge
#                 2/ Envoi d'un message d'alerte general avant toute execution de plugin, utilise pour suppression
#                    Du coup le nom source se comprend comme: "Le premier plugin appele !"
#

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

import json

from picgestion import PicGestion
from plugin import Plugin
from ouinon import ouiNon
from ouinon import entrerFiltrer
from erreur import *
from picconfig import getConf

# NOTE: Redondant cf. wphomedir !
def checkNomobj(nomobj: str) -> None:
    '''Raise an exception if nomobj not valid'''
    values = [ 'plugin','theme','user']
    if not nomobj in values:
        raise PicFatalErreur (f'ERREUR INTERNE - {nomobj} NOT in {values}')

class WpFin(Plugin):

    @property
    def nomplugin(self)->str:
        return 'WpFin'
    
    def __init__(self,action,nom,version,pic_gestion,options=[]):

        # Appel du constructeur du parent
        Plugin.__init__(self,action,nom,version,pic_gestion,options)

    def _chkPicadminWp(self, user:str) -> bool:
        '''Précondition: On est dans un site Wordpress
           renvoie True si cette instance de wordpress a un compte "picadmin"
        '''

        conteneur = getConf('DKRMAINT')

        www_dir = f"{getConf('HOMES')}/{user}/www"
        cmd = f"cd {www_dir} && wp user get picadmin"
        try:
            self._system(cmd,conteneur,False,user)
        except PicErreur as e:
            return False
            
        return True
  
    def _readRef(self) -> dict|None:
        ''' Lit le fichier json dont le nom est spécifié par l'option --ref
            Renvoit le dict refData, ou None si pas de fichier'''
        
        #print ("ici _readRef")
        options = self.options
        if '--ref' in options:
            ref = options['--ref']
        else:
            raise PicFatalErreur("ERREUR INTERNE - Pas de switch --ref")
        
        if ref and self._dkr_fexists(ref):
            try:
                with open(ref) as f:
                    refData = json.load(f)
            except:
                refData = None
        else:
            refData = None

        return refData

    def _writeRef(self, refData) -> None:
        '''Ecrit refData en json dans le fichier dont le nom est spécifié par l'option --ref'''

        options = self.options
        if '--ref' in options:
            ref = options['--ref']
        else:
            raise PicFatalErreur("ERREUR INTERNE - Pas de switch --ref")
            
        if ref:
            with open(ref,'w') as f:
                json.dump(refData, f)

    def _reformateData(self,data,createItem) -> dict:
        ''' Renvoie une version reformattée du tableau data:
            dict (key = user / val = newData)
            createItem: fonction, on lui passe item elle renvoie newItem, un dict'''

        newData = {}
        for d in data:
            if d['erreur']:
                continue

            huser = d['huser']
            newData[huser] = createItem(d)
    
        return newData

    def __settifyValues(self, values, slice) -> set:
        '''Renvoie un set à partir de l'array values
           slice est utilisé pour simplifier et regroupe les éléments de values
           Par exemple pour extraire la version: plugin_machin#1.10 -> plugin_machin'''
           
        outValues = []
        if slice != None:
            outValues = set([ '#'.join(v.split('#')[slice]) for v in values ])
        else:
            outValues = set(values)
        
        return outValues
                   
    def _returnDataVsRef(self, refData: list, data: list , msg: str, sl=None) -> tuple[str,bool]:
        ''' Renvoie un tuple [str, bool]
            - chaîne imprimable: ce qui a changé dans les Data par rapport a refData, msg si rien n'a rien changé
            - booléen: True si quelque chose a changé, False si aucun changement
            Le slice sl est utilisé pour se restreindre à une partie des data'''
            
        sl = slice(0,1)
        firstModif = False 
        rvl = ''
        for u in data:
            n = len(data[u])
            l = f"{u:30}{n:2}     "
    
            try:
                refValues = self.__settifyValues(refData[u].values(), sl)
            except Exception:
                refValues = set()
    
            values = data[u].values()
            if sl != None:
                values = self.__settifyValues(values,sl)

            ajoutes = values.difference(refValues)
            retires = refValues.difference(values)
    
            #print ("koukou5")
            #import pprint
            #pprint.pprint(refValues)
            #pprint.pprint(values)
            #pprint.pprint(ajoutes)
            #pprint.pprint(retires)
            
            # Pas d'impression si pas de modification
            if len(ajoutes) == 0 and len(retires) == 0:
                continue
    
            h = 37*' '
            first = True
            for r in retires:
                if first:
                    l += f"-{r}"
                    first = False
                else:
                    l += f"\n{h}-{r}"
    
            for a in ajoutes:
                if first:
                    l += f"+{a}"
                    first = False
                else:
                    l += f"\n{h}+{a}"
            
            if not firstModif:
                firstModif = True
                rvl += f"{'Site':30}{'Nombre':6} -suppressions +ajouts\n"
             
            rvl += f"{l}\n"
            
        if not firstModif:
            rvl += f"{msg}\n"
    
        return (rvl, firstModif)

class WpFinCore(WpFin):
    '''Classe mère pour WpFinListecore et WpFinMajcore
       Supprime les éléments de data s'ils sont en erreur
       on génère le dictionnaire versions, indexé par version de wp
       Renvoie versions'''
    
    def _finaliseVersions(self) -> dict:
        pg   = self.picgestion
        data = PicGestion.adata
        versions  = {}
        for d in data:
            if d['erreur']:
                continue
            v = d['version']
            u = d['huser']
            w = d['wpout']
            
            if not v in versions:
                versions[v] = {}
                versions[v]['husers'] = []
                for o in w:
                    versions[v][o['update_type']] = o['version']
                if not 'minor' in versions[v]:
                    versions[v]['minor'] = '-'
                if not 'major' in versions[v]:
                    versions[v]['major'] = '-'
            
            versions[v]['husers'].append(u)

        return versions
    
class WpFinListe_core(WpFinCore):
    '''Appelle _finaliseVersions (de la classe mère)
       On est à la fin du traitement: affichage de versions'''
    
    def derniere(self) -> str:
        versions = self._finaliseVersions()
        rvl = ''
        if len(versions) > 0:
            print ("Version\tminor\tmajor\tSites")
            for v in versions:
                rvl += f"{v}\t{versions[v]['minor']}\t{versions[v]['major']}\t{','.join(versions[v]['husers'])}\n"
        return rvl

class WpFinListe_mails(WpFin):
    '''Appelle _finaliseVersions (de la classe mère)
       On est à la fin du traitement: affichage de versions'''
    
    def derniere(self) -> str:
        pg = self.picgestion
        data = PicGestion.adata
        rvl = ''
        for d in data:
            if d['erreur']:
                continue
            if d['mail'] != '':
                rvl += f"{d['mail']} {d['huser']} {d['nom']}\n"
        return rvl

class WpFinMaj_core(WpFinCore):
    '''Appelle _finaliseVersions (de la classe mère)
       On est à la fin du traitement: mises à jour proprement dites'''
       
    def derniere(self) -> str:
        options = self.options
        minmaj = 'minor'
        if '--major' in options:
            minmaj = 'major'
        rvl = ""
        versions = self._finaliseVersions()
        for v in sorted(versions.keys()):
            info = versions[v]
            if info[minmaj] != '-':
                for u in info['husers']:
                    if not self._chkPicadminWp(u):
                        continue
                    if '--oui' in options or ouiNon(f"Compte {u}: maj de {v} vers {info[minmaj]} ?",False):
                        cmd1 = f"cd {getConf('HOMES')}/{u}/www && wp core update"
                        if minmaj == 'minor':
                            cmd1 += ' --minor'
                        cmd2 = f"cd {getConf('HOMES')}/{u}/www && wp core update-db"
                        rvl += f"--> {u}\n"
                        if '--dry-run' in options:
                            rvl += f"dry-run: {cmd1}\n"
                            rvl += f"dry-run: {cmd2}\n"
                        else:
                            out = self._systemWithWarning(cmd1,getConf('DKRMAINT'),False,u)
                            rvl += "\n".join(out)
                            out = self._systemWithWarning(cmd2,getConf('DKRMAINT'),False,u)
                            rvl += "\n\n".join(out)
                            rvl += "\n"
                        rvl += "============\n"
        return rvl
                

class WpFinPluginsThemes(WpFin):
    ''' Classe mère pour WpFinListeplugins, WpFinMajplugins, WpFinListeThemes '''

    def __p2nom(self,p: list) -> str:
        ''' Calcule le "nom" du plugin/theme à partir des données dans p '''
        n = p['name'] + ' ' + p['version']
        return n

    def _finaliseVersions(self) -> dict:
        ''' Appelé par derniere(), donc APRES avoit traité tous les utilisateurs
            on génère le dictionnaire versions, indexé par version de plugin
            Renvoie versions'''

        pg = self.picgestion
        data = PicGestion.adata

        versions = {}
        for d in data:
            if d['erreur']:
                continue
            u  = d['huser']
            w1 = d['wpout1']
            w2 = d['wpout2']
            for p in w1:
                v = self.__p2nom(p)
                if not v in versions:
                    versions[v] = {}
                    versions[v]['husers'] = []
                versions[v]['husers'].append(u)
                versions[v]['update_version'] = '-'
                
            for p in w2:
                v = self.__p2nom(p)
                if not v in versions:
                    raise PicFatalErreur("ERREUR INTERNE - {v} devrait être dans versions".format(v=v))
                versions[v]['update_version'] = p['update_version']
                            
        return versions

    def _derniere(self, objet: str) -> str:
        '''Appelé par WpFinMaj_plugins:derniere et WpFinMaj_themes:derniere
           objet: 'theme' ou 'plugin' (au singulier !) '''
           
        if objet != 'plugin' and objet != 'theme':
            raise PicErreur(f"ERREUR INTERNE - objet = {objet}, devrait être theme ou plugin")

        rvl = ''
        versions = self._finaliseVersions()
        options = self.options
        exclure = []
        if '--exclure' in options:
            exclure = options['--exclure'].replace(' ','').split(',')

        if objet == 'plugin':
            if '--plugin' in options:
                plug = options['--plugin'].lower()
            else:
                plug = None
        else:
            if '--theme' in options:
                plug = options['--theme'].lower()
            else:
                plug = None
            
        if '--major' in options:
            minmaj = ''
        else:
            minmaj = '--patch' if objet == 'plugin' else '--minor'

        for v in sorted(versions.keys()):
            plugmaj = v.partition(' ')[0] # Nom-du-plugin 1.0.0 ==> Nom-du-plugin
            
            # Traitement de l'option --plugin ou --theme
            if plug != None and plugmaj != plug:
                continue
            
            # Traitement de l'option --exclure, ignorée si on a aussi --plugin
            if plug == None and plugmaj in exclure:
                continue

            info = versions[v]
            if info['update_version'] != '-':
                for u in info['husers']:
                    if not self._chkPicadminWp(u):
                        continue
                    if '--oui' in options or ouiNon(f"Compte {u}: maj du {objet} {v} vers {info['update_version']} ?",False):
                        www_dir = f"{getConf('HOMES')}/{u}/www"
                        cmd     = f"cd {www_dir} && wp {objet} update {plugmaj} {minmaj}"
                        if '--dry-run' in options:
                            rvl += f"dry-run: {cmd}\n"
                        else:
                            rvl += f"--> {u} {plugmaj}\n"
                            out = self._systemWithWarning(cmd,getConf('DKRMAINT'),False,u)
                            if rvl != None:
                                rvl += '\n'.join(out)
                        rvl += "============\n"
        return rvl

class WpFinListepluginsthemes(WpFinPluginsThemes):
    '''Appelle _finaliseVersions (de la classe mère)
       Si _finaliseVersions renvoie un versions non vide, on est à la fin du traitement: affichage de versions'''

    def __newItem(self, item: dict) -> dict:
        '''item = Un item à reformatter
           newItem = dict
                     key = nom du plugin wp
                     val = nom#version'''
        newItem = {}
        for p in item['wpout1']:
            id = p['name']
            newItem[id] = f"{id}#{p['version']}"
        return newItem

    def _returnVersions(self, nomobj: str) -> str:
        '''Renvoie sous forme de chaine imrimable la liste des versions de nomobj, avec les users qui les utilisent'''

        checkNomobj(nomobj)
        rvl = ''
        versions = self._finaliseVersions()

        if len(versions) > 0:
            rvl += f"{nomobj.capitalize()+'s':50s}\t{'Version':15s}\t{'Update version':15s}\t{'Sites'}\n"

            for v in sorted(versions):
                (p,d,pv) = v.partition(' ')
                upd = versions[v]['update_version']
                usr = ','.join(versions[v]['husers'])
                (v,d,a) = pv.partition('#')
                rvl += f"{p:50s}\t{v:15s}\t{upd:15s}\t{usr}\n"

        return rvl
    
    def _returnDiff(self, nomobj: str) -> str:
        '''Renvoie sous forme de chaine imprimable la différence entre les données collectées et la référence'''

        checkNomobj(nomobj)
        pg = self.picgestion
        options = self.options
        rvl = ''
        raw_data = PicGestion.adata
        fmt_data = self._reformateData(raw_data,self.__newItem)
        ref_data = self._readRef()
        # Nouveau format de fichier de reference (depuis v 4.0.0))
        if isinstance(ref_data, list):
            ref_data = ref_data[0]
        (str, sts) = self._returnDataVsRef(ref_data, fmt_data, f"Aucune modification dans les {nomobj}s", slice(0,1))
        if sts and '--update-ref' in options:
            self._writeRef([fmt_data,raw_data])
        rvl += str
                    
        return rvl

    def _loadData(self, nomobj: str) -> list:
        '''Ecrit dans le adata de PicGestion les données lues depuis le fichier de référence'''

        checkNomobj(nomobj)
        pg = self.picgestion
        options = self.options
        ref_data = self._readRef()
        # Nouveau format de fichier de reference (depuis v 4.0.0))
        if isinstance(ref_data, list) and len(ref_data) >= 2:
            PicGestion.adata = ref_data[1]
        else:
            raise PicFatalErreur(f"ERREUR: Le fichier de référence n'a pas le bon format - Utilisez picwpgs liste {nomobj} --ref={options['--ref']} --update-ref pour le mettre à jour")

class WpFinListe_plugins(WpFinListepluginsthemes):
    
    def derniere(self) -> str:
        
        options = self.options
        rvl = ""
        if '--ref' in options:
            rvl += "DIFFERENCES PAR RAPPORT A LA REFERENCE\n"
            rvl += "======================================\n"
            rvl += self._returnDiff('plugin')
        if not '--only-diff' in options:
            rvl += "\n"
            rvl += self._returnVersions('plugin') 
        return rvl

class WpFinListe_refplugins(WpFinListepluginsthemes):
    
    def execute(self) -> None:
        self._loadData('plugin')
        print(self._returnVersions("plugin"))

class WpFinListe_themes(WpFinListepluginsthemes):
    
    def derniere(self) -> str:
        
        options = self.options
        rvl = ""
        if '--ref' in options:
            rvl += "DIFFERENCES PAR RAPPORT A LA REFERENCE\n"
            rvl += "======================================\n"
            rvl += self._returnDiff('theme')
        if not '--only-diff' in options:
            rvl += "\n"
            rvl += self._returnVersions('theme') 
        return rvl

class WpFinListe_refthemes(WpFinListepluginsthemes):
    
    def execute(self) -> None:
        self._loadData('theme')
        print(self._returnVersions("theme"))

class WpFinListe_users(WpFin):
    
    def __newItem(self, item: dict) -> dict:
        '''item = Un item à reformatter
           newItem = dict
                     key = ID user wordpress
                     val = ID#login#email
           ON NE GARDE QUE LES USERS ADMINISTRATOR ! '''

        newItem = {}
        for u in item['wpout1']:
            if u['roles'] == 'administrator':
                id = u['ID']
                nom_mail = str(id) + ',' + u['user_login'] + ',' + u['user_email']
                newItem[str(id)] = nom_mail
        return newItem

    def derniere(self) -> str:
        ''' on appelle _reformateData
            puis on lit/sauvegarde la référence et enfin on imprime les résultats '''

        pg   = self.picgestion
        options = self.options
        data = PicGestion.adata

        users = {}

        data = self._reformateData(data, self.__newItem)
        refData = self._readRef()

        rvl = ''
        rvl += f" UTILISATEURS ADMINISTRATEURS - CREATIONS et SUPPRESSIONS\n{58*'='}\n"
        
        (str, sts) = self._returnDataVsRef(refData, data, "Aucune modification dans les utilisateurs wordpress administrateurs")
        if sts and '--update-ref' in options:
            self._writeRef(data)
        rvl += str
        return rvl
                    
class WpFinMaj_plugins(WpFinPluginsThemes):
    '''Appelle _finaliseVersions (de la classe mère)
       On est à la fin du traitement: mises à jour proprement dites'''

    def derniere(self) -> str:
        return self._derniere('plugin')

class WpFinMaj_themes(WpFinPluginsThemes):
    '''Appelle _finaliseVersions (de la classe mère)
       On est à la fin du traitement: mises à jour proprement dites'''

    def derniere(self) -> str:
        return self._derniere('theme')
