#! /usr/bin/python

#
# MODULE source = 1/ Definition de la source des donnees, utilise pour recharge
#                 2/ Envoi d'un message d'alerte general avant toute execution de plugin, utilise pour suppression
#                    Du coup le nom source se comprend comme: "Le premier plugin appele !"
#

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

import json

from plugin import Plugin
from ouinon import ouiNon
from ouinon import entrerFiltrer
from erreur import *
from picconfig import getConf

# NOTE: Redondant cf. wpfin !
def checkNomobj(nomobj: str) -> None:
    '''Raise an exception if nomobj not valid'''
    values = [ 'plugin','theme','user']
    if not nomobj in values:
        raise Exception (f'ERREUR INTERNE - {nomobj} NOT in {values}')

class WpHomeDir(Plugin):

    @property
    def nomplugin(self)->str:
        return 'WpHomeDir'

    def __init__(self,action,nom,version,pic_gestion,options=[]):

        # Appel du constructeur du parent
        Plugin.__init__(self,action,nom,version,pic_gestion,options)

    def _calc_data(self, nomobj: str) -> None:
        '''Utilisé par liste plugins, liste themes et par maj plugins/themes - Renseigne le dernier élément de la structure de données data,
           qui donne l'état des versions des plugins de wordpress, indexé par utilisateur'''

        checkNomobj(nomobj)
        
        pg   = self.picgestion
        huser = pg.user
        www  = pg.www
        data_item = pg.adata[-1]
        options = self.options
        
        conteneur = getConf('DKRMAINT')
        out1 = [""]
        out2 = [""]

        # On appelle la commande wp, qui renvoie 1 si nous ne sommes pas dans une installation wordpress
        # Dans ce cas on se choppe une exception, on marque le data_item en erreur et on sort
        try:
            cmd = f"cd {www} && wp {nomobj} list --json 2>/dev/null"
            out1 = self._system(cmd,conteneur,False,huser)

            # TODO - code dupliqué depuis wpfin.py !
            #        en outre code pas beau
            if nomobj == 'user':
                out2 = [""]
            else:
                if '--major' in options:
                    minmaj = ''
                else:
                    minmaj = '--patch' if nomobj == 'plugin' else '--minor'
    
                cmd = f"cd {www} && wp {nomobj} update --dry-run {minmaj} --all --json 2>/dev/null"
                out2 = self._system(cmd,conteneur,False,huser)
                
            if len(out1) > 0 and len(out2) > 0:
                data_item['erreur'] = False
                data_item['huser']   = huser
                if out1[0] == "":
                    data_item['wpout1'] = ""
                else:
                    try:
                        data_item['wpout1'] = json.loads(out1[0])
                    except json.decoder.JSONDecodeError as e:
                        data_item['wpout1'] = ""
                        
                if out2[0] == "":
                    data_item['wpout2'] = ""
                else:
                    try:
                        data_item['wpout2'] = json.loads(out2[0])
                    except json.decoder.JSONDecodeError as e:
                        data_item['wpout2'] = ""

        # Une exception signifie que wp a renvoyé une erreur, ce n'est sans doute pas un site wp
        # On marque cet utilisateur en erreur
        except PicErreur as e:
            data_item['erreur']  = True

class WpHomeDirListe_plugins(WpHomeDir):
                
    def execute(self) -> None:
        self._calc_data('plugin')
        
class WpHomeDirListe_themes(WpHomeDir):
                
    def execute(self) -> None:
        self._calc_data('theme')

class WpHomeDirListe_users(WpHomeDir):
                
    def execute(self) -> None:
        self._calc_data('user')

class WpHomeDirMaj_plugins(WpHomeDirListe_plugins):
    pass

class WpHomeDirMaj_themes(WpHomeDirListe_themes):
    pass
    
class WpHomeDirListe_core(WpHomeDir):
    '''Utilisé par liste core et par maj core - Renseigne le dernier élément de la structure de données data,
       qui donne l'état des versions de wordpress, indexé par utilisateur'''
       
    def _execute(self, check_update: bool) -> None:
        pg   = self.picgestion
        huser = pg.user
        www  = pg.www
        mail = pg.mail
        nom  = pg.nomlong
        data_item = pg.adata[-1]
        #print(data)

        conteneur = getConf('DKRMAINT')
        out1 = ""
        out2 = ""
        
        # On appelle la commande wp, qui renvoie 1 si nous ne sommes pas dans une installation wordpress
        # Dans ce cas on se choppe une exception, on marque le data_item en erreur et on sort
        try:
            cmd  = f"cd {www} && wp core version 2>/dev/null"
            out1  = self._system(cmd,conteneur,False,huser)
            
            if len(out1) > 0:
                #print(huser + ' ' + out[0])
                data_item['erreur']  = False
                data_item['version'] = out1[0]
                data_item['huser'] = huser
                data_item['mail'] = mail
                data_item['nom'] = nom
                data_item['wpout'] = ""
                
            if check_update:
                cmd  = "cd " + www + " && wp core check-update --format=json 2>/dev/null"
                out2 = self._system(cmd,conteneur,False,huser)

                if len(out2) > 0:
                    if out2[0] == "":
                        data_item['wpout'] = ""
                    else:
                        try:
                            data_item['wpout'] = json.loads(out2[0])
                        except Exception as e:
                            print ("Oups - Pas du json : " + out2[0])
                            data_item['wpout'] = []
                            #raise e

        # Une exception signifie que wp a renvoyé une erreur, ce n'est sans doute pas un site wp
        # On marque cet utilisateur en erreur
        except PicErreur as e:
            data_item['erreur']  = True
            
    def execute(self) -> None:
        self._execute(True)
        
class WpHomeDirListe_mails(WpHomeDirListe_core):

    def execute(self) -> None:
        self._execute(False)

class WpHomeDirMaj_core(WpHomeDirListe_core):
    pass
