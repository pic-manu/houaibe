#! /usr/bin/python

MAJOR='4'
MINOR='1'
PATCH='5'

VERSION = f"{MAJOR}.{MINOR}.{PATCH}"
