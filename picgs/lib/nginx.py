#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# MODULE nginx =  The Nginx object, used to define nginx + fpm containers
#

version = '0.1'

import os
import re
import subprocess
from plugin import Plugin
from webserver import Webserver
from erreur import *
from ouinon import ouiNon
from ouinon import entrerFiltrer
from picconfig import getConf
from affiche import *

def isNginx(func):
    '''Décorateur pour les fonctions execute et demandeParams'''
       
    def mfunc(self):
        if self.isNginx():
            func(self)
            
    return mfunc

class Nginx(Webserver):
    
    @property
    def nomplugin(self)->str:
        return 'Nginx'
    
    def __init__(self,action,nom,version,pic_gestion,options={}):
        # Appel du constructeur du parent
        Plugin.__init__(self,action,nom,version,pic_gestion,options)
        self._prod_urls = set()
        self._prod_user = ""
        self._statinf = {}

    def isNginx(self, archi=None) -> bool:
        '''Renvoie True si archi est ARCHI_NGINX_FPM'''
        pg = self.picgestion
        if not archi:
            archi = pg.archi
        return archi == pg.ARCHI_NGINX_FPM
        
    def initialise(self) -> None:
        self._calcProd()
        self._calcStatinf()

    def _createPrependSpip(self) -> None:
        '''Crée une config php pour forcer l'exécution du dernier ecran de securite spip'''
        
        pg = self.picgestion
        containersdir = getConf('CONTAINERSDIR')
        conteneur = pg.conteneur
        php_prepend = f"{containersdir}/{conteneur}/php-suppl.d/auto_prepend.ini"
        with open(php_prepend,'w') as fh_p:
            fh_p.write("; Créé par houaibe pour spip\n")
            fh_p.write('auto_prepend_file = "/home/root/ecran_securite/ecran_securite.php"')
    
    def _createHtaccessSpip(self) -> None:
        '''Crée une config nginx pour héberger un site sous spip'''
        self.__createHtaccess('htaccess-spip.conf')

    def _createHtaccessWp(self) -> None:
        '''Crée une config nginx pour héberger un site sous wordpress'''
        self.__createHtaccess('htaccess-wp.conf')

    def _createHtaccessGalette(self) -> None:
        '''Crée une config nginx pour héberger un site sous galette'''
        self.__createHtaccess('htaccess-galette.conf')

    def _createHtaccessDolibarr(self) -> None:
        '''Crée une config nginx pour héberger un site sous dolibarr'''
        self.__createHtaccess('htaccess-dolibarr.conf')

    def __createHtaccess(self, conf) -> None:
        '''Crée une config nginx pour héberger un site sous wordpress
           conf = le nom du fichier de configuration à copier dans le répertoire htaccess
           Return None
        '''
        
        pg = self.picgestion
        containersdir = getConf('CONTAINERSDIR')
        user = pg.user
        conteneur = pg.conteneur
        maint = getConf('DKRMAINT')
        home = pg.home
        
        htaccess_dir_src = f"{containersdir}/{conteneur}/htaccess"
        htaccess_dir_dst = f"{home}/priv/htaccess"
        htaccess_conf = f"{htaccess_dir_src}/{conf}"
        
        # Création du répertoire priv/htaccess et copie du fichier qui va bien
        cmd = f"mkdir -p {htaccess_dir_dst}"
        self._system(cmd, maint)
        self._dkr_cpto(htaccess_conf, htaccess_dir_dst, maint)
        
        # Changement de proprio et de permissions de l'arborescence
        cmd = f"chown -R {user}: {htaccess_dir_dst} && chmod -R o= {htaccess_dir_dst}"
        self._system(cmd, maint)

    def _executeAjout(self) -> None:
        '''Appelé par NginxAjout et NginxDemenagement'''

        pg = self.picgestion
        home = pg.home
        log = f"{home}/log"
        user = pg.user
        uid2 = pg.uid2
        user2 = 'w' + user
        conteneur = pg.conteneur
        maint = getConf('DKRMAINT')
        domainname = getConf('DOMAINNAME')
        image = pg.image
        containersdir = getConf('CONTAINERSDIR')
        tmpl = f"{containersdir}/{image}.tgz"
        cont_dir = f"{containersdir}/{conteneur}"

        print(f"=== Nginx: création du répertoire {conteneur} ")
        cmd = f'mkdir {cont_dir} && cd {cont_dir} && tar xf {tmpl}'
        self._system(cmd)
        
        smtpaddr = getConf('SMTPADDR')
        mariadbaddr = getConf('MARIADBADDR')
        if smtpaddr != None or mariadbaddr != None:
            print(f"=== Nginx: création du fichier compose.override.yml")
            coverr = f"{cont_dir}/compose.override.yml"
            with open(coverr,'w') as fh_comp:            
                fh_comp.write("services:\n")
                fh_comp.write(2 * ' ' + "php:\n")
                fh_comp.write(4 * ' ' + "extra_hosts:\n")
                if smtpaddr != None:
                    fh_comp.write(6 * ' ' + f"- \"pic-smtp:{smtpaddr}\"\n")
                if mariadbaddr != None:
                   fh_comp.write(6 * ' ' + f"- \"pic-db:{mariadbaddr}\"\n")

        print("=== Génération du fichier user.txt ")
        cmd = f'cd {cont_dir} && echo {user} > user.txt'
        self._system(cmd)

        print("=== Création du fichier msmtprc")
        cmd = f'cd {cont_dir} && sed -e "s/USER/{user}/" -e "s/DOMAINNAME/{domainname}/" msmtprc.tmpl >msmtprc'
        self._system(cmd)
        
        print("=== Configuration de nginx: mise en place du template")
        cmd = f'cd {cont_dir}/templates && cp -a default.dyn.conf.template.dyn default.dyn.conf.template'
        self._system(cmd)
        
        # Permissions de cont_dir: 
        #    g+w pour que tous les membres du groupe docker puissent modifier les fichiers
        #    o=rX pour que le user de nginx et de php puisse lire les fichiers de conf
        print(f"=== Changement de permissions de {cont_dir} et de son contenu")
        cmd = f"cd {cont_dir}/.. && chmod -R g+w,o=rX {cont_dir}"
        self._system(cmd)

        print(f"=== changement de permissions de {log}")
        self._system(f"chown -R w{user}:{user} {log}", maint)

    def _executeSuppression(self, conteneur) -> None:
        '''Appelé par NginxSuppression et NginxDemenagement'''

        print (f"=== Sauvegarde et suppression du répertoire de {conteneur}")
        containersdir = f"{getConf('CONTAINERSDIR')}"
        cmd = f"cd {containersdir} && tar czf {conteneur}.tgz {conteneur}"
        self._system(cmd)
        
        cmd = f"cd {containersdir} && rm -rf {conteneur}"
        try:
            self._system(cmd)
        except:
            print(afficheJaune(f"ATTENTION - {cmd} n'a pas fonctionné"))
    
    @property
    def prod(self) -> tuple[set,str]:
        '''renvoie le nom des sites de prod, le user du site de prod '''
        return (self._prod_urls,self._prod_user)

    @property
    def statinf(self) -> dict:
        '''renvoie le dictionnaire des données sur les sites statique/dynamique, éventuellement {}'''
        return self._statinf

    def _calcStatinf(self) -> None:
        '''Initialise la variable self._statinf à partir des fichiers présents - ou pas
           Lance une exception si les fichiers ne sont pas cohérents'''
        pg = self.picgestion
        conteneur = pg.conteneur
        containersdir = getConf('CONTAINERSDIR')
        cont_dir = f"{containersdir}/{conteneur}"

        self._statinf['prod2stat'] = False
        self._statinf['prod2dyn'] = False
        self._statinf['staturl'] = []
        self._statinf['dynurl'] = []
        
        f_stat = f"{cont_dir}/stat.txt"
        f_dyn = f"{cont_dir}/dyn.txt"
        f_prod = f"{cont_dir}/prod.txt"

        b_stat = os.path.exists(f_stat)
        b_dyn = os.path.exists(f_dyn)
        b_prod = os.path.exists(f_prod)

        if (b_stat and not b_dyn) or (not b_stat and b_dyn):
            raise ("ERREUR - Incohérence dans les fichiers stat.txt et dyn.txt !")
            
        if os.path.realpath(f_stat) == os.path.realpath(f_dyn):
            raise ("ERREUR - stat.txt et dyn.txt pointent vers le même fichier !")
        
        if os.path.realpath(f_stat) == os.path.realpath(f_prod):
            self._statinf['prod2stat'] = True
            self._statinf['prod2dyn'] = False
            self._statinf['staturl'] = []
            self._statinf['dynurl'] = self._readUrls(f_dyn)
        elif os.path.realpath(f_dyn) == os.path.realpath(f_prod):
            self._statinf['prod2stat'] = False
            self._statinf['prod2dyn'] = True
            self._statinf['staturl'] = self._readUrls(f_stat)
            self._statinf['dynurl'] = []
        else:
            self._statinf['prod2stat'] = False
            self._statinf['prod2dyn'] = False
            self._statinf['staturl'] = self._readUrls(f_stat)
            self._statinf['dynurl'] = self._readUrls(f_dyn)
            
    def _calcProd(self) -> None:
        ''' recherche les infos sur la version de production et garde le resultat dans self: 1 user, plusieurs prods
            Exception si plusieurs users ou si le fichier a un format inattendu
            Cette fonction sert aussi dans le cas de mutualisation spip'''
        
        nom_court = self.nomcourt
        
        # Si nom_court pas initialise, on laisse tomber (commande picgs cert par exemple)
        if nom_court==None:
            return  

        # print (f"ICI Nginx._calcProd - {nom_court}")               

        # Recherche tous les répertoires de conteneurs ayant comme nom pic-monasso-stX
        # Recherche dans ces répertoires les fichiers prod.txt, ils contiennent les noms de prod
        # TODO - Utiliser _readUrls !
        prefix = getConf('CONTAINERSDIR') + '/'
        suffix = '/prod.txt'
        cmd = f"find {prefix}pic-{nom_court}* -maxdepth 2 -name prod.txt"
        try:
            prod_txt = self._system(cmd)
            prod_txt.remove('')
        except:
            return
            
        if len(prod_txt) == 0:
            return
                        
        # Un dictionnaire, keys = nom d'utilisateur, values = contenu de prod.txt (un array, une adresse par cellule)
        lignes = {}
        for p in prod_txt:
            with open(p,'r') as fh_p:
                file_lignes = fh_p.readlines()
            for l in file_lignes:
                if l.strip().startswith('#') or l.strip() == '':
                    continue
                u = p.removeprefix(prefix).removesuffix(suffix).removeprefix('pic-')
                prods = l.strip().split(',')
                lignes[u] = prods
                break
                
        if len(lignes)==0:
            return
        
        if len(lignes)>1:
            msg  = 'ERREUR - Plusieurs versions de prod de plusieurs users: ' + "\n" + "\n".join(lignes)
            msg += "\ncorrigez cela a la main et reessayez\n"
            raise PicErreur(msg)

        # L'utilisateur qui a la version de production
        self._prod_user = list(lignes.keys())[0]

        # Les sites de production
        self._prod_urls = set(list(lignes.values())[0])

        return

    def _readUrls(self, f_url:str) -> list:
        '''Lit un fichier texte: url1,url2 
           Les lignes commençant par # ou vides sont ignorées
           On ne lit que la première ligne non ignorée
           Utile pour lire les fichiers prod.txt, dyn.txt, stat.txt'''

        urls = []
        try:
            with open(f_url,'r') as fh_url:
                file_lignes = fh_url.readlines()
                for l in file_lignes:
                    if l.strip().startswith('#') or l.strip() == '':
                        continue
                    urls = l.strip().split(',')
                    break
        except FileNotFoundError as x:
            pass
        
        return urls

 
    def _urls2conf(self, prod_urls, conteneur, user) -> None:
        ''' Supprime et recrée le fichier prod.txt à partir de prod_urls
        '''
        dir_name = f"{getConf('CONTAINERSDIR')}/{conteneur}"
        prod_txt = f"{dir_name}/prod.txt"
        stat_txt = f"{dir_name}/stat.txt"
        dyn_txt = f"{dir_name}/dyn.txt"

        # Suppression des adresses de prod MAIS AUSSI du site statique s'il existe - cf. Debut::DebutProduction
        if len(prod_urls) == 0:
            for f in [ prod_txt, stat_txt, dyn_txt]:
                try:
                    os.unlink(f)
                except FileNotFoundError:
                    pass

        else:
            urls = ','.join(prod_urls)
            with open(prod_txt,'w') as fh_prod:
                fh_prod.write(urls)

    @property
    def isinactif(self) -> True:
        ''' Renvoie True si inactif '''

        pg = self.picgestion
        user = pg.user
        conteneur = pg.conteneur
        containersdir = getConf("CONTAINERSDIR")
        desac = f"{containersdir}/{conteneur}/DESACTIVE"
        return self._dkr_fexists(desac)

class NginxAjout(Nginx):

    #@ isNginx
    #def demandeParams_S(self):
    #    '''Essaie de passer en sudo, pour forcer l'utilisateur à entrer son mot de passe'''
    #    self._system('true', None, False, 'root') 

    @isNginx
    def execute(self) -> None:
        pg = self.picgestion
        conteneur = pg.conteneur
        self._executeAjout()

        if pg.cmswp or pg.cmswpmig:
            print ("=== Création du fichier htaccess pour wordpress")
            self._createHtaccessWp()
            
        if self.fonction == "galette" and pg.cmsinstall:
            print ("=== Création du fichier htaccess pour galette")
            self._createHtaccessGalette()

        if self.fonction == "dolibarr" and pg.cmsinstall:
            print ("=== Création du fichier htaccess pour dolibarr")
            self._createHtaccessDolibarr()

        print(f"=== Premier démarrage du conteneur {conteneur} ")
        self._restart(conteneur)
        
        return
        
class NginxSuppression(Nginx):

    @isNginx        
    def execute(self) -> None:
        # pg est l'objet PicGestion dans lequel nous habitons
        pg = self.picgestion
        conteneur = pg.conteneur
        
        self._executeSuppression(conteneur)

class NginxDemenagement(Nginx):
    
    def execute(self) -> None:
        
        pg = self.picgestion
        user = pg.user
        conteneur = pg.conteneur
        archi = pg.archi
        image = pg.image
        conteneur_old = pg.conteneur_old
        archi_old = pg.archi_old
        image_old = pg.image_old
        
        # Si on change simplement l'image 
        # print (f"koukou {archi} {archi_old}")
        if self.isNginx(archi) and self.isNginx(archi_old):
            print (f"=== Arrêt du conteneur {conteneur_old} - Image {image_old}")
            self._stop(conteneur_old)
            
            print (f"Edition du fichier houaibe.conf")
            containersdir = getConf('CONTAINERSDIR')
            cont_dir = f"{containersdir}/{conteneur}"
            (nimage,pimage) = image.split('-')
            nim = 'export NIMAGE=\${DKRCPTE}\${DKRCPTE:+/}' + nimage
            cmd = f"cd {cont_dir} && sed -i -e 's@.*NIMAGE=.*@{nim}@' houaibe.conf"
            self._system(cmd)

            pim = 'export PIMAGE=\${DKRCPTE}\${DKRCPTE:+/}' + pimage
            cmd = f"cd {cont_dir} && sed -i -e 's@.*PIMAGE=.*@{pim}@' houaibe.conf"
            self._system(cmd)

            print (f"=== Démarrage du conteneur {conteneur_old} - Image {image}")
            self._restart(conteneur_old)
            
        # Si on atterrit sur un Nginx
        elif self.isNginx(archi):
            (prod_urls, prod_user) = pg.prod
            self._executeAjout()
        
            # Sites sous spip = Ecran de sécurité + htaccess
            if pg.isSpip():
                print ("=== Activation de l'écran de sécurité mutualisé")
                self._createPrependSpip()
                print ("=== Création du fichier htaccess pour spip")
                self._createHtaccessSpip()
                
            # Sites sous wordpress = htaccess
            if pg.isWordpress():
                print ("=== Création du fichier htaccess pour wordpress")
                self._createHtaccessWp()

            # Sites sous galette = htaccess
            if pg.isGalette():
                print ("=== Création du fichier htaccess pour galette")
                self._createHtaccessGalette()

            # Sites sous dolibarr = htaccess
            if pg.isDolibarr():
                print ("=== Création du fichier htaccess pour dolibarr")
                self._createHtaccessDolibarr()
                
            # Si l'utilisateur en déménagement a la version de prod,
            # on la récupère dans le nouveau conteneur
            if prod_user == user:
                print("=== Récupération des sites de production")
                self._urls2conf(prod_urls, conteneur, user)
        
            self._restart(conteneur)

        # Si on part d'un Nginx on arrête puis on détruit le conteneur
        elif self.isNginx(archi_old):
            self._stop(conteneur_old)
            self._executeSuppression(conteneur_old)

        return

class NginxDesactiver(Nginx):
    
    @isNginx        
    def execute(self) -> None:
        pg = self.picgestion
        user = pg.user
        conteneur = pg.conteneur
        containersdir = getConf("CONTAINERSDIR")

        print(f"=== Création du fichier DESACTIVE dans le répertoire {conteneur} ===")
        desac = f"{containersdir}/{conteneur}/DESACTIVE"
        if self._dkr_fexists(desac):
            raise PicFatalErreur(f"{user} est DEJA inactif")
        
        open (desac, 'w')
        
        self._stop(conteneur)

class NginxReactiver(Nginx):
    
    @isNginx        
    def execute(self) -> None:
        pg = self.picgestion
        user = pg.user
        conteneur = pg.conteneur
        containersdir = getConf("CONTAINERSDIR")

        print(f"=== Suppression du fichier DESACTIVE dans le répertoire {conteneur} ===")
        desac = f"{containersdir}/{conteneur}/DESACTIVE"
        if not self._dkr_fexists(desac):
            raise PicFatalErreur(f"{user} est DEJA activé")

        os.unlink(desac)

        self._restart(conteneur)

class NginxInactifs(Nginx):
    
    #@isNginx        
    def execute(self) -> None:
        pg = self.picgestion
        tmp = []
        containersdir = getConf('CONTAINERSDIR')
        
        # On cherche les fichier DESACTIVE dans les répertoires pic-xxx-stN
        cmd = f"find {containersdir} -maxdepth 2 -name DESACTIVE"
        inactifs = [ s for s in self._system(cmd) if s != '' ]
        
        print ("Coté nginx:")
        if len(inactifs) > 0:
            for d in inactifs:
                s = d.removeprefix(f"{containersdir}/pic-").removesuffix(f"/DESACTIVE")
                print(s)
        else:
            print ("Pas de sites inactifs côté nginx")

class NginxProduction(Nginx):

    @isNginx
    def execute(self) -> None:
        self._executeProduction()

class NginxWpclone(Nginx):
    '''Important pour que la fonction initialise soit appelée '''
    pass

class NginxSpipinstall(Nginx):
    
    @isNginx
    def execute(self) -> None:
        pg = self.picgestion
        conteneur = pg.conteneur
        print ("=== Activation de l'écran de sécurité mutualisé")
        self._createPrependSpip()
        print ("=== Création du fichier htaccess pour spip")
        self._createHtaccessSpip()
        self._restart(conteneur)

class NginxWpinstall(Nginx):
    
    @isNginx
    def execute(self) -> None:
        pg = self.picgestion
        conteneur = pg.conteneur
        print ("=== Création du fichier htaccess pour wordpress")
        self._createHtaccessWp()

        self._restart(conteneur)

class NginxInfo(Nginx):
    
    @isNginx
    def execute(self) -> None:
        pg = self.picgestion
        conteneur = pg.conteneur
        containersdir = getConf('CONTAINERSDIR')
        cont_dir = f"{containersdir}/{conteneur}"
        user = pg.user
        domaine= getConf('DOMAINNAME')

        url_stat = self._readUrls(f'{cont_dir}/stat.txt')
        if len(url_stat) > 0:
            print()
            print ("Un site statique est adossé à ce site dynamique !")
            print (f"urls statiques  : {str(url_stat)}")
            url_dyn = self._readUrls(f'{cont_dir}/dyn.txt')
            print (f"urls dynamiques : {user}.{domaine} + {str(url_dyn)}")
            
class NginxStatique(Nginx):
 
    @isNginx
    def demandeParams(self) -> None:
        pg = self.picgestion
        user = pg.user
        domaine = getConf('DOMAINNAME')

        prod = self.prod
        prod_urls = prod[0]
        prod_usr = prod[1]
        self._statinf['prod2stat'] = False
        self._statinf['prod2dyn'] = False
        self._statinf['staturl'] = []
        self._statinf['dynurl'] = []

        # S'il y a des adresses de prod elles doivent pointer sur le site dynamique OU le site statique
        if prod_usr == user:
            print (f"Urls de production: {str(prod_urls)}")
            if ouiNon (f"Doivent-elles renvoyer sur le site statique ?", False):
                self._statinf['prod2stat'] = True
                self._statinf['prod2dyn'] = False
            else:
                self._statinf['prod2stat'] = False
                self._statinf['prod2dyn'] = True

        # Si les urls de prod pointent pas sur le site statique, il faut en trouver une
        if self._statinf['prod2stat'] == False:
            while len(self._statinf['staturl']) == 0:
                tmp = entrerFiltrer (f"Entrez une url pour le site statique ", autorise='abcdefghijklmnopqrstuvwxyz0123456789.-_')
                if len(tmp) > 0:
                    self._statinf['staturl'].append(tmp)
        # Si les url de prod ne pointent pas sur le site dynamique, on peut en proposer une autre !
        if self._statinf['prod2dyn'] == False:
            tmp = entrerFiltrer (f"Entrez une url pour le site dynamique (facultatif, l'url {user}.{domaine} est utilisable) ", autorise='abcdefghijklmnopqrstuvwxyz0123456789.-_')
            if len(tmp) > 0:
                self._statinf['dynurl'].append(tmp)
    @isNginx
    def execute(self) -> None:
        pg = self.picgestion
        conteneur = pg.conteneur
        containersdir = getConf('CONTAINERSDIR')
        cont_dir = f"{containersdir}/{conteneur}"
        
        print("=== Configuration de nginx: mise en place du template pour sites statiques")
        cmd = f'cd {cont_dir}/templates && cp -a --backup default.stat.conf.template.stat default.stat.conf.template'
        self._system(cmd)

        print("=== Configuration de nginx: urls pour les sites statique et dynamique")
        if self._statinf['prod2stat']:
            cmd = f"cd {cont_dir} && rm -f stat.txt && ln -s prod.txt stat.txt"
        else:
            cmd = f"cd {cont_dir} && rm -f stat.txt && echo {','.join(self._statinf['staturl'])} > stat.txt"
        self._system(cmd)

        if self._statinf['prod2dyn']:
            cmd = f"cd {cont_dir} && rm -f dyn.txt && ln -s prod.txt dyn.txt"
        else:
            cmd = f"cd {cont_dir} && rm -f dyn.txt && echo {','.join(self._statinf['dynurl'])} > dyn.txt"
        self._system(cmd)
            
        # Redémarrage du conteneur
        self._restart(conteneur)

class NginxNostatique(Nginx):

    @isNginx
    def execute(self) -> None:
        pg = self.picgestion
        conteneur = pg.conteneur
        containersdir = getConf('CONTAINERSDIR')
        cont_dir = f"{containersdir}/{conteneur}"
        
        print("=== Configuration de nginx: suppression du template pour site statique")
        cmd = f'cd {cont_dir}/templates && rm -f default.stat.conf.template'
        self._system(cmd)

        print("=== Configuration de nginx: suppression des fichiers dyn.txt et stat.txt")
        cmd = f'cd {cont_dir} && rm -f dyn.txt stat.txt'
        self._system(cmd)

        # Redémarrage du conteneur
        self._restart(conteneur)
