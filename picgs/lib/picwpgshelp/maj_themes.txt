# 
# Message d'aide pour l'action maj themes
#         Les lignes débutant à la colonne 0 seront écrites en gras
#

picwpgs maj themes [--oui] [--major] [--filtre=monsite-st1,tonsite-st2] [--nom=toto] [--dry-run] [--theme=thm_toto] [--exclure=thm_toto1,thm_toto2]
        Met à jour s'il y a lieu (mises à jour PATCHES) tous les plugins des wordpress ayant un compte picadmin
        --oui:   Ne demande pas confirmation (pour utilisation dans un script)
        --major: Effectue les mises à jour MAJEURES, ce qui est plus dangereux !
        --filtre: Limite l'action aux sites spécifiés
        --nom: Limite l'action aux noms commençant par la chaine spécifiée
        --dry-run: ne fait rien, affiche simplement ce qu'il faut faire
        --theme: Se limite au thème spécifié
        --exclure: Au contraire, traite tous les thèmes sauf le ou les thèmes spécifiés

VARIABLES D'ENVIRONNEMENT:
    PICGS_DEBUG=1   Affichage des commandes externes et de la pile d'appel en cas d'erreur
                    Affichage du numéro d'itération dans la boucle principale
    PICGS_LOOPALGO_MIN=x Numéro d'itération minimum
    PICGS_LOOPALGO_MAX=y Numéro d'itération maximum
