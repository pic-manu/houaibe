# 
# Message d'aide pour l'action liste plugins
#         Les lignes débutant à la colonne 0 seront écrites en gras
#
picwpgs liste plugins [--nom=toto] [--ref=fichier.txt] [--only-diff] [--update-ref] [--filtre=monsite-st1,tonsite-st2]
        Liste les versions de plugins wordpress installés, pour chaque plugin la liste des sites concernés est affichée
        --ref Donne le chemin d'un fichier permettant de lire et écrire l'état des plugins
              L'état actuel est comparé avec ce fichier afin d'afficher les plugins mis à jour, installés, retirés...
        --update-ref Met à jour la référence si nécessaire
        --only-diff Affiche uniquement les plugins ajoutés/retirés par rapport au fichier de référence
        --nom Filtre sur le nom
        --filtre: Limite l'action aux sites spécifiés

