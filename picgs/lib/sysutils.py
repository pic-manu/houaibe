#
# MODULE sysutls =  _system et autre fonctions systemes bien utiles
#

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

import os
import pwd
import time
import subprocess
import tempfile
import traceback
import re
import requests
from bs4 import BeautifulSoup
from urllib.parse import urlparse

from erreur import *
from affiche import *
from picconfig import getConf

version = '0.1'

class SysUtils(object):
    
    __dkrstarted = []
    
    def __discoverUid(self, user, conteneur) -> str:
        '''Appelle la commande id dans le conteneur pour connaître uid à partir de user
        Renvoie le uid, si pas de user lance une exception'''
        cmd = "id -u {user}".format(user=user)
        return self._system(cmd,conteneur)[0]

    def _restart(self,conteneur) -> None:
        '''(Re)demarre le conteneur passé en parametres'''

        print("=== Redémarrage du conteneur " + conteneur + " ===")
        cmd = f"{getConf('CONTAINERSDIR')}/{conteneur}/run.sh"
        self._system(cmd)

    def _stop(self,conteneur) -> None:
        '''Arrête et supprime le conteneur passe en parametres'''

        print("=== Arrêt du conteneur " + conteneur + " ===")
        cmd = f"{getConf('CONTAINERSDIR')}/{conteneur}/stop.sh"
        self._systemWithWarning(cmd)

    def __restartIfNeeded(self,conteneur) -> None:
        '''Si conteneur est dans DKRTOSTART vérifie qu'il tourne, et le démarre au besoin'''
        
        # Si le conteneur est dans __dkrstarted, on est déjà passé par là: pas la peine de recommencer !
        # Note - Cela suppose que le conteneur ne 'est pas arrêté entre temps
        #        pic-maint dure au moins 15 minutes, ce qui est largement plus long qu'une utilisation de picgs
        #        (à moins que l'utilisateur ne s'endorme quand on lui pose une question)
        if conteneur in getConf("DKRTOSTART") and not conteneur in self.__dkrstarted:
            
            # Crée le fichier /sleeping pour qu'il ne s'arrête pas tout de suite
            # (cf. le fichier entrypoint.sh dans images/maint)
            # ATTENTION - Risque de récursion infinie
            cmd = f"docker exec {conteneur} touch /sleeping"
            try:
                self._system(cmd)
                ok = True
            except:
                ok = False
            
            if not ok:
                cmd1 = f"{getConf('CONTAINERSDIR')}/{conteneur}/run.sh"
                self._system(cmd1)
                time.sleep(5)

            # Marquer le conteneur comme démarré par picgs, on ne vérifiera pas à nouveau
            self.__dkrstarted.append(conteneur)

    def _system(self, cmd, conteneur=None, interactif=False, user=None, env=[], input=None, shellpgm='bash') -> list:
        '''S'il y a lieu utiliser docker exec, eventuellement en interactif 
        Appelle subprocess.Popen, leve une exception si erreur, sinon renvoie le stdout dans un tableau
        Si user est specifie, utilise sudo pour conteneur=None,ou docker -u (donc pas besoin d'avoir sudo dans le conteneur)
        Si env est spécifié (un tableau de noms de variables), il contient la liste de toutes les variables d'environnement à passer
           - Ignoré si conteneur = None
           - Une exception est remontée si on passe une variable qui n'existe pas
        Si input est spécifié cette str sera envoyée dans la stdin de la commande
        Si shellpgm est spécifié on appelle ce shell à la place de bash
        Si le conteneur est dans le tableau DKRTOSTART on vérifie que le conteneur est up et au besoin on le démarre'''
        
        # Echapper le " car on va appeler la commande à travers bash -c
        cmd = cmd.replace('"','\\"')
            
        stdout  = subprocess.PIPE
        tmp_cmd = cmd
        if conteneur != None:
            it = ''
            u = ''
            ev = ' '
            b = f'{shellpgm} -c "{cmd}"'

            self.__restartIfNeeded(conteneur)

            if interactif:
                it = '-it'
                stdout = None
            elif input != None:
                it = '-i'

            if user != None:
                u = "-u {uid}:{uid}".format(uid=self.__discoverUid(user,conteneur))
                
            for v in env:
                ev += f'--env={v} '

            cmd = f"docker exec {it} {u} {ev} {conteneur} {b}"

        else:
            # Si conteneur==None, le paramètre interactif est ignoré
            if interactif:
                stdout = None
                
            u = ''
            b  = f'{shellpgm} -c "{cmd}"'
            if user != None:
                # Pour éviter de faire sudo -u avec le user courant !
                pwd_info = pwd.getpwuid(os.getuid())
                if user != pwd_info.pw_name:
                    u = 'sudo -u {user}'.format(user=user)
            cmd = '{u} {b}'.format(u=u,b=b)

        # input et stdin
        if input != None:
            stdin = subprocess.PIPE
        else:
            stdin = None
        
        # Variable d'environnement "PICGS_DEBUG=1" pour afficher la commande
        if 'PICGS_DEBUG' in os.environ:
            print ("Execution de " + cmd)
            if input != None:
                print (f"input = {input}")

        p=subprocess.Popen(cmd,shell=True,stdout=stdout,stderr=subprocess.STDOUT,stdin=stdin)
        if input != None:
            p.communicate(input=input.encode('utf8'))
        p.wait()
        if 'PICGS_DEBUG' in os.environ:
            print (f"CODE DE RETOUR {p.returncode}")
            if p.returncode !=0:
                traceback.print_stack()
        if p.returncode !=0:
            msg =  "ERREUR - commande= " + cmd + "\n"
            msg += "valeur de retour = " + str(p.returncode) + "\n"
            if stdout == subprocess.PIPE:
                msg += "stdout ou stderr = " + p.communicate()[0].decode('utf8')
            raise PicErreur(msg,p.returncode)
        if stdout == subprocess.PIPE:
            return p.communicate()[0].decode('utf8').split('\n')

    def _systemWithWarning(self,cmd,conteneur=None,interactif=False,user=None, env=[]) -> list | None:
        '''Encadre _system d'un try/except permettant d'écrire la commande en Warning
           Si une exception est générée, on affiche le warning et on renvoie None'''

        try:
            rvl = self._system(cmd, conteneur, interactif, user, env)
            return rvl

        except PicErreur as e:
            print (afficheJaune(f"OUPS - Erreur avec la commande {cmd} - {e.message}"))
        return None
        
    def _dkr_cpto(self,src,dst,conteneur=None) -> list:
        '''Copie un fichier dans un conteneur (ou pas)'''
        if conteneur == None:
            if src == dst:
                return ''
            else:
                cmd = "cp " + src + " " + dst
        else:
            self.__restartIfNeeded(conteneur)
            cmd = "docker cp " + src + " " + conteneur + ':' + dst
        return self._system(cmd)

    def _dkr_cpfrom(self,src,dst,conteneur=None) -> list:
        '''Copie un fichier depuis un conteneur (ou pas)'''
        if conteneur == None:
            if src == dst:
                return ''
            else:
                cmd = "cp " + src + " " + dst
        else:
            self.__restartIfNeeded(conteneur)
            cmd = "docker cp " + conteneur + ':' + src + ' ' + dst
        return self._system(cmd)
        
    def _dkr_fexists(self,path,conteneur=None) -> bool:
        '''Teste l'existence d'un fichier ou repertoire et renvoie true/false'''
        if path==None or path=="":
            return False
            
        if conteneur==None:
            return os.path.exists(path)
        else:
            cmd = "[ -r " + path +" ] && echo 1 || echo 0";
            rst = self._system(cmd,conteneur)[0];
            if rst=="1":
                return True
            else:
                return False

    def _dkr_f_lien(self,path,conteneur=None) -> bool:
        '''Teste si path est un lien. Si oui renvoie True, si pas un lien ou si n'existe pas renvoie False'''
        if conteneur==None:
            return os.path.islink(path)
        else:
            cmd = "[ -L " + path +" ] && echo 1 || echo 0";
            rst = self._system(cmd,conteneur)[0];
            if rst=="1":
                return True
            else:
                return False

    def _dkr_f_dir(self,path,conteneur=None) -> bool:
        '''Teste si path est un dir. Si oui renvoie True, si pas un lien ou si n'existe pas renvoie False'''
        if conteneur==None:
            return os.path.isdir(path)
        else:
            cmd = "[ -d " + path +" ] && echo 1 || echo 0";
            rst = self._system(cmd,conteneur)[0];
            if rst=="1":
                return True
            else:
                return False

    def _dkr_rename(self,src,dst,conteneur=None) -> list:
        '''Renomme un fichier dans le conteneur (ou pas), lance une exception si echec'''
        cmd = "mv " + src + " " + dst
        self._system(cmd,conteneur)[0];

    def _sql(self,req,db,conteneur=None) -> list:
        '''Execute une requete sql et renvoie le resultat
           host,user et pwd ne sont pas necessaires car on utilise le fichier ~/.my.cnf
           Si exception durant la requete l'exception est propagee '''

        if 'PICGS_SQLDEBUG' in os.environ:
            print ("Requête SQL: {}".format(req))
            
        [sql,sql_name] = tempfile.mkstemp()
        fsql = os.fdopen(sql,'w')
        fsql.write(req)
        fsql.close()

        if conteneur != None:
            self._dkr_cpto(sql_name,sql_name,conteneur)
            
        cmd     = "cat " + sql_name + "|mysql " + db
        err_flg = False
        err     = None
        rvl     = ''
        try:
            rvl = self._system(cmd,conteneur)
        except PicErreur as e:
            err_flg = True
            err     = e

        # Pour eviter de supprimer deux fois le meme fichier        
        if conteneur != None:
            cmd = "rm " + sql_name
            # commenter pour debug !
            self._system(cmd,conteneur)
        
        os.unlink(sql_name)

        if err_flg:
            raise err       

        return rvl

    def _sed(self, sed_cmd, fichier, conteneur=None, user=None) -> list|None:
        '''Appelle sed pour lui faire executer le programme cmd_sed sur fichier.
           Le programme sed est passé par la variable d'environnement SED_CMD, ce qui permet d'éviter les problèmes
           de caractères à échapper, genre /, \, ! etc.
        '''
        
        if sed_cmd == '':
            raise PicErreur("Erreur sed = Pas de sed_cmd !")
            
        if sed_cmd == '':
            raise PicErreur("Erreur sed = Pas de nom de fichier !")

        os.environ['SED_CMD'] = sed_cmd
        return self._systemWithWarning(f"sed -i -f <(echo \$SED_CMD) {fichier}", conteneur, False, user, [ 'SED_CMD' ])
                    
    def _http_suppr(self,url: str) -> str:
        '''remplace une url http(s)://exemple.com/ par exemple.com'''
        if url.startswith('http://'):
            url=url.replace('http://','',1)
        if url.startswith('https://'):
            url=url.replace('https://','',1)
        return url.strip('/')

    def _https_add(self, url: str) -> str:
        '''Si la chaine commence par http: ou https:, ne fait rien. Sinon, ajoute https://'''
        if url.startswith('https:') or url.startswith('http:'):
            return url
        else:
            return 'https://' + url
            
    def _detecteCanonical(self, user:str="", url:str="") -> str:
        '''Fait un wget sur l'url technique ou sur url si spécifiée et détecte l'url canonique
           Retourne "" si pas d'url canonique, l'url canonique sinon
        '''
        if len(url) == 0 and len(user) == 0:
            return ""
            
        if len(url) == 0:
            url = self._https_add(user + '.' + getConf('DOMAINNAME'))
        else:
            url = self._https_add(url)
        
        try:
            resp = requests.get(url)
            
        except requests.exceptions.RequestException:
            #print (afficheJaune(f"L'url {url} n'est PAS JOIGNABLE."))
            return ''
            
        soup = BeautifulSoup(resp.content, 'html.parser')
        link = soup.find('link',rel='canonical')
        if link == None:
            return ''
        else:
            return link['href'].rstrip('/')
