#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# MODULE picconfig = Definition de la fonction getConf()

version = '0.1'

import os
import re
import sys
from erreur import *


def __LONG_MAX_NOM() -> int:
    ''' We define a constant, giving the max length of the user name part: if the user is monasso-st1, the name part is monasso'''
    return 32    

# Maintenant que picgs est intégré à Houaibe, les paramètres sont dans des variables d'environnement
houaibedir = os.getenv('HOUAIBEDIR')
if houaibedir == None:
    houaibedir = '/usr/local/houaibe'

# Les variables d'environnement MUTUNOM et MUTUVER sont optionnelles
if 'MUTUNOM' in os.environ:
    mutunom = os.getenv('MUTUNOM')
else:
    mutunom = 'mutu'
    
if 'MUTUVER' in os.environ:
    mutuver = os.getenv('MUTUVER')
else:
    mutuver = '1'

# DOCKER_RUNNING is True if we live in a container
if not os.getenv('DOCKER_RUNNING'):
    CONFIG  = {
    
        # PARAMETRES A AJUSTER A VOTRE GUISE
        
        # LDAP: URI ET MOT DE PASSE
        'LDAPURI'        : os.getenv('LDAPURI'),
        'LDAPPASSWDFILE' : os.getenv('LDAPPASSWDFILE'),
    
        # LE SERVEUR MYSQL
        'HSTMYSQL'       : os.getenv('HSTMYSQL'),
        'CLIMYSQL'       : os.getenv('CLIMYSQL'),
    
        # LES CONTENEURS POUR L'HERGEMENT
        'DKRSRVWEB'      : os.getenv('DKRSRVWEB'),
        'DKRSRVIMG'      : os.getenv('DKRSRVIMG'),
        'DKRCPTE'        : os.getenv('DKRCPTE'), 
        
        # ==================================================================
        # PARAMETERS EXPORTES DEPUIS /usr/local/houaibe/etc/houaibe.conf ... ou fixés ci-dessus
        # NOM DE DOMAINE PRINCIPAL
        'DOMAINNAME'  : os.getenv('DOMAINNAME'),
        
        # COMPTE ADMIN
        'ADMIN'    : os.getenv('ADMIN'),
    
        # ADRESSE ASSOCIEE AU COMPTE ADMIN
        'ADMINMAIL': os.getenv('ADMINMAIL'),
    
        # LE SERVEUR MARIADB
        'MARIADBADDR' : os.getenv('MARIADBADDR'),

        # LE SERVEUR LDAP
        'LDAPADMIN'   : f"cn=admin,{os.getenv('BASENAME')}",
        'LDAPBASEUSR' : f"ou=Users,{os.getenv('BASENAME')}",
        'LDAPBASEGRP' : f"ou=Groups,{os.getenv('BASENAME')}",

        # LE SERVEUR SMTP
        'SMTPADDR'    : os.getenv('SMTPADDR'),
        
        # PHPMYADMIN
        'PHPMYADMIN'  : f"https://phpmyadmin.{os.getenv('DOMAINNAME')}",
        
        # CONTENEUR PAHEKO
        'PAHEKONAME' : os.getenv('PAHEKONAME'),
    
        # CONTENEUR UTILISE SEULEMENT PAR PICGS (MAINT=MAINTENANCE)
        'DKRMAINT'    : 'pic-maint',
    
        # Les fichiers run.sh des conteneurs se trouvent dans les sous-répertoires de CONTAINERSDIR
        'CONTAINERSDIR' : os.getenv('CONTAINERSDIR'),
    
        # LES FONCTIONS ASSOCIEES AUX NOMS COURTS
        'FONCTIONS'   : ['site','galette','dolibarr','autre'],
        'CODEFCT'     : ['st','gt','db','au'],
    
        # LA CLE ABUSEIPDB (SI ON BLOQUE DES IP)
        'ABUSKEYFILE' : os.getenv('ABUSKEYFILE'),
        
        #  LA MUTU SPIP - ici le user est mutu-st1 et le préfixe des noms de B.D.: mu_
        'MUTUNOM'     : mutunom,
        'MUTUVER'     : mutuver,
        'MUTUINSTPRFX': 'mu_',
    
        # RIEN A CHANGER A PARTIR DE CE POINT
        # ===================================
        
        # LE PROXY
        # 'DKRPROXY'    : os.getenv('PROXYNAME'),
        'PROXYNAME'   : os.getenv('PROXYNAME'),
    
        # L'ACCES SSH
        # 'DKRSSH'      : os.getenv('SSHNAME'),
        'SSHNAME'     : os.getenv('SSHNAME'),
        
        # LES CONTENEURS QUI DOIVENT ETRE DEMARRES A LA DEMANDE
        # SEULEMENT pic-maint POUR L'INSTANT
        'DKRTOSTART'  : [ 'pic-maint' ],
    
        # LE SERVEUR SFTP
        'SFTP'        : f"sftp.{os.getenv('DOMAINNAME')}",
        'SFTPPORT'    : 2200,
    
        # LES TRANCHES DE UID
        'FIRSTUID'    :10000,
        'FIRSTUID2'   :20000,
        'LASTUID'     :10999,
        'LONG_MAX_NOM': __LONG_MAX_NOM(), 
    
        'SFTPONLY'    : 'sftponly',
        'SFTPONLYGID' : 500,
        'HOMES'       : '/home/',
        'APACHE_AVAIL': '/etc/apache2/sites-available/',
        'APACHE_ENABL': '/etc/apache2/sites-enabled/',
        'APACHE_TEMPL': '/etc/apache2/sites-available/pic/template',
        'APACHE_CEXT' : '.conf',
        'APACHE_LOG'  : '/var/log/apache2/',
    
        # Pour les archives
        'ARCHIVES'    : '/home/archives/',
        'PICUSERS'    : '.picusers',
    
        # Pour les certificats avec le proxy nginx
        'CERTDIR'       : f"{os.getenv('DEHYDRATEDDIR')}/certs",
        'CERTPROGDIR'   : os.getenv('DEHYDRATEDDIR'),
        'PROXYCERT'     : f"{os.getenv('CONTAINERSDIR')}/pic-proxy/certs",
        'PROXYCONFFILE' : f"{os.getenv('CONTAINERSDIR')}/pic-proxy/conf.d/default.conf",
    
        # Pour le blocage des IP dangereuses (version iptables/ipset)
        'IPSETNAME'     : 'picbloc4',
        'IP6SETNAME'    : 'picbloc6',
        'IPSETDB'       : f"{houaibedir}/etc/iptables/picbloc.sqlite",
        'ABUSAPIURL'    : 'https://api.abuseipdb.com/api/v2/blacklist',
        'ABUSCONFID'    : '90',
        'IPSETBLACKDIR' : f"{houaibedir}/etc/iptables",
    }

# If we live in a container, only a very few stuff specified
else:
    CONFIG = {
        # PARAMETERS EXPORTES DEPUIS /usr/local/houaibe/etc/houaibe.conf
        # NOM DE DOMAINE PRINCIPAL
        'DOMAINNAME'     : os.getenv('DOMAINNAME'),
        
        # COMPTE ADMIN
        'ADMIN'    : os.getenv('ADMIN'),
    
        # ADRESSE ASSOCIEE AU COMPTE ADMIN
        'ADMINMAIL': os.getenv('ADMINMAIL'),
        
        # PHPMYADMIN
        'PHPMYADMIN'  : f"https://phpmyadmin.{os.getenv('DOMAINNAME')}",
    
        # CONTENEUR UTILISE SEULEMENT PAR PICGS (MAINT=MAINTENANCE)
        'DKRMAINT'    : None,
    
        # LES FONCTIONS ASSOCIEES AUX NOMS COURTS
        'FONCTIONS'   : ['site','galette','dolibarr','autre'],
        'CODEFCT'     : ['st','gt','db','au'],
        'LONG_MAX_NOM': __LONG_MAX_NOM(), 
    }
    
# Ancien format (python)
try:
    if 'DKRSRVWEB' in CONFIG:
        CONFIG['DKRSRVWEB'] = eval(CONFIG['DKRSRVWEB'])

# Nouveau format (bash)
except SyntaxError:
    CONFIG['DKRSRVWEB'] = re.split(' +',CONFIG['DKRSRVWEB'])

# Chaine vide
except TypeError:
    CONFIG['DKRSRVWEB'] = []

# Nouveau format seulement
try:
    CONFIG['DKRSRVIMG'] = re.split(' +',CONFIG['DKRSRVIMG'])

# Chaine vide
except TypeError:
    CONFIG['DKRSRVIMG'] = []

def getConf(prm):
    '''renvoie la valeur du parametre de config passe en parametres, ou None'''
    try:
        return CONFIG[prm]

    except:
        return None
        
# TEST
if __name__ == '__main__':
    print(getConf('FIRSTUID'))
    print(getConf('NAWAK'))

