#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# MODULE ouinon = Fonction ouinon
#

import os
import re
from erreur import *

version = '0.1'


def ouiNon(msg,defaut: bool) -> bool:
    '''Appelle raw_input jusqu'a ce que l'utilisateur ait repondu y,Y,o,O,1 (OUI) ou n,N,0 (NON)
     Renvoie True/False'''
    rep = ''
    if defaut:
        rep = '(O/n)'
    else:
        rep = '(o/N)'

    while True:
        tmp_in = input(msg +' ' + rep)
        if tmp_in == '':
            return defaut
        if tmp_in in ['y','Y','1','o','O']:
            return True
        if tmp_in in ['n','N','0']:
            return False
            
def entrerFiltrer(msg,autorise='abcdefghijklmnopqrstuvwxyz0123456789\'-_()[] +=@.') -> str:
    '''Appelle raw_input et:
       1/ Vire les espaces au debut et a la fin
       2/ Verifie les caracteres entres
       3/ Renvoie ce qui est entre'''

    car_autorises = list(autorise);
    while True:
        tmp_in = input(msg).strip()
        
        car_interdit = False
        for c in list(tmp_in):
            if not c.lower() in car_autorises:
                print("ATTENTION: " + c + " est un caractere interdit ! limitez-vous a " + autorise)
                car_interdit = True
                break

        if car_interdit:
            continue
        else:
            break
        
    return tmp_in
