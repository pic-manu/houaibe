# houaibe
**h**ébergement de sites **ouaib** utilisant des conteneurs dock**e**r

houaibe a été conçu à partir de l'expérience d'hébergement de sites web et autres applications web du chatons toulousain Projet Internet et Citoyenneté: https://le-pic.org Le PIC utilise docker depuis 2017 pour son hébergement de production.

## Pour qui ?
`houaibe` s'adresse aux administrateurs système qui connaissent gnu/linux, et spécifiquement mariadb ou mysql, ldap, docker.

## Pour quoi ?

Pour les aider à mettre en place une infrastructure d'hébergement de sites web, sans (trop) galérer...

### Principales caractéristiques:

`houaibe` propose une infrastructure d'hébergement composée de plusieurs conteneurs docker:

- Un reverse proxy utilisant nginx
- Un ou plusieurs conteneurs contenant diverses versions d'apache ou php afin d'héberger plusieurs types de CMS
- Un conteneur permettant de donner aux utilisateurs (administrateurs de sites web) un accès sftp ou ssh à leurs fichiers
- Vous pouvez ajouter les conteneurs de votre choix 

`houaibe` inclut également:

- Le logiciel `dehydrated` pour la gestion des certificats, avec des 'hooks'  pour la communication avec le reverse proxy.
- Un programme d'administration (`picgs`) permettant de gérer l'infrastructure au quotidien
- Plusieurs scripts d'administration normalement appelés par cron et destinés à diverses opérations de sécurisation.

#### Sécurité:

L'ensemble est conçu pour permettre de mettre en place un hébergement sécurisé:

- Une configuration "durcie" d'Apache est proposée

- Grâce au module `mpm-itk`, chaque site web tourne sous son propre utilisateur. Les fichiers des différents sites se trouvent dans le même volume Docker, mais avec des propriétaires différents.

- La base des utilisateurs est maintenue dans un serveur ldap, elle est totalement séparée des utilisateurs du "host"

- Les utilisateurs accèdent à leurs fichiers via un conteneur dédié, ils n'ont pas accès à des commandes administratives comme su ou sudo

- Les tâches administratives "de routine" peuvent être réalisées par des personnes n'ayant pas de droits sudo (pas besoin d'être root)

- La gestion des certificats letsencrypt est intégrée à l'environnement par l'intermédiaire du logiciel `dehydrated`

#### Schéma de l'installation:

  Ci-dessous un schéma simplifié de l'installation: nous utilisons deux réseaux Docker, picnet et picnetssh. Dans le premier se trouvent les conteneurs utilisés pour faire tourner apache/php ou le proxy inverse, tandis que le second est dédié au conteneur ssh. Les conteneurs `pic-itkxxx` et `pic-ssh` partagent un volume appelé `www-data`.

![schema.png](schema.png)

#### Conteneurs et images:

Les serveurs `apache` avec le module `mpm-itk` se trouvent dans les conteneurs appelés `pic-itkXX`. Les images correspondantes sont appelées `itkXX`. Le conteneur `pic-ssh` (image `ssh`) est utilisé afin que les utilisateurs puissent se connecter à leur site via `sftp` ou `ssh` (utilisation le plus souvent de filezilla pour envoyer des fichiers). Nous avons également un conteneur appelé `pic-maint` (image `maint`), qui est utilisé par `picgs`, le script principal d'aministration (création des comptes, installations de CMS, etc.)

Les schéma général des images Docker, indiquant les dépendances des images entre elles, se trouve résumé ci-dessous. Adopter une structure hiérarchique est permet d'optimiser les reconstructions d'image et de fiabiliser le processus.

Dans le schéma ci-dessous, `deb4pic` est une image intermédiaire (logiciel de base Debian), `deb4pica` ajoute essentiellement Apache, alors que les `itkxx` ajoutent php dans diverses versions et les `itkxxs` ajoutent le support de l'écran de séurité pour les sites sous `spip`.

![schema-images.png](schema-images.png)

## Installation

### Prérequis

Vous devez avoir installé au préalable:
1. Un serveur `mariadb` ou `mysql`:
    ```
    apt install mariadb
    mysql> CREATE USER 'un-user'@'localhost' IDENTIFIED BY '1bonmotdepasse';
    mysql> GRANT ALL PRIVILEGES ON *.* TO 'un-user'@'localhost' WITH GRANT OPTION;
    ```
    Ensuite, créez un fichier, peu importe son nom et son emplacement, mais il doit appartenir à `root`, droits `400`, et son contenu est:
    ```
    [mysqldump]
    host=localhost
    user=un-user
    password=1bonmotdepasse
    
    [mysqladmin]
    host=localhost
    user=un-user
    password=1bonmotdepasse
    
    [mysql]
    host=localhost
    user=un-user
    password=1bonmotdepasse
    ```
    Vous allez utiliser mariadb en production, il est donc recommandé d'exécuter le script:
    ```
    mysql_secure_installation
    ```
2. Un serveur `ldap` (slapd):

    ```
    apt install slapd
    ```
    Ensuite, créez un fichier, peu importe son nom ni son emplacement, mais il doit appartenir à `root.docker`, droits `440`, et son contenu est constitué d'une seule ligne: le mot de passe administrateur que vous avez donné lors de l'installation de slapd

    **NOTE** - Si dans la suite vous n'arrivez pas à vous connecter au serveur ldap (`pics ajout` ne fonctionne pas), vous devrez refaire la configuration de votre serveur:

    ```
    dpkg-reconfigure slapd
    ```

3. Ces deux serveurs peuvent tourner sur la même machine que les conteneurs, ou sur une machine différente (si le host est chargé à cause d'un trafic important il peut être utile d'héberger `mariadb` et `slapd` sur une machine tierce, de préférence dans le même datacentre)

4. Le logiciel `docker`
    cf. https://docs.docker.com/engine/install/debian/#install-using-the-repository

5. Une bonne pratique consiste à définir un groupe d'administrateurs qui prendront en charge les tâches usuelles, et les faire appartenir au groupe docker. Ainsi il n'est pas nécessaire que ces personnes aient les droits sudo, ce qui est meilleur du point-de-vue de la sécurité:

    ```
    sudo addgroup xxxxxx docker
    ```

6. Volumes docker: Vous devez créer deux volumes docker, `www-data` et `pic-logrotate`:

    ```
    docker volume create www-data
    docker volume create pic-logrotate
    ```
7. Réseau: De même, vous devez créer deux réseaux docker, `picnet` et `picnetssh`
    ```
    docker network create picnet
    docker network create picnetssh
    ```

### Installation

L'installation se fait en plusieurs phases. L'application vit en-dessous du répertoire `/usr/local/houaibe` (Ce chemin peut être modifié en définissant la variable d'environnement `HOUAIBEDIR`, mais ce n'est pas recommandé car pas testé). Les principaux paramètres sont définis dans le fichier `/usr/local/houaibe/etc/houaibe.conf`

Dans la suite, le répertoire `$WORK` correspond au répertoire de travail, celui qui a été créé par le `git clone` initial.

Les scripts d'installation vont créer un ou plusieurs répertoires et installer des choses dans ces répertoires: *si le répertoire concerné existe déjà, ils ne feront rien, afin de ne pas écraser de données.* 

#### Phase 1: 

Création de l'arborescence et initialisation:

```
cd $WORK && sudo ./install.sh
```

#### Phase 2:

Éditez le fichier de configuration et renseignez les paramètres demandés:

```
sudo vi /usr/local/houaibe/etc/houaibe.conf
```

Concernant le paramètre `IPV6=0` vous devez le passer à 1 si vous utilisez le conteneur `pic-ipv6nat`, qui vous permettra d'avoir des logs corrects même avec une adresse source en IPv6. Si vous n'utilisez pas cette fonctionnalité, vous pouvez sans doute le laisser à 0. Il n'agit que sur le script de lancement de `pic-proxy`. Voir le dossier `ipv6` pour les détails.

#### Phase 3: 

Terminez la création de l'arborescence (cette phase utilise les paramètres que vous venez de renseigner):

```
cd $WORK && sudo ./install.sh
```

#### Phase 4:

Installation du logiciel dehydrated:

```
cd $WORK/dehydrated && sudo ./installdehydrated.sh
```

Le script d'installation détarre le fichier et installe deux fichiers de "hook"

- `hooks.bash` utilisé pour copier les certificats à l'endroit et au format attendus par nginx
- `hooks-dns.bash`, qui fait la même chose dans le cas d'un certificat '*'. Ce certificat est important pour que les sites puissent fonctionner automatiquement en https. Avec hooks-dns.bash, le répertoire wildcard-scripts contient des scripts adaptés des scripts écrits pour certbot avec hébergement DNS chez Gandi, par Sébastien Blaisot (https://blog.blaisot.org/letsencrypt-wildcard-part1.html). Si votre hébergeur DNS n'est pas Gandi, vous devrez sans doute modifier ces scripts *(merci de nous envoyer vos modifications)*

`dehydrated`est utilisé pour générer et renouveler les certificats letsencrypt. Afin que le renouvellement se fasse correctement il faut mettre une ou deux lignes dans le crontab de root (voir le répertoire crontab pour les détails)

#### Phase 5:

Construction des conteneurs: la variable `DKRBUILDALL`, renseignée à la phase 2, donne la liste des images à construire, à laquelle seront ajoutées diverses images intermédiaires (`deb4pic` `deb4pica`) ou indispensables au fonctionnement de l'ensemble (`proxy`, `maint`, `ssh` etc.):

```
cd $WORK && images/scripts/upgradeall.sh
```

Si vous avez déjà construit une partie des images et voulez en ajouter (après modification de `houaibe.conf.dist`), vous pouvez ajouter le switch 
`--build` pour que `upgradeall.sh` ne reprenne pas tout à zéro.

Peut-être préférez-vous récupérer directement les conteneurs depuis dockerhub: dans ce cas vous n'aves pas besoin de construire les images, il suffit de positionner la variable `DKRCPTE` et d'utiliser la commande:

```
cd $WORK && images/scripts/pullall.sh
```

Le script build.sh ou upgrade.sh permet de construire une seule image. Pour construire l'image itk74 par exemple:

```
cd $WORK && images/itk74/build.sh
```

ou

```
cd $WORK && image/itk74/upgrade.sh
```

Cette deuxième forme reprend tout au début en ignorant les cces Docker, elle est à privilégier pour une mise à jour du conteneur.

Et pour télécharger un conteneur depuis dockerhub:

```
cd $WORK && scripts/pull.sh itk74
```

Les scripts `build.sh` ou `upgrade.sh`  vont générer une image avec deux labels: `latest`, d'une parte, la date au format `yymm` (`2111` pour Novembre 2021) d'autre part. Cela permet de revenir aisément en arrière en cas de besoin (mise à jour foireuse par exemple).

#### Phase 6:

##### Création des réseaux et des volumes:

```
docker network create picnet
docker network create picnetssh
docker volume create www-data
docker volume create pic-logrotate
```

##### Installation des conteneurs.

 Quelque soit le conteneur, la séquence est à peu près la même. Allez voir les `README` de chaque répertoire pour les détails. En plus de la phase de construction, il faut générer des volumes (répertoires partagés et persistants) qui seront utilisés par les conteneurs. Pour pic-itk74, cela se fait par:

```
cd $WORK/images/itk74 && ./installcont.sh
```

Il vous faut installer a minima les conteneurs suivants, et de préférence dans cet ordre:

1. pic-proxy (image: proxy)
2. pic-maint (image: maint)
3. pic-ssh (image: ssh)
4. pic-itkXXX (image: itkXXX): Vous devez installer au moins l'un de ces conteneurs, il n'est pas indispensable de tous les installer. N'oubliez pas de modifier en conséquence le paramètre DKRSRVWEB dans /usr/local/houaibe/etc/houibe.conf

**ATTENTION** - Si vous souhaitez installer un conteneur `itk72s` ou `itk74s`, vous devez **impérativement** commencer par installer l'écran de sécurité, sinon apache ne démarrera pas. Pour cela, installez le conteneur pic-ssh, démarrez-le, puis exécutez les commandes:

```
docker exec -it pic-ssh bash
mkdir -p /home/root/ecran_securite && \
cd /home/root/ecran_securite && \
wget https://git.spip.net/spip-contrib-outils/securite/raw/branch/master/ecran_securite.php
```

*Règles de nommage et identification des chemins:*

- Les fichiers nécessaires à la construction d'une image appelée `toto`se trouvent dans le répertoire `$WORK/images/toto` Dans ce répertoire, vous trouverez également un fichier README.md 
- Le conteneur correspondant s'appellera `pic-toto`. Tous les conteneurs gérés par `houaibe` s'appellent `pic-xxx`, cela permet de les identifier facilement: en effet rien ne vous empêche d'ajouter d'autres conteneurs, dans ce cas nous vous conseillons de les appeler autrement.
- Ce conteneur utilisera un script de démarrage, des volumes docker, tout cela se trouvera dans le répertoire `/usr/local/houaibe/containers/pic-toto`
- Pour (re)démarrer le conteneur pic-toto, il suffit d'appeler la commande:   
   - `/usr/local/houaibe/containers/pic-toto/run.sh` *ou*
   - `$WORK/images/toto/run.sh` *ou*
   - `toctoc restart pic-toto ` *(voir ci-dessous)*
- Pour arrêter le conteneur pic-toto, il suffit d'appeler la commande (si elle existe):
   - `/usr/local/houaibe/containers/pic-toto/stop.sh` ou
   - `$WORK/images/toto/stop.sh` ou
   - `toctoc stop pic-toto` *(voir ci-dessous)*

**NOTE** - La plupart des conteneurs ne possèdent pas de script d'arrêt `stop.sh`, parce que dans l'usage courant ils ne sont pas censés s'arrêter. Il faudra donc utiliser les commandes natives de docker pour les arrêter, par exemple en cas de maintenance.

#### Phase 7:

Installation de `picgs`:

```
cd $WORK/picgs
sudo ./installpicgs.sh
```

Voir le README.md du répertoire picgs pour en savoir plus...

#### Phase 8:

##### Santé des conteneurs (healthcheck):

Normalement la commande docker ps devrait s'afficher ainsi:

```
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS                   ... NAMES
3c0052e69802   proxy     "/app/docker-entrypo…"   5 minutes ago   Up 5 minutes (healthy)   ... pic-proxy
c1087ef7311b   itk74s    "/bin/bash -c /entry…"   6 minutes ago   Up 6 minutes (healthy)   ... pic-itk74s
...
```

Attention toutefois le test de santé des conteneurs pic-itk74s est à ce stade incomplet: en effet on teste qu'apache est bien fonctionnel, mais on ne vérifie pas la base de données. On peut d'ailleurs le vérifier ainsi:

```
$ docker exec pic-itk74s health.sh
apache - Apache
pic-itk74s OK
```

##### Pour remédier à cela, il faut:

- Créer un utilisateur appelé health, par exemple en utilisant phpmyadmin.
- Créer un fichier dans le volume `www-data` et mettre dans ce fichier le mot de passe de `health`. Ce fichier doit être lisible par l'utilisateur www-data
- Renseigner dans `houaibe.conf` le chemin d'accès à ce fichier (paramètre `HEALTHPASSWDFILE`)

##### Procédures dans le crontab:

Mise en place des procédures dans la crontab. Voir le README.md du répertoire crontab pour en savoir plus...

