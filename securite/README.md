# La sécurité avec iptables et fail2ban

Il est **indispensable** de configurer des règles `iptables` afin d'assurer la sécurité: sinon vous allez exposer des services sensibles, comme par exemple le service `ldap` et ainsi mettre toute votre infrastructure en danger. Par ailleurs, `iptables` vous permettra de bloquer des adresses IP connues pour leurs comportements problématiques, et aussi de protéger vos ports ssh avec `fail2ban`.

## fail2ban:

Le logiciel `fail2ban`  protège les accès `ssh` contre les tentatives d'intrusion. Dès qu'une IP échoue plus de 5 fois à se connecter en ssh ("fail"), `fail2ban` va la "bannir" (ban), c'est-à-dire la bloquer en définissant de nouvelles règles `iptables`. Le bannissement ne dure que quelques minutes.

Avec `houaibe`, vous devrez surveiller deux ports:

- Le port 22 (utilisé par les administrateurs du serveur)
- Le port 2200 (utilisé par les utilisateurs pour mettre à jour leur site web)

Il vous faudra donc deux prisons ("jail") `fail2ban`, la prison standard et une prison `pic-ssh`. La prison standard est automatiquement configurée à l'installation, mais pour la prison `pic-ssh` il vous faudra:

- déposer le fichier  `/etc/fail2ban/jail.d/pic.conf`:
  ```
  [pic-sshd]
  enabled = true
  port    = 2200
  filter  = sshd
  logpath = /var/log/pic-ssh/auth.log
  maxretry= 5
  ```
- Faire un lien symbolique pour que `fail2ban` trouve les logs:

  ```
  cd /var/log
  ln -s /usr/local/houaibe/containers/pic-ssh/log pic-ssh
  ```

**ATTENTION** : Le port 2200 étant servi par l'ntermédiaire d'un conteneur, il faudra définir une configuration particulière d'iptables. Celle-ci est intégrée aux règles que nous vous proposons (voir ci-dessous)

## abuseipdb:

AbuseIpDb https://abuseipdb.com est un projet collaboratif dont l'objectif est de "rendre l'internet plus sûr, une IP à la fois". Les participants ont la possibilité de signaler les IP "abusives", en retour ils peuvent télécharger tous les jours une liste de 10000 IP "dangereuses" qu'il est utile de bloquer. La base de données est constituée à partir des signalement de nombreux internautes, ce qui est un gage de fiabilité.

### Récupérer des listes noires d'adresses IP:

Le programme `picbloc` télécharge depuis `abuseipdb` une liste de 10000 IP, ces adresses seront ensuite incorporées dans un bloc `ipset` afin d'être effectivement bloquées par iptables.

### Contribuer à abuseipdb:

Si vous utilisez `fail2ban` pour protéger vos ports ssh, vous pouvez le configurer afin que les adresses IP bloquées soient effectivement signalées à `abuseipdb`, afin de contribuer au programme. La démarche et la configuration sont expliquées ici: https://www.abuseipdb.com/fail2ban.html

## iptables:

`iptables` permet de définir des filtres afin de bloquer l'accès à certains ports pour toutes les adresses, ou encore de bloquer complètement l'accès à certaines adresses IP. Nous proposons une configuration pour `iptables` qui fonctionne avec `Docker` et `fail2ban` (deux logiciels qui eux-mêmes modifient les règles `iptables`), et permet de bloquer les listes d'IP fournies par abuseipdb (en utilisant le logiciel `ipset`). La configuration proposée d'iptables s'appuie sur cet article: https://unrouted.io/2017/08/15/docker-firewall/

## ipset:

`ipset` permet d'incorporer au noyau des listes d'adresses IP avec indexation afin de pouvoir les rechercher rapidement par la suite. Il travaille en lien avec `iptables`: une seule règle `iptables` permet ainsi de bloquer l'accès au serveur pour tout un ensemble d'adresses. Cet ensemble `ipset` sera construit par les IP fournies par abuseipdb (soit un ensemble de 20000 adresses environ)

## picbloc:

`picbloc` est un programme intégré à `houaibe`, il permet de:

- Télécharger les adresses d'abuseIpdb (10000 par jour avec l'abonnement gratuit)
- Eventuellement intégrer à cette liste votre liste noire personnelle (fichier `/usr/local/houaible/etc/iptables/blacklist.local`)
- Les insérer dans une base de données `sqlite3`: puisque tous les jours les 10000 IP changent en partie, on garde les IP fournies les jours précédents. Ainsi si une IP n'apparait plus aujourd'hui, elle sera toujours dans notre base de données, et ce durant 5 jours. Cela permet de garder en permanence une liste de blocage de 20000 adresses IP environ.
- Générer deux blocs `ipset` (un pour les adresses IPv4 et un pour les adresses IPv6), ce qui permettra de bloquer l'accès au serveur pour les 20000 adresses IP

## Installation:

L'intallation est simple, mais doit être faite à la main. N'oubliez pas de tester après installation que vous n'avez rien cassé (vérifier que les sites web sont toujours opérationnels), et que les services sont correctement protégés (vérifiez avec nmap sur une machine distante)

### iptables et ipset:

```
apt update && apt install iptables ipset
```

### picbloc:

1. Il fait partie de la suite `picgs`, donc si `picgs` est installé `picbloc` également.
2. Vous devez mettre votre clé abuseipdb dans le fichier défini par la variable `ABUSKEYFILE` de `/usr/local/houaibe/etc/houaibe.conf`
3. Vous pouvez ajouter des adresses IP à bloquer (par exemple une IP que vous maîtrisez, à des fins de test, ou alors une IP que vous avez repérée dans les logs) dans le fichier /usr/local/houaibe/etc/blacklist.local

### Règles iptables:

1. Mettre en place les règles iptables proposées en exécutant les commandes suivantes:
   ```
   cd .../securite
   sudo mkdir /usr/local/houaibe/etc/iptables 
   sudo cp iptables/iptables.conf iptables/ip6tables.conf /usr/local/houaibe/etc/iptables
   sudo cp iptables/blacklist.local /usr/local/houaibe/etc/iptables
   ```

1. Vérifier que ces règles vous conviennent, décommentez la ligne marquée `picbloc` si vous souhaitez bloquer des listes d'IP avec `picbloc` et `abuseipdb`

1. Exécuter `picbloc` pour la première fois, afin de générer la base de données `sqlite`, les `ipset`, ainsi que des fichiers de sauvegarde qui seront utilisés en cas de reboot de la machine afin de régénérer les ipset très rapidement
   ```
   sudo picbloc
   ```

1. Mettre en place les fichiers de démarrage afin de gérer iptables par systemd:
   ```
   cd .../securite
   sudo cp iptables.service /etc/systemd/system
   ```
   Editer `/etc/systemd/system/iptables.service`: suivant que vous utilisez `blocpic` (et donc `ipset`) ou pas, il y a peut-être des choses à modifier.
   ```
   systemctl daemon-reload
   systemctl enable iptables
   systemctl restart iptables
   systemctl restart fail2ban
   ```

1. tester:

   - Utilisez le logiciel `nmap` (https://nmap.org) pour vérifier que les ports qui doivent être bloqués le sont bien:
     *Depuis votre poste de travail:*

     ```
     nmap nom-de-machine
     nmap -6 nom-de-machine
     ```

   - Vérifiez que `fail2ban` protège correctement le port 22 et 2200 (n'hésitez pas à vous faire bannir !)

     ```
     fail2ban-client banned
     ```

   - Intégrez à `picbloc` une IP d'un serveur ou d'un poste de travail que vous maîtrisez, et vérifiez qu'elle est bien bloquée:

     ```
     vi /usr/local/houaibe/etc/iptables/blacklist.local
     picbloc
     ```

   -  Vérifiez que les services "légitimes" sont toujours accessibles, et en particulier que les sites web fonctionnent bien (dans le cas contraire cela veut peut-être dire que le serveur `ldap` n'est plus accessible)

1. Mettre une ligne dans le `crontab` de root afin d'exécuter `picbloc` tous les jours:

   ```
   MM HH * * * /usr/local/bin/picbloc >/dev/null 2>&1
   ```

   

### Reboot:

Il n'est pas indispensable de rebooter, mais un reboot suivi des tests évoqués plus haut est une étape importante pour être sûr que son serveur reste correctement sécurisé à chaque démarrage.

# Installer et mettre à jour les sites wordpress:

La sécurité c'est aussi garder ses CMS à jour, au moins faire les mises à jour mineures (corrections de bugs).

## Installation d'un site sous Wordpress:

La commande suivante permet d'intaller un site wordpress:

```
picgs wpinstall site 2
```

Le plugin `ssh-sftp-updater-support` est installé par la commande précédente, et configuré automatiquement (dans le fichier `wp-config.php`). Ce plugin est indispensable pour permettre aux administrateurs de site de mettre à jour leur wordpress et leurs plugins à partir de l'espace d'aministration de wordpress. 

### Mise à jour automatique:

Il est recommandé d'utiliser la configuration de mise à jour automatique proposée par wordpress pour le logiciel de base ainsi que pour les plugins. Seules les mises à jour mineures (les plus importantes du point de vue de la sécurité) seront effectuées automatiquement. Grâce à `ssh-sftp-updater-support`, cette fonctionnalité peut être utilisée en toute sécurité.

## Mise à jour manuelle:

La commande suivante permet de faire toute mise à jour wordpress (mineure ou majeure), mais sur un seul site:

```
picgs wpmaj monsite site 2
```

## Mises à jour en masse:

Le script `picwpgs` permet d'effectuer les mises à jour *mineures* de tous les sites wordpress se trouvant sur la plateforme. `picwpgs` ne proposera jmais de mises à jour majeures, afin de minimiser les risques de casse. `picwpgs` fait partie de la suite `picgs` et a donc été installé avec `picgs`.

### Vérifier les versions des wordpress installés et des plugins:

    picwpgs liste core
    picwpgs liste plugins

### Mettre à jour (mises à jour mineures) tous les sites sous wordpress qui en ont besoin:

```
picwpgs maj core
```

### Mettre à jour (mises à jour mineures) tous les plugins de tous les sites:

```
picwpgs maj plugin
```

### Mettre à jour (mises à jour mineures) un seul plugin, sur tous les sites qui en ont besoin:

```
picwpgs maj plugin --plugin=machin
```

# Installer et mettre à jour les sites sous spip:

`picgs` permet d'installer ou mettre à jour le cms `spip`. Avant de pouvoir utiliser ces fonctionnalités, il vous faudra télécharger à la main le fichier de la version désirée. Par exemple pour la version 3.2.11:

```
toctoc entrer
wget https://files.spip.net/spip/archives/spip-v3.2.11.zip
```

La commande suivante permet alors d'installer spip :

```
picgs spipinstall monsite site 1
```

Et la commande suivante permet de mettre Spip à jour:

```
picgs spipmaj monsite site 1
```

## Mutualisation spip:

Il est également possible de gérer une "mutualisation" de sites sous spip, c'est-à-dire un ensemble de sites sous spip qui partagent le même "moteur". Le principal avantage de cette solution est de faciliter les mises à jour de spip et de ses plugins. `picgs` possède plusieurs commandes permettant de faciliter la gestion de ce type d'installations.

## Ecran de sécurité de spip:

Les sites sous `spip` doivent être installés dans les conteneurs `pic-itkxxs`, car ces conteneurs incluent une configuration de sécurité qui oblige php à exécuter l'écran de sécurité avant toute autre chose. Les sites sous spip sont alors protégés contre un certain nombre d'attaques, même en l'absence de mise à jour. Bien sûr, cela suppose que le fichier de l'écran de sécurité soit mis en place, et régulièrement téléchargé (voir à ce sujet le chapitre consacré à la crontab).

Les autres conteneurs ne forcent pas 'utilisation de l'écrn de sécurité de spip, en raison des risques d'interférence avec les autres CMS qui seront utilisés dans ces conteneurs.

# Installer et mettre à jour galette:

`galette` est installé automatiquement lors de la création d'un espace web de type `galette`, avec la commande suivante:

```
picgs ajout monsite galette 
```

La commande suivante permet de déclencher la mise à jour de `galette` sur un seul site:

```
picgs galettemaj monsite galette 1
```

# Installer et mettre à jour dolibarr:

`dolibarr` est installé automatiquement lors de la création d'un espace web de type `dolibarr`, avec la commande suivante:

```
picgs ajout monsite dolibarr 
```

La commande suivante permet de déclencher la mise à jour de `dolibarr` sur un seul site:

```
picgs dolibarrmaj monsite dolibarr 1
```

