# Release notes

## v 4.1.5 - Février 2025
- Correction de plusieurs bugs picgs
- Ajout de règles dans les wp-htaccess de nginx-fpm83 et nginx-fpm84

## v 4.1.4 - Février 2025
- Correction de plusieurs bugs picgs

## v 4.1.0-4.1.3 - Janvier 2025
- Hébergement nginx/fpm: Possibilité d'adosser un site statique (100% nginx) à  un site dynamique (permet d'utiliser les extensions wp SimpleStatic ou Staatic)
- Nouvelle image pmbitk83 (pour pmb)
- Utilisation des conteneurs nginx-fpm avec galette et dolibarr

## v 4.0.0-4.0.6 - Décembre 2024
- Réécriture des programmes principaux picgs et picwpgs: utilisation des "algos" définis dans main.py
  Cela permet d'avoir un code plus clair et plus générique
- Répertoire scripts et programme picscript: il est maintenant simple d'écrire des scripts pour faciliter la maintenance
- Ajout de picwpgs liste/maj themes
- Améliorations de picwpgs maj themes/plugins/core

## v 3.25.4 - Novembre 2024
- Sécurité améliorée en protégeant les fichiers de configuration des CMS

## v 3.25.3 - Novembre 2024
- Quelques corrections de bugs

## v 3.25.1, 3.25.2 - Octobre 2024
- paheko: modification des scripts `factory_cron.sh` et `factory_cron_emails.sh` pour fonctionner avec la version `1.3.12`
- picpkgs: correction d'un bug limitant la longueur des urls de manière abusive

## v 3.25.0 - Octobre 2024
- picpkgs pour gérer les instances paheko
- Intégration d'une installation de Nextcloud
- Les conteneurs php 8.2 et 8.3 (itk82/3 et nginx-fpm82/3) intègrent la bibliothèque PHPMailer
- Améliorations du code de picgs

## v 3.24.3.24.4, 3.24.5, 3.24.6 - Octobre 2024
- picgs : Quelques corrections de bugs

## v 3.24.3 - Août 2024
- picgs archivage: possibilité de ne pas archiver le répertoire IMG de spip pour gagner du temps et de l'espace disque
- pic-proxy: Modification du run.sh.dist, afin de prendre en compte la varaible $SSL_POLICY

## v 3.24.2 - Août 2024
- picgs: correction de bugs (mutuprod, mutuupgrade, mdp)

## v 3.24.1 - Août 2024
- picgs mutusauvebd et mutusauveplugins plus robustes
- Correction d'un warning sur le build du proxy
- bug dans le script buildall.sh
- Modifications dans logrotate et le crontab

## v 3.24.0 - Juillet 2024
- picgs mutuexporte pour exporter un site depuis la mutu spip 
- picgs spipclone a été réécrit: maintenant il va au bout de l'installation du site spip
- picgs archivage (un peu) plus performant car on peut refuser l'archivage des logs
- Les scripts pull.sh et push.sh permettent de puller/pusher les images de type nginx-fpmxy
- Les scripts upgrade.sh ont été supprimés, remplacés par les build.sh - en définissant la variable d'environnemnt NOCACHE

## v 3.23.14 - Juillet 2024
- Conteneurs nginx-fpm82/3: big imagick
- picgs: mauvaise détection de galette pour les versions >= 1.1

## v 3.23.13 - Juillet 2024
- Conteneurs nginx-fpm82/3: Installation des extensions imagick et exif
                            Paramétrage de la mémoire de php (fichiers houaibe.conf et memory-limit.ini)
- Ajout de l'image itk83 (conteneur pic-itk83)

## v 3.23.12 - Juillet 2024
- Conteneurs nginx-fpm82/3: redéfinition du paramètre nginx client_max_body_size, bug dans memory-limit.ini

## v 3.23.11 - Juin 2024
- BUGS dans picgs mutuajout, mutuimporte, mutusuppr

## v 3.23.10 - Juin 2024
- BUGS dans les Dockerfile de certaines images nginx-fpmxy
- paheko 1.3.10

## v 3.23.9 - Mai 2024
- BUG dans la conf de logrotate
- Conteneurs nginx-fpmxy: Ajout de limite et réservation de mémoire

## v 3.23.8 - Mai 2024
- BUG dans la conf de logrotate
- BUG dans picgs collecte (pas possible de voir les adresses de production)
- BUG dans picgs mauvaises permissions pour le répertoire priv/htaccess
- BUG d'affichage dans install.py, script d'installation de picgs (conteneurs lginx)
- Amélioration du fichier htaccess-wp.conf (conteneurs nginx)
- picwpmigrate installe maintenant le plugin cache-control

## v 3.23.7 - Mai 2024
- picgs: problèmes lors de la mise en production de sites wordpress - les warnings éventuels lors d'appels à la commande wp sont maintenant supprimés

## v 3.23.6 - Mai 2024
- picgs: problèmes d'installation de dolibarr, en particulier le character set de la B.D. est maintenant utf8mb4
- proxy (nginx.tmp): ajout optionnel d'une ligne pour changer le timeout

## v 3.23.5 - Mai 2024
- BUGS dans picgs
- picgs archivage: le fichier de base de données est en .sql.gz et pas .mysqldump.gz, pour une meilleure homogénéité
- paheko: passage en 1.3.9

## v 3.23.4 - Mai 2024
- Prise en compte des conteneurs nginx/fpm pour toctoc restart, stop
- Prise en compte des conteneurs nginx/fpm dan le script health

## v 3.23.3 - Mai 2024
- bugs picgs 
- Réécriture de toctoc pour entrer dans les conteneurs nginx et php

## v 3.23.2 - Mai 2024
- BUGS - Plusieurs bugs picgs et AUSSI houaibe.conf

## v 3.23.1 - Mai 2024
- BUG - Problèmes avec la mutu Spip

## v 3.23.0 - Avril 2024
- Introduction d'un nouveau type de conteneurs, reposant sur `nginx` et `fpm`
  Ces conteneurs ne sont plus exécutés en tant que root, et il y a un ensemble de 2 conteneurs par site web

## v 3.22.8 - Mai 2024
- Conteneurs pipc-maint et pic-php: mise en place du paramètre memory_limit, sinon risque de plantage

## v 3.22.7 - Avril 2024
- BUG - Mauvaises permissions du répertoire db lors de picgs ajout
- BUG - Oubli d'un switch pour tar x dans picgs recharge
- paheko passe en version 1.3.8

## v 3.22.6 - Avril 2024
- BUG - Mauvaises permissions des fichiers logrotate.conf, perturbation lors de la rotation des logs

## v 3.22.5 - Mars 2024
- BUG - Installation de Galette, détermination de la version de galette pà partir de la 1.0.0

## v 3.22.4 - Mars 2024
- BUG - dans picgs, sauvegarde des bases de données (picgs archivage, sauvbd, mutusauvebd), le switch --hex-blob est nécessaire

## v 3.22.2 3.22.3 - Mars 2024

- BUG - dans `picgs ajout` et `picgs mutu ajout` 

## v 3.22.1 - Fev 2024

- BUG - dans `picwpgs liste mails` 

## v 3.22.0 - Fev 2024

- `picwpgs liste` donne plus d'informations, pour plus de sécurité. En partiulier sur les users et les thèmes. Mise en évidence des users/themes/plugins ajoutés ou retirés.
- Configuration du cache du proxy nginx
- Correction de plusieurs bugs `picgs` (régression)

## v 3.21.3 - Fev 2024

- Bug d'install `picgs`
- Bugs dans les crons de paheko
- Modification de `health`, si un seul conteneur est malade on le redémarre
- `pic-maint` reste allumé en permanence

## v 3.21.2 - Jan 2024

- Correction de deux bug `picgs`

## v 3.21.0 - Jan 2024

- Intégration d'un conteneur paheko'
- `picgs prod` met automatiquement à jour le siteurl dans le cas spip et wordpress
- Script `picwpmigrate` pour aider à la migration de sites wordpress depuis un autre hébergeur
- `picgs` est maintenant une zipapp (tout le code est dans un unique fichier .pyz)

## v 3.20.6 - Dec 2023

- Optimisation de `toctoc`: on détecte correctement si le paramètres est un conteneur ou un user
- Optimisation de supervisor: envoi du signak WINCH lorsque le conteneur s'arrête, afin d'arrêter proprement apache

## v 3.20.5 - Dec 2023

- bugs dans `picgs`:
  - `toctoc`: fonctionne correctement même lorsque bash n'est pas installé dans le conteneur
  - `picgs mdp`: Dans certains cas une fausse erreur était affichée lorsqu'on change les mots de passe de la mutu
- conteneur `pic-maint`: Il manquait le swtich `--hostname` 

## v 3.20.4 - Dec 2023

- bug - Fichier /containers/run-include.sh: Volume tmpfs monté sur /var/lib/php/sessions

## v 3.20.3 - Dec 2023

- bug - vi et autres utilitaires doivent etre installés dans maint plutôt que dans ssh

## v 3.20.2 - Dec 2023

- bug - gestion des erreurs de toctoc
- itk81 et itk82: on installe maintenant le paquet php-imaps

## v 3.20.1 - Nov 2023

- bug - dans le `run.dist.sh` des images itk82 et itk82s

## v 3.20.0 - Nov 2023

- Amélioration des scripts de démarrage des conteneurs: implémentation du principe du "moindre privilège" pour améliorer la sécurité

## v 3.19.5 - Oct 2023

- bug - Lors de la fin d'une sauvegarde le conteneur `bbackup-srv` doit pouvoir être éteint par un user non root
- bug - Le run.sh du conteneur `bbackup` refuse de démarrer si une ressource n'existe pas

## v 3.19.4 - Oct 2023

- Amélioration du script verrou-ouvrir.sh (conteneur brestit) - Les messages permettent de contrôler ce qui est fait

## v 3.19.3 - Oct 2023

- bug dans `brestit/run.sh.dist`: le cache n'était pas partagé avec `bbackup`

## v 3.19.2 - Oct 2023
- Correction de deux bugs dans `picgs`

## v 3.19.0,3.19.1 - Sept 2023

- Refonte du système de sauvegardes: meilleure sécurité car les dépôts appartiennent à des utilisateurs non privilégiés
- Amélioration de `toctoc` qui permet maintenant d'arrêter tous les conteneurs, d'entrer dans tous les conteneurs, et de passer un paramètre au run 

## v 3.18.5,3.18.6 - Sept 2023

- Image `maint`:
  - Mise à jour de php vers la version 8.1
  - Correction d'un bug qui empêchait le build

- Image `deb4pic`:
  - Amélioration de la configuration de `msmtp` (mail de meilleure qualité important pour la délivrabilité)

## v 3.18.4 - Août 2023

- Bug: lors de l'installation du conteneur `pic-itk81`
- Bug: `picgs mutuvidelescache` ne vidait pas tous les caches de spip

## v 3.18.3 - Août 2023

- Un vilain bug empêchait le premier démarrage du conteneur `pic-bbackup-srv`

## v 3.18.2 - Août 2023

- Script `borgbackup.bash`: le mail de rapport de sauvegarde distingue entre WARNINGS (borg rc 1) et ERREURS (borg rc 2)
- Ajout du script `borg-compact.bash` au conteneur `bbackup-srv`

## v 3.18.1 - Août 2023

- Corrections de divers bugs dans picgs

## v 3.18.0 - Mai 2023

- **bbackup**:
  - Utilisation de borg version 1.2.4
  - Modification du fichier `run.sh` afin d'ajouter les répertoires .config et .cache
- **itkxx**:
  - Suppression de l'image itk70
  - deb4pic, donc les images maint, ssh, pic-itkxx dérivent maintenant de *Debian bookworm*

## v 3.17.4 - Juillet 2023

- **bbackup:**
  - Ajout d'un script, borg_repos.bash, permettant de déterminer aisément le nom des dépôts borg, dans le but de les vérifier ou éventuellement de les réparer.

## v 3.17.3 - Mai 2023

- **picgs**:
  - BUG: picgs mdp ne fonctionne pas avec des sites spip

## v 3.17.2 - Mai 2023

- **picgs**:
  - Génération du mot de passe admin lors de l'installation automatique de wordpress

## v 3.17.1 - Mai 2023

- **picgs**:
  - BUG dans picwpgs, les plugins doivent être adaptés 

## v 3.17.0 - Mai 202**3**

- **picgs:** 
  - Lors de l'installation de worpress, le plugin wp-easy-installa-managers est automatiquement et configuré
  - Nouvelle action: picgs mdp permet de modifier le mot de passe picadmin
  - Les objets sont légèrement modifiés afin de pouvoir être réutilisés pour d'autres scripts de gestion ystème

- **Image ssh:**
  - L'aspirateur de site `httrack` est installé dans ce conteneur


## v 3.16.0 - Janvier 202**3**

- **Nouvelles images:** itk82 et itk82s (php 8.2), et `itk00` (pas de php)
- **picgs:** diverses améliorations

## v 3.15.0 - Novembre 2022

- **ipv6:**
  - Intégration du conteneur ipv6nat, cf. https://github.com/robbertkl/docker-ipv6nat

## v 3.14.5 - Octobre 2022

- **picgs**:
  - Correction de bug: les noms de compte (et de sites) sont convertis en minuscules, sinon ldap ne fonctionne pas
  - Correction de bug: Mauvaise redirection dans certains cas avec la commande `picgs canon`

## v 3.14.4 - Octobre 2022

- **Images**:
  - msmtp écrit maintenant ses logs sur un volume persistent
  - Le sendmail de php est redéfini afin de passer par un wrapper bash: ainsi on peut logguer le nom du site web concerné

## v 3.14.3 - Septembre 2022

- **picgs**:
  - Correction de bugs

## v 3.14.2 - Août 2022

- **picgs**:
  - Correction de bugs et amélioration de l'aide

## v 3.14.1 - Juillet 2022

- **picgs**:
  - Correction de plusieurs bugs (analyse des paramètres, permissions lors de l'installation de galette, privilèges de base de donnée, picgs info pour la mutu)

## v 3.14.0 - Juin 2022

- **picgs**:
  - Amélioration de picgs info: maintenant pour chaque adresse de production on vérifie si elle fonctionne et si renvoie un code 200 ou un code 301 (redirection vers l'adresse canonique). On affiche aussi l'adresse canonique, i elle existe
  - Ajout de la commande `picgs canon` qui permet de rediriger toutes les url de production vers l'adresse canonique.

## v 3.13.3 - Mai 2022

- **picgs**:
  - BUG - Installation de dolibarr et galette ne fonctionnait pas
  - Amélioration de l'aide de xxxclone

## v 3.13.2 - Mai 2022

- **picgs**:
  - BUG dans dolibarrmaj. Problème de permissions

## v 3.13.1 - Mai 2022

- **picgs**:
  - Nouvelles commandes: `wpclone`, `galetteclone`, `dolibarrclone`
  - La limite du nom de caractères pour un nom de site passe de 12 à 32
  - Amélioration du système d'aide introduit en 3.12, et correction de plusieurs bugs

## v 3.12.3 - Avril 2022

- **picgs, toctoc, picwpgs:**
  - Correction de plusieurs bugs

## v 3.12.2 - Mars 2022

- **picgs, picwpgs:**
  - Correction de plusieurs bugs

## v 3.12.1 - Février 2022

- **picgs:** 
  - Correction de bug

## v 3.12.0 - Février 2022

- **picgs**:
  - Nouveau système d'aide, plus précis et plus complet
  - Ajout des trois commandes `desactiver`, `reactiver`, `inactifs`
- **Conteneurs**:
  - Installation de la locale `fr_FR.UTF-8`
  - Ajout des paquets `exa` et `php-magick` (demandé en particulier par wordpress)
  - Ajout d'un conteneur `pic-itk81` (php 8.1)

## v 3.11.3 - 29 Décembre 2021

- **BUG** - Le système HEALTH (supervision des conteneurs) ne fonctionnait pas correctement

## v 3.11.2 - 21 Décembre 2021

- **BUG** - Dans `picgs`: La fonction `_isDolibarr` pas assez robuste

## v 3.11.0 - 1er Décembre 2021

- **HEALTHCHECK** **dans les conteneurs:**
  - Il est maintenant possible de vérifier non seulement le serveur apache, mais aussi la base de données
  - Le check se fait avec un script (dans les conteneurs) `health.sh`, appelé par le script de surveillance en cas de problème, afin d'envoyer un mail d'alerte plus précis
  - Paramètre `HEALTH_MAIL`: utilisé par le script health de n'envoyer le mail qu'aux personnes concernées
- **Construction des conteneurs**: Les Dockerfile ont été remaniés et le système de construction de conteneurs est optimisé
- Installation de mise à jour de spip plus simple
- Correction de plusieurs bugs

## v 3.10.4 - 11 Novembre 2021

- **BUG** - Dans `picgs`: Le répertoire `install` de galette était supprimé sans sauvegarde

## v 3.10.3 - 31 Octobre 2021

- **BUG** - Installation et mise à jour de galette ne fonctionne pas
- **BUG** - `toctoc liste` ne fonctionne pas

## v 3.10.2 - 28 Octobre 2021

- **Hotfix**: dans certaines conditions `wpinstall` ne fonctionnait pas

## v 3.10.1 - 17 Octobre 2021

- **HEALTHCHECK dans les conteneurs**
  - Si un conteneur tombe malade, les admins reçoivent un mail !
- **Les conteneurs sont sur dockerhub**
  - Plus besoin de construire les conteneurs proposés, il suffit de les récupérer tout faits sur dockerhub

## v 3.9.2 - 26 Septembre 2021

- **bugfix release:**
  - Positionnement correct du umask lors du lancement d'apache dans les conteneurs

## v 3.9.1 - 24 Septembre 2021

- **bugfix release:**
  - Correction d'un bug picgs dans la commande prod (lorsqu'on changeait d'utilisateur pour la version de production)

## v 3.9.0 - 28 août 2021

- **Nouveaux conteneurs pic-bbackup, pic-brestit, pic-bbackup-srv:**
  - Ces conteneurs embarquent des scripts de sauvegarde et de restitution utilisant le logiciel `borgbackup`

## v 3.8.3 - 18 août 2021

- **bugfix release:**
  
  Correction d'un bug picgs, il y avait risque de mélange dans les versions de production lors d'un déménagement de sites (`picgs demenage`)
  
  Blocage de xmlrpc.php, afin de protéger les sites wordpress des attaques xmlrpc

## v 3.8.2 - 18 juillet 2021

- **bugfix release:**
  - Correction d'un bug dans picgs/lib/picconfig.py, qui rendait picbloc inutilisable

## v 3.8.1 - 16 juillet 2021
- **bugfix release:**
  - Correction d'un bug d'affichage dans picgs

## v 3.8.0 - 12 juin 2021
- **Nouveau conteneur pic-itk74:**
  - Ce conteneur embarque apache2 et php 7.4
- **Nouvelles variables d'environnement dans houaibe.conf**
  - Ces variables doivent être positionnées **seulement** si vous avez configuré une mutualisation spip avec des noms non standards (c-à-d différents de `mutu-st1`)
- **Nouvelle version du conetneur pic-ssh:**
  - L'image a changé: installation de supervisor via pip3 plutôt que apt-get, et ajout de quelques modules python pour que les utilisateurs puissent exécuter des procédures

## v 3.7.0 - 6 Juin 2021

- **Nouvelle version de picgs:**
  - Commande nohttps pour supprimer un certificat
  - ssh-sftp-updater automatiquement configuré lors de wpinstall
- **Nouvelles versions de conteneurs pic-itk73 et pic-itk74s:**
  - `exim4` est remplacé par `msmtp` pour l'envoi des mails, cela permet de lancer le conteneur avec moins de privilèges
- **Conteneurs pic-itk70 et pic-itk72s:**
  - Ces conteneurs seront supprimés dans une prochaine version
  - En l'état les sites web hébergés ne peuvent envoyer de mails: pour corriger le problème, veuillez supprimer la ligne `--security-opt no-new-privileges`  des fichiers de démarrage `run.sh`

## v 3.6.1 - 24 Mai 2021

- **Conteneurs:**
  - Bugs dans certains scripts de démarrage des conteneurs
  - Bugs dans les règles iptables
- **Corrections de doc**

## v 3.6.0 - 14 Mai 2021

- Première révision
