#!/bin/bash
#
# Script de démarrage du conteneur
#
# Usage: path/to/container/directory/run.sh
#
# Ce script arrête le conteneur si besoin, et le redémarre aussitôt
#

# set -v

# Calcul du repertoire dans lequel run.sh est depose - ca marche meme si on part d'un lien symbolique
fullpath=$(readlink --no-newline --canonicalize-existing $0) 
if [[ -z $fullpath ]]; then echo "ERREUR - run.sh pointe sur le vide !"; exit 1; fi
DIR=$(dirname "$fullpath")

(
cd $DIR

# Variables générales
if [[ -f ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf ]]; then . ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf; else echo "Merci de terminer la configuration en créant le fichier /usr/local/etc/houaibe/houaibe.conf"; exit 1; fi

# Variables spécifiques à ce conteneur
if [[ -f ./houaibe.conf ]]; then . ./houaibe.conf; else echo "${DIR} n'est pas un repertoire de conteneur défini dans houaibe."; exit 1; fi

# Arrêt du conteneur
docker stop $CONTAINER 2> /dev/null
docker rm   $CONTAINER 2> /dev/null

# Les switches à utiliser au run, dépendent de houaibe.conf
[[ -n "$HEALTHPASSWDFILE" ]] && SWHEALTH="--env HEALTHPASSWDFILE=$HEALTHPASSWDFILE"
[[ -n "$LDAPADDR" ]] && SWLDAP="--add-host pic-ldap:$LDAPADDR"
[[ -n "$SMTPADDR" ]] && SWSMTP="--add-host pic-smtp:$SMTPADDR"
[[ -z "$MARIADBADDR" ]] && SWMARIADB="-v /var/run/mysqld/mysqld.sock:/var/run/mysqld/mysqld.sock"
[[ -n "$MARIADBADDR" ]] && SWMARIADB="--add-host pic-db:$MARIADBADDR"
[[ -n "$MARIADBMYCNF" ]] && SWMARIADBMYCNF="-v $MARIADBMYCNF:/root/.my.cnf:ro"
[[ -n "$SSL_POLICY" ]] && SWSSLPOLICY="--env SSL_POLICY=$SSL_POLICY"

# Crée les variables d'environnement, important pour le proxy nginx qui est devant
source ../createEnv.sh

# Démarrage du conteneur
docker run --restart=unless-stopped \
        --cap-add=DAC_READ_SEARCH \
        --cap-add=SYS_NICE       \
        --security-opt no-new-privileges \
        ${SWLDAP} \
        ${SWSMTP} \
        ${SWMARIADB} \
        -v www-data:/home \
        -v $DIR/log:/var/log \
        -v $DIR/sites-available:/etc/apache2/sites-available \
        -v $DIR/sites-enabled:/etc/apache2/sites-enabled \
        ${SWHEALTH} \
        --env-file env.txt \
        --env BASENAME=$BASENAME \
        --env DOMAINNAME=$DOMAINNAME \
        --env CONTAINER=$CONTAINER \
        $SWSSLPOLICY \
        --net picnet \
        -d --name $CONTAINER $IMAGE

)

