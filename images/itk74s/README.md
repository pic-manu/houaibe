# itk74s

L'image `itk74s` permet de construire un conteneur qui repose sur une debian 10, avec dedans apache2 ainsi que les modules php 7.4 et mpm-itk4

## Ecran de sécurité spip:

Ce conteneur est prévu pour héberger des sites utilisant spip. En effet, la configuration de php est modifiée de sorte que l'écran de sécurité soit lu en premier. Pour que cela fonctionne correctement, il faut que:

- l'écran de sécurité soit à sa place (voir le README.md du répertoire $WORK)

- l'écran de sécurité soit réinstallé quotidiennement, afin de toujours bénéficier de la dernière version (voir le répertoire `cron`)

## Versions de spip

Ce conteneur doit être utilisé pour héberger des sites spip plus avec un spip postérieur à version 3.2.11

## Installation:

- **Construire** le conteneur:

```
cd $WORK/images/itk74s
./build.sh
```

- **Installer** les fichiers et volumes utilisés par le conteneur:

```
sudo ./installcont
```

- **Démarrer** le conteneur:

```
./run.sh
```

## Mise à jour:

Il est bon de reconstruire le conteneur de temps en temps, afin de bénéficier des mises à jour mises à disposition sur le DockerHub:

```
cd $WORK/images/proxy
./build.sh
```

Si `houaibe` a été mis à jour, et donc le Dockerfile ou d'autres fichiers, il est préférable d'ignorer le cache de Docker:

```
cd $WORK/images/proxy
NOCACHE=1 ./build.sh 
```

