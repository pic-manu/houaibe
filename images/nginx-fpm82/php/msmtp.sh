#! /bin/sh


# Mettre l'entree standard dans un fichier temporaire
MAIL="/tmp/msmtpbash.$$"
trap "rm -f $MAIL" EXIT

cat | tee $MAIL > /dev/null

# S'il y a --from ou -f dans les arguments
if $( echo "$@"|fgrep  -e '-f' >/dev/null  2>&1)
then
    cmd="/usr/bin/msmtp $@"

else
    # S'il y a From dans les headers du mail
    if grep -i '^From:' $MAIL >/dev/null 2>&1
    then
       cmd="/usr/bin/msmtp --read-envelope-from $@"

    # S'il n'y a ni l'un ni l'autre
    else
       cmd="/usr/bin/msmtp -f ${USER}@${DOMAINNAME} $@"
    fi
fi

# Avec debug
if [ -d /home/$USER/log/debug ]
then
   # Executer la commande
   STDERR=$($cmd <$MAIL 2>&1)
   STATUS=$?

   DEBUG="/home/$USER/log/debug/msmtp.txt"
   echo $(date) >>$DEBUG
   echo "APPEL   = $0 $@" >> $DEBUG
   echo "COMMAND = $cmd" >> $DEBUG
   echo "STATUS  = $STATUS" >> $DEBUG
   echo "STDERR  = $STDERR" >> $DEBUG
   echo "=================" >> $DEBUG
   cat $MAIL >> $DEBUG
   echo "=================" >> $DEBUG

# Sans debug
else
   $cmd <$MAIL
   STATUS=$?
fi

# Renvoyer le status au programme appelant
exit $STATUS

