#! /bin/bash

#
# Script de construction d'une image
# Ne reconstruit pas les images intermédiaires si elles existent déjà
# Utile pour mettre au point le Dockerfile
#
# Usage: cd /path/to/houaibe/images/itkxxx && ./build.sh
#
# Ce script construit l'image à partir du dockerfile 
#
# Le nom de l'image est dans houaibe.conf 
#
# On construit deux tags: 
#         - %y%m (2110 pour Octobre 2021)
#         - latest
#

#set -v

# Variables générales
if [[ -f ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf ]]; then source ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf; else echo "Merci de terminer la configuration en créant le fichier /usr/local/etc/houaibe/houaibe.conf"; exit 1; fi

# Variables spécifiques à cette image
if [[ -f ./houaibe.conf ]]; then source ./houaibe.conf; else  echo "houaibe n'est pas correctement installé, ou le nom de conteneur est faux..."; exit 1; fi

TAG=$(date +%y%m)
(
   cd php
   docker build -t $PIMAGE:$TAG . 
   docker image tag $PIMAGE:$TAG $PIMAGE:latest 
)
(
   cd nginx
   docker build -t $NIMAGE:$TAG . 
   docker image tag $NIMAGE:$TAG $NIMAGE:latest 
)

