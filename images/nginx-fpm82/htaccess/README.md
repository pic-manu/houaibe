# Modifier les paramètres nginx

Les fichiers se trouvant dans ce répertoire remplacent le fichier `.htaccess` utilisé avec Apache
On doit en choisir un seul **lors de l'installation du CMS**
Le répertoire `htaccess` se trouve **obligatoirement** dans `~/priv`
Il peut être modifié par l'utilisateur, mais il devra prévenir les admins **afin qu'ils redémarrent nginx**
