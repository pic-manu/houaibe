#! /bin/sh

#echo "ici $0 moved $1 to $2"
#
# Calcule le bon user + groupe, à partir du chemin d'accès'
g=$(basename $(dirname $(dirname $2)))
u=w${g}

# Crée le nouveau fichier log avec les $u, $g calculés précédemment
# ATTENTION - Seul le chmod d'un fichier appartenant à root est autorisé
touch $1 && chown root:root $2 && chmod 640 $1 $2 && chown $u:$g $1 $2 
