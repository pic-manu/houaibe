#
# Ce script remplace BASENAME par la variable denvironnement $BASENAME
# dans le fichier:
#  - /etc/nslcd.conf
#
# Enfin il appelle supervisord
#
#
# Emmanuel, le-pic.org

# On vérifie qu'on a bien le serveur pic-ldap et la variable BASENAME
[ "0" == $(grep -c pic-ldap /etc/hosts) ] && echo "ERREUR - Ajoutez le switch --add-host pic-ldap=1.2.3.4 sans serveur ldap, rien ne marche" && exit 0
[ -z "$BASENAME"                        ] && echo "ERREUR - Ajoutez le switch --env BASENAME='dc=exemple',dc=com' à la commande docker run !" && exit 0

sed -i -e "s/BASENAME/${BASENAME}/" /etc/nslcd.conf

# pic-maint reste en vie jusqu'à ce qu'on l'arrête par totoc start'
/usr/sbin/nslcd -n

