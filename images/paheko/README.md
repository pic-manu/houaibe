# paheko

L'image `paheko` permet de construire un conteneur qui repose sur une debian, avec dedans `apache2`, `mod_php`, et le logiciel `paheko`

## Installation:

- **Construire** le conteneur:

```
cd $WORK/images/paheko
./build.sh
```

- **Installer** les fichiers et volumes utilisés par le conteneur:

```
sudo ./installcont
```

- **Vérifier** et éventuellement **modifier** le fichier `$CONTAINERSDIR/config.local.php` 

- **Démarrer** le conteneur:

```
toctoc restart pic-paheko
```

- **Ajouter** des lignes dans le crontab:

```
07 03 * * * docker exec -u www-data pic-paheko /src/factory_cron.sh >/dev/null
*/5 * * * * docker exec -u www-data pic-paheko /src/factory_cron_emails.sh >/dev/null
15 00 * * * docker exec pic-paheko /src/factory_cron_backup.sh >/dev/null
```

**Note** - Le script factory_cron_backup.sh crée pour chaque association une copie de la base de données sqlite dans un répertoire `db`

## Mise à jour:

```
cd $WORK/images/paheko
NOCACHE=1 ./build.sh 
toctoc restart pic-paheko
```

Les bases de données sqlite seront mises à jour automatiquement lors de l'utilisation suivante.

## Lister toutes les associations

```
docker exec pic-paheko ls -l /src/data/users
```

## Ajouter une association

### Ajouter une URL en `https://toto.DOMAINNAME` (ex. `toto.le-pic.org`)

```
source /usr/local/houaibe/etc/houaibe.conf 
docker exec pic-paheko sh -c "mkdir /src/data/users/toto && chown 33:33 /src/data/users/toto"
sudo sed -i -e "s/#ServerAlias PROD/#ServerAlias PROD\n    ServerAlias toto.$DOMAINNAME\n/" $CONTAINERSDIR/pic-paheko/sites-available/000-default.conf
toctoc restart pic-paheko
```

### Ajouter une URL en `https://toto.exemple.org`

```
source /usr/local/houaibe/etc/houaibe.conf 
docker exec pic-paheko sh -c "mkdir /src/data/users/toto.exemple.org && chown 33:33 /src/data/users/toto.exemple.org"
sudo sed -i -e "s/#ServerAlias PROD/#ServerAlias PROD\n    ServerAlias toto.exemple.org\n/" $CONTAINERSDIR/pic-paheko/sites-available/000-default.conf
toctoc restart pic-paheko
```

**Attention**: Penser à générer un certificat pour cette URL !!!

## Supprimer une association

### Supprimer une URL en `https://toto.DOMAINNAME` (ex. `toto.le-pic.org`)

```
source /usr/local/houaibe/etc/houaibe.conf 
docker exec pic-paheko rm -rf /src/data/users/toto
sudo sed -i -e "/ServerAlias toto.$DOMAINNAME/d" $CONTAINERSDIR/pic-paheko/sites-available/000-default.conf
toctoc restart pic-paheko
```

### Pour une URL en `https://toto.exemple.org`

```
source /usr/local/houaibe/etc/houaibe.conf 
docker exec pic-paheko sh -c "mkdir /src/data/users/toto.exemple.org && chown 33:33 /src/data/users/toto.exemple.org"
sudo sed -i -e "/ServerAlias toto.exemple.org/D" $CONTAINERSDIR/pic-paheko/sites-available/000-default.conf
toctoc restart pic-paheko
```





