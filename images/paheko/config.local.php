<?php

namespace Paheko;

/**
 * Ce fichier permet de configurer Paheko pour une utilisation
 * avec plusieurs associations, mais une seule copie du code source.
 * (aussi appelé installation multi-sites, ferme ou usine)
 *
 * Voir la doc : https://fossil.kd2.org/paheko/wiki?name=Multi-sites
 *
 * N'oubliez pas d'installer également le script cron.sh fournit
 * pour lancer les rappels automatiques et sauvegardes.
 *
 * Si cela ne suffit pas à vos besoins, contactez-nous :
 * https://paheko.cloud/contact
 * pour une aide spécifique à votre installation.
 */
 
// VERSION MODIFIEE PAR Emmanuel Courcelle (manu) pour intégration à houaibe

// Décommenter cette ligne si vous n'utilisez pas NFS,
// pour rendre les bases de données plus rapides.
//
// Si vous utilisez NFS, décommenter cette ligne risque
// de provoquer des corruptions de base de données !
const SQLITE_JOURNAL_MODE = 'WAL';

// Nom de domaine parent des associations hébergées
// Exemple : si vos associations sont hébergées en clubdetennis.paheko.cloud,
// indiquer ici 'paheko.cloud'
// const FACTORY_DOMAIN = 'monsite.tld';
const FACTORY_DOMAIN = 'DOMAINNAME';

// Répertoire où seront stockées les données des utilisateurs
// Dans ce répertoire, un sous-répertoire sera créé pour chaque compte
// Ainsi 'clubdetennis.paheko.cloud' sera dans le répertoire courant (__DIR__),
// sous-répertoire 'users' et dans celui-ci, sous-répertoire 'clubdetennis'
//
// Pour chaque utilisateur il faudra créer le sous-répertoire en premier lieu
// (eg. mkdir .../users/clubdetennis)
const FACTORY_USER_DIRECTORY = __DIR__ . '/data/users';

// Envoyer les erreurs PHP par mail à l'adresse de l'administrateur système
// (mettre à null pour ne pas recevoir d'erreurs)
//const MAIL_ERRORS = 'administrateur@monsite.tld';

// IMPORTANT !
// Modifier pour indiquer une valeur aléatoire de plus de 30 caractères
//const SECRET_KEY = 'Indiquer ici une valeur aléatoire !';
const SECRET_KEY = 'PAHEKOKEY';

// Quota de stockage de documents (en octets)
// Définit la taille de stockage disponible pour chaque association pour ses documents
// const FILE_STORAGE_QUOTA = 1 * 1024 * 1024 * 1024; // 1 Go
const FILE_STORAGE_QUOTA = 1 * 1024 * 1024 * 128; // 1 Mo

////////////////////////////////////////////////////////////////
// Réglages conseillés, normalement il n'y a rien à modifier ici

// Indiquer que l'on va utiliser cron pour lancer les tâches à exécuter (envoi de rappels de cotisation)
const USE_CRON = true;

/**
 * Répertoire où est situé le cache,
 * exemples : graphiques de statistiques, templates Brindille, etc.
 *
 * Défaut : sous-répertoire 'cache' de DATA_ROOT
 */

//const CACHE_ROOT = DATA_ROOT . '/cache';
// manu - cacheroot est un volume monté en rw
const CACHE_ROOT = '/src/data/cacheroot/cache';

/**
 * Répertoire où est situé le cache partagé entre instances
 * Paheko utilisera ce répertoire pour stocker le cache susceptible d'être partagé entre instances, comme
 * le code PHP généré à partir des templates Smartyer.
 *
 * Défaut : sous-répertoire 'shared' de CACHE_ROOT
 */

const SHARED_CACHE_ROOT = CACHE_ROOT . '/shared';

/**
 * Motif qui détermine l'emplacement des fichiers de cache du site web.
 *
 * Le site web peut créer des fichiers de cache pour les pages et catégories.
 * Ensuite le serveur web (Apache) servira ces fichiers directement, sans faire
 * appel au PHP, permettant de supporter beaucoup de trafic si le site web
 * a une vague de popularité.
 *
 * Certaines valeurs sont remplacées :
 * %host% = hash MD5 du hostname (utile en cas d'hébergement de plusieurs instances)
 * %host.2% = 2 premiers caractères du hash MD5 du hostname
 *
 * Utiliser NULL pour désactiver le cache.
 *
 * Défault : CACHE_ROOT . '/web/%host%'
 *
 * @var null|string
 */

const WEB_CACHE_ROOT = CACHE_ROOT . '/web/%host%';

// Désactiver le log des erreurs PHP visible dans l'interface (sécurité)
const ENABLE_TECH_DETAILS = false;

// Désactiver les mises à jour depuis l'interface web
// Pour être sûr que seul l'admin sys puisse faire des mises à jour
const ENABLE_UPGRADES = false;

// Ne pas afficher les erreurs de code PHP
const SHOW_ERRORS = false;

////////////////////////////////////////////////////////////////
// Code 'magique' qui va configurer Paheko selon les réglages
//
// manu - On accepte les noms: toto dont l'URL sera toto.DOMAINNAME
//                             toto.exemple.org dont l'URL sera toto.exemple.org
//

$login = null;

// Un sous-domaine ne peut pas faire plus de 63 caractères
$login_regexp = '([a-z0-9_-]{1,63})';
$domain_regexp = sprintf('/^%s\.%s$/', $login_regexp, preg_quote(FACTORY_DOMAIN, '/'));
if (isset($_SERVER['SERVER_NAME'])) { 
	if (preg_match($domain_regexp, $_SERVER['SERVER_NAME'], $match)) {
		$login = $match[1];
		define('Paheko\WWW_URL', 'https://' . $login . '.' . FACTORY_DOMAIN . '/');
	} else {
		$login = $_SERVER['SERVER_NAME'];
		define('Paheko\WWW_URL', 'https://' . $login . '/');
	}
	
	// Définir l'URL
	define('Paheko\WWW_URI', '/');
}
elseif (PHP_SAPI == 'cli' && !empty($_SERVER['PAHEKO_FACTORY_USER'])) {
       if (preg_match('/^' . $login_regexp . '$/', $_SERVER['PAHEKO_FACTORY_USER'])) {
		$login = $_SERVER['PAHEKO_FACTORY_USER'];
		define('Paheko\WWW_URL', 'https://' . $login . '.' . FACTORY_DOMAIN . '/');
	} else {
		$login = $_SERVER['PAHEKO_FACTORY_USER'];
		define('Paheko\WWW_URL', 'https://' . $login . '/');
	}
	
	// Définir l'URL
	define('Paheko\WWW_URI', '/');
} else {
	http_response_code(404);
	die('<h1>Page non trouvée</h1>');
}

$user_data_dir = rtrim(FACTORY_USER_DIRECTORY, '/') . '/' . $login;

if (!is_dir($user_data_dir)) {
	http_response_code(404);
	die("<h1>$user_data_dir Cette association n'existe pas.</h1>");
}

// Définir le dossier où sont stockées les données
define('Paheko\DATA_ROOT', $user_data_dir);


// Ajouté par manu

/**
 * Emplacement de stockage des plugins
 *
 * Défaut : DATA_ROOT . '/plugins'
 */

const PLUGINS_ROOT = '/src/data/plugins';

/**
 * Hôte du serveur SMTP, mettre à false (défaut) pour utiliser la fonction
 * mail() de PHP
 *
 * Défaut : false
 */

// hum pas fameux, si cela ne marche pas mettez le nom DNS du serveur SMTP plutôt que l'IP
// Avec l'IP vous aurez des problèmes de certificat !
const SMTP_HOST = "SMTPADDR";

/**
 * Port du serveur SMTP
 *
 * 25 = port standard pour connexion non chiffrée (465 pour Gmail)
 * 587 = port standard pour connexion SSL
 *
 * Défaut : 587
 */

//const SMTP_PORT = 587;

/**
 * Login utilisateur pour le server SMTP
 *
 * mettre à null pour utiliser un serveur local ou anonyme
 *
 * Défaut : null
 */

//const SMTP_USER = 'paheko@monserveur.com';

/**
 * Mot de passe pour le serveur SMTP
 *
 * mettre à null pour utiliser un serveur local ou anonyme
 *
 * Défaut : null
 */

//const SMTP_PASSWORD = 'abcd';

/**
 * Sécurité du serveur SMTP
 *
 * NONE = pas de chiffrement
 * SSL = connexion SSL native
 * TLS = connexion TLS native (le plus sécurisé)
 * STARTTLS = utilisation de STARTTLS (moyennement sécurisé)
 *
 * Défaut : STARTTLS
 */

//const SMTP_SECURITY = 'TLS';

/**
 * Nom du serveur utilisé dans le HELO SMTP
 *
 * Si NULL, alors le nom renseigné comme SERVER_NAME (premier nom du virtual host Apache)
 * sera utilisé.
 *
 * Defaut : NULL
 *
 * @var null|string
 */

//const SMTP_HELO_HOSTNAME = 'mail.domain.tld';
const SMTP_HELO_HOSTNAME = "DOMAINNAME";

/**
 * Adresse e-mail destinée à recevoir les erreurs de mail
 * (adresses invalides etc.) — Return-Path
 *
 * Si laissé NULL, alors l'adresse e-mail de l'association sera utilisée.
 * En cas d'hébergement de plusieurs associations, il est conseillé
 * d'utiliser une adresse par association.
 *
 * Voir la documentation de configuration sur des exemples de scripts
 * permettant de traiter les mails reçus à cette adresse.
 *
 * Défaut : null
 */

//const MAIL_RETURN_PATH = 'returns@monserveur.com';


/**
 * Adresse e-mail expéditrice des messages (Sender)
 *
 * Si vous envoyez des mails pour plusieurs associations, il est souhaitable
 * de forcer l'adresse d'expéditeur des messages pour passer les règles SPF et DKIM.
 *
 * Dans ce cas l'adresse de l'association sera indiquée en "Reply-To", et
 * l'adresse contenue dans MAIL_SENDER sera dans le From.
 *
 * Si laissé NULL, c'est l'adresse de l'association indiquée dans la configuration
 * qui sera utilisée.
 *
 * Défaut : null
 */

const MAIL_SENDER = 'paheko-ne-pas-repondre@DOMAINNAME';

/**
 * Informations légales sur l'hébergeur
 *
 * Ce texte (HTML) est affiché en bas de la page "mentions légales"
 * (.../admin/legal.php)
 *
 * S'il est omis, l'association sera indiquée comme étant auto-hébergée.
 *
 * Défaut : null
 *
 * @var  string|null
 */
//const LEGAL_HOSTING_DETAILS = 'OVH<br />5 rue de l'hébergement<br />ROUBAIX';

/**
 * Message d'avertissement
 *
 * Sera affiché en haut de toutes les pages de l'administration.
 *
 * Code HTML autorisé.
 * Utiliser NULL pour désactiver le message.
 *
 * Défaut : null
 *
 * @var null|string
 */
// const ALERT_MESSAGE = '';

