#!/bin/sh

# Répertoire où sont stockées les données des utilisateurs
# veiller à ce que ce soit le même que dans config.local.php
FACTORY_USER_DIRECTORY="/src/data/users"

cd $FACTORY_USER_DIRECTORY
for user in $(ls -1d */)
do
    mkdir -p $user/db \
    && rm -f $user/db/backup.sqlite \
    && [ -f $user/association.sqlite ] \
    && sqlite3 $user/association.sqlite "VACUUM INTO '$user/db/backup.sqlite'"
done

