#!/bin/sh

# Répertoire où sont stockées les données des utilisateurs
# veiller à ce que ce soit le même que dans config.local.php
FACTORY_USER_DIRECTORY="/src/data/users"

# Chemin vers la commande cron de Paheko
PAHEKO_CRON_SCRIPT="/src/bin/paheko cron"

for user in $(cd ${FACTORY_USER_DIRECTORY} && ls -1d */)
do
	USER=$(basename "$user")
	#echo PAHEKO_FACTORY_USER=$USER php $PAHEKO_CRON_SCRIPT
        [ -f ${FACTORY_USER_DIRECTORY}/$USER/association.sqlite ] && PAHEKO_FACTORY_USER=$USER php $PAHEKO_CRON_SCRIPT
done

