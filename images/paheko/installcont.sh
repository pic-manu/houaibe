#! /bin/bash

#
# Script d'installation du répertoire associé au conteneur 
#
# Usage: cd ....
#        ./installcont.sh
#

[[ "$(id -u)" -ne "0" ]] && echo "VOUS DEVEZ ETRE ROOT !" && exit 1

if [[ ! -f ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf ]]
then
   echo "houaibe n'est pas encore configuré"
   exit 1;
fi

# Lire les paramètres généraux et spécifiques
. ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf
. ./houaibe.conf

# Reprtoire courant
DIR=$CONTAINERSDIR/$CONTAINER

# Rien à faire si le répertoire existe déjà
[[ -d $DIR ]] && echo "Le repertoire $DIR existe - Je ne fais rien" && exit 0

echo "installation du conteneur $CONTAINER"
mkdir -p $DIR
chmod g+w $CONTAINERSDIR/$CONTAINER

# Copie de fichiers
cp run.sh.dist $DIR/run.sh
cp stop.sh.dist $DIR/stop.sh
ln -sf $DIR/run.sh .
ln -sf $DIR/stop.sh .
cp houaibe.conf $DIR

IMGDIR=$(pwd)

(
    cd $DIR
    chmod 550 run.sh

    # ----- Début de partie spécifique à chaque conteneur
    mkdir -p sites-available sites-enabled
    mkdir -p log/apache2 paheko-cacheroot 

    # Creer un fichier vide env.txt et le rendre rw-rw----
    touch env.txt && chmod 660 env.txt
    
    # Copier plusieurs fichiers 
    cp -r $IMGDIR/mpm_prefork.conf $IMGDIR/php.ini $IMGDIR/sites-available $IMGDIR/config.local.php .
    (cd sites-enabled && ln -s ../sites-available/000-default.conf)
    chown -R root:docker mpm_prefork.conf php.ini sites-available sites-enabled
    chmod 650 mpm_prefork.conf php.ini sites-available sites-enabled

    # Création de quelques sous-répertoires 
    mkdir -p log/apache2
    ( cd paheko-cacheroot && mkdir -p cache/shared cache/web && chown -R www-data: cache && chmod -R ug=rwX cache )

    # Garder les sessions php persistantes
    mkdir $DIR/php-sessions && chmod u=rwx,go=wx $DIR/php-sessions && chmod o+t $DIR/php-sessions

    # Edition de config.local.php et 000-default.conf
    PAHEKOKEY=$(openssl rand -base64 47)
    sed -i -e "s/DOMAINNAME/${DOMAINNAME}/" -e "s/ADMINMAIL/${ADMINMAIL}/" -e "s!PAHEKOKEY!${PAHEKOKEY}!" -e "s/SMTPADDR/${SMTPADDR}/" config.local.php
    sed -i -e "s/DOMAINNAME/${DOMAINNAME}/" sites-available/000-default.conf

    # ----- Fin de partie spécifique
)

# Donner au groupe docker la permission de bosser !
chgrp -R docker $DIR


