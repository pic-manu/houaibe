# deb4pic

L'image `deb4pic` est un conteneur intermédiaire, utilisé dans les conteneurs pic-itkxxx

## Utilisation de msmtp:

Beaucoup de CMS écrits en php envoient des mails. deb4pic installe les outils suivants pour envoyer des mails avec php:

- Le binaire /usr/bin/msmtp
- Le script `/usr/bin/msmtp.bash` s'interpose entre le code php et msmtp. Il permet de s'assurer que les headers et les paramètres déclarés dans l'enveloppe sont corrects, afin que le mail soit bien formé et non vu comme un spam
- La configuration de php est réalisée dans les conteneurs `itk-xxx`, le fichier de configuration est: /etc/php/x.yz/apache2/conf.d/php

### Journaux et déboguage:

- `msmtp` écrira ses journaux dans `/usr/local/houaibe/containers/pic-itkxxx/log/msmtp/msmtp.log`
- Si un CMS n'arrive pas à envoyer les mails, il est possible de créer un répertoire `debug` afin de conserver le mail d'origine, ses headers, la commande employée:

```
cd /usr/local/houaibe/containers/pic-itkxxx/log/msmtp
mkdir debug
chmod 777 debug
```

## Mise à jour:

Il est bon de reconstruire le conteneur de temps en temps, afin de bénéficier des mises à jour mises à disposition sur le DockerHub:

```
cd $WORK/images/proxy
./build.sh
```

Si `houaibe` a été mis à jour, et donc le Dockerfile ou d'autres fichiers, il est préférable d'ignorer le cache de Docker:

```
cd $WORK/images/proxy
NOCACHE=1 ./build.sh 
```

