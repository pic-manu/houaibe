#! /bin/bash


# Mettre l'entree standard dans un fichier temporaire
MAIL="/tmp/msmtpbash.$$"
trap "rm -f $MAIL" EXIT

cat | tee $MAIL > /dev/null

# S'il y a --from ou -f dans les arguments
if grep -i -e '-f' <<<"$@" >/dev/null 2>&1
then
    cmd="/usr/bin/msmtp $@"

else
    # S'il y a From dans les headers du mail
    if grep -i '^From:' $MAIL >/dev/null 2>&1
    then
       cmd="/usr/bin/msmtp --read-envelope-from $@"

    # S'il n'y a ni l'un ni l'autre
    else
       cmd="/usr/bin/msmtp -f $(id -nu 2>/dev/null)@${DOMAINNAME} $@"
    fi
fi

# Avec debug
if [ -d /var/log/msmtp/debug ]
then
   # Executer la commande
   STDERR=$($cmd <$MAIL 2>&1)
   STATUS=$?

   DEBUG="/var/log/msmtp/debug/$(id -nu 2>/dev/null).txt"
   echo $(date) >>$DEBUG
   echo "APPEL   = $0 $@" >> $DEBUG
   echo "COMMAND = $cmd" >> $DEBUG
   echo "STATUS  = $STATUS" >> $DEBUG
   echo "STDERR  = $STDERR" >> $DEBUG
   echo "=================" >> $DEBUG
   cat $MAIL >> $DEBUG
   echo "=================" >> $DEBUG

# Sans debug
else
   $cmd <$MAIL
   STATUS=$?
fi

# Renvoyer le status au programme appelant
exit $STATUS

