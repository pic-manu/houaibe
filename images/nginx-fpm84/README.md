# nginx-fpmxy

Les images `nginx-fpmxy` permet de construire un ensemble de deux conteneurs qui reposent sur nginx et php (version x.y) en mode fpm
`docker compose` est utilisé pour gérer ces conteneurs

## Installation:

- **Construire** les conteneurs:

```
./build.sh
```

- **Installer** le template (format tgz) qui sera utilisé lors de la création des conteneurs proprement dits (donc du site web):

```
sudo ./installtmpl
```

- **Construire un conteneur utilisant cette image**:

```
picgs ajout ...
```

## Mise à jour:

Il est bon de reconstruire le conteneur de temps en temps, afin de bénéficier des mises à jour mises à disposition sur le DockerHub:

```
./build.sh
```

Si `houaibe` a été mis à jour, et donc le Dockerfile ou d'autres fichiers, il est préférable d'ignorer le cache de Docker:

```
NOCACHE=1 ./build.sh 
```

## Redémarrage:

Pour redémarrer tous les conteneurs ayant comme image `nginx-fpm84` (par exemple après mise à jour):

```
toctoc restart nginx-fpm84
```

## Les conteneurs nginx-fpmXY:

### Le principe:

Lorsqu'on utilise ce type de conteneurs, la situation est assez différente de celle rencontrée avec les conteneurs du genre `pic-itkxy`: 

- Pour chaque site web, on a {{deux conteneurs}} reliés par un <code>docker-compose</code>, alors que dans le cas <code>pic-itkxy</code>, le conteneur contient tous les sites web utilisant php x.y
- Ces deux conteneurs contiennent un serveur nginx, qui jouera le rôle d'un reverse proxy, et un démon php en mode Fastcgi Php Manager
- Les conteneurs sont donc générés lors de l'installation d'un nouveau site web (<code>picgs ajout</code>,voir [ici->article33]) ou lors d'un changement de conteneur (<code>picgs demenagement</code>)
- Le répertoire contenant les fichiers de paramètres se trouve dans <code>/containers</code>, et a pour nom <code>pic-monasso-st1</code>
- Les deux conteneurs (nginx et php) tournent sous le compte <code>monasso-st1</code>, donc {{pas}} root, ce qui améliore la sécurité

### Ajouter un hébergement web nginx:

La commande <code>picgs ajout</code> doit être utilisée:



```
picgs ajout monasso site`
`...`
`Entrer le conteneur (parmi ['pic-itk00', 'pic-itk81', 'pic-itk82', 'pic-itk84', 'pic-itk82s', 'pic-monasso-st1']) []pic-monasso-st1`
`Entrer l'image (parmi ['nginx-fpm81', 'nginx-fpm82', 'nginx-fpm83','nginx-fpm84']) []nginx-fpm84`
```

### Modifier les paramètres par défaut

Trois fichiers devront éventuellement être modifiés si les paramètres par défaut se révèlent insuffisants.

### Qui peut les modifier ?

Tous les fichiers définissant les paramètres des conteneurs ou des démons enfermées dedans ont les permissions `rw-rw-r--`, ce qui permet à toutes les personnes appartenant au groupe docker de les modifier. 

### houaibe.conf

Le fichier houaibe.conf contient la ligne:

```export PLIMIT_MEMORY=128M```

Il s'agit de la limite de mémoire pouvant être utilisée par le conteneur php. Si elle s'avère insuffisante, il peut être utile de l'augmenter.

### php-suppl.d/

Ce répertoire contient les fichiers de configuration de php (du moins ceux qui sont modifiables au runtime). Il y a par défaut deux fichiers:

- `memory.ini`  La mémoire que php peut prendre lors d'une requête
- `upload-size.ini` Les paramètres de téléversement (taille maximum etc)
- On peut rajouter d'autres fichiers s'il y a d'autres paramètres à renseigner. Dans ce cas, les permissions du fichier doivent être `rw-rw-r--`

Traitement de gros fichiers images avec wordpress: Si les utilisateurs ne peuvent retraiter de gros fichiers d'image .jpg (erreur lors du téléversement), il faut intervenir dans les fichiers `houaibe.conf` et `php-suppl.d/memory.conf` (voir les commentaires dans ces fichiers)

### templates

Ce répertoire contient le fichier de configuration de nginx. En principe il ne devrait pas y avoir besoin de les modifier...

### user.txt

Ce fichier contient le nom de l'utilisateur qui exécutera les démons nginx et php. Ne pas modifier !

### prod.txt

Ce fichier est présent si le site a une ou plusieurs adresse de production. A manipuler avec `picgs prod`

### DESACTIVE

Si ce fichier est présent, le site est désactivé. A manipuler avec <code>picgs desactiver</code> etc.
