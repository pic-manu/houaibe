# Modifier les paramètres php

Les fichiers se trouvant dans ce répertoire permettent de modifier les paramètres php de ce site.
Il est préférable de mettre **un** fichier par fonctionnalité.

Les fichiers par défaut sont:

- `upload-size.conf` pour augmenter la taille des fichiers pouvant être téléversés (35 Mo)
- `memory-size.conf` pour augmenter la taille mémoire utilisable par un code php (defaut = 128 Mo)
- `include-path.conf` pour permettre d'utiliser phpmailer


