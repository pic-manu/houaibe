#
# Ce script remplace DOMAINNAME par la variable d'environnement $DOMAINNAME
# dans les fichiers:
#  - /etc/mailname
#  - /etc/update-exim4.conf.conf
#
# Ensuite il remplace BASENAME par la varible denvironnement $BASENAME
# dans le fichier:
#  - /etc/nslcd.conf
#
# Puis il ajoute une commande umask dans /etc/apache2/envvars
#
# Enfin il appelle supervisord
#
#
# E. Courcelle, le-pic.org

# On vérifie qu'on a bien le serveur pic-ldap et la variable BASENAME
# TODO - SUPPRIMER -  A PARTIR DE HOUAIBE 3.20.0 C'EST FAIT DANS L'INSTALL
[ "0" == $(grep -c pic-ldap /etc/hosts) ] && echo "ERREUR - Ajoutez le switch --add-host pic-ldap=1.2.3.4 sans serveur ldap, rien ne marche" && exit 0
[ -z "$BASENAME"                        ] && echo "ERREUR - Ajoutez le switch --env BASENAME='dc=exemple',dc=com' à la commande docker run !" && exit 0

# Si on ne peut pas utiliser le mail, imprimer un warning
# TODO - SUPPRIMER -  A PARTIR DE HOUAIBE 3.20.0 C'EST FAIT DANS L'INSTALL
[ "0" == $(grep -c pic-smtp /etc/hosts) ] && echo "ATTENTION - Si vous voulez que vos sites puissent envoyer des mails, ajoutez --add-host pic-smtp=1.2.3.4  à la commande docker run !"
[ -z "$DOMAINNAME"                      ] && echo "ATTENTION - si vous voulez que vos sites puissent envoyer des mails, ajoutez --env DOMAINNAME=exemple.com à la commande docker run !"

# TODO - SUPPRIMER -  A PARTIR DE HOUAIBE 3.20.0 C'EST FAIT DANS L'INSTALL
sed -i -e "s/DOMAINNAME/${DOMAINNAME}/" /etc/msmtprc
sed -i -e "s/CONTAINER/${CONTAINER}/" /etc/msmtprc
sed -i -e "s/BASENAME/${BASENAME}/" /etc/nslcd.conf

# Cet umask assure que les permissions des fichiers créés par apache soient rw-rw----
# TODO - SUPPRIMER -  A PARTIR DE HOUAIBE 3.20.0 C'EST DANS /tmp/apache2/envvars
echo "umask g=rwx,o=" >> /etc/apache2/envvars

# A PARTIR DE HOUAIBE 3.20.0 ON ME envvars DANS /tmp pour pouvoir le modifier !
mkdir /tmp/apache2 && cp -a /etc/apache2/envvars /tmp/apache2 && echo "umask g=rwx,o=" >> /tmp/apache2/envvars

# Création de quelques sous-répertoires ou fichiers de /var/log, qui est persistent
# TODO - SUPPRIMER -  A PARTIR DE HOUAIBE 3.20.0 C'EST FAIT DANS L'INSTALL
mkdir -p /var/log/apache2 /var/log/supervisor /var/log/msmtp && touch /var/log/msmtp/msmtp.log 
#chgrp msmtp /var/log/msmtp 
chmod a=rwX /var/log/msmtp /var/log/msmtp/msmtp.log

# DEPUIS HOUAIBE 3.20 /var/run EST UN tmpfs DONC IL FAUT RECREER CE REPERTOIRE
mkdir /var/run/apache2 /run/lock

exec /bin/supervisord -c /etc/supervisor/supervisord.conf
#sleep 1000

