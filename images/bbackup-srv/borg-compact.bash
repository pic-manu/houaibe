#! /bin/bash

#
# This file is part of the houaibe software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# Ce script itère à travers tous les répertoires se trouvant sous /backup
# Pour chaque répertoire identifié comme borg repository:
#
# (Seulement le dimanche)
#    Configure append_only -> 0
#    borg compact
#
# (tous les jours)
#    Configure append_only -> 1
#
# Ainsi le dépôt est mis sur append_only, ce qui est plus sûr, et borg compact est appelé une fois pas semaine
#
# Le conteneur est arrêté à la fin du script
#
#

# -------------------------------------------------------------------
# Fonctions

#
# Appelle le script is_borg_repo.bash qui se trouve obligatoirement en local
# Puisqu'on tourne sur le serveur'
#

function is_borg_repo() {
    rep=$("/is_borg_repo.bash" $1)
    echo $rep
}

# main

#set -vx

# Quelques variables globales
BORG_BINARY=/bin/borg
BORG_LOGD=/var/log/borg-compact
BORG_LOG=${BORG_LOGD}/borg-compact.$(date +%j).log

w=$(date +%w)
mkdir -p $BORG_LOGD
for d in /backup/*
do
    if [ -d ${d} ]
    then
        IBD=$(is_borg_repo ${d})
        if [ $IBD = "OK" ]
        then
            u=$(stat --format='%u' ${d} )
            if [ ${u} -ne 0 ]
            then
                SUDO="sudo -u #${u} "
            else
                SUDO=""
            fi

            # On compacte seulement le dimanche
            if [ $w -eq 0 ]
            then
                ${SUDO}${BORG_BINARY} config ${d} append_only 0
                ${SUDO}${BORG_BINARY} compact -v ${d}  >> $BORG_LOG 2>&1
            fi
            
            # On repasse tous les jours en mode append_only
            ${SUDO}${BORG_BINARY} config ${d} append_only 1
        fi
    fi
done

# Arrêter le conteneur
/bin/supervisorctl shutdown


