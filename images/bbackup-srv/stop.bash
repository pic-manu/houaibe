#! /bin/bash

#
# This file is part of the houaibe software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

# Arrête le conteneur dans certaines conditions:
#
# Vérifie la présence des fichiers /tmp/*.running
# S'il y en a un:
#   exit 0
# fin si

# vérifie l'âge du conteneur (nb de secondes depuis le démarrage)
# Si inférieur à $AGE_MIN:
#   exit 0
# fin si

# Arrête le conteneur
#

SUPERVISORCTL=/bin/supervisorctl

# On doit rester au moins $BACKUP_SSH_MIN_UP
AGE_MIN=$(cat /tmp/BACKUP_SSH_MIN_UP)
TIMESTAMP=$(date +%s)
START_TIMESTAMP=$(cat /tmp/START_TIMESTAMP)
AGE=$(( TIMESTAMP - START_TIMESTAMP ))


if ls /tmp/*.running >/dev/null 2>&1 >/dev/null
then
    logger "STOP: $(ls /tmp/*.running) - PAS DE SHUTDOWN"
    exit 0
fi

if [ $AGE -lt $AGE_MIN ]
then
    logger "STOP: SEULEMENT ${AGE}s UP - PAS DE SHUTDOWN"
    exit 0
fi

logger "STOP: /bin/supervisorctl shutdown"
sudo $SUPERVISORCTL shutdown

