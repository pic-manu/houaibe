#! /bin/bash

#
# Script d'ajout d'un client de sauvegarde
#
# Ce script doit être joué sur le SERVEUR DE SAUVEGARDES
# Il fait les actions suivantes:
#    - Demander le nom du nouveau client
#    - Ajouter une ligne aux fichiers passwd et group
#    - Créer et initialiser le fichier home/nouveau-client/.ssh/authorized_keys
#

#set -xv

# Calcul du repertoire dans lequel le script est depose - ca marche meme si on part d'un lien symbolique
fullpath=$(readlink --no-newline --canonicalize-existing $0) 
if [[ -z $fullpath ]]; then echo "ERREUR - $0 pointe sur le vide !"; exit 1; fi
DIR=$(dirname "$fullpath")

# Si on n'est pas root, abandon
[ $(id -u) -ne 0 ] && echo "Il faut être root pour exécuter ce script" && exit 1

(
cd $DIR

# Variables générales
#if [[ -f ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf ]]; then source ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf; else echo "Merci de terminer la configuration en créant le fichier /usr/local/etc/houaibe/houaibe.conf"; exit 1; fi

# Variables spécifiques à cette image
if [[ -f ./houaibe.conf ]]; then source ./houaibe.conf; else  echo "houaibe n'est pas correctement installé, ou le nom de conteneur est faux..."; exit 1; fi

# Utilisateur min
USERMIN=10000

# Demander le nom du nouveau client
read -p "Nom du nouveau client (par exemple 'hostname -s' sur la machine cliente) : " BACKUP_CLIENT
[ -z "$BACKUP_CLIENT" ] && BACKUP_CLIENT=" "
if [ -n "$(LANG=C sed -e 's/[a-z0-9-]//g' <<<$BACKUP_CLIENT)" ]
then
    echo "Ce nom n'est pas valide: $BACKUP_CLIENT"
    echo "Caractères acceptés= abcdefghijklmnopqrstuvwxyz0123456789-_"
    exit 1
fi

PASSWD="$(pwd)/passwd"
SHADOW="$(pwd)/shadow"

if [ \! -f $PASSWD ] 
then
    echo "Le fichier $PASSWD n'existe pas"
    exit 1
fi

# Le client existe-t-il déjà ?
if [ $(cut -d: -f1 ${PASSWD} | grep -cw ${BACKUP_CLIENT}) -ne 0 ]
then
    echo "Le client ${BACKUP_CLIENT} est DEJA enregistré !"
    exit 1
fi

# Calculer un uid
USERID=$(( $(cut -d: -f 3 $PASSWD | sort -nur | head -1) + 1 ))

# Sauf qu'il doit être >= $USERMIN
[ $USERID -lt $USERMIN ] && USERID=$USERMIN

echo "${BACKUP_CLIENT}:x:${USERID}:0:Client borg ${BACKUP_CLIENT}:/home/${BACKUP_CLIENT}:/bin/bash" >> $PASSWD

# Ajouter une ligne dans le fichier shadow
echo "${BACKUP_CLIENT}:*:19000:0:99999:7:::" >> $SHADOW

# Initialiser le fichier authorized_keys
HDIR="$(pwd)/home/${BACKUP_CLIENT}"
AUTH="${HDIR}/.ssh/authorized_keys"

mkdir ${HDIR} && chmod g+rx ${HDIR}
mkdir "${HDIR}/.ssh"
echo "command=\"/borg-wrapper.bash\" ssh-xxxxxxxx votre-cle-publique-ssh root@${BACKUP_CLIENT}" > ${AUTH}
chown -R ${USERID} ${HDIR} && chmod -R go= ${HDIR} && chmod -R u-w ${HDIR}

# Comment configurer la machine à sauvegarder
echo "Configuration sur la machine cliente: BACKUP_REPO_PREFIX_HOST=${BACKUP_CLIENT}@$(hostname -f)"

)
