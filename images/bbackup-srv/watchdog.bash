#! /bin/bash

#
# This file is part of the houaibe software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

# Dort tout le temps, se réveille au bout de $BACKUP_SSH_MIN_UP secondes
# Appelle /stop.bash, ce qui arrêtera le conteneur s'il ne se passe rien
#
STOP=/stop.bash

# On se réveiolle toutes les $BACKUP_SSH_MIN_UP
AGE_MIN=$(cat /tmp/BACKUP_SSH_MIN_UP)

while true
do
    sleep $AGE_MIN
    /stop.bash
done
