#!/bin/bash

#
# This file is part of the houaibe software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# Usage: isborgrepo.bash directory
#

#
# Verifie si le répertoire passé en paramètres est bien un dépôt Borgbackup
#
#    - Si Oui: Renvoie OK
#    - Si le répertoire est vide ou inexistant: Renvoie INIT, on peut l'initialiser
#    - Si le répertoire n'est pas vide: Renvoie KO
#

d=$1
if [ -z "$d" ]
then
  echo "KO";
  exit 0;
fi

if [ "$(head -1 $d/README 2>/dev/null)" = "This is a Borg Backup repository." ]
then 
  echo "OK"
  exit 0
else
  if [ -z "$(ls $d 2>/dev/null)" ]
  then 
    echo "INIT"
    exit 0
  else 
    echo "KO"
    exit 0
  fi;
fi

exit 0
