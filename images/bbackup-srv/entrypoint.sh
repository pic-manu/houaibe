#! /bin/bash

# Configuration sécurisée pour sshd
sed -e 's/#Port 22/Port 2200/' \
    -e 's/X11Forwarding yes/X11Forwarding no/' \
    -e 's/Subsystem/# Subsystem/' \
    -e 's/#PasswordAuthentication yes/PasswordAuthentication no/' \
    -e 's/#PermitEmptyPasswords no/PermitEmptyPasswords no/' \
    -i /etc/ssh/sshd_config

# Ajouter deux paramètres à la conf ssh
# cf. https://borgbackup.readthedocs.io/en/stable/usage/serve.html
echo -e "\nClientAliveInterval 10\nClientAliveCountMax 30\n" >>/etc/ssh/sshd_config

# Garder l'heure de démarrage et la durée minimale
date +%s > /tmp/START_TIMESTAMP
echo $BACKUP_SSH_MIN_UP > /tmp/BACKUP_SSH_MIN_UP

# Démarrer supervisord, et avec lui syslog et sshd
# Créer /var/log/supervisor
mkdir -p /var/log/supervisor
exec /bin/supervisord -c /etc/supervisor/supervisord.conf
