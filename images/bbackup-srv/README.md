# bbackup-srv

Les trois conteneurs `pic-bbackup`, `pic-brestit` et `pic-bbackup-srv` permettent de sauvegarder des répertoires ou des volumes Docker (nous les appelons ressources), en utilisant le logiciel `borgbackup` (cf. https://www.borgbackup.org)

`pic-bbackup` assure la sauvegarde, `pic-brestit` permet de restaurer les données, et `pic-bbackup-srv` est utilisé comme serveur dans le cas de sauvegardes sur une machine distante.

Le fait d'utiliser un conteneur sur la machine distante augmente la sécurité car celui-ci ne sera allumé que lors des sauvegardes, donc la plupart du temps toute connexion sera impossible depuis la machine cliente sur le serveur de  sauvegarde. La machine de sauvegarde doit bien sûr être particulièrement protégée:

- Pas de comptes hormis les comptes administrateurs
- Authentification par clé ssh uniquement
- Pas d'autre service

## Installation:

- Installer `houaibe` comme expliqué dans le README général

- Configuration du fichier `houaibe.conf`, seuls les paramètres suivants doivent être renseignés: ADMIN, ADMINMAIL, CONTAINERSDIR, IPV6, 

  PICGSDIR, DOMAINNAME, BACKUP_REPO_PREFIX_PATH, BACKUP_SSH_MIN_UP

- Lors de l'installation les phases 4,5,6,8 peuvent être sautées.

- **Installer** et **Construire** le conteneur:

```
cd $WORK
sudo ./install
./build.sh
```

### Installation des clés ssh

Pour donner l'accès ssh à une machine cliente:

```
cd /usr/local/houaibe/containers/pic-backup-srv
./ajouter-client.bash 
Nom du nouveau client (par exemple 'hostname -s' sur la machine cliente) : toto
Configuration sur la machine cliente: BACKUP_REPO_PREFIX_HOST=toto@picbackup.le-pic.org
```

Le fichier `./home/toto/.ssh/authorized_keys` a été créé il contient:

```
command="/borg-wrapper.bash" ssh-xxxxxxxx votre-cle-publique-ssh root@toto
```

Vous devez maintenant remplacer `ssh-xxxxxx` par la clé publique de la machine cliente. Par ailleurs, le script vous donne la valeur du paramètre `BACKUP_REPO_PREFIX_HOST` que vous devrez recopier dans le fichier `houaibe.conf` de la machine cliente

### Fichiers logs

Les logs générés par ssh ou le wrapper sont conservés de manière pérenne, pour la sécurité notamment, dans le répertoire `./log`

## Utilisation:

Ce conteneur doit être lancé avant (quelques secondes suffisent) toute sauvegarde ou toute restitution de fichiers sur les clients:

```
toctoc restart pic-bbackup-srv
```

A partir du démarrage, un script est lancé toutes les `BACKUP_SSH_MIN_UP` (voir `houaibe.conf`, par défaut 3600 secondes ou 1 heure): s'il y a une sauvegarde ou une restitution en cours rien ne se passe, sinon le conteneur est arrêté.

## borg compact:

Les scripts de sauvegarde appellent `borg prune`, afin de supprimer les anciennes sauvegardes. Mais la suppression n'est en fait pas effective, il faut appeler la commande `borg compact` pour récupérer effectivement l'espace-disque dans le dépôt. 

Par ailleurs, pour plus de sécurité, il est utile de positionner le paramètre `append_only` à 1: de la sorte, il est impossible de supprimer des archives à partir d'un client de sauvegarde, donc si le client est compromis son pouvoir de nuisance sera limité. Le script  `borg-compact.bash` va donc:

- passer le paramètre `append_only` à 1 sur tous les dépôts borg
- Si le jour de la semaine est 0 (dimanche) il va appeler `borg compact`. 

## crontab:

Pour des sauvegardes quotidiennes et nocturnes, on aura donc deux lignes dans le `crontab`: une ligne pour accueillir les sauvegardes, une ligne pour borg-compact:

```
58 21 * * * /usr/local/houaibe/containers/pic-bbackup-srv/run.sh
56 11 * * * /usr/local/houaibe/containers/pic-bbackup-srv/run.sh /borg-compact.bash
```

## Sécurité:

Ce système peut apparaître compliqué, mais il est conçu pour assurer une bonne sécurité. En effet:

- Si le serveur de sauvegarde est compromis, il n'y aura pas de fuite de données, car il faut disposer du mot de passe permettant de déverrouiller la clé de cryptage. Or celui-ci est conservé sur les clients, pas sur le serveur.
- Si l'un des clients de sauvegarde est compromis, il ne pourra pas être utilisé pour rebondir sur le serveur, car la plupart du temps le conteneur `pic-bbackup-srv` est éteint (sauf pendant les sauvegardes ou les restitutions). Et lorsqu'il est allumé, très peu de commandes peuvent être exécutées depuis le client, en particulier on ne peut pas appeler de shell.
- Toutefois lorsque `pic-bbackup-srv` est allumé borg peut être utilisé depuis les clients, ce qui  constitue une fenêtre de vulnérabilité (possibilité de fuite de données ou destruction de données): il convient donc de s'assurer que le conteneur `pic-bbackup-srv` est allumé le moins de temps possible. C'est pour cette raison que `pic-bbackup` et `pic-brestit` éteignent `pic-bbackup-srv` lorsqu'ils ont terminé... à condition, dans le cas de `pic-brestit`, que l'utilisateur sorte proprement du conteneur (`CTRL-D`).

## Mise à jour:

Il est bon de reconstruire le conteneur de temps en temps, afin de bénéficier des mises à jour mises à disposition sur le DockerHub:

```
cd ~/backup/docker
NOCACHE=1 ./build.sh .h
```

