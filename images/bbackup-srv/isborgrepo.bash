#!/bin/bash

#
# This file is part of the houaibe software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# Usage: isborgrepo.bash directory
#
# Ecrit sur stdout: YES si directory est un dépôt borg, NO sinon
#
# Permet d'écrire:
#
# if [ $(isborgrepo.bash directoy) = 'YES' ]
# then
#    echo "directory est un depot Borg"
# else
#    echo "directory n'est pas un depot Borg"
# fi
#

DIRECTORY=${1}

# TODO - Trouver une manière plus robuste de tester si c'est un dépôt Borg !
if grep -F -qs -e '[repository]' ${DIRECTORY}/config
then
  echo "YES"
else
  echo "NO"
fi

exit 0
