#! /bin/bash

#
# This file is part of the houaibe software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# Ce wrapper permet d'utiliser un nombre réduit de commandes via ssh en déclarant uniquement
# le chemin vers le wrapper dans le fichier authorized_keys (cf. paramètre command)
# Il est fait pour fonctionner via ssh:
#
# Fichier Authorized_keys
#
# command="/borg-wrapper.bash" ssh-ed25519 AAAA... root@exemple.com
#
# Sur la machine distante:
#
# Usage: borg-wrapper.sh borg        -> Appelle borg backup
#        borg-wrapper.sh isaborgrepo -> Appelle le script isaborgrepo.sh 
#        borg-wrapper.sh start       -> Commence à travailler à partir de la machine $2
#        borg-wrapper.sh stop        -> Signale que le travail concernant la machine $2 est terminé
#
# En local (pour test uniquement), on peut faire:
#
# SSH_ORIGINAL_COMMAND=borg /bord-wrapper.bash
#
# On utilise logger pour ecrire tout ce qu'on fait dans /var/log/messages
#

[ -z "$SSH_ORIGINAL_COMMAND" ] && exit 0

STOP=/stop.bash

# On parse la commande, on la met dans le tableau CMD
read -a CMD <<<${SSH_ORIGINAL_COMMAND}
case "${CMD[0]}" in
    
    "borg")
        CMD[0]='/bin/borg'
        logger "BORG: ${CMD[*]}"
        exec ${CMD[*]}
        ;;
        
    "isborgrepo")
        CMD[0]='/isborgrepo.bash'
        logger "ISBORGREPO: ${CMD[*]}"
        exec ${CMD[*]}
        ;;

    "is_borg_repo")
        CMD[0]='/is_borg_repo.bash'
        logger "ISBORGREPO: ${CMD[*]}"
        exec ${CMD[*]}
        ;;
        
    # Crée un fichier ayant comme nom le paramètre numéro 2
    # Vérifie que ce nom n'a pas des caractères louches (* etc)
    "start")
        if [[ ${CMD[1]} =~ ^[_a-zA-Z0-9.]+$ ]]
        then
           logger "START: touch /tmp/${CMD[1]}.running"
           touch "/tmp/${CMD[1]}.running"
        else
           logger ATTENTION ${CMD[1]} BIZARRE
        fi
        ;;
    # Supprime le fichier ayant comme nom le paramètre numéro 2
    # Vérifie que ce nom n'a pas des caractères louches (* etc)
    # S'il ne reste aucun fichier *.running, arrête le conteneur   
    # NOTE - Si on n'est pas root le conteneur ne pourra pas s'arrêter
    "stop")
        if [ -f /tmp/${CMD[1]}.running ]
        then
            if [[ ${CMD[1]} =~ ^[_a-zA-Z0-9.]+$ ]] 
            then 
               logger "STOP: rm -f /tmp/${CMD[1]}.running"
               rm -f "/tmp/${CMD[1]}.running"
            else
               logger "STOP: ATTENTION ${CMD[1]} BIZARRE"
            fi
            $STOP
        else
            logger "STOP: ATTENTION /tmp/${CMD[1]}.running n'existe pas !"
        fi
        ;;
        
    *)
        logger "ERREUR DE COMMANDE - ${CMD[*]}"
        echo "plop";
        ;;        
esac

exit 0
