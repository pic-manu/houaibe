#! /bin/bash

#
# Script d'installation du répertoire associé au conteneur 
#
# Usage: cd ....
#        ./installcont.sh
#

if [[ ! -f ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf ]]
then
   echo "houaibe n'est pas encore configuré"
   exit 1;
fi

# Lire les paramètres généraux et spécifiques
. ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf
. ./houaibe.conf

# Repertoire courant
DIR=$CONTAINERSDIR/$CONTAINER

# Rien à faire si le répertoire existe déjà
[[ -d $DIR ]] && echo "Le repertoire $DIR existe - Je ne fais rien" && exit 0

echo "installation du conteneur $CONTAINER"
mkdir -p $DIR
chmod g+w $CONTAINERSDIR/$CONTAINER

# Copie de fichiers
cp run.sh.dist $DIR/run.sh
ln -sf $DIR/run.sh .
cp houaibe.conf $DIR
cp ../scripts/stop.sh $DIR
ln -sf $DIR/stop.sh .

IMGDIR=$(pwd)

(
    cd $DIR
    chmod 550 run.sh

    # ----- Début de partie spécifique à chaque conteneur

    mkdir etc
    docker run --name $CONTAINER -d $IMAGE
    sleep 5
    
    # Copie sur le host du répertoire /etc/ssh, afin de le rendre pérenne
    ( cd etc && docker cp $CONTAINER:/etc/ssh . )

    # Arrêt du conteneur
    docker stop $CONTAINER
    docker rm $CONTAINER

    mkdir log home .ssh
    echo root:x:0:0:root:/root:/bin/bash > passwd
    echo sshd:x:101:65534::/var/run/sshd:/usr/sbin/nologin >> passwd
    echo 'root:*:19583:0:99999:7:::' > shadow
    echo 'sshd:!:19588::::::' >> shadow

    cp ${IMGDIR}/ajouter-client.bash . && chmod a=rx ajouter-client.bash

    # ----- Fin de partie spécifique
)

# Donner au groupe docker la permission de bosser !
chgrp -R docker $DIR
