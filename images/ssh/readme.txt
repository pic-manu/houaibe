# Installation de ssh:

Construction de l'image:
------------------------

```
cd pic/images/ssh
docker build -t ssh:$(date +%d%m%y) .
docker build -t ssh:latest .
```

Démarrage de ssh dockerisé
--------------------------
```
docker run  --link pic-ldap -v ssh-etc:/etc/ssh -v www-data:/home -p $(cat .portssh):22 --name pic-ssh -d ssh
```

## Applications disponibles:

Plusieurs applications sont installées dans ce conteneur:

- `spip` (maintenance du CMS spip)
- `wp` (maintenance du CMS wp)
- `composer` (installation, mises à jour d'applications php)
- `PHPDeobfuscator` (utile pour lire les fichiers php obscurs)
- `picwpmigrate` (migration d'un site wordpress depuis un autre hébergement)
