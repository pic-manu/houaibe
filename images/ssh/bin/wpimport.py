#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

# Importation d'un site wordpress pour l'installer chez nous
# Utilise certains modules de picgs
# DOIT ETRE INSTALLE ET EXECUTE DANS LE CONTENEUR pic-ssh

# Prérequis: 1/ Un fichier .tgz ou .zip contenant le répertoire de l'ancien wp doit avoir été uploadé dans l'espace des fichiers
#            2/ Un fichier .sql contenant la se de données doit également être accessible
#            3/ Doit être exécuté en tant que utilisateur de la forme monasso-stX

#
# Usage: python3 wpimport.py
#

import sys
import re
import time
import socket
import subprocess
import traceback

import json

from params import *
from picgestion import PicGestion
from erreur import PicErreur
from ouinon import ouiNon
from picconfig import getConf
from plugin import *

from sysutils import SysUtils
from affiche import afficheRouge

from homedir import *

#from params import *

class HomeDirDefaut(HomeDirInstallation):

    def __isInstalled(self, wpplugin):
        '''renvoie True si le plugin wordpress est déja installé'''

        pg = self.getPicGestion()
        www = pg.getWww()
        user = pg.getUser()
        conteneur = getConf('DKRMAINT')
        
        cmd = f"cd {www} && wp plugin list --name={wpplugin} --format=json"
        out = self._system(cmd, conteneur,False,user)
        if len(out) > 1:
            import pprint
            print(afficheRouge(f"ATTENTION - La commande:\n{cmd}\nretourne un truc pas clair (erreur ou warning php)\n"))
            print(afficheRouge(pprint.pformat(out)))
            print(afficheRouge("\nCorrigez et recommencez\n"))
            raise Exception

        jsout = json.loads(out[0])
        if len(jsout) > 0:
            return True
        else:
            return False
        
    def execute(self):

        pg = self.getPicGestion()
        www = pg.getWww()
        user = pg.getUser()
        conteneur = getConf('DKRMAINT')
        wpplugin = 'stops-core-theme-and-plugin-updates'

        if self.__isInstalled(wpplugin):
            print(afficheRouge (f"Le plugin {wpplugin} est déjà installé !"))
            return
            
        print ("==== Installation du plugin wordpress easy-update-manager")
        self._systemWithWarning(f"cd {www} && wp plugin install {wpplugin} --activate", conteneur, False, user)

        print ("==== Configuration du plugin wordpress easy-update-manager")
        self._configEasyUpdateManager()


        print("OK")
        
# PROGRAMME PRINCIPAL
def main():

    # arguments = sys.argv
    
    # On introduit le nom de l'action 'install' dans les arguments en position 1
    # arguments.insert(1,'defaut')
    
    
    sysu = SysUtils()

    # Déterminer l'utilisateur sous lequel nous sommes connectés
    user = sysu._system('id -un')[0]
    
    # Arguments et appel de Params
    arguments = [ user ] 
    
    prms = Params('wpimport',arguments,True,True)
    [action,nom,fonction,version,options] = prms.usage()

    if fonction != 'site':
        raise PicErreur("Cette commande ne s'applique qu'aux sites web" )

    if version == None:
        raise PicErreur("Il faut spécifier le numéro de version !")
        
    # Lève une exception si la version recherchée n'existe pas !
    prms.chercheVersionPourInfo()

    # On construit pic_gestion, qui va à son tour construire tous les plugins !
#    plugins = ['Source','Compte','Mysql',HomeDirDefaut,'Apache','Cert','Fin']
#    pic_gestion = PicGestion(plugins, action, nom, version, options)

    # On fait le boulot !
#    pic_gestion.agir()

    return 0    
            
erreur = False
try:
    if 'PICGS_DEBUG' in os.environ:
        print(afficheJaune('DEBUG ACTIVE'))

    if 'PICGS_PROFIL' in os.environ:
        print(afficheJaune('PROFILAGE ACTIVE'))
        import cProfile, pstats, io, re
        pr = cProfile.Profile()
        pr.enable()
    
    main()

    if 'PICGS_PROFIL' in os.environ:
        pr.disable()
        s = io.StringIO()
        #sortby = SortKey.CUMULATIVE
        sortby = 'cumulative'
        ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
        #ps = pstats.Stats(pr, stream=s)
        ps.print_stats()
        print(s.getvalue(15))

except KeyboardInterrupt:
    print("\n\n===> Action interrompue par un CTRL-C")

except PicErreur as x:
    erreur = True
    print(afficheRouge(x))

except Exception as x:
    erreur = True
    print(x)
    # Décommentez pour déboguer !
    traceback.print_exception(x,None,0)

if erreur:
    sys.exit(1)
