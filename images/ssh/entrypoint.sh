#
# Ce script remplace BASENAME par la varible denvironnement $BASENAME
# dans le fichier:
#  - /etc/nslcd.conf
#
# Enfin il appelle supervisord
#
#
# Emmanuel, le-pic.org

# On vérifie qu'on a bien le serveur pic-ldap et la variable BASENAME
[ "0" == $(grep -c pic-ldap /etc/hosts) ] && echo "ERREUR - Ajoutez le switch --add-host pic-ldap=1.2.3.4 sans serveur ldap, rien ne marche" && exit 0
[ -z "$BASENAME"                        ] && echo "ERREUR - Ajoutez le switch --env BASENAME='dc=exemple',dc=com' à la commande docker run !" && exit 0

sed -i -e "s/BASENAME/${BASENAME}/" /etc/nslcd.conf

# Cet umask assure que les fichiers créés par les personnes connectées soient illisibles pas les autres
# TODO - inutile depuis houaibe 3.20.0
echo "umask o=" >> /etc/bash.bashrc

# DEPUIS HOUAIBE 3.20 /var/run EST UN tmpfs DONC IL FAUT RECREER CE REPERTOIRE
mkdir /run/sshd

exec /bin/supervisord -c /etc/supervisor/supervisord.conf

