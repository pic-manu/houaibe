#! /bin/bash

#
# Script de mise à jour du répertoire associé au conteneur - pour la version 3.20.O et plus
#
# Usage: cd ....
#        ./upgc-320.sh
#

[[ "$(id -u)" -ne "0" ]] && echo "VOUS DEVEZ ETRE ROOT !" && exit 1

if [[ ! -f ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf ]]
then
   echo "houaibe n'est pas encore configuré"
   exit 1;
fi

# Lire les paramètres généraux et spécifiques
. ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf
. ./houaibe.conf

# Reprtoire courant
DIR=$CONTAINERSDIR/$CONTAINER

# Rien à faire si le répertoire n'existe pas
[[ ! -d $DIR ]] && echo "Le répertoire $DIR n'exite pas - Utilisez plutôt installcont.sh" && exit 0
[[ -f $DIR/run.sh.bkp ]] && echo "Le fichier $DIR/run.sh.bkp existe - Je ne fais rien" && exit 0

echo "mise à jour du conteneur $CONTAINER"

# Recopie de fichiers
mv $DIR/run.sh $DIR/run.sh.bkp
cp run.sh.dist $DIR/run.sh
ln -sf $DIR/run.sh .
[[ -f stop.sh.dist ]] && cp stop.sh.dist $DIR/stop.sh
[[ -f $DIR/stop.sh ]] && ln -sf $DIR/stop.sh .
mv $DIR/houaibe.conf $DIR/houaibe.conf.bkp
cp houaibe.conf $DIR

IMGDIR=$(pwd)

(
    cd $DIR
    chmod 550 run.sh

    # ----- Début de partie spécifique à chaque conteneur
    # On vérifie qu'on a bien le serveur ldap et la variable BASENAME
    [ -z $LDAPADDR ] && echo "ERREUR - Manque le paramètre LDAPADDR dans ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf" && exit 0
    [ -z $LDAPURI  ] && echo "ERREUR - Manque le paramètre LDAPURI  dans ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf" && exit 0
    [ -z $BASENAME ] && echo "ERREUR - Manque le paramètre BASENAME dans ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf" && exit 0

    DEB4PIC=${IMGDIR}/../deb4pic
    sed -e "s/BASENAME/${BASENAME}/" ${DEB4PIC}/nslcd.conf >nslcd.conf

    # Création de quelques sous-répertoires ou fichiers de log
    mkdir -p log/supervisor 

    # ----- Fin de partie spécifique
)

# Donner au groupe docker la permission de bosser !
chgrp -R docker $DIR


