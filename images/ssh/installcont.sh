#! /bin/bash

#
# Script d'installation du répertoire associé au conteneur 
#
# Usage: cd ....
#        ./installcont.sh
#

if [[ ! -f ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf ]]
then
   echo "houaibe n'est pas encore configuré"
   exit 1;
fi

# Lire les paramètres généraux et spécifiques
. ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf
. ./houaibe.conf

# Reprtoire courant
DIR=$CONTAINERSDIR/$CONTAINER

# Rien à faire si le répertoire existe déjà
[[ -d $DIR ]] && echo "Le repertoire $DIR existe - Je ne fais rien" && exit 0

echo "installation du conteneur $CONTAINER"
mkdir -p $DIR
chmod g+w $CONTAINERSDIR/$CONTAINER

# Copie de fichiers
cp run.sh.dist $DIR/run.sh
cp stop.sh.dist $DIR/stop.sh
ln -sf $DIR/run.sh .
ln -sf $DIR/stop.sh .
cp houaibe.conf $DIR

IMGDIR=$(pwd)

(
    cd $DIR
    chmod 550 run.sh

    # ----- Début de partie spécifique à chaque conteneur
    mkdir -p log/supervisor

    # On vérifie qu'on a bien le serveur ldap et la variable BASENAME, aussi DOMAINNAME
    [ -z $LDAPADDR ] && echo "ERREUR - Manque le paramètre LDAPADDR dans ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf" && exit 0
    [ -z $LDAPURI  ] && echo "ERREUR - Manque le paramètre LDAPURI  dans ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf" && exit 0
    [ -z $BASENAME ] && echo "ERREUR - Manque le paramètre BASENAME dans ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf" && exit 0
    [ -z $DOMAINNAME ] && echo "ERREUR - Manque le paramètre DOMAINNAME dans ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf" && exit 0

    DEB4PIC=${IMGDIR}/../deb4pic
    sed -e "s/BASENAME/${BASENAME}/" ${DEB4PIC}/nslcd.conf >nslcd.conf

    # Installation de ssh la première fois
    echo "Installation de ssh - Attendez quelques secondes SVP"

    [[ -n "$LDAPADDR" ]] && SWLDAP="--add-host pic-ldap:$LDAPADDR"

    # Première exécution du conteneur = création des fichiers de clé de ssh
    mkdir etc
    docker run $SWLDAP --env BASENAME=$BASENAME --name $CONTAINER -d $IMAGE
    sleep 5

    # Copie sur le host du répertoire /etc/ssh, afin de le rendre pérenne
    ( cd etc && docker cp $CONTAINER:/etc/ssh . )

    # Arrêt du conteneur
    docker stop $CONTAINER
    docker rm $CONTAINER

    # Copie au bon endroit du fichier sshd_config qui a été personnalisé
    # (utilisation de sftp en chroot et avec un shell à /bin/false, port 2200, environnement)
    cp ${IMGDIR}/sshd_config $DIR/etc/ssh
    sed -i -e "s/DOMAINNAME=DOMAINNAME/DOMAINNAME=${DOMAINNAME}/" $DIR/etc/ssh/sshd_config

    # ----- Fin de partie spécifique
)

# Donner au groupe docker la permission de bosser !
chgrp -R docker $DIR


