# itk74

L'image `itk80` permet de construire un conteneur qui repose sur une debian 10, avec dedans apache2 ainsi que les modules php 7.4 et mpm-itk4

## Installation:

- **Construire** le conteneur:

```
cd $WORK/images/itk80
./build.sh
```

- **Installer** les fichiers et volumes utilisés par le conteneur:

```
sudo ./installcont
```

- **Démarrer** le conteneur:

```
./run.sh
```

## Mise à jour:

Il est bon de reconstruire le conteneur de temps en temps, afin de bénéficier des mises à jour mises à disposition sur le DockerHub:

```
cd $WORK/images/proxy
./build.sh
```

Si `houaibe` a été mis à jour, et donc le Dockerfile ou d'autres fichiers, il est préférable d'ignorer le cache de Docker:

```
cd $WORK/images/proxy
NOCACHE=1 ./build.sh 
```

