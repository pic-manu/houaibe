#!/bin/bash
#
# Script d'arret du conteneur
#
# Usage: path/to/container/directory/stop.sh
#
# Ce script arrête le conteneur si besoin
#

# set -v

# Calcul du repertoire dans lequel run.sh est depose - ca marche meme si on part d'un lien symbolique
fullpath=$(readlink --no-newline --canonicalize-existing $0) 
if [[ -z $fullpath ]]; then echo "ERREUR - run.sh pointe sur le vide !"; exit 1; fi
DIR=$(dirname "$fullpath")

(
cd $DIR

# Variables générales
if [[ -f ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf ]]; then . ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf; else echo "Merci de terminer la configuration en créant le fichier /usr/local/etc/houaibe/houaibe.conf"; exit 1; fi

# Variables spécifiques à ce conteneur
if [[ -f ./houaibe.conf ]]; then . ./houaibe.conf; else echo "${DIR} n'est pas un repertoire de conteneur défini dans houaibe."; exit 1; fi

# Arrêt du conteneur
docker stop $CONTAINER 2> /dev/null
docker rm   $CONTAINER 2> /dev/null

)

