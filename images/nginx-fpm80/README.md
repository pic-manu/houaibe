# nginx-fpmxy

Les images `nginx-fpmxy` permet de construire un ensemble de deux conteneurs qui reposent sur nginx et php (version x.y) en mode fpm
`docker compose` est utilisé pour gérer ces conteneurs

## Installation:

- **Construire** les conteneurs:

```
./build.sh
```

- **Installer** le template (format tgz) qui sera utilisé lors de la création des conteneurs proprement dits (donc du site web):

```
sudo ./installtmpl
```

- **Construire un conteneur utilisant cette image**:

```
picgs ajout ...
```

## Mise à jour:

Il est bon de reconstruire le conteneur de temps en temps, afin de bénéficier des mises à jour mises à disposition sur le DockerHub:

```
./build.sh
```

Si `houaibe` a été mis à jour, et donc le Dockerfile ou d'autres fichiers, il est préférable d'ignorer le cache de Docker:

```
NOCACHE=1 ./build.sh 
```

