#! /bin/bash

#
# Script d'installation du répertoire associé au conteneur 
#
# Usage: cd ....
#        ./installcont.sh
#

if [[ ! -f ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf ]]
then
   echo "houaibe n'est pas encore configuré"
   exit 1;
fi

# Lire les paramètres généraux et spécifiques
. ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf
. ./houaibe.conf

# Reprtoire courant
DIR=$CONTAINERSDIR/$CONTAINER

# Rien à faire si le répertoire existe déjà
[[ -d $DIR ]] && echo "Le repertoire $DIR existe - Je ne fais rien" && exit 0

echo "installation du conteneur $CONTAINER"
mkdir -p $DIR
chmod g+w $CONTAINERSDIR/$CONTAINER

# Copie de fichiers
cp run.sh.dist $DIR/run.sh
cp stop.sh.dist $DIR/stop.sh
ln -sf $DIR/run.sh .
ln -sf $DIR/stop.sh .
cp houaibe.conf $DIR

IMGDIR=$(pwd)

(
    cd $DIR
    chmod 550 run.sh

    # ----- Début de partie spécifique à chaque conteneur
    mkdir -p sites-available/pic sites-enabled
    mkdir log
    cp $IMGDIR/../../picgs/conf/template-pic-itk81 sites-available/pic/template

    # Creer un fichier vide env.txt et le rendre rw-rw----
    touch env.txt && chmod 660 env.txt
    
    # Copier mpm_prefork.conf et php.ini
    cp $IMGDIR/mpm_prefork.conf $IMGDIR/php.ini $DIR
    chown root:docker $DIR/mpm_prefork.conf $DIR/php.ini
    chmod 640 $DIR/mpm_prefork.conf $DIR/php.ini

    # On vérifie qu'on a bien le serveur ldap et la variable BASENAME
    [ -z $LDAPADDR ] && echo "ERREUR - Manque le paramètre LDAPADDR dans ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf" && exit 0
    [ -z $LDAPURI  ] && echo "ERREUR - Manque le paramètre LDAPURI  dans ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf" && exit 0
    [ -z $BASENAME ] && echo "ERREUR - Manque le paramètre BASENAME dans ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf" && exit 0
    [ -z $SMTPADDR ] && echo "ERREUR - Manque le paramètre SMTPADDR dans ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf" && exit 0
    [ -z $DOMAINNAME ] && echo "ERREUR - Manque le paramètre DOMAINNAME dans ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf" && exit 0

    DEB4PIC=${IMGDIR}/../deb4pic
    sed -e "s/DOMAINNAME/${DOMAINNAME}/" -e "s/CONTAINER/${CONTAINER}/" ${DEB4PIC}/msmtprc >msmtprc
    sed -e "s/BASENAME/${BASENAME}/" ${DEB4PIC}/nslcd.conf >nslcd.conf

    # Création de quelques sous-répertoires ou fichiers de log
    mkdir -p log/apache2 log/supervisor log/msmtp && touch log/msmtp/msmtp.log
    chmod a=rwX log/msmtp log/msmtp/msmtp.log

    # ----- Fin de partie spécifique
)

# Donner au groupe docker la permission de bosser !
chgrp -R docker $DIR


