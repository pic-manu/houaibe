# itk81

L'image `itk81` permet de construire un conteneur qui repose sur une debian, avec dedans apache2 ainsi que les modules php 8.1 et mpm-itk

## Installation:

- **Construire** le conteneur:

```
cd $WORK/images/itk81
./build.sh
```

- **Installer** les fichiers et volumes utilisés par le conteneur:

```
sudo ./installcont
```

- **Démarrer** le conteneur:

```
./run.sh
```

## Mise à jour:

Il est bon de reconstruire le conteneur de temps en temps, afin de bénéficier des mises à jour mises à disposition sur le DockerHub:

```
cd $WORK/images/proxy
./build.sh
```

Si `houaibe` a été mis à jour, et donc le Dockerfile ou d'autres fichiers, il est préférable d'ignorer le cache de Docker:

```
cd $WORK/images/proxy
NOCACHE=1 ./build.sh 
```

