#! /bin/bash

#
# This file is part of the houaibe software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# Ce script remplace DOMAINNAME, CONTAINER par leurs valeurs dans certains fichiers 
# puis il appelle /borgbackup.sh
#

# Si on ne peut pas utiliser le mail, imprimer un warning
[ "0" == $(grep -c pic-smtp /etc/hosts) ] && echo "ATTENTION - Si vous voulez que vos sites puissent envoyer des mails, ajoutez --add-host pic-smtp=1.2.3.4  à la commande docker run !"
[ -z "$DOMAINNAME"                      ] && echo "ATTENTION - si vous voulez que vos sites puissent envoyer des mails, ajoutez --env DOMAINNAME=exemple.com à la commande docker run !"

sed -i -e "s/DOMAINNAME/${DOMAINNAME}/" /etc/msmtprc
sed -i -e "s/BACKUP_HOSTNAME/${BACKUP_HOSTNAME}/" /etc/msmtprc

# Ajout de paramètres à la config ssh
# cf. https://borgbackup.readthedocs.io/en/stable/usage/serve.html
echo -e "\n    ServerAliveInterval 10\n    ServerAliveCountMax 30\n" >>/etc/ssh/ssh_config
