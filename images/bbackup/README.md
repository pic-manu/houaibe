# bbackup

Les trois conteneurs `pic-bbackup`, `pic-brestit` et `pic-bbackup-srv` permettent de sauvegarder des serveurs complets, des répertoires, ou des volumes Docker (nous les appelons `ressources`), en utilisant le logiciel `borgbackup` (cf. https://www.borgbackup.org)

`pic-bbackup` assure la sauvegarde, `pic-brestit` permet de restaurer les données, et `pic-bbackup-srv` est utilisé comme serveur dans le cas de sauvegardes sur une machine distante: Le fait d'utiliser un conteneur sur la machine distante augmente la sécurité car celui-ci ne sera allumé que lors des sauvegardes, donc la plupart du temps toute connexion sera impossible depuis la machine cliente sur le serveur de  sauvegarde. La machine de sauvegarde doit bien sûr être particulièrement protégée (peu de comptes, authentification par clé ssh uniquement, etc.)

## Installation:

- **Construire** le conteneur:

```
cd $WORK/images/bbackup
./build.sh
```

- **Installer** les fichiers et volumes utilisés par le conteneur:

```
sudo ./installcont
```

### Cas de dépôts sur une machine distante

Lors de l'installation, une clé ssh sera générée et déposée dans le fichier `/usr/local/houaibe/containers/pic-bbackup/.ssh/id_ed25519.pub`. Si vous sauvegardez sur une machine distante, cette clé devra être installée dessus. En étant connecté sur le serveur de sauvegarde, vous devrez:

- Choisir un nom d'utilisateur pour le dépôt de sauvegarde, de préférence le nom de la cliente: `hostname -s` Ici on supposera que la cliente s'appelle `toto.exemple.org`, l'utilisateur est donc `toto`.
- Utiliser le script `ajouter-client.bash` pour ajouter l'utilisateur `toto` (voir le `README` de `bbackup-srv`)
- Ajouter la clé publique avec un éditeur dans le fichier `/usr/local/houaibe/containers/pic-bbackup-srv/home/toto/.ssh/authorized_keys`

### Initialisation de la connexion ssh:

Il est recommandé d'initier la connexion ssh de la manière suivante et de répondre Y si l'empreinte est correcte:

*Côté serveur de sauvegarde* *(sauvegarde.exemple.org)* démarrer le conteneur de `pic-bbackup-srv`:

```
toctoc start pic-bbackup-srv
```

*Côté machine à sauvegarder (toto.exemple.org)* démarrer le conteneur `pic-bbackup`, entrer dedans et lancer une connexion ssh:

```
toctoc start pic-bbackup bash
ssh -p 2200 toto@sauvegarde.exemple.org ls
plop
```

Normalement, `pic-bbackup` est autorisé à exécuter *seulement* quelques commandes sur la machine `sauvegarde.exemple.org`. `ls` n'en faisant pas partie, la réponse `plop` est normale: elle signifie que la communication se fait normalement, et que `ls` (ainsi que la plupart des commandes unix) est interdite.

### Configuration:

Les sauvegardes sont gouvernées par neuf paramètres à configurer dans le fichier `/usr/local/houaibe/etc/houaibe.conf` *(tous les autres paramètres de ce fichier peuvent être commentés si non utilisés par ailleurs)*:

- `CONTAINERSDIR=${HOUAIBEDIR-/usr/local/houaibe}/containers`
- `PICGSDIR=${HOUAIBEDIR-/usr/local/houaibe}/picgs`

- `DOMAINNAME`: Le nom de domaine
- `SMTPADDR`: L'adresse IP du serveur smtp qui sera utilisé pour envoyer les mails de sauvegarde
- `BACKUP_MAIL`: L'adresse qui recevra les rapports de sauvegarde
- `BACKUP_REPO_PREFIX_HOST`: Utilisé seulement dans le cas d'une sauvegarde sur une machine distante, de la forme: root@sauvegarde.exemple.org
- `BACKUP_REPO_PREFIX_PORT`: Le port ssh utilisé pour les sauvegardes sur une machine distante
- `BACKUP_REPO_PREFIX_PATH`: Le répertoire parent de tous les dépôts, *vu depuis le conteneur `pic-bbackup-srv`.* Le répertoire se trouve sur la machine distante dans le cas où les deux paramètres précédents sont définis, sur la machine locale sinon.
- `BACKUP_RESSOURCES`: La liste de "ressources" (chemin vers un répertoire ou volume Docker) à sauvegarder. 
  **NOTES:** On utilisera le dernier élément du chemin comme nom de dépôt, *il doit donc être unique*. Par exemple on ne peut spécifier "`/etc /usr/local/etc`" comme ressources à sauvegarder, car les deux chemins se terminent par `etc`

### Chemin des ressources dans le conteneur

Qu'elles soient un répertoire du host ou un nom de volume Docker, les ressources sont montées en-dessous du répertoire `/ressources` dans le conteneur `pic-bbackup`: par exemple `www-data` apparaîtra comme `/ressources/www-data`, et c'est ce répertoire qui sera sauvegardé.
Le cas du répertoire `/` est traité différemment des autres: `/` est monté dans pic-bbackup en tant que `/ressources/root`, et le nom de la ressource sera root (*voir aussi plus loin*). Il en résulte que si vous sauvegardez `/` vous ne pouvez sauvegarder en même temps `root` (le home directory du compte root) en tant que ressource distincte (d'ailleurs, cela ne servirait à rien puisque `/root` est déjà sauvegardé puisqu'il est en-dessous du répertoire `/`).

### Tests de connexion

Pour tester la connexion sur la machine distante, il est possible de lancer le conteneur de la manière suivante:

`toctoc start pic-bbackup bash`

Vous avez le prompt depuis l'intérieur du conteneur, et vous pouvez alors lancer des commandes ssh pour voir si la machine distante répond correctement

## Options de sauvegarde

La sauvegarde se fait avec les options suivantes:

`--info --stats --show-rc --compression lz4 --exclude-caches` et éventuellement `--exclude-from` (voir plus loin). Il est possible d'ajouter des options en les inscrivant dans un fichier:

-  à la source de la ressource sauvegardée (c'est-à-dire, pour `/etc`, dans le répertoire `/etc`) 
- appelé `.borg-create-options`. Par exemple le fichier pourrait contenir:

`--one-file-system`

### Liste d'exclusion

Le switch `--exclude-caches` étant spécifié systématiquement, il suffit d'insérer un fichier appelé `CACHEDIR.TAG` dans un répertoire pour que celui-ci ne soit pas sauvegardé.

Mais il est possible, en créant à la source de la ressource (c'est-à-dire, pour `/etc`, dans le répertoire `/etc`) un fichier appelé `.borg-exclude-list`, de spécifier les répertoires qui *ne doivent pas* être sauvegardés (ce fichier, s'il existe, sera utilisé avec le switch `--exclude-from`). 

**ATTENTION**, le chemin spécifié doit être précédé de `/ressources`, car les ressources à sauvegarder sont montées dans le conteneur avec ce chemin. Par exemple, si on sauvegarde `/home`, il peut être utile d’exclure de la sauvegarde le répertoire `.cache` en mettant dans `/home/.borg-exclude-list` la ligne suivante:

`/ressources/home/*/.cache/*`

## Exécution de la sauvegarde

Pour sauvegarder *toto.exemple.org* sur le serveur de sauvegardes *sauvegarde.exemple.org*, il faut:

Sur *sauvegarde.exemple.org* lancer le conteneur `pic-bbackup-srv`:

```
toctoc start pic-bbackup_srv
```

Sur toto.exemple.org lancer le conteneur `pic-bbackup` par:

```
toctoc start pic-bbackup`
```

Lorsque toutes les ressources ont été sauvegardées `pic-bbackup` éteint le conteneur distant `pic-bbackup-srv`, avant de s'éteindre lui-même. Si la sauvegarde a duré moins d'une heure (configurable, voir le `README` de `pic-bbackup-srv`), le conteneur distant ne s'éteindra pas, mais il s'éteindra une heure après avoir été lancé.... s'il n'est toujours pas utilisé à ce moment

### Intégration dans un script:

Très souvent la sauvegarde proprement dite se passe de la manière suivante:

- Préparation: par exemple désactivation d'un CMS, snapshot, etc
- Sauvegarde
- Post traitement: remise en service du CMS

La sauvegarde proprement dite est dans ce cas intégrée à un script, et il faut attendre la fin de la sauvegarde afin de démarrer le post traitement. Or `run.sh` n'attend pas la fin du conteneur pour rendre la main, car le switch `--detach` est utilisé. Pour changer ce comportement il suffit de l'appeler ainsi:

```
toctoc start pic-bbackup attendre
```

### Première sauvegarde d'une ressource:

La première fois qu'une ressource est sauvegardée, `borg init` est appelé afin d'initialiser le dépôt. Le mode `repokey` est utilisé, ce qui signifie que:

- La clé se trouve sur le dépôt
- La clé ne peut être déverouillée qu'en donnant un mot de passe *qui ne se trouve pas sur le dépôt*.

Le fichier de clé ainsi que le mot de passe associé sont copiés dans un fichier caché dans le répertoire `/usr/local/houaibe/containers/pic-bbackup` **Il est recommandé de mettre ces fichiers en lieu sûr, en effet leur perte rendra la sauvegarde totalement inutilisable**. Les conteneurs `pic-bbackup` et `pic-brestit` utiliseront ces fichiers cachés, de sorte que leur utilisation est transparente pour l'utilisateur. Voir la documentation de bbackup-srv pour une discussion sur la sécurité du dispositif.

### Elagage

Lorsque la sauvegarde est terminée, le script appelle `borg prune` afin de supprimer du disque les anciennes sauvegardes. La stratégie par défaut est:

```
--keep-daily 15 --keep-weekly 8 --keep-monthly 6 --keep-yearly 5
```

On peut la modifier en créant à la racine de la ressource un fichier nommé `.borg-prune-options-strat`. Par exemple, pour ne conserver que la dernière sauvegarde on pourrait écrire dans ce fichier:

```
--keep-daily 1
```

## Sauvegarder le disque système de l'hôte:

Pour sauvegarder le disque système de l'hôte, il convient de spécifier `root` comme nom de ressource. Par ailleurs il est important de définir soigneusement la liste d'exclusion, afin de ne pas sauvegarder bêtement toute la machine. En particulier le répertoire `/var/lib/docker` ne doit généralement pas être sauvegardé. Afin de contrôler la quantité de données sauvegardées et affiner la liste d'exclusion, on peut procéder ainsi:

```
/usr/local/houaibe/container/pic-bbackup/run.sh bash
cd /ressources/root
du  -sh --one-file-system --exclude-from .borg-exclude-list /
```

La taille renvoyée par la commande `du` sera la même que la quantité de données sauvegardées (compte non tenu de la compression ni de la déduplication). Un exemple de fichier `/.borg-exclude-list` se trouve ci-dessous:

```
/ressources/root/dev
/ressources/root/proc
/ressources/root/sys
/ressources/root/*/cache/*
/ressources/root/*/.cache/*
/ressources/root/*/tmp/*
/ressources/root/var/lib/docker
```

D'autre part si on ne veut sauvegarder que le disque système il est important de créer le fichier `/.borg-create-options`:

```
echo "--one-file-system" >/.borg-create-options
```

## Vérifier les dépôts:

Il est simple de vérifier que les dépôts sont en bonne santé. Il est d'ailleurs souhaitable de le faire de temps en temps.

Pour cela:

```
./run.sh bash
borg_repos.bash

RESSOURCE root
=========   
repository= ssh://root@sauvegarde.exemple.org:2200/backup/monhost-root
base      = root
ress_path = /ressources/root
log       = /tmp/borgbackup-monhost-root.log
key       = /basecont/.monhost-root.key
key_pass  = /backup/monhost-root

Remote: Starting repository check
...

RESSOURCE /data
=========   
repository= ssh://root@sauvegarde.exemple.org:2200/backup/monhost-data
base      = data
ress_path = /ressources/data
log       = /tmp/borgbackup-monhost-data.log
key       = /basecont/.monhost-data.key
key_pass  = /backup/monhost-data

Remote: Starting repository check
...
```

`borg` vous demandera le mot de passe de la clé. C'est aussi une bonne occasion pour vérifier que  vous l'avez correctement sauvegardé !  Étant entendu que le key_pass indiqué ci-dessus ne constitue pas une sauvegarde du mot de passe !

## Mise à jour:

Il est bon de reconstruire le conteneur de temps en temps, afin de bénéficier des mises à jour mises à disposition sur le DockerHub:

```
cd $WORK/images/bbackup
NOCACHE=1 ./build.sh .sh
```

