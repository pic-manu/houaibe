#!/bin/bash

#
# This file is part of the houaibe software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# Ce script affiche simplement "pathes" utilisés par borg - Il est utilisé uniquement pour des opérations de maintenance
#
# Il doit être exécuté à l'intérieur du conteneur pic-bbackup
#
# Voir les scripts borgbackup.bash et borgbakcup-include.bash pour les détails !
#
#    La première fois le dépôt est initialisé (borg init)
#    Les ressources sont sauvegardées en même temps
#
# ATTENTION - Dans le cas d'une sauvegarde distante, le conteneur distant N'EST PAS AUTOMATIQUEMENT ARRETE !
#    

# -------------------------------------------------------------------
# Fonctions

# --------------------------------------------------------
# main 

# Calcul du chemin du fichier d'include
FULLPATH=$(readlink --no-newline --canonicalize-existing $0) 
if [ -z "$FULLPATH" ]; then echo "ERREUR - On pointe sur le vide !"; exit 1; fi
BORGBACKUP_INCLUDE="$(dirname "$FULLPATH")/borgbackup-include.bash"

. ${BORGBACKUP_INCLUDE}

for p in ${BACKUP_RESSOURCES}
do
    read base ress_path log repo pass key repo_pass<<< $(init_path "${p}")

    echo "RESSOURCE $p"
    echo "=========   "
    echo "repository= $repo"
    echo "base      = $base"
    echo "ress_path = $ress_path"
    echo "log       = $log"
    echo "key       = $key"
    echo "key_pass  = $repo_pass"
    echo
    
    borg check -v $repo
done
