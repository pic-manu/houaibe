#!/bin/bash

#
# This file is part of the houaibe software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

# 
# Pour utilisation avec borgbackup.bash et borgrestit.bash: Initialisation de quelques variables globales,
# à partir des variables d'environnement
#

#
# Initialisation de $base et $log (variables locales dans les fonctions do_xxx)
#
# Prérequis: Les variables globales doivent être initalisées !
#
# Usage:     read base log repo pass key repo_pass<<< $(init_path $path)
#
#            $base = Le basename de $path (répertoire le plus profond)
#            $ress_path = Le chemin du répertoire à sauvegarder, dans le montage du conteneur
#            $log  = Le chemin du fichier log
#            $repo = Le nom du dépôt
#            $pass = Le chemin du fichier contenant le mot de passe de la clé
#            $key  = Le chemin du fichier contenant la clé
#            $repo_path = Le chemin du répertoire utilisé comme dépôt
#                         != $repo si on est sur une machine distante
#
function init_path() {
    local path=${1}
    local base
    if [ "${path}" = "/" ]
    then
        base="root"
    else
        base=$(basename ${path})
    fi
    local ress_path="/ressources/${base}"
    local log="/tmp/borgbackup-${BACKUP_HOSTNAME}-${base}.log"
    local repo=${BACKUP_REPO_PREFIX}/"${BACKUP_HOSTNAME}-${base}"
    local pass_file="/basecont/.${BACKUP_HOSTNAME}-${base}.pass"
    local key_file="/basecont/.${BACKUP_HOSTNAME}-${base}.key"
    local repo_path=${BACKUP_REPO_PREFIX_PATH}/"${BACKUP_HOSTNAME}-${base}"
    echo "${base} ${ress_path} ${log} ${repo} ${pass_file} ${key_file} ${repo_path}"
}


#
# Vérification qu'on a un jeu minimum de variables globales
#
ok=1
[ -z "${BACKUP_RESSOURCES}" ] && echo "ERREUR - Pas de BACKUP_RESSOURCES" && ok=0
[ -z "${BACKUP_REPO_PREFIX_PATH}" ] && echo "ERREUR - Pas de BACKUP_REPO_PREFIX_PATH" && ok=0

BORG_BINARY=/bin/borg
BORG_CREATE_OPTIONS="--info --stats --show-rc --compression lz4 --exclude-caches"
BORG_PRUNE_OPTIONS_BASE="--list --show-rc" 
BORG_PRUNE_OPTIONS_STRAT="--keep-daily 15 --keep-weekly 8 --keep-monthly 6 --keep-yearly 5"

BACKUP_NAME=$(date "+%y%m%d-%H%M")

# BACKUP_HOSTNAME est déjà initialisé en tant que variable d'environnement
# C'est le nom du host, PAS du conteneur !
# BACKUP_HOSTNAME=$(hostname -s)

# borg ess installé dans les conteneurs à son endroit "officiel", soit /usr/local/bin
# export BORG_REMOTE_PATH

MAIL_BINARY=/usr/local/bin/simplesendmail.bash

# Construit la variable $BACKUP_REPO_PREFIX
if [ -n "${BACKUP_REPO_PREFIX_HOST}" ]
then
    if [ -z ${BACKUP_REPO_PREFIX_PORT} ]
    then
      BACKUP_REPO_PREFIX_PORT=22
    fi
    BACKUP_REPO_PREFIX="ssh://${BACKUP_REPO_PREFIX_HOST}:${BACKUP_REPO_PREFIX_PORT}${BACKUP_REPO_PREFIX_PATH}"
    
    # Pour prévenir le conteneur distant qu'on est au travail... ou au contraire qu'on a fini
    BACKUP_START="ssh -p ${BACKUP_REPO_PREFIX_PORT} ${BACKUP_REPO_PREFIX_HOST} start $BACKUP_HOSTNAME"
    BACKUP_STOP="ssh -p ${BACKUP_REPO_PREFIX_PORT} ${BACKUP_REPO_PREFIX_HOST} stop $BACKUP_HOSTNAME"
else
    BACKUP_START=""
    BACKUP_STOP=""
    BACKUP_REPO_PREFIX=${BACKUP_REPO_PREFIX_PATH}
fi

# On peut appeler init_path maintenant 
# On vérifie que les ressources sont bien montées
for p in ${BACKUP_RESSOURCES}
do
    read base ress_path autres <<< $(init_path $p)
    [ ! -d "${ress_path}" ] && echo "ERREUR - La ressource ${ress_path} n'existe pas" && ok=0
done

# On sort si quelque chose ne va pas
[ "${ok}" = "0" ] && exit 1
    
