#!/bin/bash

#
# This file is part of the houaibe software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# Ce script sauvegarde un ou plusieurs répertoires (appelés ressources) soit sur un dépôt local
# (par exemple un disque externe) soit sur une machine distante.
#
# Il doit être exécuté à l'intérieur du conteneur pic-bbackup
#
# Tout est réglé par les variables d'environnement suivantes:
#
#    BACKUP_REPO_PREFIX_HOST: Si elle est définie, le nom de la machine distante. Sinon, la sauvegarde se fait en local
#                             Obligatoirement sous la forme user@machine.exemple.org
#
#    BACKUP_REPO_PREFIX_PORT: Si on sauvegarde à distance, le numéro de port. 
#                             Valeur par défaut 22 (port ssh)
#
#    BACKUP_REPO_PREFIX_PATH: Chemin absolu vers le répertoire parent des dépôts borg. 
#
#    BACKUP_RESSOURCES: Les volumes à sauvegarder (répertoire ou volume Docker)
#
#    Exemples:
#        BACKUP_REPO_PREFIX_HOST=root@machine.exemple.org
#        BACKUP_REPO_PREFIX_PORT=1234
#        BACKUP_REPO_PREFIX_PATH=/backup
#        BACKUP_RESSOURCES="/usr/data1 /usr/data2 /usr/data3"
#
#    Les trois répertoires /usr/data1 etc. seront sauvegardés dans les dépôts /backup/data1, /backup/data2, /backup/data3 sur la machine machine.exemple.org
#
#    ATTENTION: On ne doit pas spécifier deux fois le même nom de répertoire, même avec un chemin différent.
#               Par exemple: Si BACKUP_RESSOURCES="/home/user1/data /home/user2/data" ça ne va pas fonctionner 
#
#    La première fois le dépôt est initialisé (borg init)
#    Les ressources sont sauvegardées en même temps
#
# Dans le cas d'une sauvegarde distante, le conteneur distant est arrêté lorque ce script se termine (commande stop)
#    
# Usage: .../borgbackup.bash
#

# -------------------------------------------------------------------
# Fonctions

#
# Appelle le script is_borg_repo.bash
#

function is_borg_repo() {
    if [ -z "$BACKUP_REPO_PREFIX_HOST" ]
    then
        rep=$("/usr/local/bin/is_borg_repo.bash" $1)
    else
        rep=$(ssh -p "$BACKUP_REPO_PREFIX_PORT" "$BACKUP_REPO_PREFIX_HOST" is_borg_repo $1)
    fi

    echo $rep
}

#
# Fait effectivement la sauvegarde, thread safe
#
# Usage: do_bbackup $path $base $repo $pass_file 
#
function do_bbackup() {
    local path base ress_path backup_repo borg_pass_file borg_key_file backup_repo_path
    path=${1}
    base=${2}
    ress_path=${3}
    backup_repo=${4}
    borg_pass_file=${5}
    borg_key_file=${6}
    backup_repo_path=${7}
    
    # Si on trouve le fichier de mot de passe, on considère que le dépot est initialisé.
    # Sinon, on tente de l'initialiser
    IBD=$(is_borg_repo $backup_repo_path)
    case "$IBD" in
    "INIT")
        if [ ! -f "${borg_pass_file}" ]
        then
            export BORG_PASSPHRASE=$(openssl rand -base64 40)
            echo "=> [$(date "+%F %T")] INITIALISATION DU DEPOT ${backup_repo} ..."
            echo ${BORG_BINARY} init --make-parent-dirs --encryption repokey $backup_repo
            if ${BORG_BINARY} init --make-parent-dirs --encryption repokey $backup_repo
            then
                echo $BORG_PASSPHRASE > $borg_pass_file
                ${BORG_BINARY} key export --paper ${backup_repo} $borg_key_file 
                chmod 400 $borg_pass_file $borg_key_file
    
                echo "=============================================================="
                echo "LE DEPOT $backup_repo A ETE INITIALISE"
                echo "PENSEZ A GARDER LA CLE EN LIEU SUR ! "
                echo "FICHIERS A CONSERVER:"
                echo "                     $borg_pass_file"
                echo "                     $borg_key_file"
                echo "=============================================================="
             else
                echo "ERREUR - ${backup_repo} n'a pas pu être initialisé"
                return 1
            fi
            echo "<= [$(date "+%F %T")] ...Initialisation terminée pour ${backup_repo}"
        else
            echo "ERREUR - Il y a un fichier de mot de passe (${borg_pass_file}), je n'initialise pas le dépôt ${backup_repo}"
            return 1
        fi
    ;;
    "OK")
        if ! BORG_PASSPHRASE=$(cat $borg_pass_file); then echo "ERREUR - Le fichier $borg_pass_file ne peut pas être lu."; exit 1; fi
        export BORG_PASSPHRASE
    ;;
    "KO")
        echo "ERREUR - ${backup_repo} n'est pas un dépôt et ne PEUT PAS être initialisé"
        return 1
    ;;
    *)
        echo "ERREUR INTERNE - IBD vaut $IBD "
        return 1
    ;;
    esac
    
    # Tenir compte de la liste d'exclusion s'il y en a une
    local borg_create_options=${BORG_CREATE_OPTIONS}
    [ -f "${ress_path}/$F_BORG_EXCLUDE_LIST" ] &&  borg_create_options="${borg_create_options} --exclude-from ${ress_path}/$F_BORG_EXCLUDE_LIST "
  
    # Autres options à ajouter éventuellement
    [ -f "${ress_path}/$F_BORG_CREATE_OPTIONS" ] &&  borg_create_options="${borg_create_options} $(cat ${ress_path}/$F_BORG_CREATE_OPTIONS) "  
   
    echo "=> [$(date "+%F %T")] SAUVEGARDE ${path} sur le dépôt ${backup_repo} ..."
    echo ${BORG_BINARY} create ${borg_create_options} ${backup_repo}::${BACKUP_NAME} "${ress_path}"
    ${BORG_BINARY} create ${borg_create_options} ${backup_repo}::${BACKUP_NAME} "${ress_path}"
    local sts=$?
    echo "<= [$(date "+%F %T")] ...FINI POUR ${path}"
    echo ""

    # Sortir en cas d'erreur
    if [ ! "$sts" = "0" ]; then return 1; fi

    # ELAGUAGE

    # Stratégie d'élagage éventuelle
    borg_prune_options=$BORG_PRUNE_OPTIONS_BASE
    if [ -f "${ress_path}/$F_BORG_PRUNE_OPTIONS_STRAT" ] 
    then
        borg_prune_options="${borg_prune_options} $(cat ${ress_path}/$F_BORG_PRUNE_OPTIONS_STRAT) "
    else
        borg_prune_options="${borg_prune_options} ${BORG_PRUNE_OPTIONS_STRAT} "
    fi
  
    echo "=> [$(date "+%F %T")] ELAGUAGE ${path} sur le dépôt ${backup_repo} ..."
    echo ${BORG_BINARY} prune ${borg_prune_options} ${backup_repo}
    ${BORG_BINARY} prune ${borg_prune_options} ${backup_repo}
    echo "<= [$(date "+%F %T")] ...FINI POUR ${path}"
    echo ""

}

# Cette fonction est appelée par le trap
# lorsque le script se termine
# Elle ne fait pas grand-chose, tout est déjà affiché !
# On n'a rien mis dans les logs
function terminer() {

    # Infanticides ;)
    [ -n "$(jobs -p)" ] && kill $(jobs -p) 2>/dev/null
    
    # Pour ne pas tomber deux fois dans le piège !
    trap - EXIT
      
    maildest=${BACKUP_MAIL}
    mailbody="/tmp/mailbody.txt"

    borg_logs=""
    ErrorsCount=0
    WarningsCount=0;
    for p in ${BACKUP_RESSOURCES}
    do
        read base ress_path log autres <<< $(init_path "${p}")
        borg_logs="$borg_logs $log"

        # On compte les lignes: terminating with error status, rc X 
        # avec X!=0
        cnt1=$(grep -c 'terminating with .* status' $log)
        
        # S'il n'y a aucune ligne de status (-> pas de borg lancé)
        if [ "$cnt1" -eq 0 ]
        then 
            (( ErrorsCount += 1));
        else
            cnt_rc1=$(grep 'terminating with .* status' $log | grep -c 'rc 1')
            cnt_rc2=$(grep 'terminating with .* status' $log | grep -c 'rc 2')
            
            [ "$cnt_rc1" -gt "0" ] && (( WarningsCount += 1));
            [ "$cnt_rc2" -gt "0" ] && (( ErrorsCount += 1));
        fi
          
        # Ecriture du mail
        echo "RESSOURCE $p" >> $mailbody
        grep -E '^Archive name|^Duration|^terminating with|^Number of files' $log >>$mailbody
        echo "--------------------------------------------------------------" >> $mailbody
        sed -n '/Original size/, /^Chunk/p' $log >>$mailbody
        echo "--------------------------------------------------------------" >> $mailbody
    done

    # En cas d'erreurs ou warnings on ajoute l'intégralité des logs
    if [ $(( ErrorsCount + WarningsCount )) -eq 0 ]
    then
        cat $mailbody | $MAIL_BINARY "Sauvegarde - $BACKUP_HOSTNAME - OK" ${maildest}
    else 
        if [ "$ErrorsCount" -gt "0" ]
        then
            cat $mailbody $borg_logs | $MAIL_BINARY "Sauvegarde - $BACKUP_HOSTNAME - ${ErrorsCount} ERREURS" ${maildest}
        else
            cat $mailbody $borg_logs | $MAIL_BINARY "Sauvegarde - $BACKUP_HOSTNAME - ${WarningsCount} WARNINGS" ${maildest}
        fi
    fi

    # A chaque sauvegarde on ajoute un fichier log
    # TODO - Il faudra gérer ces logs !  
    cat $borg_logs >> "/basecont/log/${BACKUP_NAME}.log"

    # Ménage
    rm $borg_logs $mailbody

    # Signaler que le backup est terminé
    # En cas de dépôt distant, s'il n'y a pas d'autre opération en cours et si le conteneur est resté up assez de temps
    # cela déclenche l'arrêt du conteneur
    $BACKUP_STOP
}

# --------------------------------------------------------
# main 

# Calcul du chemin du fichier d'include
FULLPATH=$(readlink --no-newline --canonicalize-existing $0) 
if [ -z "$FULLPATH" ]; then echo "ERREUR - On pointe sur le vide !"; exit 1; fi
BORGBACKUP_INCLUDE="$(dirname "$FULLPATH")/borgbackup-include.bash"

. ${BORGBACKUP_INCLUDE}

trap terminer 1 2 3 15 EXIT

#
# Initialisation des fichiers d'exclusion et de création d'options
#

F_BORG_EXCLUDE_LIST=.borg-exclude-list
F_BORG_CREATE_OPTIONS=.borg-create-options
for p in ${BACKUP_RESSOURCES}
do
    read base ress_path log autres <<< $(init_path $p)
    if [ ! -f "${ress_path}/${F_BORG_EXCLUDE_LIST}" ]
    then
        echo -e "${ress_path}/*/.cache/*\n/tmp/*" >"${ress_path}/${F_BORG_EXCLUDE_LIST}" 2>> ${log}
        if [ ! "$?" = "0" ]; then echo "ERREUR ! "; exit 1; fi 


        if [ "$p" = "/" ]
        then
            echo -e "${ress_path}/proc\n${ress_path}/sys\n" >>"${ress_path}/${F_BORG_EXCLUDE_LIST}"  2>> ${log}
            if [ ! "$?" = "0" ]; then echo "ERREUR ! "; exit 1; fi
        fi
        
        
        
        echo -e '\n' >>"${ress_path}/${F_BORG_EXCLUDE_LIST}"  2>> ${log}
        if [ ! "$?" = "0" ]; then echo "ERREUR ! "; exit 1; fi
    fi

    if [ ! -f "${ress_path}/${F_BORG_CREATE_OPTIONS}" ]
    then
        echo -e "--one-file-system " >"${ress_path}/${F_BORG_CREATE_OPTIONS}"  2>> ${log}
        if [ ! "$?" = "0" ]; then echo "ERREUR ! "; exit 1; fi 
    fi
done 

# Stratégie d'élagage, peut être modifiée par l'utilisateur
F_BORG_PRUNE_OPTIONS_STRAT=.borg-prune-options-strat

# Signaler que le backup a commencé
# Utile en cas de dépôts distants (voir le STOP dans la fonction termine)
$BACKUP_START

# Sauvegarder les chemins demandés et élaguer
for p in ${BACKUP_RESSOURCES}
do
    read base ress_path log repo pass_file key_file repo_path <<< $(init_path "${p}")
    do_bbackup ${p} ${base} ${ress_path} ${repo} ${pass_file} ${key_file} ${repo_path} > ${log} 2>&1 &
done

wait

# Appelle la fonction terminer()
exit 0
