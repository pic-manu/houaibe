#! /bin/bash

#
# This file is part of the houaibe software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

# Envoie un mail en utilisant smtp
#

#
# Usage:
#   cat some-files | simplesendmail.bash $SUBJECT $TO

SUBJECT=$1
TO=$2

if [ -z "$SUBJECT" ]
then
    echo "ERREUR - PAS DE SUJET !"
    exit 1
fi

if [ -z "$TO" ]
then
    echo "ERREUR - PAS DE DESTINATAIRE !"
    exit 1
fi

# Envoyer via msmtp en ajoutant quelques headers pour ne pas ressembler à un spam !
HEADERS="Subject: $SUBJECT\nContent-Transfer-Encoding: 8bit\nMIME-Version: 1.0\nContent-Type: text/plain; charset=UTF-8\nTo:$TO\n"
( echo -e "${HEADERS}\n"; cat - ) | /usr/bin/msmtp -t $TO
