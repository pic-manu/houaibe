#! /bin/bash

#
# Script d'installation du répertoire associé au conteneur 
#
# Usage: cd ....
#        ./installcont.sh
#

if [[ ! -f ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf ]]
then
   echo "houaibe n'est pas encore configuré"
   exit 1;
fi

# Lire les paramètres généraux et spécifiques
. ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf
. ./houaibe.conf

# Repertoire courant
DIR=$CONTAINERSDIR/$CONTAINER

# Rien à faire si le répertoire existe déjà
[[ -d $DIR ]] && echo "Le repertoire $DIR existe - Je ne fais rien" && exit 0

echo "installation du conteneur $CONTAINER"
mkdir -p $DIR
chmod g+w $CONTAINERSDIR/$CONTAINER

# Copie de fichiers
cp run.sh.dist $DIR/run.sh
ln -sf $DIR/run.sh .
cp houaibe.conf $DIR
cp ../scripts/stop.sh $DIR
ln -sf $DIR/stop.sh .

IMGDIR=$(pwd)

(
    cd $DIR
    chmod 550 run.sh

    # ----- Début de partie spécifique à chaque conteneur
    # Installation de ssh la première fois
    echo "Installation de ssh - Attendez quelques secondes SVP"

    # Première exécution du conteneur = création des fichiers de clé de ssh
    mkdir .ssh
    ssh-keygen -t ed25519 -N '' -f $(pwd)/.ssh/id_ed25519
    mkdir log

    # Création du répertoire .config
    mkdir .config && chmod go= .config

    # ----- Fin de partie spécifique
)

# Donner au groupe docker la permission de bosser !
chgrp -R docker $DIR


