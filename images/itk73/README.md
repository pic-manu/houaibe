# itk73

L'image `itk73` permet de construire un conteneur qui repose sur une debian 10, avec dedans apache2 ainsi que les modules php 7.3 et mpm-itk

Il peut accueillir tous les CMS qui peuvent tourner dans cette version de php

## Hébergement https:

La configuration apache de ce conteneur est conçue pour un hébergement https. Si vous préférez héberger les sites en http, en particulier s'il s'agit de sites sous wordpress ou drupal, vous devrez probablement modifier le fichier de configuration d'apache (variable d'environnement HTTPS).

Vous pouvez aussi modifier le "template" (répertoire `sites-available/pic/template`) afin que les sites créés  avec picgs aient la bonne valeur.

Faites comme vous le souhaitez, mais sachez que la bonne pratique est bien d'héberger en https....

## Installation:

- **Construire** le conteneur:

```
cd $WORK/images/itk74s
./build.sh
```

- **Installer** les fichiers et volumes utilisés par le conteneur:

```
sudo ./installcont
```

- **Démarrer** le conteneur:

```
./run.sh
```

## Mise à jour:

Il est bon de reconstruire le conteneur de temps en temps, afin de bénéficier des mises à jour mises à disposition sur le DockerHub:

```
cd $WORK/images/proxy
./build.sh
```

Si `houaibe` a été mis à jour, et donc le Dockerfile ou d'autres fichiers, il est préférable d'ignorer le cache de Docker:

```
cd $WORK/images/proxy
NOCACHE=1 ./build.sh 
```

