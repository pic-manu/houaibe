#! /bin/bash

#
# Script d'installation du répertoire associé au conteneur 
#
# Usage: cd ....
#        ./installcont.sh
#

if [[ ! -f ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf ]]
then
   echo "houaibe n'est pas encore configuré"
   exit 1;
fi

# Lire les paramètres généraux et spécifiques
. ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf
. ./houaibe.conf

# Reprtoire courant
DIR=$CONTAINERSDIR/$CONTAINER

# Rien à faire si le répertoire existe déjà
[[ -d $DIR ]] && echo "Le repertoire $DIR existe - Je ne fais rien" && exit 0

echo "installation du conteneur $CONTAINER"
mkdir -p $DIR
chmod g+w $CONTAINERSDIR/$CONTAINER

# Copie de fichiers
cp run.sh.dist $DIR/run.sh
ln -sf $DIR/run.sh .
cp houaibe.conf $DIR

IMGDIR=$(pwd)

(
    cd $DIR
    chmod 550 run.sh

    # ----- Début de partie spécifique à chaque conteneur
    mkdir -p sites-available/pic sites-enabled
    mkdir log
    cp $IMGDIR/../../picgs/conf/template-pic-itk73 sites-available/pic

    # Creer un fichier vide env.txt et le rendre rw-rw----
    touch env.txt && chmod 660 env.txt

    # ----- Fin de partie spécifique
)

# Donner au groupe docker la permission de bosser !
chgrp -R docker $DIR


