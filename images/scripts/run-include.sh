#
# Ce fichier est sourcé par la plupart des scripts run.sh
#

# Mode DEVT - NE PAS UTILISER EN PRODUCTION
# DEVT=1 ./run.sh

if [ "${DEVT}" ]
then
    echo -e "\033[31;1mATTENTION - devt\033[0m"
    SWSECU="--security-opt no-new-privileges"
else
    echo prod
    SWSECU="--cap-drop=ALL --cap-add=NET_RAW --cap-add=NET_BIND_SERVICE --cap-add=SETUID --cap-add=SETGID --cap-add=CHOWN --cap-add=DAC_OVERRIDE"
    SWSECU="$SWSECU --security-opt no-new-privileges"
    SWSECU="$SWSECU --read-only --tmpfs /tmp:rw,noexec,nosuid --tmpfs /var/lib/php/sessions:rw,noexec,nosuid --tmpfs /run:rw,noexec,nosuid"
fi

