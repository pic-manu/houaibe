# Creation du fichier env.txt qui sera utilise par le proxy nginx
# Utilise par les run.sh des conteneurs pic-itkXXX
#
# Si pas de sites enabled on definit la variable NO_VIRTUAL_HOST, qui sera inutilisée
# Si des sites sont trouvés on définit VIRTUAL_HOST pour le proxy nginx
#
# Manu, oct 2020
#

VH=$(cat sites-enabled/*.conf 2>/dev/null | fgrep -i -e servername -e serveralias|fgrep -v '#' |expand|tr -s ' '|cut -d ' ' -f 3 | tr '\n' ',' | sed -e 's/,$//' )
if [ -z $VH ]
then
    echo NO_VIRTUAL_HOST="1" >env.txt
else
    echo VIRTUAL_HOST="$VH" > env.txt
fi

