#! /bin/bash

#
# Script pour envoyer TOUTES les images utiles sur le dépôt Docker
#
# Usage: /path/to/houaibe/images/scripts/pushall.sh
#

# Variables générales
if [[ -f ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf ]]; then source ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf; else echo "Merci de terminer la configuration en créant le fichier /usr/local/etc/houaibe/houaibe.conf"; exit 1; fi

# Calcul du repertoire dans lequel pushall.sh est depose - ca marche meme si on part d'un lien symbolique
fullpath=$(readlink --no-newline --canonicalize-existing $0)
if [[ -z $fullpath ]]; then echo "ERREUR - $0 pointe sur le vide !"; exit 1; fi
DIR=$(dirname "$fullpath")

# On pousse les conteneurs qu'on construit... sauf deb4pic et deb4pica qui ne servent qu'au build
IMAGES="maint ssh proxy phpmyadmin ${DKRBUILDALL} ${DKRSRVIMG}" 
for image in ${IMAGES}
do
    # On ne pousse pas deb4pic et deb4pic4, qui ne servent qu'à la construction des conteneurs	
    if [ "${image:0:7}" != "deb4pic" ]
    then
       $DIR/push.sh "$image"
    fi
done

