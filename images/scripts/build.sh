#! /bin/bash

#
# Script de construction d'une image
# Ne reconstruit pas les images intermédiaires si elles existent déjà
# Utile pour mettre au point le Dockerfile
#
# Ce script construit l'image à partir du dockerfile 
#
# Le nom de l'image est dans houaibe.conf 
#
# On construit deux tags: 
#         - %y%m (2110 pour Octobre 2021)
#         - latest
#
# Usage:
#     cd houaibe/images/itkxy && ./build.sh
#     cd houaibe/images/itkxy && NOCACHE=1 ./build.sh
#

#set -x

# Variables générales
if [[ -f ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf ]]; then source ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf; else echo "Merci de terminer la configuration en créant le fichier /usr/local/etc/houaibe/houaibe.conf"; exit 1; fi

# Variables spécifiques à cette image
if [[ -f ./houaibe.conf ]]; then source ./houaibe.conf; else  echo "houaibe n'est pas correctement installé, ou le nom de conteneur est faux..."; exit 1; fi

TAG=$(date +%y%m)
[ -n NOCACHE ] && NOCACHE="--no-cache"

docker build ${NOCACHE} ${FROM:+--build-arg FROM=${FROM}} -t $IMAGE:$TAG . 
docker image tag $IMAGE:$TAG $IMAGE:latest 
