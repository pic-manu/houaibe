#! /bin/bash

#
# Script de construction de TOUTES les images, dans l'ordre qui va bien
# On utilise les .upgrade.sh 
# Utile pour la mise à jour de tous les conteneurs
#
# Usage: 
# houaibe/images/scripts/upgradeall.sh
# NOCACHE=1 && houaibe/images/scripts/upgradeall.sh
#
# 
# Variables générales
if [[ -f ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf ]]; then source ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf; else echo "Merci de terminer la configuration en créant le fichier /usr/local/etc/houaibe/houaibe.conf"; exit 1; fi

# Calcul du repertoire dans lequel buildall.sh est depose - ca marche meme si on part d'un lien symbolique
fullpath=$(readlink --no-newline --canonicalize-existing $0)
if [[ -z $fullpath ]]; then echo "ERREUR - $0 pointe sur le vide !"; exit 1; fi
DIR=$(dirname "$fullpath")

EXE="build.sh"

#set -v
images="deb4pic deb4pica maint ssh proxy phpmyadmin ${DKRBUILDALL}"
for rep in $images
do
    echo "================="
    echo "Travail dans $rep"
    echo "================="
    if ! (cd $DIR/../$rep && ./${EXE} "${NOCACHE}"); then exit 1; fi
done
