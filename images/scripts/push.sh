#! /bin/bash

#
# Script de récupération d'une image sur le dépôt docker
#
# Usage: .../path/to/push.sh image
#
# Exemple: .../path/to/push lepic/ssh
#
# Ce script:
#    - Vérifie s'il y a une ou deux images à envoyer: 
#        -> S'il y a un - dans le nom il faut récupérer deux images
#        -> Sinon, une seule image
#    - Envoie la ou les images (tag latest) sur le dépôt Docker
#      On envoie le tag %y%m (2110 pour Octobre 2021), et aussi le tag latest
#      En principe ce sont les mêmes (si on vient de construire l'image)
#

#set -v

# Variables générales
if [[ -f ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf ]]; then source ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf; else echo "Merci de terminer la configuration en créant le fichier /usr/local/etc/houaibe/houaibe.conf"; exit 1; fi

if [ ! "$#" == "1" ]
then
    echo "ERREUR - PAS OU TROP DE PARAMETRE !"
    exit 1
fi

IMAGE=$1
TAG=$(date +%y%m)

if [ -z $DKRCPTE ]
then
    echo "La variable DKRCPTE n'est pas positionnée dans houaibe.conf !"
    exit 1
fi

# xxx-yyy = 2 images !
IFS='-' read -r -a IMAGES <<<"${IMAGE}"
for image in ${IMAGES[*]}
do
    for tag in $TAG latest
    do
        echo docker push ${DKRCPTE}/${image}:$tag
        docker push ${DKRCPTE}/${image}:$tag
    done
done

