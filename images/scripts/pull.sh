#! /bin/bash

#
# Script de récupération d'une image sur le dépôt docker
#
# Usage: /path/to/houaibe/imges/scripts/pull.sh image
#
# Ce script:
#    - Vérifie s'il y a une ou deux images à récupérer: 
#        -> S'il y a un - dans le nom il faut récupérer deux images
#        -> Sinon, une seule image
#    - Récupère la ou les images (tag latest) sur le dépôt Docker
#

#set -v

# Variables générales
if [[ -f ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf ]]; then source ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf; else echo "Merci de terminer la configuration en créant le fichier /usr/local/etc/houaibe/houaibe.conf"; exit 1; fi

IMAGE=$1

if [ -z $DKRCPTE ]
then
    echo "La variable DKRCPTE n'est pas positionnée dans houaibe.conf !"
    exit 1
fi

# xxx-yyy = 2 images !
tag=latest
IFS='-' read -r -a IMAGES <<<"${IMAGE}"
for image in ${IMAGES[*]}
do
    echo docker pull ${DKRCPTE}/${image}:$tag
    docker pull ${DKRCPTE}/${image}:$tag
done

