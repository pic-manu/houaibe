#! /bin/bash

#
# Script pour récupérer TOUTES les images utiles depuis le dépôt Docker
#
# Usage: /path/to/houaibe/images/scripts/pullall.sh
#

# Variables générales
if [[ -f ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf ]]; then source ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf; else echo "Merci de terminer la configuration en créant le fichier /usr/local/etc/houaibe/houaibe.conf"; exit 1; fi

if [ -z $DKRCPTE ]
then
    echo "La variable DKRCPTE n'est pas positionnée dans houaibe.conf !"
    exit 1
else
    echo "docker pull $DKRCPTE/$IMAGE"
    docker pull $DKRCPTE/$IMAGE
fi

# Calcul du repertoire dans lequel pullall.sh est depose - ca marche meme si on part d'un lien symbolique
fullpath=$(readlink --no-newline --canonicalize-existing $0)
if [[ -z $fullpath ]]; then echo "ERREUR - $0 pointe sur le vide !"; exit 1; fi
DIR=$(dirname "$fullpath")

#set -v
images="maint ssh proxy phpmyadmin ${DKRBUILDALL}"
for image in ${images}
do
    $DIR/pull.sh $image
done
