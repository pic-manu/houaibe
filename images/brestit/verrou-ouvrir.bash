#!/bin/bash

#
# This file is part of the houaibe software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

# Calcul du chemin du fichier d'include
FULLPATH=$(readlink --no-newline --canonicalize-existing $0) 
if [ -z "$FULLPATH" ]; then echo "ERREUR - On pointe sur le vide !"; exit 1; fi
BORGBACKUP_INCLUDE="$(dirname "$FULLPATH")/borgbackup-include.bash"

. ${BORGBACKUP_INCLUDE}

function do_ouvrir() {

    local pass_file=${1}
    local repo=${2}
  
    export BORG_PASSPHRASE=$(cat $pass_file)
    ${BORG_BINARY} break-lock ${repo}
}

#
# main
#

for p in ${BACKUP_RESSOURCES}
do
    read base ress_path log repo pass_file key_file repo_path <<< $(init_path "/ressources/${p}")
    echo "========================="
    echo -n "${repo} -> "
    if do_ouvrir ${pass_file} ${repo}
    then
       echo OK
    else
       echo "KO - ERREUR $rvl"
    fi
done

echo "========================="

