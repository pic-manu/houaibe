#! /bin/bash

# Ajout de paramètres à la config ssh
# cf. https://borgbackup.readthedocs.io/en/stable/usage/serve.html
echo -e "\n    ServerAliveInterval 10\n    ServerAliveCountMax 30\n" >>/etc/ssh/ssh_config

/usr/local/bin/borgrestit.bash

