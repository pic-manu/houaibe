#!/bin/bash

#
# This file is part of the houaibe software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# Ce script est utilisé pour les restitutions de fichiers sauvegardés avec pic-bbackup
#
# Il doit être exécuté à l'intérieur du conteneur pic-brestit
#
# Les ressources déclarées dans BACKUP_RESSOURCES sont montées en fuse, puis on donne la main à l'utilisateur
# Pour faire la restitunio il n'y a plus qu'à faire cp -a depuis le répertoire /restit sur /ressources
#
# Dans le cas d'une sauvegarde distante, le conteneur distant est arrêté lorque ce script se termine (commande stop)
#    
# Usage: .../borgrestit.bash
#       

# Usage
if [ "${1}" = "-h" ] ||  [ "${1}" = "--help" ]
then
   echo "Usage: borgrestit.bash"
   exit 1
fi

# Calcul du chemin du fichier d'include
FULLPATH=$(readlink --no-newline --canonicalize-existing $0) 
if [ -z "$FULLPATH" ]; then echo "ERREUR - On pointe sur le vide !"; exit 1; fi
BORGBACKUP_INCLUDE="$(dirname "$FULLPATH")/borgbackup-include.bash"

. ${BORGBACKUP_INCLUDE}

BACKUP_RESTIT="/restit"
[ -z "${BACKUP_RESTIT}" ] && echo "ERREUR - Pas de BACKUP_RESTIT" && ok=0

# Usage: do_bbackup $path $base $repo $pass_file 
function do_brestit() {

    local path base backup_repo borg_pass_file
    path=${1}
    base=${2}
    backup_repo=${3}
    borg_pass_file=${4}

    local mountpoint="${BACKUP_RESTIT}/${BACKUP_HOSTNAME}-${base}"
    mkdir -p ${mountpoint}
    
    export BORG_PASSPHRASE=$(cat $borg_pass_file)
    
    echo ${BORG_BINARY} mount ${backup_repo} ${mountpoint}
    ${BORG_BINARY} mount ${backup_repo} ${mountpoint}
}

function terminer {
    # Pour ne pas tomber deux fois dans le piège !
    trap - EXIT
      
    for p in ${BACKUP_RESSOURCES}
    do
        local path base others
        read base others <<< $(init_path /ressources/${p})
                
        local mountpoint="${BACKUP_RESTIT}/${BACKUP_HOSTNAME}-${base}"
        borg umount $mountpoint
    done

    # Signaler que le backup est terminé
    # En cas de dépôt distant, s'il n'y a pas d'autre opération en cours déclenche l'arrêt du conteneur
    $BACKUP_STOP
}

trap terminer 1 2 3 15 EXIT

# Signaler que le backup a commencé
# Utile en cas de dépôts distants (voir le STOP dans la fonction terminer)
# echo $BACKUP_START
$BACKUP_START


for p in ${BACKUP_RESSOURCES}
do
    read base ress_path log repo pass_file autre <<< $(init_path ${p})
    do_brestit ${p} ${base} ${repo} ${pass_file} 2>&1
done

# On fait la restitution par un ou des cp -a
echo ""
echo "VOUS POUVEZ MAINTENANT RESTITUER FICHIERS OU REPERTOIRES PAR LA COMMANDE:"
echo ""
echo "cp -a ${BACKUP_RESTIT}/source destination"
echo ""
echo "Lorsque vous aurez terminé vos sauvegardes, tapez CTRL-D ou exit pour sortir"

/bin/bash

# Le démontage se fait dans la fonction terminer
