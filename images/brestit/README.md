# brestit

Les trois conteneurs `pic-bbackup`, `pic-brestit` et `pic-bbackup-srv` permettent de sauvegarder des répertoires ou des volumes Docker, en utilisant le logiciel `borgbackup` (cf. https://www.borgbackup.org)

`pic-bbackup` assure la sauvegarde, `pic-brestit` permet de restaurer les données, et `pic-bbackup-srv` est utilisé dans le cas de sauvegardes sur une machine distante.

Le fait d'utiliser un conteneur sur la machine distante augmente la sécurité car celui-ci ne sera allumé que lors des sauvegardes, donc la plupart du temps toute connexion sera impossible depuis la machine cliente sur le serveur de  sauvegarde. La machine de sauvegarde doit bien sûr être particulièrement protégée (peu de comptes, authentification par clé ssh uniquement, etc.)

## Installation:

- **Construire** le conteneur:

```
cd $WORK/images/brestit
./build.sh
```

- **Installer** les fichiers et volumes utilisés par le conteneur:

```
sudo ./installcont
```

### Configuration:

`pic-brestit` utilisant les mêmes clés ssh que `pic-bbackup`, il n'y a pas de configuration particulière à effectuer pour la communication ssh, celle-ci a déjà été réalisée ors de l'installation de `pic-bbackup`.

### Restitution de fichiers

Pour restituer des fichiers sauvegardés, il faut:

- Se connecter sur la machine distante *(sauvegarde.exemple.org*) et lancer le conteneur `pic-bbackup-srv`:

  ```
  toctoc start pic-bbackup-srv
  ```

- Sur la machine cliente, lancer le conteneur `pic-brestit` par:
  
  ```
  toctoc start pic-brestit
  ```
  
  Les dépôts correspondant aux ressources définies dans `BACKUP_RESSOURCES` sont montés (en lecture seule) dans le répertoire `/restit`, les ressources elles-mêmes sont montées dans le répertoire `/ressources`. Pour restituer les données, il suffit alors de faire un `cp -a` à partir d'un sous-répertoire de  `/restit` sur `/ressources`

**ATTENTION:** `borg mount` fonctionne de manière paresseuse: le mount est très rapide, mais lorsqu'on a choisi un dépôt le `ls` peut prendre quelques secondes ou minutes (suivant la taille de la sauvegarde) avant de donner un résultat. De même, un `find /restit` *n'est pas du tout recommandé*, car il provoquera la lecture de tous les dépôts, ce qui peut prendre des ressources... et du temps.

## Verrous:

Il peut arriver que rien (c'est-à-dire ni la sauvegarde ni la restitution) ne fonctionne à cause de la présence d'un verrou sur le serveur. Dans ce cas, la commande suivante exécutée sur la machine cliente permet d'ouvrir les verrous:

```
toctoc start pic-brestit verrou-ouvrir.bash
```

## Arrêt du conteneur:

Lorsque la restitution des fichiers est terminée, il convient de sortir du conteneur proprement en tapant `CTRL-D` ou `exit`. Le conteneur distant est alors éteint automatiquement, puis `pic-brestit` est éteint à son tour.

## Mise à jour:

Il est bon de reconstruire le conteneur de temps en temps, afin de bénéficier des mises à jour mises à disposition sur le DockerHub:

```
cd $WORK/images/bbackup
NOCACHE=1 ./build.sh .h
```



