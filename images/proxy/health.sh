#! /bin/sh

if wget -4 --quiet --no-verbose --tries=1 --spider http://localhost/ 
then
   echo "$(hostname -s) OK"
else
   echo "$(hostname -s) - serveur inaccessible"
fi
