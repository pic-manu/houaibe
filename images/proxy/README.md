# proxy

L'image `proxy` permet de construire un reverse proxy qui repose sur nginx, avec génération automatique du fichier de configuration.

Cette image repose sur le proxy nginx de Jason Wilder. Quelques modifications ont été apportées:

- Le fichier `nginx.tmpl` est légèrement modifié par rapport au conteneur original, et est exposé de manière à simplifier sa personnalisation éventuelle.
- Les `aliases` permettant d'utiliser `dehydrated` pour générer les certificats ont été intégrés à la configuration (fichier `vhost.d/default`)
- Les urls de wordpress wplogin sont protégées contre les dénis de service (fichiers `conf.d/0limit.conf` et `vhost.d/default`)
- Le cahce de nginx peut être activé si on le désire (voir ci-dessous)

## Installation:

- **Construire** le conteneur:

```
cd $WORK/images/proxy
./build.sh
```

- **Installer** les fichiers et volumes utilisés par le conteneur:

```
sudo ./installcont
```

- **Démarrer** le conteneur:

```
./run.sh
```

## Mise à jour:

Il est bon de reconstruire le conteneur de temps en temps, afin de bénéficier des mises à jour mises à disposition sur le DockerHub:

```
cd $WORK/images/proxy
./build.sh
```

Si `houaibe` a été mis à jour, et donc le Dockerfile ou d'autres fichiers, il est préférable d'ignorer le cache de Docker:

```
cd $WORK/images/proxy
NOCACHE=1 ./build.sh 
```

## Mise en service du cache nginx:

1. Changer le nom du fichier `conf.d/b-cache.conf.dist`:

   ```
   cd conf.d && mv b-cache.conf.dist b-cache.conf
   ```

2. Pour vérifier que le cache est utilisé, décommentez une ligne du fichier `vhost.d/default`:

   ```
   add_header X-Cache-Status $upstream_cache_status;
   ```

3. Suivant le CMS, le cache sera activé ou non: il faut en effet que le CMS envoie le header `cache-control` avec un formattage correct, par exemple:

   ```
   cache-control: max-age=300
   ```

   Avec wordpress, il est nécessaire pour cela d'installer le plugin `cache-control` (installé par défaut lors de l'installation de wordpress)
   Avec spip, le cache ne sera pas activé
   Avec dolibarr, galette, paheko non plus
