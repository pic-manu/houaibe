# Nextcloud

Installation d'une instance Nextcloud - voir https://nextcloud.com 

Ce code est une intégration du code proposé par le groupe Oxytanet: cf. https://framagit.org/oxyta.net/docker-atelier/-/tree/master/cloud

## Installation:

Créez un volume docker appelé `cloud-data`:

```
docker volume create cloud-data
```
Appelez le script d'installation:

```
cd images/cloud
sh ./installcont.sh
```

Le script se termine en vous proposant de vous connecter au cloud avec l'utilisateur admin, et un mot de passe. Mais d'abord, il faut...

## Redémarrer le cloud

```
toctoc restart pic-cloud
```

## Arrêter le cloud

```
toctoc stop pic-cloud
```

## Définir plusieurs "trusted_domains"
Ce sont des domaines qui permettent de rentrer dans le cloud: `cloud.exemple.fr`, `cloud.exemple.com` peuvent tous les deux envoyer sur le cloud. Modifiez le fichier `env.txt`:
```
VIRTUAL_HOST=cloud.exemple.fr,cloud.monasso.org
```

puis redémarrez le cloud:

```
toctoc restart pic-cloud
```

## Optimisation:

Si vous avez des problèmes de performances, vous pouvez être amené à optimiser le cloud. Éditez le fichier `houaibe.conf`, et éventuellement définissez les variables optionnelles, qui permettent d'avoir de meilleures performances. Puis redémarrez le cloud.

## Opérations de maintenance
L'utilisation de l'utilitaire de maintenance occ peut se faire de la manière suivante:
```
cd /usr/local/houaibe/containers/pic-cloud
source /usr/local/houaibe/etc/houaibe.conf
source houaibe.conf
docker compose exec -u www-data app php occ 
```

## Mise à jour:
Avant tout: **Sauvegardez tout** !

### Mise à jour de nextcloud:

Pour mettre à jour le cloud, allez dans le répertoire `pic-cloud`, puis modifiez `nextcloud/Dockerfile` pour introduire le numéro de version correct.  Il est conseilleé d'entrer un numéro complet (ex. 28.0.11 plutôt que 28) afin d'avoir une meilleure maîtrise de la version de nextcloud que vous utilisez. Ensuite, reconstruisez les conteneurs et redémarrez. 
```
cd /usr/local/houaibe/containers/pic-cloud/nextcloud
vi Dockerfile

cd /usr/local/houaibe/containers/pic-cloud
source /usr/local/houaibe/etc/houaibe.conf
source houaibe.conf
./build.sh
docker compose pull
toctoc restart pic-cloud
```

Le conteneur mettra à jour le code dans le répertoire `app`, pour vérifier la progression de la mise à jour on peut faire:
```
docker compose logs -t -f app
...
app-1  | 2024-10-13T07:43:11.709034935Z Starting code integrity check...
...
app-1  | 2024-10-13T07:43:57.195869733Z [13-Oct-2024 07:43:57] NOTICE: ready to handle connections
```

### Mise à jour de postgresql:

Sauvegarder la base de données:

Arrêter le cloud - sauf postgresql puis faire un dump de la base de données, et une copie physique des données (ceinture et bretelle !)

```
cd /usr/local/houaibe/containers/pic-cloud
source /usr/local/houaibe/etc/houaibe.conf
source houaibe.conf
toctoc stop pic-cloud
docker compose up -d db

mkdir BKPDB
# dump
docker compose exec db pg_dumpall -U nextcloud > BKPDB/pgdumpall.sql
# Conserver la base de données au format physique
docker compose down
mv db BKPDB
```

Puis changez la version de postgresql dans docker-compose.yml, et redémarrez postgresql:

```
cd /usr/local/houaibe/containers/pic-cloud/nextcloud
vi docker-compose.yml
docker compose up -d db
```

Recharger les données:

```
docker compose exec db createdb -U nextcloud nextcloud
docker cp BKPDB/pgdumpall.sql pic-cloud-db-1:/
docker compose exec db psql -U nextcloud -f /pgdumpall.sql
```

Enfin, redémarrez le cloud:

```
toctoc restart pic-cloud
```

