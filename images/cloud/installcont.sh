#! /bin/bash

#
# Script d'installation du répertoire associé au conteneur 
#
# Usage: cd ....
#        ./installcont.sh
#

if [[ ! -f ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf ]]
then
   echo "houaibe n'est pas encore configuré"
   exit 1;
fi

# Lire les paramètres généraux et spécifiques
. ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf
. ./houaibe.conf

# Il faut etre root !
[[ "$(id -u)" -ne "0" ]] && echo "VOUS DEVEZ ETRE ROOT !" && exit 1

# Repertoire courant
DIR=$CONTAINERSDIR/$CONTAINER

# Rien à faire si le répertoire existe déjà
[[ -d $DIR ]] && echo "Le repertoire $DIR existe - Je ne fais rien" && exit 0

#set -xv
echo "installation du conteneur $CONTAINER"

mkdir -p $DIR
chmod g+w $CONTAINERSDIR/$CONTAINER

# Copie de fichiers
cp run.sh.dist $DIR/run.sh
cp stop.sh.dist $DIR/stop.sh

ln -sf $DIR/run.sh .
ln -sf $DIR/stop.sh .
cp houaibe.conf $DIR

IMGDIR=$(pwd)

(
    cd $DIR
    chmod 550 run.sh

    # ----- Début de partie spécifique à chaque conteneur

    cp $IMGDIR/build.sh.dist build.sh
    ( cd $IMGDIR && ln -sf $DIR/build.sh && ln -sf $DIR/upgrade.sh )

    export POSTGRES_PASSWORD=$(openssl rand -base64 32)
    echo export POSTGRES_PASSWORD=${POSTGRES_PASSWORD} >.postgres_password
    chmod o= .postgres_password
    
    export ADMIN_PASSWORD=$(openssl rand -base64 32) 

    cp ${IMGDIR}/docker-compose.yml.dist docker-compose.yml
    cp -r ${IMGDIR}/nextcloud.dist nextcloud
    cp -r ${IMGDIR}/nginx.dist nginx
    
    echo "Construction des images"
    ./build.sh

    echo "Génération du fichier env.txt"
    echo "VIRTUAL_HOST=cloud.${DOMAINNAME}" > env.txt
    
    echo PREMIER DEMARRAGE DES CONTENEURS
    . houaibe.conf
    docker compose up -d

    echo "ATTENDRE 60 SECONDES L'INITIALISATION COMPLETE"
    sleep 60

    echo "INSTALLATION AUTOMATIQUE DE NEXTCLOUD"
    docker compose exec --user www-data app php /var/www/html/occ maintenance:install \
                                                    --database "pgsql" --database-name "nextcloud" --database-host="db" \
                                                    --database-user "nextcloud" \
                                                    --database-pass "$POSTGRES_PASSWORD" --admin-user "admin" --admin-pass "$ADMIN_PASSWORD"

    docker compose exec --user www-data app php occ config:system:set trusted_domains 0 --value=cloud.${DOMAINNAME}
    docker compose exec --user www-data app php occ config:system:set overwrite.cli.url --value=http://cloud.${DOMAINNAME}
    docker compose exec --user www-data app php occ config:system:set maintenance_window_start --value=2
    docker compose exec --user www-data app php occ config:system:set default_phone_region --value="ISO 3166-2:FR"

    # to avoid error message related to untrusted proxies
    idx=0
    for ip_range in $(docker network inspect picnet | jq '.[].IPAM.Config[].Subnet');
    do
      docker compose exec --user www-data app php occ config:system:set trusted_proxies ${idx} --value=$(echo ${ip_range} | tr -d '"' ) 
      idx=$((idx+1))
    done

    # to ensure reset password is working fine behind proxy
    docker compose exec --user www-data app php occ config:system:set  overwriteprotocol --value="https" --type="string"

    echo CONNECTION A NEXTCLOUD SUR https://$VIRTUAL_HOST
    echo UTILISATEUR=admin
    echo PASSWORD=$ADMIN_PASSWORD
    echo

    # ----- Fin de partie spécifique
)

# Donner au groupe docker la permission de bosser !
chgrp -R docker $DIR

