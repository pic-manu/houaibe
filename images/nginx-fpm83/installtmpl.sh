#! /bin/bash

#
# Script d'installation du rtemplate qui permettra à picgs de créer le répertoire associé au conteneur 
#
# Usage: cd ....
#        ./installcont.sh
#

if [[ ! -f ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf ]]
then
   echo "houaibe n'est pas encore configuré"
   exit 1;
fi

# Lire les paramètres généraux et spécifiques
. ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf
. ./houaibe.conf

# Repertoire courant et nom du fichier .tgz
TMPL=$(basename $NIMAGE)-$(basename $PIMAGE)
DIR=$CONTAINERSDIR/$TMPL

echo "installation du template $TMPL"
mkdir -p $DIR
chmod g+w $DIR

# Copie de fichiers
cp run.sh.dist $DIR/run.sh
cp stop.sh.dist $DIR/stop.sh
cp houaibe.conf $DIR

IMGDIR=$(pwd)
(
    cd $DIR
    chmod 550 run.sh

    cp -r $IMGDIR/templates $IMGDIR/docker-compose.yml $IMGDIR/php-suppl.d $IMGDIR/htaccess $IMGDIR/msmtprc.tmpl .

    # Donner au groupe docker la permission de bosser !
    chgrp -R docker $DIR

    # Faire un tgz et supprimer le répertoire
    tar czf $DIR.tgz * && cd .. && rm -rf $DIR
)
