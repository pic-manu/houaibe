<?php

$ver_apache = apache_get_version();
$container = getenv('CONTAINER');

echo "apache - $ver_apache\n";

# Si possible, ouvrir le fichier de mot de passe
$passwdfile=getenv('HEALTHPASSWDFILE');
if ($passwdfile === false) {
    $password = false;
} else {
    $password = file_get_contents($passwdfile,false,null,0,50);
}

# Si on n'a pas trouvé de mot de passe on s'arrête
if ( $password === false ) {
	echo "$container OK\n";

# Sinon on vérifie seulement qu'on peut se connecter à la base de données
} else {
        $p=explode(':',rtrim($password),3);
	$servername = $p[0];
	$username = $p[1];
	$password = $p[2];

	// Create connection
	$conn = new mysqli($servername, $username, $password);

	// Check connection
	if ($conn->connect_errno) {
		echo "mariadb - Erreur de connection " . $conn->connect_errno. "  " . $conn->connect_error . "\n";
		echo "$container KO\n";
	} else {
		echo "mariadb - " . $conn->host_info . " - OK\n";
		echo "$container OK\n";
	}
}

