# La crontab de houaibe

`houaibe` a besoin d'exécuter des procédures régulièrement afin de sécuriser l'ensemble. Les lignes suivantes peuvent être ajoutées au crontab de root, les périodicités et les heures ne sont là que de manière indicative

## L'environnement:

Commencez votre crontab par les lignes suivantes (il faut que `/usr/sbin` se trouve dans le PATH pour que `ipset` soit utilisable)

```
SHELL=/bin/bash
PATH=/usr/bin/:/bin:/usr/sbin:/usr/local/bin
```

## logrotate:

Les conteneurs n'embarquent pas `cron`, on appelle donc `logrotate` depuis le host:

```
10 00 * * * /usr/local/houaibe/sbin/logrotate.bash >/dev/null 2>&1
```

## Ecran de sécurité:

Ecran de sécurité pour `spip`: on le télécharge chaque jour

```
xx yy * * * docker exec pic-ssh bash -c 'cd /home/root/ecran_securite && wget https://git.spip.net/spip-contrib-outils/securite/raw/branch/master/ecran_securite.php -q -nv -O ecran_securite.php'
```

## Permissions des fichiers utilisateurs:

Les utilisateurs, et surtout leurs CMS, font un peu n'importe quoi avec les umask. Du coup on retrouve des fichiers en rw par Others, ou des choses affreuses comme ça. Toutes les nuits on remet tout ça d'aplomb.

```
20 0 * * * docker exec pic-maint find /home -mindepth 1 -not -path '/home/root*' -not -xtype l -perm /o+rwX -execdir chmod o= '{}' +
```

## Dump des bases de données:

Pour faciliter la vie aux utilisateurs, on leur met à disposition tous les jours leur base de données sous la forme d'un fichier .sql. La seconde ligne n'est utile que si vous avez démarré une installation de spip mutualisé.

```
22 0 * * * /usr/local/bin/picgs sauvebd
24 0 * * * /usr/local/bin/picgs mutusauvebd --silence
```

## picbloc:

Voir le chapitre "Sécurité" pour l'explication de picbloc. Il doit être exécuté une fois par jour afin de mettre à jour la liste d'IP bloquées.

```
xx yy * * * /usr/local/bin/picbloc >> /usr/local/houaibe/etc/picbloc.summary
```

## Surveillance des conteneurs:

Il n'est pas toujours simple de détecter le plantage d'un apache puisqu'il y a plusieurs conteneurs. Tous les conteneurs lancent apache par l'intermédiaire de `supervisord`, qui se charge de redémarrer apache en cas de plantage. Par ailleurs, La commande `HEALTHCHECK` de Docker est là pour s'assurer que tout va bien dans les conteneurs. Elle appelle toutes les 30 secondes le script `/usr/local/bin/health`. Celui-ci fait une requête sur `localhost`, cela active le fichier `/var/www/html/index.php`. Cela permet de vérifier que le serveur apache est fonctionnel. De plus, le script `index.php` essaie de se connecter au serveur de base de données, *à condition que la configuration soit correcte:* en effet, il utilise des données d'authentification que vous devez avoir déposées dans le fichier signalé par le paramètre `HEALTHPASSWDFILE` (cf. `houaibe.conf`). Le format du fichier est décrit dans `houaibe.conf`: il faut en particulier définir un nom d'utilisateur et un mot de passe pour la connexion à la base de données. **Afin de ne pas diminuer la sécurité, il est important que cet utilisateur n'ait aucun privilège sur aucune base de donées, seule la connexion au serveur est testée**.

Sur le host, le script `/usr/local/bin/health` fait le tour des conteneurs, et envoie un mail à l'adresse signalée par `HEALTH_MAIL` si l'un d'eux n'est pas en parfaite santé (c'est-à-dire si `docker` le signale comme `unhealthy`, c'est-à-dire si le mécanisme évoqué ci-dessus a signalé un problème). Il regarde toutes les 15 minutes, libre à vous de changer la périodicité si vous pensez que 15 mn n'est pas assez souvent !

```
*/15 * * * * /usr/local/bin/health
```

## Renouvellement des certificats:

```
xx yy * * * cd /usr/local/houaibe/dehydrated && ./dehydrated --cron --hook /usr/local/houaibe/dehydrated/hooks.bash >>/var/log/dehydrated/dehydrated.log 2>&1 
xx yy * * * cd /usr/local/houaibe/dehydrated && ./dehydrated -c -d "*.picttn.le-pic.org" --alias "picttn.le-pic.org" -t dns-01 -k /usr/local/houaibe/dehydrated/hooks-dns.bash >>/var/log/dehydrated/dehydrated.log 2>&1
```

## Et encore...

Il y aura certainement d'autres trucs dans votre crontab: le dump global de mysql avant la sauvegarde, le dump de ldap pour la même raison, l'appel du logiciel de sauvegarde, etc. 
