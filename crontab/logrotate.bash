#! /bin/bash

# Ce script fait tourner les logs dans les conteneurs
# Il n'y a pas de cron dans les conteneurs donc il faut le faire "à la main"
#
#
# MODE DEBUG - Appel de logrotate avec le switch --debug
#              On ne touche pas aux fichiers log mais on écrit les messages sur la sortie
#              Attention les serveurs web sont redémarrés même en mode debug
#              Décommenter pour avtiver
#
#DEBUG=--debug
#DEBUG=-v
DEBUG=""
#
. ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf
 
# Fait tourner les logs de TOUS les apaches
# on redemarre apache juste après pour tous les conteneurs
# NB - Au passage traduction de syntaxe python vers bash... :(
docker exec pic-ssh  logrotate "$DEBUG" -f --state /var/lib/logrotate/pic-apache-status  /etc/logrotate-pic.d

# Les conteneurs pic-itkxxx
for c in $(echo $DKRSRVWEB | tr "[],'" "    ")
do
    docker exec $c /etc/init.d/apache2 reload
done

# Les conteneurs nginx correspondant aux images déclarées dans houaibe.conf
conteneurs=$(for i in $(echo $DKRSRVIMG); do  i=${i#nginx-}; i="${DKRCPTE}${DKRCPTE:+/}$i";  docker ps --filter="ancestor=$i" --format='{{ .Names }}' | sed -e 's/-php-/-nginx-/'; done)
for c in ${conteneurs}
do
	docker exec $c sh -c 'kill -USR1 $(cat /var/run/nginx.pid)'
done

# Faire tourner les logs de pic-proxy, on garde les logs 1 année
docker exec pic-proxy  logrotate "$DEBUG" --state /var/lib/logrotate/pic-proxy-status /etc/logrotate.conf

# Pareil pour pic-ssh
docker exec pic-ssh    logrotate "$DEBUG" --state /var/lib/logrotate/pic-ssh-status /etc/logrotate.conf

