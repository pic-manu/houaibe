#! /bin/bash

#
# Script d'installation du répertoire sbin (quelques scripts systeme)
#
# Usage: cd ....
#        ./install.sh
#

if [[ ! -f ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf ]]
then
   echo "houaibe n'est pas encore configuré"
   exit 1;
fi

# Lire les paramètres généraux et spécifiques
. ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf
#. ./houaibe.conf

# Repertoire courant
DIR=${HOUAIBEDIR-/usr/local/houaibe}/sbin
mkdir -p $DIR

# Installation du fichier logrotate.bash
echo "Installation du fichier logrotate.bash"
cp logrotate.bash $DIR
chmod u=rwx $DIR/logrotate.bash

