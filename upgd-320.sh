#! /bin/bash

#
# Script de mise à jour de houaibe
#
# Usage: sudo ./upgd-320.sh
#

#set -v

[[ "$(id -u)" -ne "0" ]] && echo "VOUS DEVEZ ETRE ROOT !" && exit 1

if [[ ! -f ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf ]]
then
   mkdir -p ${HOUAIBEDIR-/usr/local/houaibe}/etc
   cp etc/houaibe.conf.dist ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf 
   echo "Veuillez configurer ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf selon vos besoins, puis appelez $0 à nouveau"
   exit 1;
fi

# Lire les paramètres
. ${HOUAIBEDIR-/usr/local/houaibe}/etc/houaibe.conf

# Installer run-include.sh au bon endroit
mkdir -p $CONTAINERSDIR || exit
cp images/scripts/run-include.sh $CONTAINERSDIR || exit
chmod -R 440 $CONTAINERSDIR/run-include.sh
chgrp -R docker $CONTAINERSDIR/run-include.sh

